/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.HttpEntity;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author Allison
 */
public class HttpResponse {

    /*Base64.getEncoder().encodeToString(nfce_xml.getBytes("utf-8"))*/
    private static final String urlEmail = "http://acomnfe.com.br/webservice/nfe/enviar_email.php";
//    private static final String urlEmail = "http://localhost:85/emissornfe/webservice/nfe/enviar_email.php";

    private static final String USER_AGENT = "Mozilla/5.0";

    public static String sendEmail(String post_email, String opcao) throws Exception {
	StringBuffer response;
	try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
	    HttpPost httpPost = new HttpPost(urlEmail);
	    httpPost.addHeader("User-Agent", USER_AGENT);
	    List<NameValuePair> urlParameters = new ArrayList<>();
	    urlParameters.add(new BasicNameValuePair("post_email", post_email));
	    urlParameters.add(new BasicNameValuePair("opcao", opcao));
	    HttpEntity postParams = new UrlEncodedFormEntity(urlParameters);
	    httpPost.setEntity(postParams);
	    CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
	    System.out.println("POST Status da resposta:: " + httpResponse.getStatusLine().getStatusCode());
	    BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
	    String inputLine;
	    response = new StringBuffer();
	    while ((inputLine = reader.readLine()) != null) {
		response.append(inputLine);
	    }
	    reader.close();
//            System.out.println(response.toString());
	    System.out.println(response.toString());
	}
	return response.toString();

    }
}
