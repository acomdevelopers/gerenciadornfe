/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.nota;

import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Enumeration;

/**
 *
 * @author allison
 */
public class CertificadoUtil {

    private ParametrosDAO pDAO;
    private static String diretoriotroProjeto;
    private String tipoCertificado;
    private static String nomeCertificado;
    private static String senhaCertificado;

    public CertificadoUtil() {
        pDAO = new ParametrosDAO();
        if (File.separator.equals("\\")) {
            diretoriotroProjeto = pDAO.getDiretorioProjeto();
        } else {
            diretoriotroProjeto = "/home/allison/NetBeansProjects/GerenciadorNfe";
        }
        senhaCertificado = pDAO.getSenhaCertificado();
        tipoCertificado = pDAO.getTipoCertificado();
        nomeCertificado = pDAO.getNomeCertificado();
    }

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public void validadeCertificado() {
        try {
            String caminhoDoCertificadoDoCliente = diretoriotroProjeto + File.separator + "certificado" + File.separator + nomeCertificado;
            String senhaDoCertificadoDoCliente = "1234";

            KeyStore keystore = KeyStore.getInstance(("PKCS12"));
            keystore.load(new FileInputStream(caminhoDoCertificadoDoCliente), senhaDoCertificadoDoCliente.toCharArray());

            Enumeration<String> eAliases = keystore.aliases();

            while (eAliases.hasMoreElements()) {
                String alias = (String) eAliases.nextElement();
                Certificate certificado = (Certificate) keystore.getCertificate(alias);

                info("Aliais: " + alias);
                X509Certificate cert = (X509Certificate) certificado;

                info(cert.getSubjectDN().getName());
                info("Version      : '" + cert.getVersion() + "'");
                info("SerialNumber : '" + cert.getSerialNumber() + "'");
                info("SigAlgName   : '" + cert.getSigAlgName() + "'");
                info("Válido de    : '" + dateFormat.format(cert.getNotBefore()) + "'");
                info("Válido até   : '" + dateFormat.format(cert.getNotAfter()) + "'");
//                getInfoAdicionais(cert);
            }
        } catch (Exception e) {
            error(e.toString());
        }

    }

    private static void error(String log) {
        System.out.println("ERROR: " + log);
    }

    private static void info(String log) {
        System.out.println("INFO: " + log);
    }

}
