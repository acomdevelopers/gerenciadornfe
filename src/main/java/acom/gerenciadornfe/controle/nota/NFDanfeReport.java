/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.nota;

import com.fincatto.documentofiscal.DFModelo;
import com.fincatto.documentofiscal.nfe400.classes.lote.envio.NFLoteEnvio;
import com.fincatto.documentofiscal.nfe400.classes.nota.NFNotaProcessada;
//import com.fincatto.documentofiscal.nfe400.parsers.DFParser;
import com.fincatto.documentofiscal.utils.DFPersister;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRPropertiesUtil;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import org.simpleframework.xml.core.Persister;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class NFDanfeReport {

    private NFNotaProcessada nota;
    private NFLoteEnvio lote;

    public NFDanfeReport(String xml) throws Exception {
        //this(new dfparser().notaProcessadaParaObjeto(xml));
        Persister persister = new DFPersister();
        this.nota = persister.read(NFNotaProcessada.class, xml, false);
        
    }

    public NFDanfeReport(NFNotaProcessada nota) {
        this.nota = nota;
    }

    public NFDanfeReport(NFLoteEnvio lote) {
        this.lote = lote;
    }
    
    public byte[] gerarDanfeNFe(byte[] logoEmpresa) throws Exception {
        return toPDF(createJasperPrintNFe(logoEmpresa));
    }

    private static byte[] toPDF(JasperPrint print) throws JRException {
        return JasperExportManager.exportReportToPdf(print);
    }

    public JasperPrint createJasperPrintNFe(byte[] logoEmpresa) throws IOException, ParserConfigurationException, SAXException, JRException {
        if (!DFModelo.NFE.equals(nota.getNota().getInfo().getIdentificacao().getModelo())) {
            throw new IllegalStateException("Nao e possivel gerar DANFe NFe de uma NFCe");
        }

        try (InputStream in = NFDanfeReport.class.getClassLoader().getResourceAsStream("danfe/DANFE_NFE_RETRATO.jasper");
                InputStream subDuplicatas = NFDanfeReport.class.getClassLoader().getResourceAsStream("danfe/DANFE_NFE_DUPLICATAS.jasper")) {
            final JRPropertiesUtil jrPropertiesUtil = JRPropertiesUtil.getInstance(DefaultJasperReportsContext.getInstance());
            jrPropertiesUtil.setProperty("net.sf.jasperreports.xpath.executer.factory", "net.sf.jasperreports.engine.util.xml.JaxenXPathExecuterFactory");

            Map<String, Object> parameters = new HashMap<>();
            parameters.put("SUBREPORT_DUPLICATAS", subDuplicatas);
            parameters.put("LOGO_EMPRESA", (logoEmpresa == null ? null : new ByteArrayInputStream(logoEmpresa)));
	    
            return JasperFillManager.fillReport(in, parameters, new JRXmlDataSource(convertStringXMl2DOM(nota.toString()), "/nfeProc/NFe/infNFe/det"));
        }
    }

    public JasperPrint createJasperPrintNFe(byte[] logoEmpresa, String status) throws IOException, ParserConfigurationException, SAXException, JRException {
        if (!DFModelo.NFE.equals(nota.getNota().getInfo().getIdentificacao().getModelo())) {
            throw new IllegalStateException("Nao e possivel gerar DANFe NFe de uma NFCe");
        }

        try (InputStream in = NFDanfeReport.class.getClassLoader().getResourceAsStream("danfe/DANFE_NFE_RETRATO.jasper");
                InputStream subDuplicatas = NFDanfeReport.class.getClassLoader().getResourceAsStream("danfe/DANFE_NFE_DUPLICATAS.jasper")) {
            final JRPropertiesUtil jrPropertiesUtil = JRPropertiesUtil.getInstance(DefaultJasperReportsContext.getInstance());
            jrPropertiesUtil.setProperty("net.sf.jasperreports.xpath.executer.factory", "net.sf.jasperreports.engine.util.xml.JaxenXPathExecuterFactory");

            Map<String, Object> parameters = new HashMap<>();
            parameters.put("SUBREPORT_DUPLICATAS", subDuplicatas);
            parameters.put("LOGO_EMPRESA", (logoEmpresa == null ? null : new ByteArrayInputStream(logoEmpresa)));
	    parameters.put("STATUS", status);
            return JasperFillManager.fillReport(in, parameters, new JRXmlDataSource(convertStringXMl2DOM(nota.toString()), "/nfeProc/NFe/infNFe/det"));
        }
    }
    
     public JasperPrint createJasperPrintNFeLote(byte[] logoEmpresa, String status) throws IOException, ParserConfigurationException, SAXException, JRException {
        if (!DFModelo.NFE.equals(lote.getNotas().get(0).getInfo().getIdentificacao().getModelo())) {
            throw new IllegalStateException("Nao e possivel gerar DANFe NFe de uma NFCe");
        }

        try (InputStream in = NFDanfeReport.class.getClassLoader().getResourceAsStream("danfe/DANFE_NFE_RETRATO_VIZUALIZAR.jasper");
                InputStream subDuplicatas = NFDanfeReport.class.getClassLoader().getResourceAsStream("danfe/DANFE_NFE_DUPLICATAS.jasper")) {
            final JRPropertiesUtil jrPropertiesUtil = JRPropertiesUtil.getInstance(DefaultJasperReportsContext.getInstance());
            jrPropertiesUtil.setProperty("net.sf.jasperreports.xpath.executer.factory", "net.sf.jasperreports.engine.util.xml.JaxenXPathExecuterFactory");

            Map<String, Object> parameters = new HashMap<>();
            parameters.put("SUBREPORT_DUPLICATAS", subDuplicatas);
            parameters.put("LOGO_EMPRESA", (logoEmpresa == null ? null : new ByteArrayInputStream(logoEmpresa)));
	    parameters.put("STATUS", status);
            return JasperFillManager.fillReport(in, parameters, new JRXmlDataSource(convertStringXMl2DOM(lote.toString()), "/enviNFe/NFe/infNFe/det"));
        }
    }
    
    private Document convertStringXMl2DOM(String xml) throws ParserConfigurationException, IOException, SAXException {
        try (StringReader stringReader = new StringReader(xml)) {
            InputSource inputSource = new InputSource();
            inputSource.setCharacterStream(stringReader);
            return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputSource);
        }
    }
}
