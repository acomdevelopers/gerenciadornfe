/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.nota;

import acom.gerenciadornfe.modelo.entidades.Filial;
import acom.gerenciadornfe.modelo.repositorio.FilialDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosFiscalDAO;
import com.fincatto.documentofiscal.DFAmbiente;
import com.fincatto.documentofiscal.DFModelo;
import com.fincatto.documentofiscal.DFUnidadeFederativa;
import com.fincatto.documentofiscal.nfe.NFTipoEmissao;
import com.fincatto.documentofiscal.nfe.NFeConfig;
import com.fincatto.documentofiscal.utils.DFCadeiaCertificados;
import com.sun.jna.platform.win32.WinNT;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.controlsfx.dialog.ExceptionDialog;
import org.droidwiki.certtest.nativebridge.CertificateStore;
import org.droidwiki.certtest.natives.CryptUILibrary;
import org.droidwiki.certtest.natives.Kernel32Library;
import org.droidwiki.certtest.structures.CERT_CONTEXT;

/**
 *
 * @author allison
 */
public class NFeConfigAcom extends NFeConfig {

    private KeyStore keyStoreCertificado = null;
    private KeyStore keyStoreCadeia = null;

    private String aliasCertificado;

    public static final String VERSAO_NFE = "4.00";

    private ParametrosDAO pDAO;
    private ParametrosFiscalDAO pfDAO;
    private FilialDAO fDAO;
    
    private Filial filial;
    
    private String senhaCertificado;
    private String diretorioProjeto;
    private String nomeCertificado;
    private String nomeAmbiente;
    private String ambiente;

    private String tipoCertificado;

    private String contigencia;
    
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
    
    public NFeConfigAcom() {
	
        pDAO = new ParametrosDAO();
        pfDAO = new ParametrosFiscalDAO();
	fDAO = new FilialDAO();
    
	filial = fDAO.getDadosFilial(pDAO.getParFilial().replace("=", ""));
	
        diretorioProjeto = pDAO.getDiretorioProjeto();
        nomeCertificado = filial.getFil_certificado();
        contigencia = pfDAO.getFormatoEmissao();
        String passCertificado = filial.getFil_certificado_senha();
        tipoCertificado = filial.getFil_certificado_serie();
        ambiente = pfDAO.getTipoAmbiente();
        
//        aliasCertificado = pDAO.getParAliasCertificado();
        
        if (ambiente.equals("1")) {
            nomeAmbiente = "producao";
        } else {
            nomeAmbiente = "homologacao";
        }
        if (passCertificado.contains("=")) {
            senhaCertificado = passCertificado.replace("=", "");
        } else {
            senhaCertificado = passCertificado;
        }
    }

//    public String selecionarAlias(String certificado) {
//	this.aliasCertificado = certificado;
//	return aliasCertificado;
//    }
    
    @Override
    public DFModelo getModelo() {
	return DFModelo.NFE;
    }

    @Override
    public DFAmbiente getAmbiente() {
	return DFAmbiente.valueOfCodigo(ambiente);
    }

    @Override
    public DFUnidadeFederativa getCUF() {
	return DFUnidadeFederativa.PE;
    }

    public NFTipoEmissao getTipoEmissao() {
	if (contigencia.equals("7")) {
	    return NFTipoEmissao.CONTINGENCIA_SVCRS;
	} else {
	    return NFTipoEmissao.EMISSAO_NORMAL;
	}
    }

    @Override
    public String getVersao() {
	return "4.00";
    }

    @Override
    public KeyStore getCertificadoKeyStore() throws KeyStoreException {
	/*Se o certificado for A1*/
	if (tipoCertificado.equals("A1")) {
	    if (this.keyStoreCertificado == null) {
		this.keyStoreCertificado = KeyStore.getInstance("PKCS12");
		try (InputStream certificadoStream = new FileInputStream(diretorioProjeto + File.separator + "certificado" + File.separator + nomeCertificado)) {
		    this.keyStoreCertificado.load(certificadoStream, this.getCertificadoSenha().toCharArray());
		} catch (NoSuchAlgorithmException | IOException e) {
		    this.keyStoreCadeia = null;
//		    throw new KeyStoreException("Nao foi possibel montar o KeyStore com a cadeia de certificados", e);
		    ExceptionDialog exceptionDialog = new ExceptionDialog(e);
		    exceptionDialog.setTitle("Erro");
		    exceptionDialog.setContentText("as");
		    exceptionDialog.setHeaderText("Nao foi possibel montar o KeyStore com a cadeia de certificados ");
		    exceptionDialog.show();
		} catch (java.security.cert.CertificateException ex) {
		    Logger.getLogger(NFeConfigAcom.class.getName()).log(Level.SEVERE, null, ex);
		    ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
		    exceptionDialog.setTitle("Erro");
		    exceptionDialog.setContentText("as");
		    exceptionDialog.setHeaderText("Erro no Certificado: ");
		    exceptionDialog.show();
		}
	    }
	    /*Se o certificado for A3*/
	} else if (tipoCertificado.equals("A3")) {
	    if (this.keyStoreCertificado == null) {
		try {
                    this.keyStoreCertificado = KeyStore.getInstance("Windows-MY", "SunMSCAPI");
		    this.keyStoreCertificado.load(null, null);
		    System.out.println(keyStoreCertificado.aliases().nextElement());
		} catch (IOException ex) {
		    Logger.getLogger(NFeConfigAcom.class.getName()).log(Level.SEVERE, null, ex);
		    ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
		    exceptionDialog.setTitle("Erro");
		    exceptionDialog.setContentText("as");
		    exceptionDialog.setHeaderText("Erro no Certificado: ");
		    exceptionDialog.show();
		} catch (NoSuchAlgorithmException ex) {
		    Logger.getLogger(NFeConfigAcom.class.getName()).log(Level.SEVERE, null, ex);
		    ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
		    exceptionDialog.setTitle("Erro");
		    exceptionDialog.setContentText("as");
		    exceptionDialog.setHeaderText("Erro no Certificado: ");
		    exceptionDialog.show();
		} catch (CertificateException ex) {
		    Logger.getLogger(NFeConfigAcom.class.getName()).log(Level.SEVERE, null, ex);
		    ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
		    exceptionDialog.setTitle("Erro");
		    exceptionDialog.setContentText("as");
		    exceptionDialog.setHeaderText("Erro no Certificado: ");
		    exceptionDialog.show();
		} catch (NoSuchProviderException ex) {
                    Logger.getLogger(NFeConfigAcom.class.getName()).log(Level.SEVERE, null, ex);
                }
	    }
	}
	return this.keyStoreCertificado;
    }

    @Override
    public String getCertificadoSenha() {
	return senhaCertificado;
    }

    @Override
    public KeyStore getCadeiaCertificadosKeyStore() throws KeyStoreException {
	if (this.keyStoreCadeia == null) {
	    this.keyStoreCadeia = KeyStore.getInstance("JKS");
	    try (InputStream cadeia = new FileInputStream(diretorioProjeto + File.separator + "cadeia_certificados" + File.separator + nomeAmbiente + ".cacerts")) {
		this.keyStoreCadeia.load(cadeia, this.getCadeiaCertificadosSenha().toCharArray());
	    } catch (NoSuchAlgorithmException | IOException e) {
		this.keyStoreCadeia = null;
		throw new KeyStoreException("Nao foi possibel montar o KeyStore com o certificado", e);
	    } catch (java.security.cert.CertificateException ex) {
		Logger.getLogger(NFeConfigAcom.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return this.keyStoreCadeia;
    }

    @Override
    public String getCadeiaCertificadosSenha() {
	return "123456";
    }

    @Override
    public String[] getSSLProtocolos() {
	return new String[]{"TLSv1.2"}; //"TLSv1.2"
    }

//    @Override
//    public String getCertificadoAlias() {
//	return aliasCertificado;
//    }

    public List<String> listarCertificados() {
        List<String> certificados = new ArrayList<>();
        try {
            KeyStore ks = KeyStore.getInstance("Windows-MY", "SunMSCAPI");
            ks.load(null, null);
            Enumeration<String> aliases = ks.aliases();
            while (aliases.hasMoreElements()) {
                String alias = aliases.nextElement();
                certificados.add(alias);
            }
        } catch (KeyStoreException | NoSuchProviderException | IOException | NoSuchAlgorithmException | CertificateException ex) {
            Logger.getLogger(NFeConfigAcom.class.getName()).log(Level.SEVERE, null, ex);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("as");
            exceptionDialog.setHeaderText("Erro ao carregar informações: ");
            exceptionDialog.showAndWait();
        }
	return certificados;
    }

    public boolean gerarCadeiaCertificado() {
	System.out.println("chamou gerarCadeiaCertificados()..." + tipoCertificado);
	try {
	    FileUtils.writeByteArrayToFile(new File(diretorioProjeto + File.separator + "cadeia_certificados" + File.separator + "homologacao.cacerts"), DFCadeiaCertificados.geraCadeiaCertificados(DFAmbiente.HOMOLOGACAO, "123456"));
	    FileUtils.writeByteArrayToFile(new File(diretorioProjeto + File.separator + "cadeia_certificados" + File.separator + "producao.cacerts"), DFCadeiaCertificados.geraCadeiaCertificados(DFAmbiente.PRODUCAO, "123456"));
	    System.out.println(diretorioProjeto + File.separator + "cadeia_certificados" + File.separator + "homologacao.cacerts");
	    return true;
	} catch (Exception e) {
	    e.printStackTrace();
	    return false;
	}
    }

    public X509Certificate getInfoA1() {
        X509Certificate retorno = null;
        try {
            String caminhoDoCertificadoDoCliente = diretorioProjeto + File.separator + "certificado" + File.separator + nomeCertificado;
            String senhaDoCertificadoDoCliente = senhaCertificado;

            KeyStore keystore = KeyStore.getInstance(("PKCS12"));
            keystore.load(new FileInputStream(caminhoDoCertificadoDoCliente), senhaDoCertificadoDoCliente.toCharArray());

            Enumeration<String> eAliases = keystore.aliases();

            while (eAliases.hasMoreElements()) {
                String alias = (String) eAliases.nextElement();
                Certificate certificado = (Certificate) keystore.getCertificate(alias);
                X509Certificate cert = (X509Certificate) certificado;
                retorno = cert;
            }
        } catch (Exception ex) {
            Logger.getLogger(NFeConfigAcom.class.getName()).log(Level.SEVERE, null, ex);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("as");
            exceptionDialog.setHeaderText("Erro ao carregar informações: ");
            exceptionDialog.showAndWait();
        }
        return retorno;
    }

    public X509Certificate getInfoA3() {
        X509Certificate retorno = null;
	String aliasBanco = pDAO.getParAliasCertificado();
        try {
            KeyStore keystore = KeyStore.getInstance("Windows-MY", "SunMSCAPI");
            keystore.load(null, null);
            Enumeration<String> al = keystore.aliases();
            while (al.hasMoreElements()) {
//                String alias = al.nextElement();
                info("--------------------------------------------------------");
                if (keystore.containsAlias(aliasBanco)) {
                    info("Emitido para........: " + aliasBanco);

                    X509Certificate cert = (X509Certificate) keystore.getCertificate(aliasBanco);
                    info("SubjectDN...........: " + cert.getSubjectDN().toString());
                    info("Version.............: " + cert.getVersion());
                    info("SerialNumber........: " + cert.getSerialNumber());
                    info("SigAlgName..........: " + cert.getSigAlgName());
                    info("Válido a partir de..: " + dateFormat.format(cert.getNotBefore()));
                    info("Válido até..........: " + dateFormat.format(cert.getNotAfter()));
		    retorno = cert;
                } else {
                    info("Alias doesn't exists : " + aliasBanco);
                }
		break;
            }
        } catch (Exception ex) {
            Logger.getLogger(NFeConfigAcom.class.getName()).log(Level.SEVERE, null, ex);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("as");
            exceptionDialog.setHeaderText("Erro ao carregar informações: ");
            exceptionDialog.showAndWait();
        }
        return retorno;
    }

    private org.droidwiki.certtest.nativebridge.Certificate selectCertificateWithDialog(CertificateStore temporaryStore) {
	CERT_CONTEXT selectedCertContext;
	try {
	    selectedCertContext = CryptUILibrary.INSTANCE.CryptUIDlgSelectCertificateFromStore(
		    (WinNT.HANDLE) temporaryStore.getNative(), null, null, null, 0, 0, 0);
	} catch (Error err) {
	    System.out.println(Kernel32Library.INSTANCE.GetLastError());
	    return null;
	}
	if (selectedCertContext == null) {
	    return null;
	}
	return new org.droidwiki.certtest.nativebridge.Certificate(selectedCertContext);
    }


    private static void info(String log) {
        System.out.println("INFO: " + log);
    }

    /**
     * Error.
     *
     * @param log
     */
    private static void error(String log) {
        System.out.println("ERROR: " + log);
    }
}
