/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.nota;

import acom.gerenciadornfe.controle.telas.ObjetoRetornoGerarNota;
import acom.gerenciadornfe.modelo.entidades.*;
import acom.gerenciadornfe.modelo.repositorio.*;
import acom.gerenciadornfe.util.MyJasperViewer;
import acom.gerenciadornfe.util.Util;
import com.fincatto.documentofiscal.DFAmbiente;
import com.fincatto.documentofiscal.DFModelo;
import com.fincatto.documentofiscal.DFPais;
import com.fincatto.documentofiscal.DFUnidadeFederativa;
import com.fincatto.documentofiscal.nfe.NFTipoEmissao;
import com.fincatto.documentofiscal.nfe.classes.distribuicao.NFDistribuicaoIntRetorno;
import com.fincatto.documentofiscal.nfe400.classes.*;
import com.fincatto.documentofiscal.nfe400.classes.evento.NFEnviaEventoRetorno;
import com.fincatto.documentofiscal.nfe400.classes.evento.NFEventoRetorno;
import com.fincatto.documentofiscal.nfe400.classes.evento.cancelamento.NFEnviaEventoCancelamento;
import com.fincatto.documentofiscal.nfe400.classes.evento.cancelamento.NFEventoCancelamento;
import com.fincatto.documentofiscal.nfe400.classes.evento.cancelamento.NFInfoEventoCancelamento;
import com.fincatto.documentofiscal.nfe400.classes.evento.cartacorrecao.NFEnviaEventoCartaCorrecao;
import com.fincatto.documentofiscal.nfe400.classes.evento.cartacorrecao.NFProtocoloEventoCartaCorrecao;
import com.fincatto.documentofiscal.nfe400.classes.evento.inutilizacao.NFRetornoEventoInutilizacao;
import com.fincatto.documentofiscal.nfe400.classes.evento.inutilizacao.NFRetornoEventoInutilizacaoDados;
import com.fincatto.documentofiscal.nfe400.classes.lote.consulta.NFLoteConsultaRetorno;
import com.fincatto.documentofiscal.nfe400.classes.lote.envio.NFLoteEnvio;
import com.fincatto.documentofiscal.nfe400.classes.lote.envio.NFLoteEnvioRetorno;
import com.fincatto.documentofiscal.nfe400.classes.lote.envio.NFLoteIndicadorProcessamento;
import com.fincatto.documentofiscal.nfe400.classes.nota.*;
import com.fincatto.documentofiscal.nfe400.classes.nota.consulta.NFNotaConsultaRetorno;
import com.fincatto.documentofiscal.nfe400.utils.NFGeraChave;
import com.fincatto.documentofiscal.nfe400.webservices.WSFacade;
import com.fincatto.documentofiscal.utils.DFAssinaturaDigital;
import com.fincatto.documentofiscal.utils.DFPersister;
import javafx.scene.control.Alert;
import javafx.stage.Modality;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;
import org.controlsfx.dialog.ExceptionDialog;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author allison
 */
public class NotaControler {

    private ParametrosNotaDAO pnDao = new ParametrosNotaDAO();
    private NotaFiscalDAO nfDao = new NotaFiscalDAO();
    private NotaFiscalItemDAO nfiDAO = new NotaFiscalItemDAO();
    private ParametrosFiscalDAO pfDao = new ParametrosFiscalDAO();
    private NotaFiscalLoteDAO nflDao = new NotaFiscalLoteDAO();
    private NotaFiscalCartaCorrecaoDAO nfccDAO = new NotaFiscalCartaCorrecaoDAO();
    private InutilizacaoDAO iDAO = new InutilizacaoDAO();
    private FilialDAO fDAO = new FilialDAO();
    private UtilDAO utilDAO = new UtilDAO();
    private NotaFiscalDimplatasDAO nfdDAO = new NotaFiscalDimplatasDAO();
    private NotaFiscalCartaCorrecaoImpressaoDAO nfcciDAO = new NotaFiscalCartaCorrecaoImpressaoDAO();

    private ObjetoRetornoGerarNota retornoGerarNota = new ObjetoRetornoGerarNota();

    BigDecimal baseCalculoIcms = new BigDecimal("0.00");
    BigDecimal valorTotalIcms = new BigDecimal("0.00");
    BigDecimal valorTotalIpi = new BigDecimal("0.00");
    BigDecimal valorTotalPis = new BigDecimal("0.00");
    BigDecimal valorTotalCofins = new BigDecimal("0.00");
    BigDecimal valorTotalFundoCombatePobreza = new BigDecimal("0.00");
    BigDecimal valorTotalFundoCombatePobrezaST = new BigDecimal("0.00");
    BigDecimal valorTotalFundoCombatePobrezaSTRetido = new BigDecimal("0.00");
    BigDecimal valorTotalIpiDevolvido = new BigDecimal("0.00");

    private String msgErroBloco = "";
    private String msgErroItens = "";
    private int numeroItem = 0;

    ParametrosFiscal pf = pfDao.listar();

    public ObjetoRetornoGerarNota gerarXml(List<NotaFiscal> lista) {
        try {
            for (NotaFiscal notaFiscal : lista) {
                System.out.println("tipo operacao---> " + notaFiscal.getNf_tipo_operacao() == null);

                NFNota nota = new NFNota();

                NFGeraChave gerador = new NFGeraChave(nota);

                NFNotaInfoIdentificacao nfNotaInfo = new NFNotaInfoIdentificacao();
                nfNotaInfo.setUf(DFUnidadeFederativa.valueOfCodigo(fDAO.getCodUf()));
                nfNotaInfo.setNaturezaOperacao(Util.removeAcentos(notaFiscal.getTes_texto().trim()));
                nfNotaInfo.setModelo(DFModelo.NFE);
                nfNotaInfo.setDataHoraSaidaOuEntrada(ZonedDateTime.ofInstant(Timestamp.valueOf(montarDataEHora(notaFiscal.getNf_data(), notaFiscal.getHora()).toString()).toInstant(), ZoneId.systemDefault()));
                nfNotaInfo.setSerie(notaFiscal.getNf_serie().trim());
                nfNotaInfo.setNumeroNota(String.valueOf(Integer.parseInt(notaFiscal.getNf_nrnota())));
                nfNotaInfo.setDataHoraEmissao(ZonedDateTime.ofInstant(Timestamp.valueOf(montarDataEHora(notaFiscal.getNf_data(), notaFiscal.getHora()).toString()).toInstant(), ZoneId.systemDefault()));
                nfNotaInfo.setTipo(NFTipo.valueOfCodigo(String.valueOf(notaFiscal.getNf_tipo())));
                if (notaFiscal.getNf_tipo_operacao() == null || notaFiscal.getNf_tipo_operacao().equals("")) {
                    nfNotaInfo.setIdentificadorLocalDestinoOperacao(NFIdentificadorLocalDestinoOperacao.valueOfCodigo("1"));
                } else {
                    nfNotaInfo.setIdentificadorLocalDestinoOperacao(NFIdentificadorLocalDestinoOperacao.valueOfCodigo(Util.getNumeros(notaFiscal.getNf_tipo_operacao())));
                }
                nfNotaInfo.setCodigoMunicipio(Util.formatarNumeros("0000000", pnDao.getCodigoMunicipio(notaFiscal.getFil_cidade())));
                nfNotaInfo.setTipoImpressao(NFTipoImpressao.DANFE_NORMAL_RETRATO);
                if (pfDao.getFormatoEmissao().contains("7")) {
                    nfNotaInfo.setTipoEmissao(NFTipoEmissao.CONTINGENCIA_SVCRS);
                    nfNotaInfo.setDataHoraContigencia(ZonedDateTime.ofInstant(Timestamp.valueOf(montarDataEHora(notaFiscal.getNf_data(), notaFiscal.getHora()).toString()).toInstant(), ZoneId.systemDefault()));
                    nfNotaInfo.setJustificativaEntradaContingencia("EMISSAO EM CONTINGENCIA SEFAZ FORA DO AR");
                } else {
                    nfNotaInfo.setTipoEmissao(NFTipoEmissao.EMISSAO_NORMAL);
                }
                if (pfDao.getTipoAmbiente() == null || pfDao.getTipoAmbiente().equals("")) {
                    nfNotaInfo.setAmbiente(DFAmbiente.PRODUCAO);
                } else {
                    nfNotaInfo.setAmbiente(DFAmbiente.valueOfCodigo(Util.getNumeros(pfDao.getTipoAmbiente())));
                }
                nfNotaInfo.setFinalidade(NFFinalidade.valueOfCodigo(Util.getNumeros(notaFiscal.getNfefinalidade())));
                if (notaFiscal.getPes_indicador_consumidor().equals("Sim")) {
                    nfNotaInfo.setOperacaoConsumidorFinal(NFOperacaoConsumidorFinal.SIM);
                } else if (notaFiscal.getPes_indicador_consumidor().equals("Nao")) {
                    nfNotaInfo.setOperacaoConsumidorFinal(NFOperacaoConsumidorFinal.NAO);
                }

                List<NotaFiscalItem> lcr = nfiDAO.listaCuponsReferenciados(notaFiscal.getNf_numero());
                List<String> lr = nfiDAO.listaNotasReferenciadas(notaFiscal.getNf_numero());
                if (!Util.getNumeros(notaFiscal.getNfefinalidade()).trim().equals("1")) {
                    if (lr.size() > 0) {
                        List<NFInfoReferenciada> referenciadas = new ArrayList<NFInfoReferenciada>();
                        NFInfoReferenciada ref = null;
                        for (String n : lr) {
                            ref = new NFInfoReferenciada();
                            ref.setChaveAcesso(n);
                            referenciadas.add(ref);
                        }
                        nfNotaInfo.setReferenciadas(referenciadas);
                    } else if (lcr.size() > 0) {
                        List<NFInfoReferenciada> referenciadas = new ArrayList<NFInfoReferenciada>();
                        NFInfoCupomFiscalReferenciado ecf = null;
                        NFInfoReferenciada ref = null;
                        for (NotaFiscalItem n : lcr) {
                            ref = new NFInfoReferenciada();
                            ecf = new NFInfoCupomFiscalReferenciado();
                            ecf.setModeloDocumentoFiscal("2D");
                            ecf.setNumeroOrdemSequencialECF(Util.formatarNumeros("000", n.getNfi_ecf_numero()));
                            ecf.setNumeroContadorOrdemOperacao(Util.formatarNumeros("000000", n.getNfi_ecf_coo()));
                            ref.setCupomFiscalReferenciado(ecf);
                            referenciadas.add(ref);
                        }
                        nfNotaInfo.setReferenciadas(referenciadas);
                    }
                } else if (lcr.size() > 0) {
                    List<NFInfoReferenciada> referenciadas = new ArrayList<NFInfoReferenciada>();
                    NFInfoCupomFiscalReferenciado ecf = null;
                    NFInfoReferenciada ref = null;
                    for (NotaFiscalItem n : lcr) {
                        ref = new NFInfoReferenciada();
                        ecf = new NFInfoCupomFiscalReferenciado();
                        ecf.setModeloDocumentoFiscal("2D");
                        ecf.setNumeroOrdemSequencialECF(Util.formatarNumeros("000", n.getNfi_ecf_numero()));
                        ecf.setNumeroContadorOrdemOperacao(Util.formatarNumeros("000000", n.getNfi_ecf_coo()));
                        ref.setCupomFiscalReferenciado(ecf);
                        referenciadas.add(ref);
                    }
                    nfNotaInfo.setReferenciadas(referenciadas);
                } else if (lr.size() > 0) {
                    List<NFInfoReferenciada> referenciadas = new ArrayList<NFInfoReferenciada>();
                    NFInfoReferenciada ref = null;
                    for (String n : lr) {
                        ref = new NFInfoReferenciada();
                        ref.setChaveAcesso(n);
                        referenciadas.add(ref);
                    }
                    nfNotaInfo.setReferenciadas(referenciadas);
                }

                nfNotaInfo.setIndicadorPresencaComprador(NFIndicadorPresencaComprador.OPERACAO_PRESENCIAL); // (mudar isso aqui quando for trabalhar com venda pela net)
                nfNotaInfo.setProgramaEmissor(NFProcessoEmissor.CONTRIBUINTE);
                nfNotaInfo.setVersaoEmissor("ACOM_FAT-3.0");

                NFNotaInfo notaInfo = new NFNotaInfo();
                notaInfo.setVersao(new BigDecimal("4.0"));
                notaInfo.setIdentificacao(nfNotaInfo);

                NFNotaInfoEmitente emitente = new NFNotaInfoEmitente();
                emitente.setCnpj(Util.getNumeros(notaFiscal.getFil_cnpj()).trim());
                emitente.setInscricaoEstadual(notaFiscal.getFil_insc().trim());
                emitente.setRazaoSocial(Util.removeAcentos(notaFiscal.getFil_nome().trim()));
                emitente.setNomeFantasia(Util.removeAcentos(notaFiscal.getFil_nreduz().trim()));
                if (notaFiscal.getFil_crt().contains("1")) {
                    emitente.setRegimeTributario(NFRegimeTributario.SIMPLES_NACIONAL);
                } else if (notaFiscal.getFil_crt().contains("2")) {
                    emitente.setRegimeTributario(NFRegimeTributario.SIMPLES_NACIONAL_EXCESSO_RECEITA);
                } else {
                    emitente.setRegimeTributario(NFRegimeTributario.NORMAL);
                }

                notaInfo.setEmitente(emitente);

                NFEndereco endereco = new NFEndereco();
                endereco.setUf(DFUnidadeFederativa.valueOfCodigo(utilDAO.getCodEstadoEmitente(notaFiscal.getFil_estado())));
                endereco.setCodigoMunicipio(Util.formatarNumeros("0000000", pnDao.getCodigoMunicipio(notaFiscal.getFil_cidade())));
                endereco.setBairro(Util.removeAcentos(notaFiscal.getFil_bairro().trim()));
                endereco.setCep(Util.getNumeros(notaFiscal.getFil_cep()).trim());
                endereco.setDescricaoMunicipio(Util.removeAcentos(notaFiscal.getFil_cidade().trim()));
                endereco.setCodigoPais(DFPais.BRASIL);
                endereco.setLogradouro(Util.removeAcentos(notaFiscal.getFil_endereço().trim()));
                endereco.setNumero(notaFiscal.getFil_numero().trim());
                endereco.setTelefone(Util.getNumeros(notaFiscal.getFil_fone()).trim());
                emitente.setEndereco(endereco);

                nota.setInfo(notaInfo);

                String cr = new NFGeraChave(nota).geraCodigoRandomico();
                nfNotaInfo.setCodigoRandomico(cr);
                notaInfo.setIdentificador(gerador.getChaveAcesso().trim());
                nfNotaInfo.setDigitoVerificador(gerador.getDV());

                NFNotaInfoDestinatario destinatario = new NFNotaInfoDestinatario();
                if (notaFiscal.getPes_suframa() != "") {
                    destinatario.setInscricaoSuframa(notaFiscal.getPes_suframa());
                }
                if (!notaFiscal.getPes_cnpj().equals("")) {
                    destinatario.setCnpj(Util.getNumeros(notaFiscal.getPes_cnpj()).trim());
                } else {
                    destinatario.setCpf(Util.getNumeros(notaFiscal.getPes_cpf()).trim());
                }
                if (pfDao.getTipoAmbiente().contains("2")) {
                    destinatario.setRazaoSocial("NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL");
                } else {
                    destinatario.setRazaoSocial(Util.removeAcentos(notaFiscal.getPes_nome45().trim()));
                }

                if (notaFiscal.getPes_indicador_ie().contains("1")) {
                    destinatario.setIndicadorIEDestinatario(NFIndicadorIEDestinatario.CONTRIBUINTE_ICMS);
                    destinatario.setInscricaoEstadual(notaFiscal.getPes_ie().trim());
                } else if (notaFiscal.getPes_indicador_ie().contains("9")) {
                    destinatario.setIndicadorIEDestinatario(NFIndicadorIEDestinatario.NAO_CONTRIBUINTE);
                } else if (notaFiscal.getPes_indicador_ie().contains("2")) {
                    destinatario.setIndicadorIEDestinatario(NFIndicadorIEDestinatario.CONTRIBUINTE_ISENTO_INSCRICAO_CONTRIBUINTES_ICMS);
                }

                destinatario.setEmail(notaFiscal.getPes_email().trim());
                NFEndereco enderecoDestinatario = new NFEndereco();
                enderecoDestinatario.setBairro(Util.removeAcentos(notaFiscal.getPes_bai().trim()));
                enderecoDestinatario.setCodigoMunicipio(Util.formatarNumeros("0000000", pnDao.getCodigoMunicipioDestinatario(notaFiscal.getPes_cidade(), utilDAO.getCodigoUF(notaFiscal.getNf_clifor())).trim()));
                enderecoDestinatario.setCep(Util.getNumeros(notaFiscal.getPes_cep()).trim());
                enderecoDestinatario.setCodigoPais(DFPais.BRASIL);
                enderecoDestinatario.setComplemento(Util.removeAcentos(notaFiscal.getPes_compl().trim() + "."));
                enderecoDestinatario.setDescricaoMunicipio(Util.removeAcentos(notaFiscal.getPes_cidade().trim()));
                enderecoDestinatario.setDescricaoPais("Brasil".trim());
                enderecoDestinatario.setLogradouro(Util.removeAcentos(notaFiscal.getPes_end().trim()));
                enderecoDestinatario.setNumero(notaFiscal.getPes_nrend().trim());
                enderecoDestinatario.setTelefone(Util.getNumeros(notaFiscal.getPes_fone1()).trim());
                enderecoDestinatario.setUf(DFUnidadeFederativa.valueOfCodigo(utilDAO.getCodigoUF(notaFiscal.getNf_clifor())));
                destinatario.setEndereco(enderecoDestinatario);
                notaInfo.setDestinatario(destinatario);

		/*
		###############################################
		########## INICIO DO BLOCO DOS ITENS ##########
		###############################################
		 */
                List<NFNotaInfoItem> nfItens = new ArrayList<>();
                List<NotaFiscalItem> listaNFItem = new ArrayList<>();
                listaNFItem = nfiDAO.listar(notaFiscal.getNf_numero());
                for (NotaFiscalItem nfi : listaNFItem) {
                    msgErroItens = "Erro ao gerar informações do produto " + nfi.getNfi_codean();
                    NFNotaInfoItem nfIten = new NFNotaInfoItem();
                    nfIten.setNumeroItem(Integer.parseInt(nfi.getNfi_item()));
                    NFNotaInfoItemProduto produto = new NFNotaInfoItemProduto();
                    produto.setCodigo(String.valueOf(nfi.getNfi_codigodanfe()));
                    produto.setCodigoDeBarras(nfi.getNfi_codean().trim());
                    produto.setCodigoDeBarrasTributavel(nfi.getNfi_codeantrib().trim());
                    produto.setDescricao(Util.removeAcentos(nfi.getNfi_nome().trim()));
                    produto.setNcm(nfi.getPro_ncm().trim());
                    if (!nfi.getPro_cest().equals("")) {
                        produto.setCodigoEspecificadorSituacaoTributaria(Util.formatarNumeros("0000000", nfi.getPro_cest()).trim());
                    }
                    produto.setCfop(nfi.getNfi_cfop().trim());
                    produto.setUnidadeComercial(nfi.getNfi_um().trim());
                    produto.setUnidadeTributavel(nfi.getNfi_um().trim());
                    produto.setQuantidadeComercial(new BigDecimal(String.valueOf(nfi.getNfi_qtdfat())));
                    double fator = 0;
                    if (notaFiscal.isNf_tipo_importacao() == true) {
                        fator = nfi.getNfi_imposto_importacao() / nfi.getNfi_qtdfat();
                        List<NFNotaInfoItemProdutoDeclaracaoImportacao> listaImportacao = new ArrayList<>();
                        List<NFNotaInfoItemProdutoDeclaracaoImportacaoAdicao> listaAdicoes = new ArrayList<>();

                        NFNotaInfoItemProdutoDeclaracaoImportacao importacao = new NFNotaInfoItemProdutoDeclaracaoImportacao();
                        NFNotaInfoItemProdutoDeclaracaoImportacaoAdicao adicoes = new NFNotaInfoItemProdutoDeclaracaoImportacaoAdicao();
                        msgErroItens = "Erro ao gerar declarações de importacões e adições do produto " + nfi.getNfi_codean();
                        importacao.setNumeroRegistro(notaFiscal.getNf_dmi_numero().trim());
                        importacao.setCnpj(Util.getNumeros(notaFiscal.getPes_cnpj()).trim());
                        importacao.setCodigoExportador(notaFiscal.getNf_dmi_codigo_exportador().trim());
                        importacao.setDataDesembaraco(notaFiscal.getNf_dmi_data().toLocalDateTime().toLocalDate());
                        importacao.setDataRegistro(notaFiscal.getNf_dmi_data().toLocalDateTime().toLocalDate());
                        importacao.setFormaImportacaoIntermediacao(NFFormaImportacaoIntermediacao.IMPORTACAO_CONTA_PROPRIA);
                        importacao.setLocalDesembaraco(Util.removeAcentos(notaFiscal.getNf_dmi_cidade().trim()));
                        importacao.setTransporteInternacional(NFViaTransporteInternacional.MARITIMA);
                        importacao.setUfDesembaraco(DFUnidadeFederativa.valueOfCodigo(pnDao.getCodigoEstado(notaFiscal.getNf_dmi_uf())));
                        importacao.setUfTerceiro(DFUnidadeFederativa.valueOfCodigo(pnDao.getCodigoEstado(notaFiscal.getNf_dmi_uf())));
                        importacao.setValorAFRMM(new BigDecimal(notaFiscal.getNf_dmi_valor_afrmm()).setScale(2, RoundingMode.HALF_UP));

                        adicoes.setSequencial(1);
                        adicoes.setNumero(1);
                        adicoes.setCodigoFabricante(notaFiscal.getNf_dmi_codigo_exportador().trim());
                        listaAdicoes.add(adicoes);
                        importacao.setAdicoes(listaAdicoes);
                        listaImportacao.add(importacao);
                        produto.setDeclaracoesImportacao(listaImportacao);
                    }
                    /*ZonedDateTime.ofInstant(Timestamp.valueOf(notaFiscal.getNf_dmi_data().toString()).toInstant(), ZoneId.systemDefault())*/
                    if (nfi.getNfi_valordespesa() > 0) {
                        produto.setValorOutrasDespesasAcessorias(new BigDecimal(nfi.getNfi_valordespesa()).setScale(2, RoundingMode.HALF_UP));
                    }
		    /*fica pra lembrar que esse bloco foi feito e sabemos pra qual situação , e que deu problema nas notas de importação da corel 
		    e que foi desabilitado o fator pq dava erro de total dos produtos nao bate com o somatorio dos itens
		    
		    String valorUnitario = String.valueOf(nfi.getNfi_prunit());
		    produto.setValorUnitario(new BigDecimal(valorUnitario).add(new BigDecimal(fator)).setScale(5, RoundingMode.HALF_UP));
		    String valorTotal = String.valueOf(nfi.getNfi_total());
		    produto.setValorTotalBruto(new BigDecimal(valorTotal).add(new BigDecimal(nfi.getNfi_imposto_importacao())).setScale(2, RoundingMode.HALF_UP));
		     */
                    String valorUnitario = String.valueOf(nfi.getNfi_prunit());
                    produto.setValorUnitario(new BigDecimal(valorUnitario).setScale(5, RoundingMode.HALF_UP));
                    String valorTotal = String.valueOf(nfi.getNfi_total());
                    produto.setValorTotalBruto(new BigDecimal(valorTotal).setScale(2, RoundingMode.HALF_UP));
//                    if (Util.getNumeros(notaFiscal.getNfefinalidade()).equals("2") || Util.getNumeros(notaFiscal.getNfefinalidade()).equals("3")) {
//                        produto.setCompoeValorNota(NFProdutoCompoeValorNota.NAO);
//                    } else {
                    produto.setCompoeValorNota(NFProdutoCompoeValorNota.SIM);
                    //}
                    produto.setQuantidadeTributavel(new BigDecimal(String.valueOf(nfi.getNfi_qtdfat())));
                    String valorUnitarioTributavel = String.valueOf(nfi.getNfi_prunit());
		    /*
		    bronca de corel, se der merda em kits elasticos mudar aqui
//		    produto.setValorUnitarioTributavel(new BigDecimal(valorUnitarioTributavel).add(new BigDecimal(fator)).setScale(5, RoundingMode.HALF_UP));
		     */
                    produto.setValorUnitarioTributavel(new BigDecimal(valorUnitarioTributavel).setScale(5, RoundingMode.HALF_UP));
                    double valorDesconto = nfi.getNfi_desconto();
                    if (valorDesconto > 0) {
                        produto.setValorDesconto(new BigDecimal(valorDesconto).setScale(2, RoundingMode.HALF_UP));
                    }

                    String s = nfi.getNfi_cfop().substring(1, 4);
                    if (nfi.getNfi_cfop().substring(1, 4).equals("651") || nfi.getNfi_cfop().substring(1, 4).equals("656") || nfi.getNfi_cfop().substring(1, 4).equals("655")) {
                        ProdutoCombustivel pComb = new ProdutoCombustivel();
                        pComb = utilDAO.getProdutoCombustivel(nfi.getNfi_codint());
                        if (!pComb.getPcom_codigo_anp().equals("")) {
                            msgErroItens = "Gerando bloco de combustível no item " + nfi.getNfi_item();
                            NFNotaInfoItemProdutoCombustivel combustivel = new NFNotaInfoItemProdutoCombustivel();
                            combustivel.setCodigoProdutoANP(pComb.getPcom_codigo_anp());
                            combustivel.setDescricaoProdutoANP(pComb.getPcom_descricao_anp());
                            combustivel.setUf(DFUnidadeFederativa.valueOfCodigo(utilDAO.getCodigoUF(notaFiscal.getNf_clifor())));
                            combustivel.setPercentualGLPDerivadoPetroleo(new BigDecimal(pComb.getPcom_pglp()).setScale(2, RoundingMode.HALF_UP));
                            combustivel.setPercentualGasNaturalImportado(new BigDecimal(pComb.getPcom_pgni()).setScale(2, RoundingMode.HALF_UP));
                            combustivel.setPercentualGasNaturalNacional(new BigDecimal(pComb.getPcom_pgn()).setScale(2, RoundingMode.HALF_UP));
                            combustivel.setValorPartida(new BigDecimal(pComb.getPcom_vpartida()).setScale(2, RoundingMode.HALF_UP));

                            produto.setUnidadeTributavel(pComb.getPcom_unidadetributavel());

                            double quantidadeTributavel = nfi.getNfi_qtdfat() * pComb.getPcom_qtdtributavel();
                            produto.setQuantidadeTributavel(new BigDecimal(quantidadeTributavel));

                            double valorUnidadeTributavel = nfi.getNfi_prunit() / pComb.getPcom_qtdtributavel();
                            produto.setValorUnitarioTributavel(new BigDecimal(valorUnidadeTributavel).setScale(10, RoundingMode.HALF_UP));

                            if (!pComb.getPcom_codif().equals("")) {
                                combustivel.setCodigoAutorizacaoCODIF(pComb.getPcom_codif());
                            }
                            produto.setCombustivel(combustivel);
                        }
                    }
                    if (nfi.getNfi_valorfrete() > 0) {
                        produto.setValorFrete(new BigDecimal(nfi.getNfi_valorfrete()).setScale(5, RoundingMode.HALF_UP));
                    }
                    String infAdicionaisItem = nfi.getNfi_obs();
                    if (infAdicionaisItem.length() > 0) {
                        nfIten.setInformacoesAdicionais(infAdicionaisItem);
                    }
                    nfIten.setProduto(produto);

                    NFNotaInfoItemImposto imposto = new NFNotaInfoItemImposto();

                    if (notaFiscal.getFil_crt().contains("3")) {
                        /*     PIS     */
                        if (nfi.getNfi_cst_pis().equals("") || nfi.getNfi_cst_pis() == null) {
                            msgErroItens = "Campo Nfi_cst_pis vazio ou nullo";
                        }
                        int cst_pis = Integer.parseInt(nfi.getNfi_cst_pis());
                        if (!"0".equals(nfi.getNfi_cst_pis())) {
                            if (cst_pis == 01 || cst_pis == 02 || cst_pis == 50) {
                                msgErroItens = "Erro gerar imposto PIS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_pis + " no item " + nfi.getNfi_item();
                                NFNotaInfoItemImpostoPIS pis = new NFNotaInfoItemImpostoPIS();
                                NFNotaInfoItemImpostoPISAliquota aliquotaPis = new NFNotaInfoItemImpostoPISAliquota();
                                aliquotaPis.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.OPERACAO_TRIBUTAVEL_CUMULATIVO_NAO_CUMULATIVO);
                                aliquotaPis.setPercentualAliquota(new BigDecimal(String.valueOf(nfi.getNfi_aliquota_pis())).setScale(2, RoundingMode.HALF_UP));
                                aliquotaPis.setValorBaseCalculo(new BigDecimal(String.valueOf(nfi.getNfi_total())).add(new BigDecimal(nfi.getNfi_imposto_importacao())).setScale(2, RoundingMode.HALF_UP));
                                aliquotaPis.setValorTributo(new BigDecimal(String.valueOf(nfi.getNfi_valor_pis())).setScale(2, RoundingMode.HALF_UP));
                                pis.setAliquota(aliquotaPis);
                                imposto.setPis(pis);
                            } else if (cst_pis == 03) {
                                throw new Exception("Item " + nfi.getNfi_item() + " com CST PIS incorreto " + cst_pis);
                            } else if (cst_pis == 05 || cst_pis == 75) {
                                msgErroItens = "Erro gerar imposto PIS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_pis + " no item " + nfi.getNfi_item();
                                NFNotaInfoItemImpostoPIS pis = new NFNotaInfoItemImpostoPIS();
                                NFNotaInfoItemImpostoPISNaoTributado pisnt = new NFNotaInfoItemImpostoPISNaoTributado();
                                pisnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.OPERACAO_TRIBUTAVEL_SUBSTITUICAO_TRIBUTARIA);
                                NFNotaInfoItemImpostoPISST pisst = new NFNotaInfoItemImpostoPISST();
                                pisst.setPercentualAliquota(new BigDecimal(String.valueOf(nfi.getNfi_aliquota_pis())).setScale(2, RoundingMode.HALF_UP));
                                pisst.setValorBaseCalculo(new BigDecimal(String.valueOf(nfi.getNfi_total())).add(new BigDecimal(nfi.getNfi_imposto_importacao())).setScale(2, RoundingMode.HALF_UP));
                                pisst.setValorTributo(new BigDecimal(String.valueOf(nfi.getNfi_valor_pis())).setScale(2, RoundingMode.HALF_UP));
                                pis.setNaoTributado(pisnt);
                                imposto.setPis(pis);
                                imposto.setPisst(pisst);
                            } else if ((cst_pis >= 4 && cst_pis <= 9 || cst_pis >= 70 && cst_pis <= 74) && cst_pis != 5) {
                                NFNotaInfoItemImpostoPIS pis = new NFNotaInfoItemImpostoPIS();
                                NFNotaInfoItemImpostoPISNaoTributado pisnt = new NFNotaInfoItemImpostoPISNaoTributado();
                                if (cst_pis == 04 || cst_pis == 70) {
                                    msgErroItens = "Erro gerar imposto PIS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_pis + " no item " + nfi.getNfi_item();
                                    pisnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.OPERACAO_TRIBUTAVEL_MONOFASICA_ALIQUOTA_ZERO);
                                } else if (cst_pis == 71) {
                                    msgErroItens = "Erro gerar imposto PIS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_pis + " no item " + nfi.getNfi_item();
                                    pisnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.OPERACAO_ISENTA_CONTRIBUICAO);
                                } else if (cst_pis == 72) {
                                    msgErroItens = "Erro gerar imposto PIS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_pis + " no item " + nfi.getNfi_item();
                                    pisnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.OPERACAO_COM_SUSPENSAO_CONTRIBUICAO);
                                } else if (cst_pis == 73) {
                                    msgErroItens = "Erro gerar imposto PIS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_pis + " no item " + nfi.getNfi_item();
                                    pisnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.OPERACAO_TRIBUTAVEL_ALIQUOTA_ZERO);
                                } else if (cst_pis == 74) {
                                    msgErroItens = "Erro gerar imposto PIS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_pis + " no item " + nfi.getNfi_item();
                                    pisnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.OPERACAO_SEM_INCIDENCIA_CONTRIBUICAO);
                                } else if (cst_pis == 75) {
                                    msgErroItens = "Erro gerar imposto PIS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_pis + " no item " + nfi.getNfi_item();
                                    pisnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.OPERACAO_TRIBUTAVEL_SUBSTITUICAO_TRIBUTARIA);
                                } else {
                                    msgErroItens = "Erro gerar imposto PIS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_pis + " no item " + nfi.getNfi_item();
                                    pisnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.valueOfCodigo(nfi.getNfi_cst_pis()));
                                }
                                pis.setNaoTributado(pisnt);
                                imposto.setPis(pis);
                            } else if (cst_pis == 49 || cst_pis == 98 || cst_pis == 99) {
                                msgErroItens = "Erro gerar imposto PIS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_pis + " no item " + nfi.getNfi_item();
                                NFNotaInfoItemImpostoPIS pis = new NFNotaInfoItemImpostoPIS();
                                NFNotaInfoItemImpostoPISOutrasOperacoes pisop = new NFNotaInfoItemImpostoPISOutrasOperacoes();
                                pisop.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.valueOfCodigo(nfi.getNfi_cst_pis()));
                                pisop.setValorBaseCalculo(new BigDecimal(String.valueOf(nfi.getNfi_total())).setScale(2, RoundingMode.HALF_UP));
                                pisop.setPercentualAliquota(new BigDecimal(String.valueOf(nfi.getNfi_aliquota_pis())).setScale(2, RoundingMode.HALF_UP));
                                pisop.setValorTributo(new BigDecimal(String.valueOf(nfi.getNfi_valor_pis())).setScale(2, RoundingMode.HALF_UP));
                                pis.setOutrasOperacoes(pisop);
                                imposto.setPis(pis);
                            }
                        }

                        /*     COFINS     */
                        if (!"00".equals(nfi.getNfi_cst_cofins())) {
                            if (nfi.getNfi_cst_cofins().equals("") || nfi.getNfi_cst_cofins() == null) {
                                msgErroItens = "Campo Nfi_cst_cofins vazio ou nullo.";
                            }
                            int cst_cofins = Integer.parseInt(nfi.getNfi_cst_cofins());
                            if (cst_cofins == 01 || cst_cofins == 02 || cst_cofins == 50) {
                                msgErroItens = "Erro gerar imposto COFINS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_cofins + " no item " + nfi.getNfi_item();
                                NFNotaInfoItemImpostoCOFINS cofins = new NFNotaInfoItemImpostoCOFINS();
                                NFNotaInfoItemImpostoCOFINSAliquota cofinsAliq = new NFNotaInfoItemImpostoCOFINSAliquota();
                                cofinsAliq.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.OPERACAO_TRIBUTAVEL_CUMULATIVO_NAO_CUMULATIVO);
                                cofinsAliq.setPercentualAliquota(new BigDecimal(String.valueOf(nfi.getNfi_aliquota_cofins())).setScale(2, RoundingMode.HALF_UP));
                                //cofinsAliq.setValorBaseCalulo(new BigDecimal(String.valueOf(nfi.getNfi_total())).add(new BigDecimal(nfi.getNfi_imposto_importacao())).setScale(2, RoundingMode.HALF_UP));
                                cofinsAliq.setValorBaseCalculo(new BigDecimal(String.valueOf(nfi.getNfi_total())).add(new BigDecimal(nfi.getNfi_imposto_importacao())).setScale(2, RoundingMode.HALF_UP));
                                cofinsAliq.setValor(new BigDecimal(String.valueOf(nfi.getNfi_valor_cofins())).setScale(2, RoundingMode.HALF_UP));
                                cofins.setAliquota(cofinsAliq);
                                imposto.setCofins(cofins);
                            } else if (cst_cofins == 03) {
                                throw new Exception("Item " + nfi.getNfi_codpro() + " com CST COFINS incorreto " + cst_cofins);
                            } else if (cst_cofins == 05 || cst_cofins == 75) {
                                msgErroItens = "Erro gerar imposto COFINS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_cofins + " no item " + nfi.getNfi_item();
                                NFNotaInfoItemImpostoCOFINS cofins = new NFNotaInfoItemImpostoCOFINS();
                                NFNotaInfoItemImpostoCOFINSNaoTributavel cofinsnt = new NFNotaInfoItemImpostoCOFINSNaoTributavel();
                                cofinsnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.OPERACAO_TRIBUTAVEL_SUBSTITUICAO_TRIBUTARIA);
                                NFNotaInfoItemImpostoCOFINSST cofinsst = new NFNotaInfoItemImpostoCOFINSST();
                                cofinsst.setPercentualAliquota(new BigDecimal(String.valueOf(nfi.getNfi_aliquota_cofins())).setScale(2, RoundingMode.HALF_UP));
                                cofinsst.setValorBaseCalculo(new BigDecimal(String.valueOf(nfi.getNfi_total())).add(new BigDecimal(nfi.getNfi_imposto_importacao())).setScale(2, RoundingMode.HALF_UP));
                                cofinsst.setValorCOFINS(new BigDecimal(String.valueOf(nfi.getNfi_valor_cofins())).setScale(2, RoundingMode.HALF_UP));
                                cofins.setNaoTributavel(cofinsnt);
                                imposto.setCofins(cofins);
                                imposto.setCofinsst(cofinsst);
                            } else if ((cst_cofins >= 4 && cst_cofins <= 9 || cst_cofins >= 70 && cst_cofins <= 74) && cst_cofins != 5) {
                                NFNotaInfoItemImpostoCOFINS cofins = new NFNotaInfoItemImpostoCOFINS();
                                NFNotaInfoItemImpostoCOFINSNaoTributavel cofinsnt = new NFNotaInfoItemImpostoCOFINSNaoTributavel();
                                if (cst_cofins == 04 || cst_pis == 70) {
                                    msgErroItens = "Erro gerar imposto COFINS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_cofins + " no item " + nfi.getNfi_item();
                                    cofinsnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.OPERACAO_TRIBUTAVEL_MONOFASICA_ALIQUOTA_ZERO);
                                } else if (cst_cofins == 71) {
                                    msgErroItens = "Erro gerar imposto COFINS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_cofins + " no item " + nfi.getNfi_item();
                                    cofinsnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.OPERACAO_ISENTA_CONTRIBUICAO);
                                } else if (cst_cofins == 72) {
                                    msgErroItens = "Erro gerar imposto COFINS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_cofins + " no item " + nfi.getNfi_item();
                                    cofinsnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.OPERACAO_COM_SUSPENSAO_CONTRIBUICAO);
                                } else if (cst_cofins == 73) {
                                    msgErroItens = "Erro gerar imposto COFINS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_cofins + " no item " + nfi.getNfi_item();
                                    cofinsnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.OPERACAO_TRIBUTAVEL_ALIQUOTA_ZERO);
                                } else if (cst_cofins == 74) {
                                    msgErroItens = "Erro gerar imposto COFINS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_cofins + " no item " + nfi.getNfi_item();
                                    cofinsnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.OPERACAO_SEM_INCIDENCIA_CONTRIBUICAO);
                                } else if (cst_cofins == 75) {
                                    msgErroItens = "Erro gerar imposto COFINS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_cofins + " no item " + nfi.getNfi_item();
                                    cofinsnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.OPERACAO_TRIBUTAVEL_SUBSTITUICAO_TRIBUTARIA);
                                } else {
                                    msgErroItens = "Erro gerar imposto COFINS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_cofins + " no item " + nfi.getNfi_item();
                                    cofinsnt.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.valueOfCodigo(nfi.getNfi_cst_cofins()));
                                }
                                cofins.setNaoTributavel(cofinsnt);
                                imposto.setCofins(cofins);
                            } else if (cst_cofins >= 49 || cst_cofins <= 98 || cst_cofins == 99) {
                                msgErroItens = "Erro gerar imposto COFINS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_cofins + " no item " + nfi.getNfi_item();
                                NFNotaInfoItemImpostoCOFINS cofins = new NFNotaInfoItemImpostoCOFINS();
                                NFNotaInfoItemImpostoCOFINSOutrasOperacoes cofinsop = new NFNotaInfoItemImpostoCOFINSOutrasOperacoes();
                                cofinsop.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.valueOfCodigo(nfi.getNfi_cst_cofins()));
                                cofinsop.setValorBaseCalculo(new BigDecimal(String.valueOf(nfi.getNfi_total())).setScale(2, RoundingMode.HALF_UP));
                                cofinsop.setPercentualCOFINS(new BigDecimal(String.valueOf(nfi.getNfi_aliquota_cofins())).setScale(2, RoundingMode.HALF_UP));
                                cofinsop.setValorCOFINS(new BigDecimal(String.valueOf(nfi.getNfi_valor_cofins())).setScale(2, RoundingMode.HALF_UP));
                                cofins.setOutrasOperacoes(cofinsop);
                                imposto.setCofins(cofins);
                            }
                        }
                    } else {
                        /* PIS */
                        msgErroItens = "Erro gerar imposto PISOP com CRT " + notaFiscal.getFil_crt() + " no item " + nfi.getNfi_item();
                        NFNotaInfoItemImpostoPIS pis = new NFNotaInfoItemImpostoPIS();
                        NFNotaInfoItemImpostoPISOutrasOperacoes pisop = new NFNotaInfoItemImpostoPISOutrasOperacoes();
                        pisop.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.valueOfCodigo(nfi.getNfi_cst_pis()));
                        if (nfi.getNfi_valor_pis() > 0) {
                            pisop.setValorBaseCalculo(new BigDecimal(String.valueOf(nfi.getNfi_total())).setScale(2, RoundingMode.HALF_UP));
                        } else {
                            pisop.setValorBaseCalculo(BigDecimal.ZERO);
                        }
                        pisop.setPercentualAliquota(new BigDecimal(String.valueOf(nfi.getNfi_aliquota_pis())).setScale(2, RoundingMode.HALF_UP));
                        pisop.setValorTributo(new BigDecimal(String.valueOf(nfi.getNfi_valor_pis())).setScale(2, RoundingMode.HALF_UP));
                        pisop.setSituacaoTributaria(NFNotaInfoSituacaoTributariaPIS.OUTRAS_OPERACOES_SAIDA);
                        pis.setOutrasOperacoes(pisop);
                        imposto.setPis(pis);

                        /* COFINS */
                        msgErroItens = "Erro gerar imposto COFINSOP com CRT " + notaFiscal.getFil_crt() + " no item " + nfi.getNfi_item();
                        NFNotaInfoItemImpostoCOFINS cofins = new NFNotaInfoItemImpostoCOFINS();
                        NFNotaInfoItemImpostoCOFINSOutrasOperacoes cofinsop = new NFNotaInfoItemImpostoCOFINSOutrasOperacoes();
                        cofinsop.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.valueOfCodigo(nfi.getNfi_cst_cofins()));
                        if (nfi.getNfi_valor_cofins() > 0) {
                            cofinsop.setValorBaseCalculo(new BigDecimal(String.valueOf(nfi.getNfi_total())).setScale(2, RoundingMode.HALF_UP));
                        } else {
                            cofinsop.setValorBaseCalculo(BigDecimal.ZERO);
                        }
                        cofinsop.setPercentualCOFINS(new BigDecimal(String.valueOf(nfi.getNfi_aliquota_cofins())).setScale(2, RoundingMode.HALF_UP));
//                            valorTotalCofins = valorTotalCofins.add(new BigDecimal(String.valueOf(nfi.getNfi_valor_cofins())));
                        cofinsop.setValorCOFINS(new BigDecimal(String.valueOf(nfi.getNfi_valor_cofins())).setScale(2, RoundingMode.HALF_UP));
                        cofinsop.setSituacaoTributaria(NFNotaInfoSituacaoTributariaCOFINS.OUTRAS_OPERACOES_SAIDA);
                        cofins.setOutrasOperacoes(cofinsop);
                        imposto.setCofins(cofins);
                    }

                    int cst_icms = 0;
                    if (notaFiscal.getFil_crt().contains("1")) {
                        if (nfi.getNfi_sittrib().substring(1, 3).equals("00") || nfi.getNfi_sittrib().substring(1, 3).equals("20")) {
                            cst_icms = Integer.parseInt(notaFiscal.getFil_crt_sn());
                        } else if (nfi.getNfi_sittrib().substring(1, 3).equals("40") || nfi.getNfi_sittrib().substring(1, 3).equals("30")) {
                            cst_icms = 103;
                        } else if (nfi.getNfi_sittrib().substring(1, 3).equals("10") || nfi.getNfi_sittrib().substring(1, 3).equals("70")) {
                            if (notaFiscal.getFil_crt_sn().equals("101")) {
                                cst_icms = 201;
                            } else {
                                cst_icms = 202;
                            }
                        } else if (nfi.getNfi_sittrib().substring(1, 3).equals("41") || nfi.getNfi_sittrib().substring(1, 3).equals("50") || nfi.getNfi_sittrib().substring(1, 3).equals("51")) {
                            cst_icms = 400;
                        } else if (nfi.getNfi_sittrib().substring(1, 3).equals("60")) {
                            cst_icms = 500;
                        } else {
                            cst_icms = 900;
                        }
                    } else {
                        cst_icms = Integer.parseInt(nfi.getNfi_sittrib().substring(1, 3));
                    }

                    /*     ICMS     */
                    if (!"".equals(nfi.getNfi_sittrib())) {
                        System.out.println("SitTrib inteiro.....: " + Integer.parseInt(nfi.getNfi_sittrib()));
//                        int cst_icms = Integer.parseInt(nfi.getNfi_sittrib());
                        NFNotaInfoItemImpostoICMS icms = new NFNotaInfoItemImpostoICMS();
                        String a = notaFiscal.getFil_crt();
                        if (notaFiscal.getFil_crt().contains("3")) {
                            switch (cst_icms) {
                                case 0:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST 0" + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMS00 icms00 = new NFNotaInfoItemImpostoICMS00();
                                    if (!"".equals(nfi.getPro_origem()) && nfi.getPro_origem() != null) {
                                        icms00.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    } else {
                                        char[] primeiroDigitoIcms = nfi.getNfi_sittrib().toCharArray();
                                        icms00.setOrigem(NFOrigem.valueOfCodigo(String.valueOf(primeiroDigitoIcms[0])));
                                    }
                                    icms00.setSituacaoTributaria(NFNotaInfoImpostoTributacaoICMS.TRIBUTACAO_INTEGRALMENTE);
                                    icms00.setModalidadeBCICMS(NFNotaInfoItemModalidadeBCICMS.VALOR_OPERACAO);

                                    if (nfi.getNfi_vicms_n() > 0) {
                                        icms00.setPercentualAliquota(new BigDecimal(String.valueOf(nfi.getNfi_picms_n())).setScale(2, RoundingMode.HALF_UP));
                                        icms00.setValorTributo(new BigDecimal(String.valueOf(nfi.getNfi_vicms_n())).setScale(2, RoundingMode.HALF_UP));
                                        icms00.setValorBaseCalculo(new BigDecimal(String.valueOf(nfi.getNfi_base_calculo())).setScale(2, RoundingMode.HALF_UP));
                                    } else {
                                        icms00.setPercentualAliquota(BigDecimal.ZERO);
                                        icms00.setValorTributo(BigDecimal.ZERO);
                                        icms00.setValorBaseCalculo(BigDecimal.ZERO);
                                    }
                                    if (nfi.getNfi_vfcp() > 0) {
                                        icms00.setValorFundoCombatePobreza(new BigDecimal(String.valueOf(nfi.getNfi_vfcp())).setScale(2, RoundingMode.HALF_UP));
                                        icms00.setPercentualFundoCombatePobreza(new BigDecimal(nfi.getNfi_pfcp()).setScale(2, RoundingMode.HALF_UP));
                                    }

                                    icms.setIcms00(icms00);

                                    break;
                                case 10:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST 0" + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMS10 icms10 = new NFNotaInfoItemImpostoICMS10();
                                    if (!"".equals(nfi.getPro_origem()) && nfi.getPro_origem() != null) {
                                        icms10.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    } else {
                                        char[] primeiroDigitoIcms = nfi.getNfi_sittrib().toCharArray();
                                        icms10.setOrigem(NFOrigem.valueOfCodigo(String.valueOf(primeiroDigitoIcms[0])));
                                    }
                                    icms10.setSituacaoTributaria(NFNotaInfoImpostoTributacaoICMS.TRIBUTADA_COM_COBRANCA_ICMS_POR_SUBSTITUICAO_TRIBUTARIA);

                                    icms10.setModalidadeBCICMS(NFNotaInfoItemModalidadeBCICMS.VALOR_OPERACAO);

//                                    baseCalculoIcms = baseCalculoIcms.add(new BigDecimal(String.valueOf(nfi.getNfi_base_s())).setScale(2, RoundingMode.HALF_UP));
                                    icms10.setValorBaseCalculo(new BigDecimal(String.valueOf(nfi.getNfi_base_calculo())).setScale(2, RoundingMode.HALF_UP));

                                    icms10.setPercentualAliquota(new BigDecimal(nfi.getNfi_picms_n()).setScale(2, RoundingMode.HALF_UP));

//                                    valorTotalIcms = valorTotalIcms.add(new BigDecimal(String.valueOf(nfi.getNfi_vicms_n())).setScale(2, RoundingMode.HALF_UP));
                                    icms10.setValorTributo(new BigDecimal(String.valueOf(nfi.getNfi_vicms_n())).setScale(2, RoundingMode.HALF_UP));

                                    icms10.setModalidadeBCICMSST(NFNotaInfoItemModalidadeBCICMSST.MARGEM_VALOR_AGREGADO);

                                    double margen = ((nfi.getNfi_base_s() / nfi.getNfi_base_calculo()) * 100 - 100);
                                    if (margen > 0) {
                                        icms10.setPercentualMargemValorAdicionadoICMSST(new BigDecimal(margen).setScale(2, RoundingMode.HALF_UP));
                                    } else {
                                        icms10.setPercentualMargemValorAdicionadoICMSST(BigDecimal.ZERO);
                                    }

                                    icms10.setPercentualReducaoBCICMSST(BigDecimal.ZERO); //genivaldo

                                    icms10.setValorBCICMSST(new BigDecimal(String.valueOf(nfi.getNfi_base_s())));

                                    String percentualAliquota = String.valueOf((nfi.getNfi_vicms_s() / nfi.getNfi_base_s()) * 100);

                                    icms10.setPercentualAliquotaImpostoICMSST(new BigDecimal(percentualAliquota).setScale(2, RoundingMode.HALF_UP));

                                    icms10.setValorICMSST(new BigDecimal(String.valueOf(nfi.getNfi_vicms_s())).setScale(2, RoundingMode.HALF_UP));

                                    if (nfNotaInfo.getOperacaoConsumidorFinal().equals(NFOperacaoConsumidorFinal.SIM)) {
                                        icms10.setValorBaseCalculoFundoCombatePobreza(new BigDecimal(String.valueOf(nfi.getNfi_fcpst_basecalculo())));
                                        icms10.setPercentualFundoCombatePobreza(new BigDecimal("2.00").setScale(2, RoundingMode.HALF_UP));
                                        icms10.setValorFundoCombatePobreza(new BigDecimal(nfi.getNfi_vfcp()).setScale(2, RoundingMode.HALF_UP));

                                        icms10.setPercentualFundoCombatePobrezaST(new BigDecimal(String.valueOf(nfi.getNfi_fcpst_aliquota())));
                                        icms10.setValorFundoCombatePobrezaST(new BigDecimal(String.valueOf(nfi.getNfi_fcpst_valor())));
                                        icms10.setValorBCFundoCombatePobrezaST(new BigDecimal(nfi.getNfi_fcpst_basecalculo()).setScale(2, RoundingMode.HALF_UP));
                                    }

                                    icms.setIcms10(icms10);
				    /*NFNotaInfoItemImpostoICMSST icmsst10 = new NFNotaInfoItemImpostoICMSST();
                        if(!"".equals(vItem.getVdi_origem())){
                            icmsst10.setOrigem(NFOrigem.valueOfCodigo(vItem.getVdi_origem()));
                        }else{
                            char[] primeiroDigitoIcms = vItem.getVdi_cst_icms().toCharArray();
                            icmsst10.setOrigem(NFOrigem.valueOfCodigo(String.valueOf(primeiroDigitoIcms[0])));                            
                        }
                        icmsst10.setSituacaoTributaria(NFNotaInfoImpostoTributacaoICMS.NAO_TRIBUTADO);
                        icmsst10.setValorBCICMSSTRetidoUFRemetente(BigDecimal.ZERO);
                        icmsst10.setValorBCICMSSTUFDestino(BigDecimal.ZERO);
                        icmsst10.setValorICMSSTRetidoUFRemetente(BigDecimal.ZERO);
                        icmsst10.setValorICMSSTUFDestino(BigDecimal.ZERO); */
                                    break;
                                case 20:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST 0" + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMS20 icms20 = new NFNotaInfoItemImpostoICMS20();
                                    icms20.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    icms20.setSituacaoTributaria(NFNotaInfoImpostoTributacaoICMS.COM_REDUCAO_BASE_CALCULO);
                                    icms20.setModalidadeBCICMS(NFNotaInfoItemModalidadeBCICMS.VALOR_OPERACAO);
                                    icms20.setPercentualAliquota(new BigDecimal(String.valueOf(nfi.getNfi_picms_n())).setScale(2, RoundingMode.HALF_UP));
                                    double preducao = ((nfi.getNfi_base_calculo() / nfi.getNfi_total()) * 100);
                                    icms20.setPercentualReducaoBC(new BigDecimal(preducao).setScale(2, RoundingMode.HALF_UP));
                                    icms20.setValorBCICMS(new BigDecimal(String.valueOf(nfi.getNfi_base_calculo())).setScale(2, RoundingMode.HALF_UP));
                                    icms20.setValorTributo(new BigDecimal(String.valueOf(nfi.getNfi_vicms_n())).setScale(2, RoundingMode.HALF_UP));

                                    icms.setIcms20(icms20);
                                    //adcionou só não tava mostrando, testa pra ver
                                    break;
                                case 40:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST 0" + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMS40 icms40 = new NFNotaInfoItemImpostoICMS40();
                                    if (!nfi.getPro_origem().equals("") && nfi.getPro_origem() != null) {
                                        icms40.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    } else {
                                        char[] primeiroDigitoIcms = nfi.getNfi_sittrib().toCharArray();
                                        icms40.setOrigem(NFOrigem.valueOfCodigo(String.valueOf(primeiroDigitoIcms[0])));
                                    }
                                    icms40.setSituacaoTributaria(NFNotaInfoImpostoTributacaoICMS.ISENTA);
                                    icms.setIcms40(icms40);
                                    break;
                                case 41:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST 0" + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMS40 icms41 = new NFNotaInfoItemImpostoICMS40();
                                    if (!nfi.getPro_origem().equals("") && nfi.getPro_origem() != null) {
                                        icms41.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    } else {
                                        char[] primeiroDigitoIcms = nfi.getNfi_sittrib().toCharArray();
                                        icms41.setOrigem(NFOrigem.valueOfCodigo(String.valueOf(primeiroDigitoIcms[0])));
                                    }
                                    icms41.setSituacaoTributaria(NFNotaInfoImpostoTributacaoICMS.NAO_TRIBUTADO);
                                    icms.setIcms40(icms41);
                                    break;
                                case 50:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST 0" + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMS40 icms50 = new NFNotaInfoItemImpostoICMS40();
                                    if (!nfi.getPro_origem().equals("") && nfi.getPro_origem() != null) {
                                        icms50.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    } else {
                                        char[] primeiroDigitoIcms = nfi.getNfi_sittrib().toCharArray();
                                        icms50.setOrigem(NFOrigem.valueOfCodigo(String.valueOf(primeiroDigitoIcms[0])));
                                    }
                                    icms50.setSituacaoTributaria(NFNotaInfoImpostoTributacaoICMS.SUSPENSAO);
                                    icms.setIcms40(icms50);
                                    break;
                                case 60:
//				    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST 0" + cst_icms + " no item " + nfi.getNfi_item();
//				    NFNotaInfoItemImpostoICMS60 icmsST = new NFNotaInfoItemImpostoICMS60();
//				    if (!"".equals(nfi.getPro_origem()) && nfi.getPro_origem() != null) {
//					icmsST.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
//				    } else {
//					char[] primeiroDigitoIcms = nfi.getNfi_sittrib().toCharArray();
//					icmsST.setOrigem(NFOrigem.valueOfCodigo(String.valueOf(primeiroDigitoIcms[0])));
//				    }
//				    icmsST.setSituacaoTributaria(NFNotaInfoImpostoTributacaoICMS.ICMS_COBRADO_ANTERIORMENTE_POR_SUBSTITUICAO_TRIBUTARIA);
//				    icmsST.setValorBCICMSSTRetidoUFRemetente(BigDecimal.ZERO);
//				    icmsST.setValorBCICMSSTUFDestino(BigDecimal.ZERO);
//				    icmsST.setValorICMSSTRetidoUFRemetente(BigDecimal.ZERO);
//				    icmsST.setValorICMSSTUFDestino(BigDecimal.ZERO);
//				    icms.setIcmsst(icmsST);
//				    break;
                                    NFNotaInfoItemImpostoICMS60 icms60 = new NFNotaInfoItemImpostoICMS60();
                                    if (!"".equals(nfi.getPro_origem()) && nfi.getPro_origem() != null) {
                                        icms60.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    } else {
                                        char[] primeiroDigitoIcms = nfi.getNfi_sittrib().toCharArray();
                                        icms60.setOrigem(NFOrigem.valueOfCodigo(String.valueOf(primeiroDigitoIcms[0])));
                                    }
//                                    icms60.setPercentualAliquotaICMSEfetiva(BigDecimal.ZERO);
//                                    icms60.setPercentualAliquotaICMSSTConsumidorFinal(BigDecimal.ZERO);
//                                    icms60.setPercentualFundoCombatePobrezaRetidoST(new BigDecimal(nfi.getNfi_opi_picms_fcp()).setScale(2, RoundingMode.HALF_UP));
//                                    icms60.setPercentualReducaoBCEfetiva(BigDecimal.ZERO);
                                    icms60.setSituacaoTributaria(NFNotaInfoImpostoTributacaoICMS.ICMS_COBRADO_ANTERIORMENTE_POR_SUBSTITUICAO_TRIBUTARIA);
//                                    icms60.setValorBCEfetiva(BigDecimal.ZERO);

                                    icms.setIcms60(icms60);
                                    break;

                                case 70:
                                    NFNotaInfoItemImpostoICMS70 icms70 = new NFNotaInfoItemImpostoICMS70();
                                    icms70.setModalidadeBCICMS(NFNotaInfoItemModalidadeBCICMS.VALOR_OPERACAO);
                                    icms70.setModalidadeBCICMSST(NFNotaInfoItemModalidadeBCICMSST.MARGEM_VALOR_AGREGADO);
                                    if (!"".equals(nfi.getPro_origem()) && nfi.getPro_origem() != null) {
                                        icms70.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    } else {
                                        char[] primeiroDigitoIcms = nfi.getNfi_sittrib().toCharArray();
                                        icms70.setOrigem(NFOrigem.valueOfCodigo(String.valueOf(primeiroDigitoIcms[0])));
                                    }
                                    icms70.setPercentualAliquota(new BigDecimal(nfi.getNfi_picms_n()).setScale(2, RoundingMode.HALF_UP));
                                    String pa = String.valueOf((nfi.getNfi_vicms_s() / nfi.getNfi_base_s()) * 100);
                                    icms70.setPercentualAliquotaImpostoICMSST(new BigDecimal(pa).setScale(2, RoundingMode.HALF_UP));
                                    double margem70 = ((nfi.getNfi_base_s() / nfi.getNfi_base_calculo()) * 100 - 100);
                                    if (margem70 > 0) {
                                        icms70.setPercentualMargemValorAdicionadoICMSST(new BigDecimal(margem70).setScale(2, RoundingMode.HALF_UP));
                                    } else {
                                        icms70.setPercentualMargemValorAdicionadoICMSST(BigDecimal.ZERO);
                                    }
                                    double reducao = (nfi.getNfi_base_calculo() / nfi.getNfi_total()) * 100;
                                    icms70.setPercentualReducaoBC(new BigDecimal(reducao).setScale(2, RoundingMode.HALF_UP));
                                    icms70.setPercentualReducaoBCICMSST(new BigDecimal(BigInteger.ZERO));
                                    icms70.setSituacaoTributaria(NFNotaInfoImpostoTributacaoICMS.COM_REDUCAO_BASE_CALCULO_COBRANCA_ICMS_POR_SUBSTITUICAO_TRIBUTARIA_ICMS_SUBSTITUICAO_TRIBUTARIA);
                                    icms70.setValorBC(new BigDecimal(String.valueOf(nfi.getNfi_base_calculo())).setScale(2, RoundingMode.HALF_UP));
                                    icms70.setValorBCST(new BigDecimal(String.valueOf(nfi.getNfi_base_s())));
                                    icms70.setValorICMSST(new BigDecimal(String.valueOf(nfi.getNfi_vicms_s())).setScale(2, RoundingMode.HALF_UP));
                                    icms70.setValorTributo(new BigDecimal(String.valueOf(nfi.getNfi_vicms_n())).setScale(2, RoundingMode.HALF_UP));
                                    /*fundo de combate a pobreza*/
                                    if (nfNotaInfo.getOperacaoConsumidorFinal().equals(NFOperacaoConsumidorFinal.SIM)) {
                                        icms70.setValorBCFundoCombatePobreza(new BigDecimal(String.valueOf(nfi.getNfi_fcpst_basecalculo())));
                                        icms70.setPercentualFundoCombatePobreza(new BigDecimal("2.00").setScale(2, RoundingMode.HALF_UP));
                                        icms70.setValorFundoCombatePobreza(new BigDecimal(nfi.getNfi_vfcp()).setScale(2, RoundingMode.HALF_UP));
                                        icms70.setPercentualFundoCombatePobrezaST(new BigDecimal(String.valueOf(nfi.getNfi_fcpst_aliquota())));
                                        icms70.setValorFundoCombatePobrezaST(new BigDecimal(String.valueOf(nfi.getNfi_fcpst_valor())));
                                        icms70.setValorBCFundoCombatePobrezaST(new BigDecimal(nfi.getNfi_fcpst_basecalculo()).setScale(2, RoundingMode.HALF_UP));
                                    }
                                    icms.setIcms70(icms70);
                                    break;
                                case 90:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST 0" + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMS90 icms90 = new NFNotaInfoItemImpostoICMS90();
                                    if (!"".equals(nfi.getPro_origem()) && nfi.getPro_origem() != null) {
                                        icms90.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    } else {
                                        char[] primeiroDigitoIcms = nfi.getNfi_sittrib().toCharArray();
                                        icms90.setOrigem(NFOrigem.valueOfCodigo(String.valueOf(primeiroDigitoIcms[0])));
                                    }
                                    icms90.setSituacaoTributaria(NFNotaInfoImpostoTributacaoICMS.OUTROS);
                                    icms90.setModalidadeBCICMS(NFNotaInfoItemModalidadeBCICMS.VALOR_OPERACAO);
                                    icms90.setPercentualAliquota(new BigDecimal(nfi.getNfi_picms_n()).setScale(2, RoundingMode.HALF_UP));
                                    icms90.setValorBC(new BigDecimal(nfi.getNfi_base_calculo()).setScale(2, RoundingMode.HALF_UP));
                                    icms90.setModalidadeBCICMSST(NFNotaInfoItemModalidadeBCICMSST.MARGEM_VALOR_AGREGADO);
                                    icms90.setValorBCST(new BigDecimal(nfi.getNfi_base_s()).setScale(2, RoundingMode.HALF_UP));
                                    double margenIcms90 = ((nfi.getNfi_base_s() / nfi.getNfi_base_calculo()) * 100 - 100);
                                    if (margenIcms90 > 0) {
                                        icms90.setPercentualMargemValorAdicionadoICMSST(new BigDecimal(margenIcms90).setScale(2, RoundingMode.HALF_UP));
                                    } else {
                                        icms90.setPercentualMargemValorAdicionadoICMSST(BigDecimal.ZERO);
                                    }

                                    double percentualAliquotaIcs90 = ((nfi.getNfi_vicms_s() / nfi.getNfi_base_s()) * 100);
                                    if (percentualAliquotaIcs90 > 0) {
                                        icms90.setPercentualAliquotaImpostoICMSST(new BigDecimal(percentualAliquotaIcs90).setScale(2, RoundingMode.HALF_UP));
                                    } else {
                                        icms90.setPercentualAliquotaImpostoICMSST(BigDecimal.ZERO);
                                    }
                                    icms90.setValorICMSST(new BigDecimal(nfi.getNfi_vicms_s()).setScale(2, RoundingMode.HALF_UP));//nfi.getNfi_vicms_s()
                                    icms90.setValorTributo(new BigDecimal(nfi.getNfi_vicms_n()).setScale(2, RoundingMode.HALF_UP)); //nfi.getNfi_vicms_n()
                                    icms.setIcms90(icms90);
                                    break;
                                default:
                                    throw new Exception("Item " + nfi.getNfi_item() + " com CST ICMS incorreto " + cst_icms);
                            }

                            if (nfNotaInfo.getIdentificadorLocalDestinoOperacao().equals(NFIdentificadorLocalDestinoOperacao.OPERACAO_INTERESTADUAL) && nfNotaInfo.getOperacaoConsumidorFinal().equals(NFOperacaoConsumidorFinal.SIM) && nfNotaInfo.getTipo().equals(NFTipo.SAIDA)) {
                                if (nfi.getNfi_opi_vicms_interestadual() > 0) {
                                    msgErroItens = "Erro ao gerar bloco de Imposto ICMS UF DESTINO no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMSUFDestino impostoDestino = new NFNotaInfoItemImpostoICMSUFDestino();
                                    impostoDestino.setPercentualAliquotaInternaDestino(new BigDecimal(nfi.getNfi_picmsufdest()).setScale(2, RoundingMode.HALF_UP));
                                    impostoDestino.setPercentualInterestadual(new BigDecimal(nfi.getNfi_picmsinterestadual()).setScale(2, RoundingMode.HALF_UP));
                                    impostoDestino.setPercentualProvisorioPartilha(new BigDecimal(nfi.getNfi_picmsinterpart()).setScale(2, RoundingMode.HALF_UP));
                                    impostoDestino.setPercentualRelativoFundoCombatePobrezaDestino(new BigDecimal(nfi.getNfi_pfcpufdest()).setScale(2, RoundingMode.HALF_UP));
                                    impostoDestino.setValorBCFundoCombatePobrezaDestino(new BigDecimal(nfi.getNfi_vbcufdest()).setScale(2, RoundingMode.HALF_UP));
                                    impostoDestino.setValorBaseCalculoDestino(new BigDecimal(nfi.getNfi_vbcufdest()).setScale(2, RoundingMode.HALF_UP));
                                    impostoDestino.setValorICMSInterestadualDestino(new BigDecimal(nfi.getNfi_vicmsufdest()).setScale(2, RoundingMode.HALF_UP));
                                    impostoDestino.setValorICMSInterestadualRemetente(new BigDecimal(nfi.getNfi_vicmsufremet()).setScale(2, RoundingMode.HALF_UP));
                                    impostoDestino.setValorRelativoFundoCombatePobrezaDestino(new BigDecimal(nfi.getNfi_vfcpufdest()).setScale(2, RoundingMode.HALF_UP));
                                    imposto.setIcmsUfDestino(impostoDestino);
                                }

                            }

                        } else if (notaFiscal.getFil_crt().contains("1")) {
                            switch (cst_icms) {
                                case 101:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMSSN101 icms101 = new NFNotaInfoItemImpostoICMSSN101();
                                    icms101.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    String aliq101 = String.valueOf(nfi.getNfi_picms_n());
                                    icms101.setPercentualAliquotaAplicavelCalculoCreditoSN(new BigDecimal(String.valueOf(notaFiscal.getFil_pcredsn())).setScale(2, RoundingMode.HALF_UP));// paramosAqui
                                    icms101.setSituacaoOperacaoSN(NFNotaSituacaoOperacionalSimplesNacional.TRIBUTADA_COM_PERMISSAO_CREDITO);
                                    String valor101 = String.valueOf(nfi.getNfi_vicms_n());
                                    icms101.setValorCreditoICMSSN(new BigDecimal(valor101).setScale(2, RoundingMode.HALF_UP));
                                    icms.setIcmssn101(icms101);
                                    break;
                                case 102:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMSSN102 icms102 = new NFNotaInfoItemImpostoICMSSN102();
                                    icms102.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    icms102.setSituacaoOperacaoSN(NFNotaSituacaoOperacionalSimplesNacional.TRIBUTADA_SEM_PERMISSAO_CREDITO);
                                    icms.setIcmssn102(icms102);
                                    break;
                                case 103:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMSSN102 icms103 = new NFNotaInfoItemImpostoICMSSN102();
                                    icms103.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    icms103.setSituacaoOperacaoSN(NFNotaSituacaoOperacionalSimplesNacional.ISENCAO_ICMS_FAIXA_RECEITA_BRUTA);
                                    icms.setIcmssn102(icms103);
                                    break;
                                case 300:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMSSN102 icms300 = new NFNotaInfoItemImpostoICMSSN102();
                                    icms300.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    icms300.setSituacaoOperacaoSN(NFNotaSituacaoOperacionalSimplesNacional.IMUNE);
                                    icms.setIcmssn102(icms300);
                                    break;
                                case 400:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMSSN102 icms400 = new NFNotaInfoItemImpostoICMSSN102();
                                    icms400.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    icms400.setSituacaoOperacaoSN(NFNotaSituacaoOperacionalSimplesNacional.NAO_TRIBUTADA);
                                    icms.setIcmssn102(icms400);
                                    break;
                                case 201:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMSSN201 icms201 = new NFNotaInfoItemImpostoICMSSN201();
                                    icms201.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    icms201.setSituacaoOperacaoSN(NFNotaSituacaoOperacionalSimplesNacional.TRIBUTADA_SIMPLES_NACIONAL_COM_PERMISSAO_DE_CREDITO_E_COBRANCA_ICMS_SUBSTITUICAO_TRIBUTARIA);
                                    icms201.setModalidadeBCICMSST(NFNotaInfoItemModalidadeBCICMSST.MARGEM_VALOR_AGREGADO);
                                    icms201.setPercentualAliquotaAplicavelCalculoCreditoSN(new BigDecimal(notaFiscal.getFil_pcredsn()).setScale(2, RoundingMode.HALF_UP));
                                    icms201.setPercentualAliquotaImpostoICMSST(new BigDecimal(nfi.getNfi_picms_n()).setScale(2, RoundingMode.HALF_UP));
                                    double valor = (nfi.getNfi_base_s() / nfi.getNfi_base_calculo() * 100) - 100;
                                    icms201.setPercentualMargemValorAdicionadoICMSST(new BigDecimal(valor).setScale(2, RoundingMode.HALF_UP));
//                                    icms201.setPercentualReducaoBCICMSST(BigDecimal.ZERO);
                                    icms201.setValorBCICMSST(new BigDecimal(nfi.getNfi_base_s()).setScale(2, RoundingMode.HALF_UP));
                                    double valorCredito = notaFiscal.getFil_pcredsn() * nfi.getNfi_base_s() / 100;
                                    icms201.setValorCreditoICMSSN(new BigDecimal(valorCredito).setScale(2, RoundingMode.HALF_UP));
                                    icms201.setValorICMSST(new BigDecimal(nfi.getNfi_vicms_s()).setScale(2, RoundingMode.HALF_UP));
                                    icms.setIcmssn201(icms201);
                                    break;
                                case 202:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMSSN202 icms202 = new NFNotaInfoItemImpostoICMSSN202();
                                    icms202.setModalidadeBCICMSST(NFNotaInfoItemModalidadeBCICMSST.MARGEM_VALOR_AGREGADO);
                                    icms202.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    icms202.setPercentualAliquotaImpostoICMSST(new BigDecimal(nfi.getNfi_picms_n()).setScale(2, RoundingMode.HALF_UP));
                                    double valor2 = (nfi.getNfi_base_s() / nfi.getNfi_base_calculo() * 100) - 100;
                                    if (valor2 > 0) {
                                        icms202.setPercentualMargemValorAdicionadoICMSST(new BigDecimal(valor2).setScale(2, RoundingMode.HALF_UP));
//                                    icms202.setPercentualReducaoBCICMSST(baseCalculoIcms);
                                    } else {
                                        icms202.setPercentualMargemValorAdicionadoICMSST(BigDecimal.ZERO);
                                    }
                                    icms202.setSituacaoOperacaoSN(NFNotaSituacaoOperacionalSimplesNacional.TRIBUTADA_SIMPLES_NACIONAL_PARA_FAIXA_RECEITA_BRUTA_E_COBRANCA_ICMS_SUBSTITUICAO_TRIBUTARIA);
                                    icms202.setValorBCICMSST(new BigDecimal(nfi.getNfi_base_s()).setScale(2, RoundingMode.HALF_UP));
                                    icms202.setValorICMSST(new BigDecimal(nfi.getNfi_vicms_s()).setScale(2, RoundingMode.HALF_UP));
                                    if (nfNotaInfo.getOperacaoConsumidorFinal().equals(NFOperacaoConsumidorFinal.SIM)) {
                                        icms202.setValorBCFundoCombatePobrezaST(new BigDecimal(nfi.getNfi_fcpst_valor()).setScale(2, RoundingMode.HALF_UP));
                                        icms202.setPercentualFundoCombatePobrezaST(new BigDecimal(nfi.getNfi_fcpst_aliquota()).setScale(2, RoundingMode.HALF_UP));
                                        icms202.setValorFundoCombatePobrezaST(new BigDecimal(nfi.getNfi_fcpst_valor()).setScale(2, RoundingMode.HALF_UP));
                                    }
                                    icms.setIcmssn202(icms202);
                                    break;
                                case 203:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMSSN202 icms203 = new NFNotaInfoItemImpostoICMSSN202();
                                    icms203.setModalidadeBCICMSST(NFNotaInfoItemModalidadeBCICMSST.MARGEM_VALOR_AGREGADO);
                                    icms203.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    icms203.setPercentualAliquotaImpostoICMSST(new BigDecimal(nfi.getNfi_picms_n()).setScale(2, RoundingMode.HALF_UP));
                                    double valor3 = (nfi.getNfi_base_s() / nfi.getNfi_base_calculo() * 100) - 100;
                                    icms203.setPercentualMargemValorAdicionadoICMSST(new BigDecimal(valor3).setScale(2, RoundingMode.HALF_UP));
//                                    icms202.setPercentualReducaoBCICMSST(baseCalculoIcms);
                                    icms203.setSituacaoOperacaoSN(NFNotaSituacaoOperacionalSimplesNacional.TRIBUTADA_SIMPLES_NACIONAL_SEM_PERMISSAO_DE_CREDITO_E_COBRANCA_ICMS_SUBSTITUICAO_TRIBUTARIA);
                                    icms203.setValorBCFundoCombatePobrezaST(new BigDecimal(nfi.getNfi_fcpst_valor()).setScale(2, RoundingMode.HALF_UP));
                                    icms203.setValorBCICMSST(new BigDecimal(nfi.getNfi_base_s()).setScale(2, RoundingMode.HALF_UP));
                                    icms203.setValorFundoCombatePobrezaST(new BigDecimal(nfi.getNfi_fcpst_valor()).setScale(2, RoundingMode.HALF_UP));
                                    icms203.setValorICMSST(new BigDecimal(nfi.getNfi_vicms_s()).setScale(2, RoundingMode.HALF_UP));
                                    icms.setIcmssn202(icms203);
                                    break;
                                case 500:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMSSN500 icms500 = new NFNotaInfoItemImpostoICMSSN500();
                                    icms500.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    icms500.setSituacaoOperacaoSN(NFNotaSituacaoOperacionalSimplesNacional.ICMS_COBRADO_ANTERIORMENTE_POR_SUBSTITUICAO_TRIBUTARIA_SUBSIDIO_OU_POR_ANTECIPACAO);
                                    icms500.setValorBCICMSSTRetido(BigDecimal.ZERO);
                                    icms500.setPercentualICMSSTRetido(BigDecimal.ZERO);
                                    icms500.setValorICMSSTRetido(BigDecimal.ZERO);
                                    icms.setIcmssn500(icms500);
                                    break;
                                case 900:
                                    msgErroItens = "Erro gerar imposto ICMS com CRT " + notaFiscal.getFil_crt() + " de CST " + cst_icms + " no item " + nfi.getNfi_item();
                                    NFNotaInfoItemImpostoICMSSN900 icms900 = new NFNotaInfoItemImpostoICMSSN900();
                                    icms900.setOrigem(NFOrigem.valueOfCodigo(nfi.getPro_origem()));
                                    icms900.setSituacaoOperacaoSN(NFNotaSituacaoOperacionalSimplesNacional.OUTROS);
                                    icms900.setModalidadeBCICMS(NFNotaInfoItemModalidadeBCICMS.VALOR_OPERACAO);
                                    icms900.setModalidadeBCICMSST(NFNotaInfoItemModalidadeBCICMSST.MARGEM_VALOR_AGREGADO);
                                    icms900.setPercentualAliquotaAplicavelCalculoCreditoSN(new BigDecimal(notaFiscal.getFil_pcredsn()).setScale(2, RoundingMode.HALF_UP)); //pes_pcredsn
                                    icms900.setPercentualAliquotaImposto(new BigDecimal(nfi.getNfi_picms_n()).setScale(2, RoundingMode.HALF_UP));//nfi_picms_n
                                    double percentualAliquota = (nfi.getNfi_vicms_s() / nfi.getNfi_base_s() * 100);
                                    if (percentualAliquota > 0) {
                                        icms900.setPercentualAliquotaImpostoICMSST(new BigDecimal(percentualAliquota).setScale(2, RoundingMode.HALF_UP)); //calculo do 010
                                    } else {
                                        icms900.setPercentualAliquotaImpostoICMSST(BigDecimal.ZERO);//nfi_picms_s //calculo do 010
                                    }

                                    double margen = ((nfi.getNfi_base_s() / nfi.getNfi_base_calculo()) * 100 - 100);
                                    if (margen > 0) {
                                        icms900.setPercentualMargemValorAdicionadoICMSST(new BigDecimal(margen).setScale(2, RoundingMode.HALF_UP));//calculo do 010
                                    } else {
                                        icms900.setPercentualMargemValorAdicionadoICMSST(BigDecimal.ZERO);//calculo do 010
                                    }

                                    icms900.setValorBCICMS(new BigDecimal(nfi.getNfi_base_calculo()).setScale(2, RoundingMode.HALF_UP)); //nfi_basecalculo
                                    icms900.setValorBCICMSST(new BigDecimal(nfi.getNfi_base_s()).setScale(2, RoundingMode.HALF_UP)); //nfi_base_s
                                    icms900.setValorCreditoICMSSN(new BigDecimal(nfi.getNfi_vicms_n()).setScale(2, RoundingMode.HALF_UP));

                                    icms900.setValorICMS(new BigDecimal(nfi.getNfi_vicms_n()).setScale(2, RoundingMode.HALF_UP)); //nfi_vicms_n
                                    icms900.setValorICMSST(new BigDecimal(nfi.getNfi_vicms_s()).setScale(2, RoundingMode.HALF_UP));  //nfi_vicms_s
                                    if (nfNotaInfo.getOperacaoConsumidorFinal().equals(NFOperacaoConsumidorFinal.SIM)) {
                                        icms900.setValorBCFundoCombatePobrezaST(new BigDecimal(nfi.getNfi_fcpst_basecalculo()).setScale(2, RoundingMode.HALF_UP)); //nfi_bcfctst
                                        icms900.setValorFundoCombatePobrezaST(new BigDecimal(nfi.getNfi_fcpst_valor()).setScale(2, RoundingMode.HALF_UP)); //nfi_fcpst
                                        icms900.setPercentualFundoCombatePobrezaST(new BigDecimal(nfi.getNfi_fcpst_aliquota()).setScale(2, RoundingMode.HALF_UP)); //nfi_pfctst
                                    }
                                    icms.setIcmssn900(icms900);
                                    break;
                                default:
                                    throw new Exception("Item " + nfi.getNfi_item() + " com CST ICMS incorreto " + cst_icms);

                            }
                        } else {
                            throw new Exception("Erro no regime tributário da empresa " + notaFiscal.getFil_crt());
                        }
                        imposto.setIcms(icms);

                        if (nfNotaInfo.getIdentificadorLocalDestinoOperacao().equals(NFIdentificadorLocalDestinoOperacao.OPERACAO_INTERESTADUAL) && nfNotaInfo.getOperacaoConsumidorFinal().equals(NFOperacaoConsumidorFinal.SIM) && nfNotaInfo.getTipo().equals(NFTipo.SAIDA)) {
                            msgErroItens = "Erro ao gerar bloco de Imposto ICMS UF DESTINO no item " + nfi.getNfi_item();

                            if (nfi.getNfi_opi_vicms_interestadual() > 0) {
                                NFNotaInfoItemImpostoICMSUFDestino impostoDestino = new NFNotaInfoItemImpostoICMSUFDestino();
                                impostoDestino.setPercentualAliquotaInternaDestino(new BigDecimal(nfi.getNfi_picmsufdest()).setScale(2, RoundingMode.HALF_UP));
                                impostoDestino.setPercentualInterestadual(new BigDecimal(nfi.getNfi_picmsinterestadual()).setScale(2, RoundingMode.HALF_UP));
                                impostoDestino.setPercentualProvisorioPartilha(new BigDecimal(nfi.getNfi_picmsinterpart()).setScale(2, RoundingMode.HALF_UP));
                                impostoDestino.setPercentualRelativoFundoCombatePobrezaDestino(new BigDecimal(nfi.getNfi_pfcpufdest()).setScale(2, RoundingMode.HALF_UP));
                                impostoDestino.setValorBCFundoCombatePobrezaDestino(new BigDecimal(nfi.getNfi_vbcufdest()).setScale(2, RoundingMode.HALF_UP));
                                impostoDestino.setValorBaseCalculoDestino(new BigDecimal(nfi.getNfi_vbcufdest()).setScale(2, RoundingMode.HALF_UP));
                                impostoDestino.setValorICMSInterestadualDestino(new BigDecimal(nfi.getNfi_vicmsufdest()).setScale(2, RoundingMode.HALF_UP));
                                impostoDestino.setValorICMSInterestadualRemetente(new BigDecimal(nfi.getNfi_vicmsufremet()).setScale(2, RoundingMode.HALF_UP));
                                impostoDestino.setValorRelativoFundoCombatePobrezaDestino(new BigDecimal(nfi.getNfi_vfcpufdest()).setScale(2, RoundingMode.HALF_UP));
                                imposto.setIcmsUfDestino(impostoDestino);
                            }

                        }
                    }

//                    /*     IPI     */
                    NFNotaInfoItemImpostoIPI ipi = new NFNotaInfoItemImpostoIPI();
                    msgErroItens = "Erro ao gerar bloco de IPI (Código de enquadramento) no item " + nfi.getNfi_item();

                    if (nfi.getNfi_vipi() > 0) {
                        ipi.setCodigoEnquadramento(nfi.getNfi_ipi_cenq());
                        if (notaFiscal.getFil_tipo_atividade().equals(Util.getNumeros("1"))) {
                            ipi.setCnpjProdutor(Util.getNumeros(notaFiscal.getFil_cnpj()).trim());
                        }
                        msgErroItens = "Erro ao gerar bloco de IPI TRIBUTADO no item " + nfi.getNfi_item();
                        NFNotaInfoItemImpostoIPITributado ipiTrinutado = new NFNotaInfoItemImpostoIPITributado();
                        ipiTrinutado.setPercentualAliquota(new BigDecimal(nfi.getNfi_pipi()).setScale(2, RoundingMode.HALF_UP));
                        ipiTrinutado.setSituacaoTributaria(NFNotaInfoSituacaoTributariaIPI.valueOfCodigo(nfi.getNfi_cst_ipi()));
                        ipiTrinutado.setValorBaseCalculo(new BigDecimal(nfi.getNfi_baseipi()).setScale(2, RoundingMode.HALF_UP));
                        ipiTrinutado.setValorTributo(new BigDecimal(nfi.getNfi_vipi()).setScale(2, RoundingMode.HALF_UP));
                        ipi.setTributado(ipiTrinutado);
                        imposto.setIpi(ipi);
                    }
//                    else {
//                        NFNotaInfoItemImpostoIPINaoTributado ipiNaoTiributado = new NFNotaInfoItemImpostoIPINaoTributado();
//                        ipiNaoTiributado.setSituacaoTributaria(NFNotaInfoSituacaoTributariaIPI.valueOfCodigo(nfi.getNfi_cst_ipi()));
//                        ipi.setNaoTributado(ipiNaoTiributado);
//                    }

                    /*     OBSERVACAO DO ITEM    */
                    String infAdicionais = "";
                    int controle = 0;
                    msgErroItens = "Erro ao gerar bloco de OBSERVAÇÕES DO ITEM no item " + nfi.getNfi_item();
                    if (nfi.getNfi_tributos_valor() > 0) {
                        NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
                        infAdicionais = "VALOR APROXIMADO DOS TRIBUTOS " + currencyFormat.format(nfi.getNfi_tributos_valor()) + " (%" + nfi.getNfi_tributos_aliquota() + ") FONTE IBPT.";
                        controle++;
                    }
                    msgErroItens = "Erro ao gerar bloco de OBSERVAÇÕES DO ITEM no item " + nfi.getNfi_item();
                    if (nfi.getNfi_obs().length() > 0) {
                        infAdicionais += nfi.getNfi_obs();
                        controle++;
                    }
                    if (controle > 0) {
                        nfIten.setInformacoesAdicionais(infAdicionais.trim());
                    }
                    nfIten.setImposto(imposto);
                    nfItens.add(nfIten);
                }
                notaInfo.setItens(nfItens);

                /*   INFORMACOES ADICIONAIS DA NOTA    */
                int temobs = 0;
                String inf = "";
                msgErroItens = "Erro ao gerar bloco de INF. ADICIONAIS DA NOTA";
                if (notaFiscal.getNf_tributos_valor() > 0) {
                    NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
                    inf = "VALOR APROXIMADO DOS TRINUTOS " + currencyFormat.format(notaFiscal.getNf_tributos_valor()) + " (%" + notaFiscal.getNf_tributos_aliquota() + ") FONTE IBPT.";
                }

                String infAdicionaisFisco = notaFiscal.getNf_infadicionaisfisco();
                NFNotaInfoInformacoesAdicionais informacoesAdicionais = new NFNotaInfoInformacoesAdicionais();
                msgErroItens = "Erro ao gerar bloco de INF. ADICIONAIS DA NOTA";
                if (infAdicionaisFisco.length() > 0 || inf.trim().length() > 0) {
                    informacoesAdicionais.setInformacoesAdicionaisInteresseFisco(Util.removeAcentos(inf + "," + infAdicionaisFisco.trim()));
                    temobs++;
                }
                String infComplementares = notaFiscal.getNf_obs();
                msgErroItens = "Erro ao gerar bloco de INF. ADICIONAIS DA NOTA";
                if (infComplementares.length() > 0) {
                    informacoesAdicionais.setInformacoesComplementaresInteresseContribuinte(Util.removeAcentos(infComplementares.trim()));
                    notaInfo.setInformacoesAdicionais(informacoesAdicionais);
                    temobs++;
                }
                if (temobs > 0) {
                    notaInfo.setInformacoesAdicionais(informacoesAdicionais);
                }

                /*   TRANSPORTES    */
                NFNotaInfoTransporte transporte = new NFNotaInfoTransporte();
                transporte.setModalidadeFrete(NFModalidadeFrete.valueOfCodigo(Util.getNumeros(String.valueOf(notaFiscal.getNf_tipofrete()))));
                msgErroItens = "Erro ao gerar bloco de TRANSPORTES";
                if (notaFiscal.getNf_pbruto() > 0) {
                    List<NFNotaInfoVolume> volumes = new ArrayList<>();
                    NFNotaInfoVolume volume = new NFNotaInfoVolume();
                    if (notaFiscal.getNf_volume() > 0) {
                        volume.setQuantidadeVolumesTransportados(new BigInteger(String.valueOf(notaFiscal.getNf_volume())));
                    }
                    if (notaFiscal.getNf_marca().length() > 0) {
                        volume.setMarca(Util.removeAcentos(notaFiscal.getNf_marca()));
                    }
                    if (notaFiscal.getNf_especie().length() > 0) {
                        volume.setEspecieVolumesTransportados(notaFiscal.getNf_especie());
                    }
                    volume.setPesoBruto(new BigDecimal(notaFiscal.getNf_pbruto()).setScale(3, RoundingMode.HALF_UP));
                    volume.setPesoLiquido(new BigDecimal(notaFiscal.getNf_pliquido()).setScale(3, RoundingMode.HALF_UP));
                    volumes.add(volume);
                    transporte.setVolumes(volumes);
                }

                /*  TRANSPORTADOR(A)   */
                msgErroItens = "Erro ao gerar bloco de TRANSPORTADOR(A)";
                if (!notaFiscal.getNf_transportadora().equals("")) {
                    NFNotaInfoTransportador transportador = new NFNotaInfoTransportador();
                    if (notaFiscal.getTransp_fj() == 0) {
                        transportador.setCpf(Util.getNumeros(notaFiscal.getTransp_cpf()).trim());
                    } else if (notaFiscal.getTransp_fj() == 1) {
                        transportador.setCnpj(Util.getNumeros(notaFiscal.getTransp_cnpj()).trim());
                    }

                    transportador.setEnderecoComplemento(notaFiscal.getTransp_end().trim());
                    if (notaFiscal.getTransp_ie().trim().length() > 0) {
                        transportador.setInscricaoEstadual(notaFiscal.getTransp_ie().trim());
                    }
                    transportador.setNomeMunicipio(Util.removeAcentos(notaFiscal.getTransp_cidade().trim()));
                    transportador.setRazaoSocial(Util.removeAcentos(notaFiscal.getTransp_nome45().trim()));
                    transportador.setUf(DFUnidadeFederativa.valueOfCodigo(utilDAO.getCodigoUF(notaFiscal.getNf_transportadora())));
                    transporte.setTransportador(transportador);
                }

                /*  VEICULO  */
                msgErroItens = "Erro ao gerar bloco de VEÍCULOS";
                if (!notaFiscal.getNf_placa().equals("")) {
                    NFNotaInfoVeiculo veiculo = new NFNotaInfoVeiculo();
                    veiculo.setPlacaVeiculo(notaFiscal.getNf_placa().replace("-", ""));
                    String codigoAntt = notaFiscal.getTransp_codigo_antt().trim();
                    if (codigoAntt.trim().length() > 0) {
                        veiculo.setRegistroNacionalTransportadorCarga(codigoAntt);
                    }
                    veiculo.setUf(DFUnidadeFederativa.valueOfCodigo(utilDAO.getCodigoUF(notaFiscal.getNf_transportadora())));
                    transporte.setVeiculo(veiculo);
                }
                notaInfo.setTransporte(transporte);

                NFNotaInfoPagamento pagamento = new NFNotaInfoPagamento();
                List<NFNotaInfoFormaPagamento> listaFormaPagamento = new ArrayList<>();
                NFNotaInfoFormaPagamento fpag = new NFNotaInfoFormaPagamento();
                List<NFNotaInfoPagamento> listaPagamento = new ArrayList<>();

                msgErroItens = "Erro ao gerar bloco de PAGAMENTOS com finalidade tipo 1";
                if (notaFiscal.getNfefinalidade().contains("1") == false) {
                    nfNotaInfo.setIndicadorPresencaComprador(NFIndicadorPresencaComprador.OPERACAO_PRESENCIAL_FORA_ESTABELECIMENTO);
                    nfNotaInfo.setIndIntermed(NFIndicadorIntermediador.OPERACAO_SEM_INTERMEDIADOR);
                    fpag.setMeioPagamento(NFMeioPagamento.SEM_PAGAMENTO);
                    fpag.setValorPagamento(BigDecimal.ZERO);
                    listaFormaPagamento.add(fpag);
                } else {
                    msgErroItens = "Erro ao gerar bloco de PAGAMENTOS com Indicador de Forma de Pagamento de tipo 0.";
                    if (Util.getNumeros(notaFiscal.getNfeIndicadorFormaPagamento()).equals("0")) {
                        fpag.setIndicadorFormaPagamento(NFIndicadorFormaPagamento.A_VISTA);
                        fpag.setMeioPagamento(NFMeioPagamento.DINHEIRO);
                        String valor = String.valueOf(notaFiscal.getNf_totnota());
                        fpag.setValorPagamento(new BigDecimal(valor));
                        fpag.setValorPagamento(new BigDecimal(notaFiscal.getNf_totnota()).setScale(2, RoundingMode.HALF_UP));
                        listaFormaPagamento.add(fpag);
                    } else if (Util.getNumeros(notaFiscal.getNfeIndicadorFormaPagamento()).equals("1")) {
                        msgErroItens = "Erro ao gerar bloco de PAGAMENTOS com Indicador de Forma de Pagamento de tipo 1.";
                        fpag.setIndicadorFormaPagamento(NFIndicadorFormaPagamento.A_PRAZO);
                        fpag.setMeioPagamento(NFMeioPagamento.DUPLICATA_MERCANTIL);
                        String valor = String.valueOf(notaFiscal.getNf_totnota());
                        fpag.setValorPagamento(new BigDecimal(valor));
                        fpag.setValorPagamento(new BigDecimal(notaFiscal.getNf_totnota()).setScale(2, RoundingMode.HALF_UP));
                        listaFormaPagamento.add(fpag);
                    } else if (Util.getNumeros(notaFiscal.getNfeIndicadorFormaPagamento()).equals("2")) {
                        msgErroItens = "Erro ao gerar bloco de PAGAMENTOS com Indicador de Forma de Pagamento de tipo 2.";
//                        fpag.setIndicadorFormaPagamento(NFIndicadorFormaPagamento.A_VISTA);
                        fpag.setMeioPagamento(NFMeioPagamento.SEM_PAGAMENTO);
                        fpag.setValorPagamento(BigDecimal.ZERO);
                        listaFormaPagamento.add(fpag);

                    }
                }
                pagamento.setDetalhamentoFormasPagamento(listaFormaPagamento);
//                listaPagamento.add(pagamento);
                notaInfo.setPagamento(pagamento);

                /* TAG COBRANCA */
                if (Util.getNumeros(notaFiscal.getNfeIndicadorFormaPagamento()).equals("1")) {
                    NFNotaInfoCobranca cobranca = new NFNotaInfoCobranca();
                    NFNotaInfoParcela parcela;
                    NFNotaInfoFatura fatura = new NFNotaInfoFatura();
                    List<NFNotaInfoParcela> listaParcelas = new ArrayList<>();
                    List<NotaFiscalDuplicatas> listaNotaFiscalDuplucatas = nfdDAO.listar(notaFiscal.getNf_numero());
                    msgErroItens = "Erro ao gerar bloco COBRANCA, nas informações das PARCELAS.";
                    for (NotaFiscalDuplicatas nd : listaNotaFiscalDuplucatas) {
                        parcela = new NFNotaInfoParcela();
                        parcela.setDataVencimento(nd.getNfd_vencimento().toLocalDateTime().toLocalDate());
                        parcela.setNumeroParcela(nd.getNfd_parcela().trim());
                        parcela.setValorParcela(new BigDecimal(nd.getNfd_valor()).setScale(2, RoundingMode.HALF_UP));
                        listaParcelas.add(parcela);
                    }
                    msgErroItens = "Erro ao gerar bloco COBRANCA, nas informações da FATURA.";
                    fatura.setNumeroFatura(notaFiscal.getNf_documento());
                    fatura.setValorDesconto(BigDecimal.ZERO);
                    fatura.setValorLiquidoFatura(new BigDecimal(notaFiscal.getNf_totnota()).setScale(2, RoundingMode.HALF_UP));
                    fatura.setValorOriginalFatura(new BigDecimal(notaFiscal.getNf_totnota()).setScale(2, RoundingMode.HALF_UP));
                    cobranca.setFatura(fatura);
                    cobranca.setParcelas(listaParcelas);
                    notaInfo.setCobranca(cobranca);
                }

                NFNotaInfoResponsavelTecnico resptecnico = new NFNotaInfoResponsavelTecnico();
                resptecnico.setEmail("genivaldo.acom@gmail.com");
                resptecnico.setTelefone("8137214817");
                resptecnico.setContatoNome("GENIVALDO NEVES");
                resptecnico.setCnpj("10621284000197");
//		resptecnico.setHashCSRT("");
//		resptecnico.setIdCSRT("");
                notaInfo.setInformacaoResposavelTecnico(resptecnico);

                NFNotaInfoTotal total = new NFNotaInfoTotal();
                NFNotaInfoICMSTotal icmstotal = new NFNotaInfoICMSTotal();
                msgErroItens = "Erro ao gerar bloco TOTAIS";
                icmstotal.setValorTotalDosProdutosServicos(new BigDecimal(notaFiscal.getNf_vproduto()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorTotalFrete(new BigDecimal(notaFiscal.getNf_frete()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorTotalSeguro(new BigDecimal(notaFiscal.getNf_seguro()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorTotalII(new BigDecimal(BigInteger.ZERO));
                icmstotal.setBaseCalculoICMSST(new BigDecimal(notaFiscal.getNf_base_s()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorTotalICMSST(new BigDecimal(notaFiscal.getNf_vicms_s()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorICMSDesonerado(new BigDecimal(BigInteger.ZERO).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setOutrasDespesasAcessorias(new BigDecimal(notaFiscal.getNf_despesas()).setScale(2, RoundingMode.HALF_UP));
                String valorDesconto = String.valueOf(notaFiscal.getNf_desconto());
                icmstotal.setValorTotalDesconto(new BigDecimal(valorDesconto).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setBaseCalculoICMS(new BigDecimal(notaFiscal.getNf_base_n()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorTotalICMS(new BigDecimal(notaFiscal.getNf_vicms_n()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorTotalIPI(new BigDecimal(notaFiscal.getNf_vipi()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorPIS(new BigDecimal(notaFiscal.getNf_valor_pis()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorCOFINS(new BigDecimal(notaFiscal.getNf_valor_cofins()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorTotalFundoCombatePobreza(new BigDecimal(notaFiscal.getNf_vfcp()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorTotalFundoCombatePobrezaST(new BigDecimal(notaFiscal.getNf_fcpst_valor()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorTotalFundoCombatePobrezaSTRetido(new BigDecimal(notaFiscal.getNf_fcpst_valorretido()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorICMSPartilhaDestinatario(new BigDecimal(notaFiscal.getNf_vicmsufdest()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorICMSPartilhaRementente(new BigDecimal(notaFiscal.getNf_vicmsufremet()).setScale(2, RoundingMode.HALF_UP));
                icmstotal.setValorTotalIPIDevolvido(BigDecimal.ZERO);
                icmstotal.setValorTotalNFe(new BigDecimal(notaFiscal.getNf_totnota()).setScale(2, RoundingMode.HALF_UP));
                total.setIcmsTotal(icmstotal);
                notaInfo.setTotal(total);

                NFLoteEnvio lote = new NFLoteEnvio();
                List<NFNota> notas = new ArrayList<>();
                notas.add(nota);
                lote.setNotas(notas);
                lote.setIdLote(String.valueOf(getIdLote()));
                lote.setVersao("4.00");
                lote.setIndicadorProcessamento(NFLoteIndicadorProcessamento.PROCESSAMENTO_ASSINCRONO);

                retornoGerarNota.setXmlLote(lote.toString());

//                AssinaturaDigital asd = new AssinaturaDigital(new NFeConfigAcom());
//                String xmlAssinado = asd.assinarDocumento(lote.toString().trim());
                //retorno = salvarNotaGerada(notaAssinada, notaFiscal.getNf_numero(), "G");
                retornoGerarNota.setStatusGeracao(salvarNotaGerada(lote, notaInfo.getChaveAcesso(), notaFiscal.getNf_numero(), "G"));
                retornoGerarNota.setXmlAssinado(lote.toString());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            ExceptionDialog exceptionDialog = new ExceptionDialog(e);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("as");
            exceptionDialog.setHeaderText(msgErroItens);
            exceptionDialog.show();
        }
        return retornoGerarNota;
    }

    public String assinarNota(String chaveAcesso) {
        NFLoteEnvio nota = carregarXmlPastaGeradas(chaveAcesso);
        DFAssinaturaDigital asd = new DFAssinaturaDigital(new NFeConfigAcom());
        String xmlAssinado = "";
        NFLoteEnvio lote;
        try {
            xmlAssinado = asd.assinarDocumento(nota.toString().trim());
            lote = new DFPersister().read(NFLoteEnvio.class, xmlAssinado);
            salvarXmlAssinado(lote);
        } catch (Exception ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return xmlAssinado;
    }

    public ObjetoRetornoEnviarMota enviarLote(String chaveAcesso) {
        ObjetoRetornoEnviarMota statusTransmissao = new ObjetoRetornoEnviarMota();
        String xmlAssinado = assinarNota(chaveAcesso);
        try {
            final NFLoteEnvioRetorno retornoEnvioNota = new WSFacade(new NFeConfigAcom()).enviaLoteAssinado(xmlAssinado, DFModelo.NFE);
            if (retornoEnvioNota.getStatus().equals("104")) {
                statusTransmissao.setStatusTransmissao(true);
                statusTransmissao.setDataHoraRecebimento(retornoEnvioNota.getDataRecebimento().toLocalDateTime());
                statusTransmissao.setMotivo(retornoEnvioNota.getMotivo());
                statusTransmissao.setNumeroRecibo(retornoEnvioNota.getInfoRecebimento().getRecibo());
                statusTransmissao.setProtocolo(retornoEnvioNota.getProtocoloInfo().getNumeroProtocolo());
                statusTransmissao.setStatus(retornoEnvioNota.getStatus());
                statusTransmissao.setXml(retornoEnvioNota.toString());

                /* (String status, String protocolo, String protocoloDataHota, String recibo, String chave) */
                if (statusTransmissao.getStatus().equals("100") || statusTransmissao.getStatus().equals("150")) {
                    nfDao.alterarNotaEnviada("E", retornoEnvioNota.getProtocoloInfo().getNumeroProtocolo(), formatarDataEHora(retornoEnvioNota.getProtocoloInfo().getDataRecebimento().toString()), retornoEnvioNota.getInfoRecebimento().getRecibo(), true, false, chaveAcesso);
                    NFNotaProcessada notaProcessada = new NFNotaProcessada();
                    notaProcessada = new NFNotaProcessada();
                    notaProcessada.setVersao(new BigDecimal(retornoEnvioNota.getVersao()));
                    NFProtocolo protocolo = new NFProtocolo();
                    protocolo.setProtocoloInfo(retornoEnvioNota.getProtocoloInfo());
                    notaProcessada.setProtocolo(protocolo);
                    NFLoteEnvio notaLote = carregarXmlPastaGeradas(chaveAcesso);
                    notaProcessada.setNota(notaLote.getNotas().get(0));
//                    String xmlNotaProcessadaPeloSefaz = notaProcessada.toString();
                    gerarNotaXml(notaProcessada, 2, formatarData(notaProcessada.getNota().getInfo().getIdentificacao().getDataHoraEmissao().toString()));
                } else if (statusTransmissao.getStatus().equals("301") || statusTransmissao.getStatus().equals("302") || statusTransmissao.getStatus().equals("303")) {
                    nfDao.alterarNotaEnviada("D", retornoEnvioNota.getProtocoloInfo().getNumeroProtocolo(), formatarDataEHora(retornoEnvioNota.getProtocoloInfo().getDataRecebimento().toString()), retornoEnvioNota.getInfoRecebimento().getRecibo(), false, true, chaveAcesso);
                    NFNotaProcessada notaDenegada = new NFNotaProcessada();
                    notaDenegada = new NFNotaProcessada();
                    notaDenegada.setVersao(new BigDecimal(retornoEnvioNota.getVersao()));
                    NFProtocolo protocolo = new NFProtocolo();
                    protocolo.setProtocoloInfo(retornoEnvioNota.getProtocoloInfo());
                    notaDenegada.setProtocolo(protocolo);
                    NFLoteEnvio notaLote = carregarXmlPastaGeradas(chaveAcesso);
                    notaDenegada.setNota(notaLote.getNotas().get(0));
                    gerarNotaXml(notaDenegada, 7, formatarData(notaDenegada.getNota().getInfo().getIdentificacao().getDataHoraEmissao().toString()));
                } else if (Integer.parseInt(statusTransmissao.getStatus()) > 200 && Integer.parseInt(statusTransmissao.getStatus()) <= 299) {
                    nfDao.alterarNotaEnviada("R", "", "", retornoEnvioNota.getInfoRecebimento().getRecibo(), false, false, chaveAcesso);
                } else if (Integer.parseInt(statusTransmissao.getStatus()) >= 304) {
                    nfDao.alterarNotaEnviada("R", "", "", retornoEnvioNota.getInfoRecebimento().getRecibo(), false, false, chaveAcesso);
                } else if (statusTransmissao.getStatus().equals("105") || statusTransmissao.getStatus().equals("108") || statusTransmissao.getStatus().equals("109")) {
                    nfDao.alterarNotaEnviada("A", "", "", retornoEnvioNota.getInfoRecebimento().getRecibo(), false, false, chaveAcesso);
                } else {
                    statusTransmissao.setStatusTransmissao(true);
                    statusTransmissao.setDataHoraRecebimento(retornoEnvioNota.getDataRecebimento().toLocalDateTime());
                    statusTransmissao.setMotivo(retornoEnvioNota.getMotivo());
                    statusTransmissao.setNumeroRecibo(retornoEnvioNota.getInfoRecebimento().getRecibo());
                    statusTransmissao.setProtocolo(retornoEnvioNota.getProtocoloInfo().getNumeroProtocolo());
                    statusTransmissao.setStatus(retornoEnvioNota.getStatus());
                    statusTransmissao.setXml(retornoEnvioNota.toString());
                }
            } else if (retornoEnvioNota.getStatus().equals("103") || retornoEnvioNota.getStatus().equals("105") || retornoEnvioNota.getStatus().equals("108") || retornoEnvioNota.getStatus().equals("109")) {
                nfDao.alterarNotaEnviada("A", "", "", retornoEnvioNota.getInfoRecebimento().getRecibo(), false, false, chaveAcesso);
                ObjetoRetornoEnviarMota retornoTipoA;
                retornoTipoA = consultarNfe(chaveAcesso);
                System.out.println(retornoTipoA.getStatus() + " - " + retornoTipoA.getMenssagem());
                statusTransmissao = retornoTipoA;
            } else {
                statusTransmissao.setStatusTransmissao(true);
                statusTransmissao.setDataHoraRecebimento(retornoEnvioNota.getDataRecebimento().toLocalDateTime());
                statusTransmissao.setMotivo(retornoEnvioNota.getMotivo());
                statusTransmissao.setNumeroRecibo(retornoEnvioNota.getInfoRecebimento().getRecibo());
                statusTransmissao.setProtocolo(retornoEnvioNota.getProtocoloInfo().getNumeroProtocolo());
                statusTransmissao.setStatus(retornoEnvioNota.getStatus());
                statusTransmissao.setXml(retornoEnvioNota.toString());
            }
            return statusTransmissao;
        } catch (IOException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (KeyManagementException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (KeyStoreException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (CertificateException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (Exception ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        }
        return statusTransmissao;
    }

    public ObjetoRetornoEnviarMota consultarNfe(String chaveAcesso) {
        ObjetoRetornoEnviarMota statusTransmissao = new ObjetoRetornoEnviarMota();
        NFProtocoloInfo npi = new NFProtocoloInfo();
        NFProtocolo protocolo = new NFProtocolo();
        try {
            NotaFiscal nota = nfDao.getNotaFiscalChave(chaveAcesso);
            final NFLoteConsultaRetorno retornoConsulta = new WSFacade(new NFeConfigAcom()).consultaLote(String.valueOf(nota.getNfeRecibo()), DFModelo.NFE);
            if (retornoConsulta.getProtocolos() != null) {
                statusTransmissao.setStatusTransmissao(true);
                statusTransmissao.setDataHoraRecebimento(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getDataRecebimento());
                statusTransmissao.setMotivo(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getMotivo());
                statusTransmissao.setProtocolo(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getNumeroProtocolo());
                statusTransmissao.setStatus(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getStatus());
                statusTransmissao.setXml(retornoConsulta.toString());
                statusTransmissao.setNumeroRecibo(retornoConsulta.getNumeroRecibo());
//                npi.setAmbiente(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getAmbiente());
//                npi.setChave(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getChave());
//                npi.setDataRecebimento(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getDataRecebimento().toString());
//                npi.setIdentificador(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getIdentificador());
//                npi.setMotivo(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getMotivo());
//                npi.setStatus(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getStatus());
//                npi.setVersaoAplicacao(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getVersaoAplicacao());
//                npi.setNumeroProtocolo(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getNumeroProtocolo());
//                protocolo.setProtocoloInfo(npi);
            } else {
                statusTransmissao.setStatusTransmissao(true);
                statusTransmissao.setDataHoraRecebimento(retornoConsulta.getDataHoraRecebimento().toLocalDateTime());
                statusTransmissao.setMotivo(retornoConsulta.getMotivo());
                statusTransmissao.setProtocolo("");
                statusTransmissao.setStatus(retornoConsulta.getStatus());
                statusTransmissao.setXml(retornoConsulta.toString());
                statusTransmissao.setNumeroRecibo(retornoConsulta.getNumeroRecibo());
//                npi.setAmbiente(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getAmbiente());
//                npi.setChave(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getChave());
//                npi.setDataRecebimento(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getDataRecebimento().toString());
//                npi.setIdentificador(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getIdentificador());
//                npi.setMotivo(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getMotivo());
//                npi.setStatus(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getStatus());
//                npi.setVersaoAplicacao(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getVersaoAplicacao());
//                npi.setNumeroProtocolo("");
//                protocolo.setProtocoloInfo(null);
            }
            if (nota.getNfestatus().equals("E")) {
                if (statusTransmissao.getProtocolo() != null) {
                    if (statusTransmissao.getStatus().equals("100") || statusTransmissao.getStatus().equals("150")) {
                        String protocoloBanco = nfDao.getProtocolo(chaveAcesso);
                        if (!protocoloBanco.trim().equals(statusTransmissao.getProtocolo().trim())) {
                            nfDao.alterarNotaNaoRecibo("E", statusTransmissao.getProtocolo().trim(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), true, chaveAcesso);
                        }
                    } else if (statusTransmissao.getStatus().equals("301") || statusTransmissao.getStatus().equals("302") || statusTransmissao.getStatus().equals("303")) {
                        nfDao.alterarNotaNaoRecibo("D", statusTransmissao.getProtocolo(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) > 200 && Integer.parseInt(statusTransmissao.getStatus()) <= 299) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) >= 304) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (statusTransmissao.getMotivo().equals("105") || statusTransmissao.getMotivo().equals("108") || statusTransmissao.getMotivo().equals("109")) {
                        nfDao.alterarNotaConsultada("A", "", chaveAcesso);
                    }
                }
            } else if (nota.getNfestatus().equals("A")) {
                if (statusTransmissao.getProtocolo() != null) {
                    if (statusTransmissao.getStatus().equals("100") || statusTransmissao.getStatus().equals("150")) {
                        nfDao.alterarNotaNaoRecibo("E", statusTransmissao.getProtocolo(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), true, chaveAcesso);
                        NFNotaProcessada notaProcessada = new NFNotaProcessada();
                        notaProcessada.setVersao(new BigDecimal(retornoConsulta.getVersao()));
                        notaProcessada.setProtocolo(retornoConsulta.getProtocolos().get(0));
                        NFLoteEnvio notaLote = carregarXmlPastaGeradas(nota.getNfeChaveAcesso());
                        notaProcessada.setNota(notaLote.getNotas().get(0));
//                        String xmlNotaProcessadaPeloSefaz = notaProcessada.toString();
                        System.out.println(notaProcessada.toString());
                        gerarNotaXml(notaProcessada, 2, formatarData(nota.getNf_dtemissao().toString()));
                    } else if (statusTransmissao.getStatus().equals("301") || statusTransmissao.getStatus().equals("302") || statusTransmissao.getStatus().equals("303")) {
                        nfDao.alterarNotaEnviada("D", statusTransmissao.getProtocolo(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), statusTransmissao.getNumeroRecibo(), false, true, chaveAcesso);
                        NFNotaProcessada notaDenegada = new NFNotaProcessada();
                        notaDenegada.setVersao(new BigDecimal(retornoConsulta.getVersao()));
                        notaDenegada.setProtocolo(retornoConsulta.getProtocolos().get(0));
                        NFLoteEnvio notaLote = carregarXmlPastaGeradas(nota.getNfeChaveAcesso());
                        notaDenegada.setNota(notaLote.getNotas().get(0));
//                        String xmlNotaProcessadaPeloSefaz = notaProcessada.toString();
                        System.out.println(notaDenegada.toString());
                        gerarNotaXml(notaDenegada, 7, formatarData(nota.getNf_dtemissao().toString()));
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) > 200 && Integer.parseInt(statusTransmissao.getStatus()) <= 299) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) >= 304) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (statusTransmissao.getMotivo().equals("105") || statusTransmissao.getMotivo().equals("108") || statusTransmissao.getMotivo().equals("109")) {
                        nfDao.alterarNotaConsultada("A", "", chaveAcesso);
                    }
                } else {
                    if (statusTransmissao.getStatus().equals("301") || statusTransmissao.getStatus().equals("302") || statusTransmissao.getStatus().equals("303")) {
                        nfDao.alterarNotaNaoRecibo("D", statusTransmissao.getProtocolo(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) > 200 && Integer.parseInt(statusTransmissao.getStatus()) <= 299) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) >= 304) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (statusTransmissao.getMotivo().equals("105") || statusTransmissao.getMotivo().equals("108") || statusTransmissao.getMotivo().equals("109")) {
                        nfDao.alterarNotaConsultada("A", "", chaveAcesso);
                    }
                }
            } else if (nota.getNfestatus().equals("R")) {
                if (statusTransmissao.getProtocolo() != null) {
                    if (statusTransmissao.getStatus().equals("100") || statusTransmissao.getStatus().equals("150")) {
                        nfDao.alterarNotaNaoRecibo("E", statusTransmissao.getProtocolo(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), true, chaveAcesso);
                        NFNotaProcessada notaProcessada = new NFNotaProcessada();
                        notaProcessada.setVersao(new BigDecimal(retornoConsulta.getVersao()));
                        notaProcessada.setProtocolo(retornoConsulta.getProtocolos().get(0));
                        NFLoteEnvio notaLote = carregarXmlPastaGeradas(nota.getNfeChaveAcesso());
                        notaProcessada.setNota(notaLote.getNotas().get(0));
//                        String xmlNotaProcessadaPeloSefaz = notaProcessada.toString();
                        System.out.println(notaProcessada.toString());
                        gerarNotaXml(notaProcessada, 2, formatarData(nota.getNf_dtemissao().toString()));
                    } else if (statusTransmissao.getStatus().equals("301") || statusTransmissao.getStatus().equals("302") || statusTransmissao.getStatus().equals("303")) {
                        nfDao.alterarNotaNaoRecibo("D", statusTransmissao.getProtocolo(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) > 200 && Integer.parseInt(statusTransmissao.getStatus()) <= 299) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) >= 304) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (statusTransmissao.getMotivo().equals("105") || statusTransmissao.getMotivo().equals("108") || statusTransmissao.getMotivo().equals("109")) {
                        nfDao.alterarNotaConsultada("A", "", chaveAcesso);
                    }
                }
            } else {
                if (statusTransmissao.getStatus().equals("301") || statusTransmissao.getStatus().equals("302") || statusTransmissao.getStatus().equals("303")) {
                    nfDao.alterarNotaNaoRecibo("D", statusTransmissao.getProtocolo(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), false, chaveAcesso);
                } else if (Integer.parseInt(statusTransmissao.getStatus()) > 200 && Integer.parseInt(statusTransmissao.getStatus()) <= 299) {
                    nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                } else if (Integer.parseInt(statusTransmissao.getStatus()) >= 304) {
                    nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                } else if (statusTransmissao.getMotivo().equals("105") || statusTransmissao.getMotivo().equals("108") || statusTransmissao.getMotivo().equals("109")) {
                    nfDao.alterarNotaConsultada("A", "", chaveAcesso);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (KeyManagementException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (KeyStoreException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (CertificateException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (Exception ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        }
        return statusTransmissao;
    }

    public ObjetoRetornoEnviarMota consultarNfePelaChave(String chaveAcesso) {
        ObjetoRetornoEnviarMota statusTransmissao = new ObjetoRetornoEnviarMota();
        NFProtocoloInfo npi = new NFProtocoloInfo();
        NFProtocolo protocolo = new NFProtocolo();
        try {
            NotaFiscal nota = nfDao.getNotaFiscalChave(chaveAcesso);
            NFLoteEnvio notaL = carregarXmlPastaGeradas(nota.getNfeChaveAcesso());
//            if(notaL.getNotas().get(0).getAssinatura() == null){
//                System.out.println("nulo");
//                assinarNota(chaveAcesso);
//            }else{
            final NFNotaConsultaRetorno retornoConsulta = new WSFacade(new NFeConfigAcom()).consultaNota(chaveAcesso);
            if (retornoConsulta.getProtocolo() != null) {
                statusTransmissao.setStatusTransmissao(true);
                statusTransmissao.setDataHoraRecebimento(retornoConsulta.getProtocolo().getProtocoloInfo().getDataRecebimento());
                statusTransmissao.setMotivo(retornoConsulta.getProtocolo().getProtocoloInfo().getMotivo());
                statusTransmissao.setProtocolo(retornoConsulta.getProtocolo().getProtocoloInfo().getNumeroProtocolo());
                statusTransmissao.setStatus(retornoConsulta.getProtocolo().getProtocoloInfo().getStatus());
                statusTransmissao.setXml(retornoConsulta.toString());
//                npi.setAmbiente(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getAmbiente());
//                npi.setChave(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getChave());
//                npi.setDataRecebimento(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getDataRecebimento().toString());
//                npi.setIdentificador(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getIdentificador());
//                npi.setMotivo(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getMotivo());
//                npi.setStatus(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getStatus());
//                npi.setVersaoAplicacao(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getVersaoAplicacao());
//                npi.setNumeroProtocolo(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getNumeroProtocolo());
//                protocolo.setProtocoloInfo(npi);
            } else {
                statusTransmissao.setStatusTransmissao(true);
                statusTransmissao.setDataHoraRecebimento(retornoConsulta.getDataHoraRecibo());
                statusTransmissao.setMotivo(retornoConsulta.getMotivo());
                statusTransmissao.setProtocolo("");
                statusTransmissao.setStatus(retornoConsulta.getStatus());
                statusTransmissao.setXml(retornoConsulta.toString());
//                npi.setAmbiente(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getAmbiente());
//                npi.setChave(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getChave());
//                npi.setDataRecebimento(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getDataRecebimento().toString());
//                npi.setIdentificador(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getIdentificador());
//                npi.setMotivo(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getMotivo());
//                npi.setStatus(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getStatus());
//                npi.setVersaoAplicacao(retornoConsulta.getProtocolos().get(0).getProtocoloInfo().getVersaoAplicacao());
//                npi.setNumeroProtocolo("");
//                protocolo.setProtocoloInfo(null);
            }
            if (nota.getNfestatus().equals("E")) {
                if (statusTransmissao.getProtocolo() != null) {
                    if (statusTransmissao.getStatus().equals("100") || statusTransmissao.getStatus().equals("150")) {
                        String protocoloBanco = nfDao.getProtocolo(chaveAcesso);
                        if (!protocoloBanco.trim().equals(statusTransmissao.getProtocolo().trim())) {
                            nfDao.alterarNotaEnviada("E", statusTransmissao.getProtocolo().trim(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), statusTransmissao.getNumeroRecibo(), true, false, chaveAcesso);
                        }
                    } else if (statusTransmissao.getStatus().equals("301") || statusTransmissao.getStatus().equals("302") || statusTransmissao.getStatus().equals("303")) {
                        nfDao.alterarNotaEnviada("D", statusTransmissao.getProtocolo().trim(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), statusTransmissao.getNumeroRecibo(), false, true, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) > 200 && Integer.parseInt(statusTransmissao.getStatus()) <= 299) {
                        nfDao.alterarNotaEnviada("R", "", "", "", false, false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) >= 304) {
                        nfDao.alterarNotaEnviada("R", "", "", "", false, false, chaveAcesso);
                    } else if (statusTransmissao.getMotivo().equals("105") || statusTransmissao.getMotivo().equals("108") || statusTransmissao.getMotivo().equals("109")) {
                        nfDao.alterarNotaEnviada("A", "", "", "", false, false, chaveAcesso);
                    }
                }
            } else if (nota.getNfestatus().equals("A")) {
                if (statusTransmissao.getProtocolo() != null) {
                    if (statusTransmissao.getStatus().equals("100") || statusTransmissao.getStatus().equals("150")) {
                        nfDao.alterarNotaNaoRecibo("E", statusTransmissao.getProtocolo(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), true, chaveAcesso);
                        NFNotaProcessada notaProcessada = new NFNotaProcessada();
                        notaProcessada.setVersao(new BigDecimal(retornoConsulta.getVersao()));
                        notaProcessada.setProtocolo(retornoConsulta.getProtocolo());
                        NFLoteEnvio notaLote = carregarXmlPastaGeradas(nota.getNfeChaveAcesso());
                        notaProcessada.setNota(notaLote.getNotas().get(0));
//                        String xmlNotaProcessadaPeloSefaz = notaProcessada.toString();
                        System.out.println(notaProcessada.toString());
                        gerarNotaXml(notaProcessada, 2, formatarData(nota.getNf_dtemissao().toString()));
                    } else if (statusTransmissao.getStatus().equals("301") || statusTransmissao.getStatus().equals("302") || statusTransmissao.getStatus().equals("303")) {
                        nfDao.alterarNotaNaoRecibo("D", statusTransmissao.getProtocolo(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) > 200 && Integer.parseInt(statusTransmissao.getStatus()) <= 299) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) >= 304) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (statusTransmissao.getMotivo().equals("105") || statusTransmissao.getMotivo().equals("108") || statusTransmissao.getMotivo().equals("109")) {
                        nfDao.alterarNotaConsultada("A", "", chaveAcesso);
                    }
                }
            } else if (nota.getNfestatus().equals("R")) {
                if (statusTransmissao.getProtocolo() != null) {
                    if (statusTransmissao.getStatus().equals("100") || statusTransmissao.getStatus().equals("150")) {
                        nfDao.alterarNotaNaoRecibo("E", statusTransmissao.getProtocolo(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), true, chaveAcesso);
                        NFNotaProcessada notaProcessada = new NFNotaProcessada();
                        notaProcessada.setVersao(new BigDecimal(retornoConsulta.getVersao()));
                        notaProcessada.setProtocolo(retornoConsulta.getProtocolo());
                        NFLoteEnvio notaLote = carregarXmlPastaGeradas(nota.getNfeChaveAcesso());
                        notaProcessada.setNota(notaLote.getNotas().get(0));
//                        String xmlNotaProcessadaPeloSefaz = notaProcessada.toString();
                        System.out.println(notaProcessada.toString());
                        gerarNotaXml(notaProcessada, 2, formatarData(nota.getNf_dtemissao().toString()));
                    } else if (statusTransmissao.getStatus().equals("301") || statusTransmissao.getStatus().equals("302") || statusTransmissao.getStatus().equals("303")) {
                        nfDao.alterarNotaNaoRecibo("D", statusTransmissao.getProtocolo(), formatarDataEHora(statusTransmissao.getDataHoraRecebimento().toString()), false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) > 200 && Integer.parseInt(statusTransmissao.getStatus()) <= 299) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (Integer.parseInt(statusTransmissao.getStatus()) >= 304) {
                        nfDao.alterarNotaNaoRecibo("R", "", "", false, chaveAcesso);
                    } else if (statusTransmissao.getMotivo().equals("105") || statusTransmissao.getMotivo().equals("108") || statusTransmissao.getMotivo().equals("109")) {
                        nfDao.alterarNotaConsultada("A", "", chaveAcesso);
                    }
                }
            }
//            }
        } catch (IOException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (KeyManagementException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (KeyStoreException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (CertificateException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (Exception ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        }
        return statusTransmissao;
    }

    public ObjetoRetornoCancelamento cancelarNfe1() {
        ObjetoRetornoCancelamento retorno = null;
        NFEnviaEventoCancelamento lote;
        NFEventoCancelamento evento;
        NFInfoEventoCancelamento infoCancelamento = new NFInfoEventoCancelamento();
        infoCancelamento.setAmbiente(new NFeConfigAcom().getAmbiente());

        return retorno;
    }

    public ObjetoRetornoCancelamento cancelarNfe(String chaveAcesso, String protocolo, String motivoJustificativa, String data) {
        ObjetoRetornoCancelamento statusTransmissao = new ObjetoRetornoCancelamento();
        NFProtocoloInfo npi = new NFProtocoloInfo();
        NFProtocolo protocoloca = new NFProtocolo();
        final NFEnviaEventoRetorno retornoCancelamento;
        try {
            NFNotaProcessada notaProcessada = new NFNotaProcessada();
            retornoCancelamento = new WSFacade(new NFeConfigAcom()).cancelaNota(chaveAcesso, protocolo, motivoJustificativa);
            if (retornoCancelamento.getCodigoStatusReposta() == 128) {

                for (NFEventoRetorno ev : retornoCancelamento.getEventoRetorno()) {
                    statusTransmissao.setStatusTransmissao(true);
                    statusTransmissao.setStatus(String.valueOf(ev.getInfoEventoRetorno().getCodigoStatus()));
                    statusTransmissao.setMotivo(ev.getInfoEventoRetorno().getMotivo());
                    statusTransmissao.setProtocolo(ev.getInfoEventoRetorno().getNumeroProtocolo());
                    statusTransmissao.setMotivoJustificativa(motivoJustificativa);
                    statusTransmissao.setXml(retornoCancelamento.toString());

                    npi.setNumeroProtocolo(ev.getInfoEventoRetorno().getNumeroProtocolo());
                    protocoloca.setProtocoloInfo(npi);
                }
                if (statusTransmissao.getStatus().equals("101") || statusTransmissao.getStatus().equals("135")) {
                    nfDao.alterarNotaCancelada(true, "C", statusTransmissao.getProtocolo(), statusTransmissao.getMotivoJustificativa(), chaveAcesso);
                    notaProcessada = carregarXmlNFNotaProcessada(chaveAcesso, data, "processadas", "_nfe");
                    gerarNotaXml(notaProcessada, 3, formatarData(notaProcessada.getNota().getInfo().getIdentificacao().getDataHoraEmissao().toString()));
                    gerarNotaXml(retornoCancelamento, 4, formatarData(retornoCancelamento.getEventoRetorno().get(0).getInfoEventoRetorno().getDataHoraRegistro().toString()));
                }
            } else {
                statusTransmissao.setStatusTransmissao(true);
                statusTransmissao.setStatus(String.valueOf(retornoCancelamento.getCodigoStatusReposta()));
                statusTransmissao.setMotivo(retornoCancelamento.getMotivo());
                statusTransmissao.setNumeroLote(retornoCancelamento.getIdLote());
                statusTransmissao.setXml(retornoCancelamento.toString());
            }
        } catch (IOException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (KeyManagementException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (KeyStoreException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (CertificateException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        } catch (Exception ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            statusTransmissao.setStatusTransmissao(false);
            ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("Erro: ");
            exceptionDialog.setHeaderText("Erro ao enviar NF-e");
            exceptionDialog.show();
        }

        return statusTransmissao;
    }

    public NFLoteEnvio carregarXmlPastaGeradas(String chaveAcesso) {
        NFLoteEnvio notaObjeto = null;
        String diretorioArquivos = "";
        if (File.separator.equals("\\")) {
            diretorioArquivos = pfDao.getDiretorioXml();
        } else {
            diretorioArquivos = "/home/allison/NetBeansProjects/GerenciadorNfe";
        }
        try {
            File arquivo = new File(diretorioArquivos + File.separator + "documentofiscal" + File.separator + "geradas" + File.separator + chaveAcesso + ".xml");
            notaObjeto = new DFPersister().read(NFLoteEnvio.class, arquivo);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            ExceptionDialog exceptionDialog = new ExceptionDialog(e);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("as");
            exceptionDialog.setHeaderText("Erro ao carregar NF-e");
            exceptionDialog.show();
        }
        return notaObjeto;
    }

    public NFNota carregarXmlNFNota(String chaveAcesso) {
        NFNota notaObjeto = null;
        try {
            File arquivo = new File(pf.getNfeDiretorioXML() + File.separator + chaveAcesso + ".xml");
            notaObjeto = new DFPersister().read(NFNota.class, arquivo);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            ExceptionDialog exceptionDialog = new ExceptionDialog(e);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("as");
            exceptionDialog.setHeaderText("Erro ao carregar NF-e");
            exceptionDialog.show();
        }
        return notaObjeto;
    }

    public File carregarXmlEventos(String chaveAcesso, String data, String pasta, String subpasta, String sufixo) {
        NFEnviaEventoCartaCorrecao nfccObjeto = null;
        String diretorioArquivos = "";
        String array[] = data.split("/");
        String mes = array[1];//dd/mm/yyyy
        String ano = array[2];
        File arquivo = null;
        try {
            if (File.separator.equals("\\")) {
                diretorioArquivos = pfDao.getDiretorioXml() + File.separator + "documentofiscal" + File.separator + ano + File.separator + mes + File.separator + pasta + File.separator + subpasta;
            } else {
                diretorioArquivos = "/home/allison/NetBeansProjects/GerenciadorNfe" + File.separator + "documentofiscal" + File.separator + ano + File.separator + mes + File.separator + pasta;
            }

            arquivo = new File(diretorioArquivos + File.separator + chaveAcesso + sufixo + ".xml");

        } catch (Exception e) {
            System.out.println(e.getMessage());
            ExceptionDialog exceptionDialog = new ExceptionDialog(e);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("as");
            exceptionDialog.setHeaderText("Erro ao carregar NF-e");
            exceptionDialog.show();
        }
        return arquivo;
    }

    public NFNotaProcessada carregarXmlNFNotaProcessada(String chaveAcesso, String data, String pasta, String sufixo) {
        NFNotaProcessada notaObjeto = null;
        String diretorioArquivos = "";
        String array[] = data.split("/");
        String mes = array[1];//dd//mm/yyy
        String ano = array[2];
        try {
            if (File.separator.equals("\\")) {
                diretorioArquivos = pfDao.getDiretorioXml() + File.separator + "documentofiscal" + File.separator + ano + File.separator + mes + File.separator + pasta;
            } else {
                diretorioArquivos = "/home/allison/NetBeansProjects/GerenciadorNfe" + File.separator + "documentofiscal" + File.separator + ano + File.separator + mes + File.separator + pasta;
            }

            File arquivo = new File(diretorioArquivos + File.separator + chaveAcesso + sufixo + ".xml");
            notaObjeto = new DFPersister().read(NFNotaProcessada.class, arquivo);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            ExceptionDialog exceptionDialog = new ExceptionDialog(e);
            exceptionDialog.setTitle("Erro");
            exceptionDialog.setContentText("as");
            exceptionDialog.setHeaderText("Erro ao carregar NF-e");
            exceptionDialog.show();
        }
        return notaObjeto;
    }

    private int getIdLote() {
        int numeroLote = Integer.parseInt(nflDao.getNumeroLote());
        int retorno = numeroLote + 1;
        return retorno;
    }

    //    private boolean salvarNotaGerada(String notaObj, String chave, String numeroNota, String status) {
    private boolean salvarNotaGerada(Object notaObj, String chave, String numeroNota, String status) {
        boolean retorno = false;
        NFLoteEnvio nota = (NFLoteEnvio) notaObj;
        boolean retornoAlteracao = nfDao.alterarNotaGerada(numeroNota, chave, status);
        if (retornoAlteracao == true) {
//            gerarNotaXml(nota, 0);
//            gerarNotaXml(notaObj, chave);
            String data = formatarData(nota.getNotas().get(0).getInfo().getIdentificacao().getDataHoraEmissao().toString());
            gerarNotaXml(nota, 0, data);
            retorno = true;
        } else {
            retorno = false;
        }
        return retorno;
    }

    private void salvarXmlAssinado(Object notaObj) {
        NFLoteEnvio nota = (NFLoteEnvio) notaObj;
        String data = formatarData(nota.getNotas().get(0).getInfo().getIdentificacao().getDataHoraEmissao().toString());
        gerarNotaXml(nota, 0, data);
    }

    //    private void gerarNotaXml(String xmlNota, String chave) {
//        try {
//            String conteudo = xmlNota;
//            File arquivo = new File(pf.getNfeDiretorioXML() + File.separator + chave + ".xml");
//            if (arquivo.exists()) {
//                arquivo.delete();
//            }
//            arquivo.createNewFile();
//            // Prepara para escrever no arquivo
//            FileWriter fw = new FileWriter(arquivo.getAbsoluteFile());
//            System.out.println("getAbsoluteFile.....: " + arquivo.getAbsoluteFile());
//            BufferedWriter bw = new BufferedWriter(fw);
//            // Escreve e fecha arquivo
//            bw.write(conteudo);
//            bw.close();
//        } catch (IOException e) {
//            System.out.println("erro ao gerar o arquivo.....: " + e.getMessage());
//        }
//    }
    public String formatarDataEHora(String dataSefaz) {
        String retorno = "";
        try {
            String dataWsSefaz = dataSefaz;
            String data = dataWsSefaz.substring(0, dataWsSefaz.indexOf("T"));
            String hora = dataWsSefaz.substring(11, dataWsSefaz.length());
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formato.parse(data);
            formato.applyPattern("dd/MM/yyyy");
            String dataFormatada = formato.format(date);
            retorno = dataFormatada + " as " + hora;
        } catch (ParseException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public String formatarData(String dataSefaz) {
        String retorno = "";
        String data = "";
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String dataWsSefaz = dataSefaz;
            if (dataWsSefaz.contains("T")) {
                data = dataWsSefaz.substring(0, dataWsSefaz.indexOf("T"));
            }
            data = dataWsSefaz;
            Date date = formato.parse(data);
            formato.applyPattern("dd/MM/yyyy");
            String dataFormatada = formato.format(date);
            retorno = dataFormatada;
        } catch (ParseException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public ObjetoRetornoTransmissao inutilizarNota(Inutilizacao inutilizacao) {
        ObjetoRetornoTransmissao objetoretorno = new ObjetoRetornoTransmissao();
        Inutilizacao inutilizacaoEnviada;
        try {
            int ano = Integer.parseInt(inutilizacao.getInu_ano());
            String cnpjEmitente = fDAO.getCnpj(inutilizacao.getInu_filial());
            String serie = inutilizacao.getInu_serie();
            String numeroInicial = String.valueOf(inutilizacao.getInu_numeronotainicial());
            String numeroFinal = String.valueOf(inutilizacao.getInu_numeronotafinal());
            String justificativa = inutilizacao.getInu_justificativa();

            final NFRetornoEventoInutilizacao retornoEventoInutilizacao = new WSFacade((new NFeConfigAcom())).inutilizaNota(ano, cnpjEmitente, serie, numeroInicial, numeroFinal, justificativa, DFModelo.NFE);
            final NFRetornoEventoInutilizacaoDados retornoEventoInutilizacaoDados = retornoEventoInutilizacao.getDados();

            /* --- VER O QUE FAZER COM ESSE IF LOGO ABAIXO DEPOIS --- */
            if (retornoEventoInutilizacaoDados.getStatus().equals("102")) {
                inutilizacaoEnviada = inutilizacao;
                inutilizacaoEnviada.setInu_numeroprotocolo(retornoEventoInutilizacao.getDados().getNumeroProtocolo());
                inutilizacaoEnviada.setInu_datahora(formatarDataEHora(retornoEventoInutilizacao.getDados().getDatahoraRecebimento().toString()));
                inutilizacaoEnviada.setInu_enviada("E");
                iDAO.alterar(inutilizacaoEnviada);

                objetoretorno.setStatusTransmissao(true);
                objetoretorno.setXmlRetorno(retornoEventoInutilizacao.toString());
                objetoretorno.setCodStatus(retornoEventoInutilizacao.getDados().getStatus());
                objetoretorno.setMotivo(retornoEventoInutilizacao.getDados().getMotivo());
                objetoretorno.setData(retornoEventoInutilizacao.getDados().getDatahoraRecebimento().toString());
                objetoretorno.setProtocolo(retornoEventoInutilizacao.getDados().getNumeroProtocolo());

                gerarNotaXml(retornoEventoInutilizacao, 6, formatarData(retornoEventoInutilizacao.getDados().getDatahoraRecebimento().toString()));

//                throw new Exception(String.format("NF[%s,%s] - Nao autorizado a inutilizacao da NF na sefaz: %s: %s", cnpjEmitente, numeroInicial, numeroFinal, retornoEventoInutilizacaoDados.getStatus(), retornoEventoInutilizacaoDados.getMotivo()));
            } else if (retornoEventoInutilizacaoDados.getStatus().equals("563")) {
                inutilizacaoEnviada = inutilizacao;
                inutilizacaoEnviada.setInu_numeroprotocolo(retornoEventoInutilizacao.getDados().getNumeroProtocolo());
                inutilizacaoEnviada.setInu_datahora(Util.formatarData(retornoEventoInutilizacao.getDados().getDatahoraRecebimento().toString()));
                inutilizacaoEnviada.setInu_enviada("R");
                iDAO.alterar(inutilizacaoEnviada);

                objetoretorno.setStatusTransmissao(true);
                objetoretorno.setXmlRetorno(retornoEventoInutilizacao.toString());
                objetoretorno.setCodStatus(retornoEventoInutilizacao.getDados().getStatus());
                objetoretorno.setMotivo(retornoEventoInutilizacao.getDados().getMotivo());
                objetoretorno.setData(retornoEventoInutilizacao.getDados().getDatahoraRecebimento().toString());
                objetoretorno.setProtocolo(retornoEventoInutilizacao.getDados().getNumeroProtocolo());
            } else {
                objetoretorno.setStatusTransmissao(false);
                objetoretorno.setXmlRetorno(retornoEventoInutilizacao.toString());
                objetoretorno.setCodStatus(retornoEventoInutilizacao.getDados().getStatus());
                objetoretorno.setMotivo(retornoEventoInutilizacao.getDados().getMotivo());
                objetoretorno.setData(retornoEventoInutilizacao.getDados().getDatahoraRecebimento().toString());
                objetoretorno.setProtocolo(retornoEventoInutilizacao.getDados().getNumeroProtocolo());
            }
            // pega as informações da nota e retornar para a exibiçao da tela
            return objetoretorno;
        } catch (IOException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            objetoretorno.setStatusTransmissao(false);
            return objetoretorno;
        } catch (KeyManagementException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            objetoretorno.setStatusTransmissao(false);
            return objetoretorno;
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            objetoretorno.setStatusTransmissao(false);
            return objetoretorno;
        } catch (KeyStoreException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            objetoretorno.setStatusTransmissao(false);
            return objetoretorno;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            objetoretorno.setStatusTransmissao(false);
            return objetoretorno;
        } catch (CertificateException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            objetoretorno.setStatusTransmissao(false);
            return objetoretorno;
        } catch (Exception ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            objetoretorno.setStatusTransmissao(false);
            return objetoretorno;
        }
    }

    public ObjetoRetornoCartaCorrecao cartaCorrecao(NotaFiscalCartaCorrecao nfcc) {
        ObjetoRetornoCartaCorrecao retornoCartaCorrecao = new ObjetoRetornoCartaCorrecao();
        try {
            NFProtocoloEventoCartaCorrecao cartaCorrecao = new WSFacade(new NFeConfigAcom()).corrigeNotaAssinadaProtocolo(nfcc.getNfcc_chavenfe(), nfcc.getNfcc_correcao(), Integer.parseInt(nfcc.getNfcc_item()));
            if (cartaCorrecao.getEventoRetorno().getInfoEventoRetorno().getCodigoStatus() == 128) {
                retornoCartaCorrecao.setStatusTransmissao(true);
                retornoCartaCorrecao.setStatus(String.valueOf(cartaCorrecao.getEventoRetorno().getInfoEventoRetorno().getCodigoStatus()));
                retornoCartaCorrecao.setMotivo(cartaCorrecao.getEventoRetorno().getInfoEventoRetorno().getMotivo());
                retornoCartaCorrecao.setProtocolo(cartaCorrecao.getEventoRetorno().getInfoEventoRetorno().getNumeroProtocolo());
                retornoCartaCorrecao.setXml(cartaCorrecao.toString());
            } else {
                retornoCartaCorrecao.setStatusTransmissao(true);
                retornoCartaCorrecao.setStatus(String.valueOf(cartaCorrecao.getEventoRetorno().getInfoEventoRetorno().getCodigoStatus()));
                retornoCartaCorrecao.setMotivo(cartaCorrecao.getEventoRetorno().getInfoEventoRetorno().getMotivo());
                retornoCartaCorrecao.setNumeroLote(retornoCartaCorrecao.getNumeroLote());
                retornoCartaCorrecao.setXml(cartaCorrecao.toString());
            }

            if (retornoCartaCorrecao.getStatus().equals("135")) {
                nfccDAO.alterarDepoisEnvio("S", nfcc.getNfcc_chavenfe(), nfcc.getNfcc_item(), cartaCorrecao);
                gerarNotaXml(cartaCorrecao, 5, formatarData(cartaCorrecao.getEventoRetorno().getInfoEventoRetorno().getDataHoraRegistro().toString()));
            } else {
                nfccDAO.alterarDepoisEnvio("R", nfcc.getNfcc_chavenfe(), nfcc.getNfcc_item(), cartaCorrecao);
            }
        } catch (IOException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyManagementException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (KeyStoreException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (CertificateException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retornoCartaCorrecao;
    }

    public void imprimirDanfe(NotaFiscal nf, String status) {
        try {

            String diretorioLogo = "";
            String pasta = "";
            String sufixo = "";
            if (File.separator.equals("\\")) {
                diretorioLogo = pfDao.getDiretorioLogomarcaDanfe();
            } else {
                diretorioLogo = "";
            }
            if (nf.isNfeEnviada() == true) {
                pasta = "processadas";
                sufixo = "_nfe";
            }

            if (nf.isNfeCancelada() == true) {
                pasta = "canceladas";
                sufixo = "_canc";
            }

            if (nf.isNf_denegada() == true) {
                pasta = "denegadas";
                sufixo = "_den";
            }

            NFNotaProcessada np = carregarXmlNFNotaProcessada(nf.getNfeChaveAcesso().trim(), formatarData(nf.getNf_data().toString()), pasta, sufixo);
            NFDanfeReport danfe = new NFDanfeReport(np);
            JasperPrint print = null;
            if (diretorioLogo.equals("")) {
                print = danfe.createJasperPrintNFe(null, status);
            } else {
                print = danfe.createJasperPrintNFe(Util.getBytesLogo(diretorioLogo), status);
            }
            /*Código usado para abrir direto a caixa de impressao do sistema*/
//            JasperPrintManager.printReport(print, true);
//            byte[] byteArray = JasperExportManager.exportReportToPdf(print);

            /*Código usado para salvar o documento no computador sem abrir caixa de diálogo*/
//	    JasperExportManager.exportReportToPdfFile(print, pfDao.getDiretorioPdf() + File.separator + nf.getNfeChaveAcesso() + "_nfe" + ".pdf");

            /*Código que abre uma caixa de vizualizaçao e tem a opção de imprimir e salvar o documento*/
            JasperViewer viewer = new JasperViewer(print, false);
            viewer.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
            viewer.setAlwaysOnTop(true);
            viewer.setTitle("Vizualização e impressão do Danfe");
            viewer.setVisible(true);

        } catch (JRException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void imprimirCartaCorrecao(String chaveNfe, String item) {
        try {
            System.out.println(chaveNfe + " - " + item);
            NotaFiscalCartaCorrecaoImpressao nfcci = nfcciDAO.listar(chaveNfe, item);
            ArrayList<NotaFiscalCartaCorrecaoImpressao> lista = new ArrayList<>();
            lista.add(nfcci);
            byte[] logo;
            System.out.println("Classe--->:  " + this.getClass());

            String diretorioLogo = pfDao.getDiretorioLogomarcaDanfe();
            if (diretorioLogo.equals("")) {
                logo = null;
            } else {
                logo = Util.getBytesLogo(diretorioLogo);
            }
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("LOGO_EMPRESA", (logo == null ? null : new ByteArrayInputStream(logo)));
            JRBeanCollectionDataSource bean = new JRBeanCollectionDataSource(lista);
            InputStream in = null;
            JasperPrint print;
            in = this.getClass().getResourceAsStream("/danfe/CCE.jasper");
            print = JasperFillManager.fillReport(in, parametros, bean);
            JasperViewer viewer = new JasperViewer(print, false);
            viewer.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
            viewer.setAlwaysOnTop(true);
            viewer.setVisible(true);
        } catch (JRException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void gerarPdfCC(String chaveNfe, String item, String diretorio) {
        try {
            String diretorioPdf = pfDao.getDiretorioPdf();
            System.out.println(chaveNfe + " - " + item);
            NotaFiscalCartaCorrecaoImpressao nfcci = nfcciDAO.listar(chaveNfe, item);
            ArrayList<NotaFiscalCartaCorrecaoImpressao> lista = new ArrayList<>();
            lista.add(nfcci);
            byte[] logo;
            System.out.println("Classe--->:  " + this.getClass());

            String diretorioLogo = pfDao.getDiretorioLogomarcaDanfe();
            if (diretorioLogo.equals("")) {
                logo = null;
            } else {
                logo = Util.getBytesLogo(diretorioLogo);
            }
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("LOGO_EMPRESA", (logo == null ? null : new ByteArrayInputStream(logo)));
            JRBeanCollectionDataSource bean = new JRBeanCollectionDataSource(lista);
            InputStream in = null;
            OutputStream saida = new FileOutputStream(diretorioPdf + File.separator + chaveNfe + "_nfe.pdf");
            JasperPrint print;
            in = this.getClass().getResourceAsStream("/danfe/CCE.jasper");
            print = JasperFillManager.fillReport(in, parametros, bean);
            JasperExportManager.exportReportToPdfStream(print, saida);
            saida.close();
        } catch (JRException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void vizualizarImpressao(NotaFiscal nf, String status) {
        try {
            String diretorioLogo = "";
            String pasta = "";
            String sufixo = "";
            if (File.separator.equals("\\")) {
                diretorioLogo = pfDao.getDiretorioLogomarcaDanfe();
            } else {
                diretorioLogo = "";
            }

            NFLoteEnvio np = carregarXmlPastaGeradas(nf.getNfeChaveAcesso().trim());
            NFDanfeReport danfe = new NFDanfeReport(np);
            JasperPrint print = null;
            if (diretorioLogo.equals("")) {
                print = danfe.createJasperPrintNFe(null);
            } else {
                print = danfe.createJasperPrintNFeLote(Util.getBytesLogo(diretorioLogo), status);
            }
            MyJasperViewer viewer = new MyJasperViewer(print, false);
            viewer.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
            viewer.setAlwaysOnTop(true);
            viewer.setTitle("Vizualização do Danfe");
            viewer.setVisible(true);
        } catch (JRException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isValidarCodeEAN(String barCode) {
        String barCode13Digitos = barCode.substring(1, barCode.length());
        int digit;
        int calculated;
        String ean;
        String checkSum = "131313131313";
        int sum = 0;

        if (barCode13Digitos.length() == 8 || barCode13Digitos.length() == 13) {
            digit = Integer.parseInt("" + barCode13Digitos.charAt(barCode13Digitos.length() - 1));
            ean = barCode13Digitos.substring(0, barCode13Digitos.length() - 1);
            for (int i = 0; i <= ean.length() - 1; i++) {
                sum += (Integer.parseInt("" + ean.charAt(i))) * (Integer.parseInt("" + checkSum.charAt(i)));
            }
            calculated = 10 - (sum % 10);
            return (digit == calculated);
        } else {
            return false;
        }
    }

    public void gerarNotaXml(Object notaObj, int tipoProcessamento, String data) {
        String diretorioArquivos = "";
        try {
            if (File.separator.equals("\\")) {
                diretorioArquivos = pfDao.getDiretorioXml();
            } else {
                diretorioArquivos = "/home/allison/NetBeansProjects/GerenciadorNfe";
            }
            System.out.println("Diretório de Arquivos---> " + diretorioArquivos);
//            String diretorioArquivos = "/home/allison/NetBeansProjects/GerenciadorNfe";
            String local = "";
            String caminho_anomes = "";
            String path = "";
            FileWriter arq = null;
            PrintWriter gravarArq = null;
	    /*
	====== TIPOS DE OBJETOS DE CADA TIPO DE NOTA ======
        - GERADAS.......: NFLoteEnvio  -----------------> 0
        - ENVIADAS......: NFLoteEnvio  -----------------> 1
        - PROCESSADAS...: NFProcessada -----------------> 2
        - PROCESSADAS...: NFProcessada -----------------> 3
        - CANCELADAS....: NFEnviaEventoRetorno----------> 4
        - CC............: NFEnviaEventoRetorno----------> 5
        - INUTILIZADAS..: NFRetornoEventoInutilizacao---> 6
	- DENEGADAS.....: NFProcessada------------------> 7
	     */
            switch (tipoProcessamento) {
                case 0:
                    local = "geradas";
                    NFLoteEnvio notaGerada = (NFLoteEnvio) notaObj;
                    caminho_anomes = pastaMesAnoAtual(diretorioArquivos + File.separator + "documentofiscal" + File.separator, data);
                    path = diretorioArquivos + File.separator + "documentofiscal" + File.separator + local + File.separator;
                    System.out.println("DIRETORIO---> " + path + notaGerada.getNotas().get(0).getInfo().getChaveAcesso() + ".xml");
                    arq = new FileWriter(path + notaGerada.getNotas().get(0).getInfo().getChaveAcesso() + ".xml");
                    gravarArq = new PrintWriter(arq);
//                    gravarArq.printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    gravarArq.printf("%s", Util.removeAcentos(notaGerada.toString()));
                    arq.close();
                    break;
                case 1:
                    local = "enviadas";
                    NFLoteEnvio notaEnviada = (NFLoteEnvio) notaObj;
                    caminho_anomes = pastaMesAnoAtual(diretorioArquivos + File.separator + "documentofiscal" + File.separator, data);
                    path = caminho_anomes + File.separator + local + File.separator;
                    System.out.println(path + notaEnviada.getNotas().get(0).getInfo().getChaveAcesso() + "_nfe.xml");
                    arq = new FileWriter(path + notaEnviada.getNotas().get(0).getInfo().getChaveAcesso() + "_nfe.xml");
                    gravarArq = new PrintWriter(arq);
//                    gravarArq.printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    gravarArq.printf("%s", Util.removeAcentos(notaEnviada.toString()));
                    arq.close();
                    break;
                case 2:
                    local = "processadas";
                    NFNotaProcessada notaProcessada = (NFNotaProcessada) notaObj;
                    caminho_anomes = pastaMesAnoAtual(diretorioArquivos + File.separator + "documentofiscal" + File.separator, data);
                    path = caminho_anomes + File.separator + local + File.separator;
                    System.out.println(path + notaProcessada.getNota().getInfo().getChaveAcesso() + "_nfe.xml");
                    arq = new FileWriter(path + notaProcessada.getNota().getInfo().getChaveAcesso() + "_nfe.xml");
                    gravarArq = new PrintWriter(arq);
                    System.out.println(notaProcessada.toString());
//                    gravarArq.printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    gravarArq.printf("%s", Util.removeAcentos(notaProcessada.toString()));
                    arq.close();
                    break;
                case 3:
                    local = "canceladas";
                    NFNotaProcessada notaCancelada = (NFNotaProcessada) notaObj;
                    caminho_anomes = pastaMesAnoAtual(diretorioArquivos + File.separator + "documentofiscal" + File.separator, data);
                    path = caminho_anomes + File.separator + local + File.separator;
                    System.out.println(path + notaCancelada.getNota().getInfo().getChaveAcesso() + "_canc.xml");
                    arq = new FileWriter(path + notaCancelada.getNota().getInfo().getChaveAcesso() + "_canc.xml");
                    gravarArq = new PrintWriter(arq);
//                    gravarArq.printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    gravarArq.printf("%s", Util.removeAcentos(notaCancelada.toString()));
                    arq.close();
                    break;
                case 4:
                    local = "eventos" + File.separator + "cancelamento";
                    NFEnviaEventoRetorno notaEventoCancelada = (NFEnviaEventoRetorno) notaObj;
                    caminho_anomes = pastaMesAnoAtual(diretorioArquivos + File.separator + "documentofiscal" + File.separator, data);
                    path = caminho_anomes + File.separator + local + File.separator;
                    System.out.println(path + notaEventoCancelada.getEventoRetorno().get(0).getInfoEventoRetorno().getChave() + "_canc.xml");
                    arq = new FileWriter(path + notaEventoCancelada.getEventoRetorno().get(0).getInfoEventoRetorno().getChave() + "_canc.xml");
                    gravarArq = new PrintWriter(arq);
//                    gravarArq.printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    gravarArq.printf("%s", Util.removeAcentos(notaEventoCancelada.toString()));
                    arq.close();
                    break;
                case 5:
                    local = "eventos" + File.separator + "cc";
                    NFProtocoloEventoCartaCorrecao notaCc = (NFProtocoloEventoCartaCorrecao) notaObj;
                    caminho_anomes = pastaMesAnoAtual(diretorioArquivos + File.separator + "documentofiscal" + File.separator, data);
                    path = caminho_anomes + File.separator + local + File.separator;
                    System.out.println(path + notaCc.getEventoRetorno().getInfoEventoRetorno().getChave() + "_cc.xml");
                    arq = new FileWriter(path + notaCc.getEventoRetorno().getInfoEventoRetorno().getChave() + "_cc.xml");
                    gravarArq = new PrintWriter(arq);
//                    gravarArq.printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                    gravarArq.printf("%s", Util.removeAcentos(notaCc.toString()));
                    arq.close();
                    break;
                case 6:
                    local = "eventos" + File.separator + "inutilizacao";
                    NFRetornoEventoInutilizacao notaInutilizada = (NFRetornoEventoInutilizacao) notaObj;
                    caminho_anomes = pastaMesAnoAtual(diretorioArquivos + File.separator + "documentofiscal" + File.separator, data);
                    path = caminho_anomes + File.separator + local + File.separator;
                    System.out.println(path + notaInutilizada.getDados().getIdentificador() + "_inu.xml");
                    arq = new FileWriter(path + notaInutilizada.getDados().getIdentificador() + "_inu.xml");
                    gravarArq = new PrintWriter(arq);
                    gravarArq.printf("%s", Util.removeAcentos(notaInutilizada.toString()));
                    arq.close();
                    break;
                case 7:
                    local = "denegadas";
                    NFNotaProcessada notaDenegada = (NFNotaProcessada) notaObj;
                    caminho_anomes = pastaMesAnoAtual(diretorioArquivos + File.separator + "documentofiscal" + File.separator, data);
                    path = caminho_anomes + File.separator + local + File.separator;
                    System.out.println(path + notaDenegada.getNota().getInfo().getChaveAcesso() + "_den.xml");
                    arq = new FileWriter(path + notaDenegada.getNota().getInfo().getChaveAcesso() + "_den.xml");
                    gravarArq = new PrintWriter(arq);
                    gravarArq.printf("%s", Util.removeAcentos(notaDenegada.toString()));
                    arq.close();
                    break;
                default:
                    System.out.println("Entrou no default");
                    break;
            }
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR);
            a.setTitle("Acom Informa");
            a.setContentText("O arquivo não foi criado verifique se as pastas necessárias foram criadas");
            a.setResizable(false);
            a.initModality(Modality.WINDOW_MODAL);
            a.show();
        }
    }

    public String pastaMesAnoAtual(String path, String data) {
        String array[] = data.split("/");
        String mes = array[1];//dd//mm/yyy
        String ano = array[2];

        File diretorioGeradas = new File(path + File.separator + "geradas");
        if (!diretorioGeradas.exists()) {
            diretorioGeradas.mkdirs(); //mkdir() cria somente um diretório, mkdirs() cria diretórios e subdiretórios.
        }

        File dir_docFiscal_ano = new File(path + ano);
        if (!dir_docFiscal_ano.exists()) {
            dir_docFiscal_ano.mkdirs(); //mkdir() cria somente um diretório, mkdirs() cria diretórios e subdiretórios.
        }
        File dir_docFiscal_mes = new File(path + ano + File.separator + mes);
        if (!dir_docFiscal_mes.exists()) {
            dir_docFiscal_mes.mkdirs(); //mkdir() cria somente um diretório, mkdirs() cria diretórios e subdiretórios.

            File diretorioProcessadas = new File(path + ano + File.separator + mes + File.separator + "processadas");
            if (!diretorioProcessadas.exists()) {
                diretorioProcessadas.mkdirs(); //mkdir() cria somente um diretório, mkdirs() cria diretórios e subdiretórios.
            }
            File diretorioDenegadas = new File(path + ano + File.separator + mes + File.separator + "denegadas");
            if (!diretorioDenegadas.exists()) {
                diretorioDenegadas.mkdirs(); //mkdir() cria somente um diretório, mkdirs() cria diretórios e subdiretórios.
            }
            File diretorioCanceladas = new File(path + ano + File.separator + mes + File.separator + "canceladas");
            if (!diretorioCanceladas.exists()) {
                diretorioCanceladas.mkdirs(); //mkdir() cria somente um diretório, mkdirs() cria diretórios e subdiretórios.
            }
            File diretorioEventos = new File(path + ano + File.separator + mes + File.separator + "eventos");
            if (!diretorioEventos.exists()) {
                diretorioEventos.mkdirs(); //mkdir() cria somente um diretório, mkdirs() cria diretórios e subdiretórios.
            }
            File eventoCancelamento = new File(path + ano + File.separator + mes + File.separator + "eventos" + File.separator + "cancelamento");
            if (!eventoCancelamento.exists()) {
                eventoCancelamento.mkdirs(); //mkdir() cria somente um diretório, mkdirs() cria diretórios e subdiretórios.
            }
            File eventoCC = new File(path + ano + File.separator + mes + File.separator + "eventos" + File.separator + "cc");
            if (!eventoCC.exists()) {
                eventoCC.mkdirs(); //mkdir() cria somente um diretório, mkdirs() cria diretórios e subdiretórios.
            }
            File eventoInutilizacao = new File(path + ano + File.separator + mes + File.separator + "eventos" + File.separator + "inutilizacao");
            if (!eventoInutilizacao.exists()) {
                eventoInutilizacao.mkdirs(); //mkdir() cria somente um diretório, mkdirs() cria diretórios e subdiretórios.
            }
        }
        return path + ano + File.separator + mes;
    }

    private Timestamp montarDataEHora(Timestamp data, Timestamp hora) {
        SimpleDateFormat formatoData = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatoHora = new SimpleDateFormat("HH:mm:ss");
        String dataF = formatoData.format(data);
        String horaF = formatoHora.format(hora);
        String dataHora = dataF + " " + horaF;
        Timestamp ts = Timestamp.valueOf(dataHora);
        return ts;
    }

    /* METODOS PARA GERENCIAMENTO DAS NOTAS DE DESTINATARIOS */

    public NFDistribuicaoIntRetorno obterNotas(String cpfCnpj, DFUnidadeFederativa uf, String chave, String nsu, String ultimoNsu) {
        NFDistribuicaoIntRetorno nfdeRetorno = new NFDistribuicaoIntRetorno();
        NFDistribuicaoIntRetorno consulta = new NFDistribuicaoIntRetorno();
        nfdeRetorno = consulta;
        try {
            nfdeRetorno = new WSFacade(new NFeConfigAcom()).consultarDistribuicaoDFe(cpfCnpj, DFUnidadeFederativa.valueOfCodigo(fDAO.getCodUf()), chave, nsu, ultimoNsu);
        } catch (KeyManagementException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            nfdeRetorno = null;
        } catch (UnrecoverableKeyException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            nfdeRetorno = null;
        } catch (KeyStoreException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            nfdeRetorno = null;
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            nfdeRetorno = null;
        } catch (Exception ex) {
            Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
            nfdeRetorno = null;
        }
        return nfdeRetorno;
    }

}
