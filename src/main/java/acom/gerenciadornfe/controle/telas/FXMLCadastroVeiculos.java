/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

//import acom.cadastro.util.MaskFieldUtil;
import acom.gerenciadornfe.modelo.entidades.Veiculo;
import acom.gerenciadornfe.modelo.enuns.EnumImagens;
import acom.gerenciadornfe.modelo.repositorio.VeiculoDAO;
import acom.gerenciadornfe.util.MaskFieldUtil;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableRow;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author allison
 */
public class FXMLCadastroVeiculos implements Initializable {

    /**
     * Initializes the controller class.
     */
    private Stage stage;

    @FXML
    private AnchorPane ap;

    @FXML
    private AnchorPane painelMsg;

    @FXML
    private Button btnIncluir;

    @FXML
    private Button btnAlterar;

    @FXML
    private Button btnExcluir;

    @FXML
    private Button btnConsulta;

    @FXML
    private Button btnSalvar;

    @FXML
    private Button btnImprimir;

    @FXML
    private Button btnCancelar;

    @FXML
    private Separator separador1;

    @FXML
    private Button btnFechar;

    @FXML
    private TabPane tabPaneGeral;

    @FXML
    private Tab tabGeralConsulta;

    @FXML
    private TableView<Veiculo> tabelaVeiculos;

    @FXML
    private TableColumn<?, ?> colunaCodigo;

    @FXML
    private TableColumn<?, ?> colunaPlaca;

    @FXML
    private TableColumn<?, ?> colunaRenavam;

    @FXML
    private TableColumn<?, ?> colunaTara;

    @FXML
    private TableColumn<?, ?> colunaCapacidadeKg;

    @FXML
    private TableColumn<?, ?> colunaCapacidadeM3;

    @FXML
    private TableColumn<?, ?> colunaTipoRodo;

    @FXML
    private TableColumn<?, ?> colunaTipoCarroceria;

    @FXML
    private TableColumn<?, ?> colunaUf;

    @FXML
    private ComboBox<?> comboCampos;

    @FXML
    private TextField txtPesquisa;

    @FXML
    private Button btnPesquisar;

    @FXML
    private Button btnLimpar;

    @FXML
    private Tab tabGeralCadastro;

    @FXML
    private TextField txtCodigo;

    @FXML
    private TextField txtPlaca;

    @FXML
    private TextField txtRenavam;

    @FXML
    private TextField txtTara;

    @FXML
    private TextField txtCapacidadeKg;

    @FXML
    private TextField txtTipoRodo;

    @FXML
    private TextField txtCapacidadeM3;

    @FXML
    private TextField txtTipoCarroceria;

    @FXML
    private TextField txtUf;

    @FXML
    private Label lblMsg;

    @FXML
    private AnchorPane ntfPane;

    private String operacao;

    private Notifications notification;

    private Node grafics;

    private Veiculo veiculo = new Veiculo();

    private static final ImageView ICON_SUCCESS = new ImageView(EnumImagens.ICON_SUCCESS.getIcon());
    private static final ImageView ICON_ERROR = new ImageView(EnumImagens.ICON_ERROR.getIcon());
    private static final ImageView ICON_WARNING = new ImageView(EnumImagens.ICON_WARNING.getIcon());
    private static final ImageView ICON_INFO = new ImageView(EnumImagens.ICON_INFO.getIcon());

    private List<Veiculo> listaVeiculos;

    private ObservableList<Veiculo> obsListVeiculos;

    private VeiculoDAO vDAO;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        vDAO = new VeiculoDAO();
        painelMsg = new AnchorPane();
        System.out.println("iniciou controlador do painel na aba");
        /*provisório ate arrumar solução*/
        btnFechar.setVisible(false);
        separador1.setVisible(false);
        /*provisório ate arrumar solução*/
        tabPaneGeral.setStyle("-fx-open-tab-animation: NONE; -fx-close-tab-animation: NONE;"); // css que tira o efeito das abas ao trocar-las, dando a impressão que uma sobrepõe a outra.
        iniciarTela();
//        validacoes();
        carregarTabelaVeiculos();
//        geraNotificacao("testando a ntf");
        lblMsg.setText("");
        funcoesTabela();
    }

    private void iniciarTela() {
        tabPaneGeral.getTabs().remove(tabGeralCadastro);
        btnConsulta.setDisable(true);
        btnSalvar.setDisable(true);
        btnCancelar.setDisable(true);
        btnExcluir.setDisable(true);
        btnAlterar.setDisable(true);
        btnImprimir.setDisable(true);
        operacao = "consultando";

//        MaskFieldUtil.maxField(txtCodigo, 3);
//
        MaskFieldUtil.placaVeicular(txtPlaca);
//        MaskFieldUtil.maxField(txtCodigo, 60);
    }

    @FXML
    private void clickBtnIncluir() {
        tabPaneGeral.getTabs().add(tabGeralCadastro);
        tabPaneGeral.getTabs().remove(tabGeralConsulta);
        operacao = "incluindo";
        abilitarCampos();
        txtCodigo.setText(String.valueOf(vDAO.ultimoId()));
//        txtCodigo.requestFocus();

        btnIncluir.setDisable(true);
        btnSalvar.setDisable(false);
        btnConsulta.setDisable(true);
        btnCancelar.setDisable(false);
    }

    @FXML
    private void clickBtnCancelar() {
        if (tabGeralCadastro.isSelected()) {
            if (operacao.equals("incluindo")) {
                operacao = "";
                this.veiculo = null;
                limparCamposInclusão();
                btnConsulta.setDisable(false);
                desabilitarCampos();
                btnSalvar.setDisable(true);
                btnCancelar.setDisable(true);
                btnIncluir.setDisable(false);
                tabPaneGeral.getTabs().remove(tabGeralCadastro);
                tabPaneGeral.getTabs().add(tabGeralConsulta);
                tabelaVeiculos.getSelectionModel().select(0);
                tabelaVeiculos.getFocusModel().focus(0);
                carregarTabelaVeiculos();
            } else if (operacao.equals("alterando")) {
                recuperarCamposAlteracao();
                operacao = "";
                this.veiculo = null;
                btnSalvar.setDisable(true);
                btnCancelar.setDisable(false);
                btnIncluir.setDisable(false);
                btnExcluir.setDisable(true);
                btnAlterar.setDisable(true);
                tabPaneGeral.getTabs().remove(tabGeralCadastro);
                tabPaneGeral.getTabs().add(tabGeralConsulta);
                tabelaVeiculos.getSelectionModel().select(0);
                tabelaVeiculos.getFocusModel().focus(0);
                carregarTabelaVeiculos();
            }
        }
    }

    @FXML
    private void cliqueBtnConsulta() {
        tabPaneGeral.getTabs().add(tabGeralConsulta);
        tabPaneGeral.getTabs().remove(tabGeralCadastro);
        btnExcluir.setDisable(true);
        btnAlterar.setDisable(true);
        btnConsulta.setDisable(true);
        btnIncluir.setDisable(false);
    }

    @FXML
    private void clickBtnAlterar() {
        abilitarCampos();
        btnAlterar.setDisable(true);
        btnCancelar.setDisable(false);
        btnSalvar.setDisable(false);
        btnConsulta.setDisable(true);
        btnExcluir.setDisable(true);
    }

    @FXML
    private void cliqueBtnSalvar() {
        try {
            Veiculo veiculo = new Veiculo();
            veiculo.setVei_codigo(Integer.parseInt(txtCodigo.getText().trim()));
            veiculo.setVei_placa(txtPlaca.getText().toString());
            veiculo.setVei_renavam(txtRenavam.getText().trim());
            veiculo.setVei_tara(Integer.parseInt(txtTara.getText().trim()));
            veiculo.setVei_capkg(Long.parseLong(txtCapacidadeKg.getText().toLowerCase()));
            veiculo.setVei_capm3(Long.parseLong(txtCapacidadeM3.getText().trim()));
            veiculo.setVei_tprod(txtTipoRodo.getText().trim());
            veiculo.setVei_tpcar(txtTipoCarroceria.getText().trim());
            veiculo.setVei_uf(txtUf.getText().trim());

            if (tabGeralCadastro.isSelected()) {
                if (operacao.equals("incluindo")) {
                    boolean retorno = vDAO.inserir(veiculo);
                    if (retorno == true) {
                        grafics = ICON_SUCCESS;
                        createNotification(Pos.BOTTOM_RIGHT, grafics, "Dados inseridos com sucesso!");
                        notification.show();
                    } else {
                        grafics = ICON_ERROR;
                        createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao inserir os dados.");
                        notification.show();
                    }
                    limparCamposInclusão();
                } else if (operacao.equals("alterando")) {
                    boolean retorno = vDAO.alterar(veiculo);
                    if (retorno == true) {
                        grafics = ICON_SUCCESS;
                        createNotification(Pos.BOTTOM_RIGHT, grafics, "Dados alterados com sucesso!");
                        notification.show();
                    } else {
                        grafics = ICON_ERROR;
                        createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao alterados os dados.");
                        notification.show();
                    }
                    recuperarCamposAlteracao();
                }
                tabPaneGeral.getTabs().remove(tabGeralCadastro);
                tabPaneGeral.getTabs().add(tabGeralConsulta);
                tabelaVeiculos.getSelectionModel().select(0);
                tabelaVeiculos.getFocusModel().focus(0);
                carregarTabelaVeiculos();
                btnSalvar.setDisable(true);
                btnCancelar.setDisable(true);
                btnExcluir.setDisable(true);
                btnIncluir.setDisable(false);
                btnAlterar.setDisable(true);
                this.veiculo = null;
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    @FXML
    private void clickBtnExcluir() {
        Veiculo veiculo = tabelaVeiculos.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("ATENÇÃO");
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setHeaderText("ATENÇÃO");
        alert.setContentText("Deseja realmente excluir este item?\n\n");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            boolean retorno = vDAO.excluir(veiculo);
            if (retorno == true) {
                grafics = ICON_SUCCESS;
                createNotification(Pos.BOTTOM_RIGHT, grafics, "Operação realizada com sucesso!");
            } else {
                grafics = ICON_ERROR;
                createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao realizar a operação");
            }
            tabPaneGeral.getTabs().remove(tabGeralCadastro);
            tabPaneGeral.getTabs().add(tabGeralConsulta);
            btnExcluir.setDisable(true);
            btnConsulta.setDisable(true);
            btnAlterar.setDisable(true);
            btnIncluir.setDisable(false);
            carregarTabelaVeiculos();
        } else {
            alert.close();
        }
    }

    private void desabilitarCampos() {
        txtCodigo.setDisable(true);;
        txtPlaca.setDisable(true);
        txtRenavam.setDisable(true);
        txtTara.setDisable(true);
        txtCapacidadeKg.setDisable(true);
        txtTipoRodo.setDisable(true);
        txtCapacidadeM3.setDisable(true);
        txtTipoCarroceria.setDisable(true);
        txtUf.setDisable(true);
    }

    private void abilitarCampos() {
        txtCodigo.setDisable(false);;
        txtPlaca.setDisable(false);
        txtRenavam.setDisable(false);
        txtTara.setDisable(false);
        txtCapacidadeKg.setDisable(false);
        txtTipoRodo.setDisable(false);
        txtCapacidadeM3.setDisable(false);
        txtTipoCarroceria.setDisable(false);
        txtUf.setDisable(false);
    }

    private void limparCamposInclusão() {
        txtCodigo.setText("");
        txtPlaca.setText("");
        txtRenavam.setText("");
        txtTara.setText("");
        txtCapacidadeKg.setText("");
        txtTipoRodo.setText("");
        txtCapacidadeM3.setText("");
        txtTipoCarroceria.setText("");
        txtUf.setText("");
    }

    private void recuperarCamposAlteracao() {
        txtCodigo.setText(String.valueOf(veiculo.getVei_codigo()));
        txtPlaca.setText(veiculo.getVei_placa());
        txtRenavam.setText(veiculo.getVei_renavam());
        txtTara.setText(String.valueOf(veiculo.getVei_tara()));
        txtCapacidadeKg.setText(String.valueOf(veiculo.getVei_capkg()));
        txtCapacidadeM3.setText(String.valueOf(veiculo.getVei_capm3()));
        txtTipoRodo.setText(veiculo.getVei_tprod());
        txtTipoCarroceria.setText(veiculo.getVei_tpcar());
        txtUf.setText(veiculo.getVei_uf());
    }

    private void carregarTabelaVeiculos() {
        try {
            colunaCodigo.setCellValueFactory(new PropertyValueFactory<>("vei_codigo"));
            colunaPlaca.setCellValueFactory(new PropertyValueFactory<>("vei_placa"));
            colunaRenavam.setCellValueFactory(new PropertyValueFactory<>("vei_renavam"));
            colunaTara.setCellValueFactory(new PropertyValueFactory<>("vei_tara"));
            colunaCapacidadeKg.setCellValueFactory(new PropertyValueFactory<>("vei_capkg"));
            colunaCapacidadeM3.setCellValueFactory(new PropertyValueFactory<>("vei_capm3"));
            colunaTipoRodo.setCellValueFactory(new PropertyValueFactory<>("vei_tprod"));
            colunaTipoCarroceria.setCellValueFactory(new PropertyValueFactory<>("vei_tpcar"));
            colunaUf.setCellValueFactory(new PropertyValueFactory<>("vei_uf"));

            listaVeiculos = vDAO.listar();
            obsListVeiculos = FXCollections.observableArrayList(listaVeiculos);
            tabelaVeiculos.setItems(obsListVeiculos);
        } catch (Exception ex) {
            System.out.println("Erro ao carregar dados da tabela veiculos");
        }
    }

    private void createNotification(Pos position, Node grafics, String text) {
        notification = Notifications.create()
                .title("Acom Informa\n\n")
                .text(" " + text)
                .graphic(grafics)
                .hideAfter(Duration.seconds(5))
                .position(position);
    }

    private void funcoesTabela() {
        /*seleciona o objeto da tabela após dar 2 clicks*/
        tabelaVeiculos.setRowFactory(tv -> {
            TableRow<Veiculo> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Veiculo veiculo = row.getItem();
                    veiculoSelecionado(veiculo);
                }
            });
            return row;
        });

    }

    private void veiculoSelecionado(Veiculo veiculo) {
        tabPaneGeral.getTabs().add(tabGeralCadastro);
        tabPaneGeral.getTabs().remove(tabGeralConsulta);
        btnAlterar.setDisable(false);
        btnExcluir.setDisable(false);
        btnIncluir.setDisable(true);
        btnSalvar.setDisable(true);
        btnConsulta.setDisable(false);
        btnCancelar.setDisable(true);
        operacao = "alterando";
        desabilitarCampos();
        this.veiculo = veiculo;
        txtCodigo.setText(String.valueOf(veiculo.getVei_codigo()));
        txtPlaca.setText(veiculo.getVei_placa());
        txtRenavam.setText(veiculo.getVei_renavam());
        txtTara.setText(String.valueOf(veiculo.getVei_tara()));
        txtCapacidadeKg.setText(String.valueOf(veiculo.getVei_capkg()));
        txtCapacidadeM3.setText(String.valueOf(veiculo.getVei_capm3()));
        txtTipoRodo.setText(veiculo.getVei_tprod());
        txtTipoCarroceria.setText(veiculo.getVei_tpcar());
        txtUf.setText(veiculo.getVei_uf());
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

}
