package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.nota.NotaControler;
import acom.gerenciadornfe.modelo.entidades.ObjetoRetornoCancelamento;
import acom.gerenciadornfe.modelo.entidades.ParametroCancelamento;
import acom.gerenciadornfe.modelo.repositorio.NotaFiscalCancelamentoDAO;
import acom.gerenciadornfe.util.MaskFieldUtil;
import acom.gerenciadornfe.util.Util;
import acom.gerenciadornfe.util.XmlFormatter;
import java.net.URL;
import java.text.NumberFormat;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

/**
 * FXML Controller class
 *
 * @author allison
 */
public class FXMLCancelamentoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TextField txtNumeroCancelamento;

    @FXML
    private TextField txtSerie;

    @FXML
    private TextField txtNumeroNota;

    @FXML
    private TextField txtFilial;

    @FXML
    private TextField txtTotalNota;

    @FXML
    private TextField txtChaveAcesso;

    @FXML
    private TextField txtDestinatario;

    @FXML
    private TextField txtMotivo;

    @FXML
    private Button btnEnviar;

    @FXML
    private Button btnCancelar;

    @FXML
    private Tab abaRetornoNfe;

    @FXML
    private TextArea txtNfe;

    @FXML
    private Tab abaXml;

    @FXML
    private TextArea txtXml;

    @FXML
    private Label labelMsg;
    
    @FXML
    private TextField txtEvento;

    private ParametroCancelamento pc;

    private NotaFiscalCancelamentoDAO nfcDao;
    
    private NotaControler notaControle;
    
    private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();
    
    public FXMLCancelamentoController(ParametroCancelamento pc) {
        this.pc = pc;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nfcDao = new NotaFiscalCancelamentoDAO();
        notaControle = new NotaControler();
        carregarTela();
    }

    private void carregarTela() {
        MaskFieldUtil.codigo(txtEvento);
        txtNumeroCancelamento.setText(String.valueOf(nfcDao.getMaxIdCancelamento() + 1));
        txtSerie.setText(pc.getNf().getNf_serie());
        txtNumeroNota.setText(pc.getNf().getNf_nrnota());
        txtFilial.setText(pc.getNf().getFil_cod());
        String valor = currencyFormat.format(pc.getNf().getNf_totnota());
        txtTotalNota.setText(valor);
        txtEvento.setText("1");
        txtChaveAcesso.setText(pc.getNf().getNfeChaveAcesso());
        txtDestinatario.setText(pc.getNf().getPes_nome45());
    }

    @FXML
    void clickBtnCancelar(ActionEvent event) {
        System.out.println("");
        this.pc.getDialogStage().close();
    }

    @FXML
    void clickBtnEnviar(ActionEvent event) {
        System.out.println("Botao enviar enviado()");
        String motivo = txtMotivo.getText();
        ObjetoRetornoCancelamento retorno;
        if (motivo.trim().length() == 0) {
            labelMsg.setTextFill(Color.web("#D93444"));
            labelMsg.setText("Campo obrigatório vazio.");
        } else {
            System.out.println("botao clicado");
            ObjetoRetornoCancelamento retornoObj = notaControle.cancelarNfe(pc.getNf().getNfeChaveAcesso(), String.valueOf(pc.getNf().getNfeProtocolo()), txtMotivo.getText().trim(), Util.formatarData(pc.getNf().getNf_dtemissao().toString()));
            if (retornoObj.isStatusTransmissao() == true) {
                txtNfe.setText(retornoObj.getStatus() + " - " + retornoObj.getMotivo());
                XmlFormatter f = new XmlFormatter();
                if (retornoObj.getXml() != null) {
                    txtXml.setText(f.format(retornoObj.getXml()));
                }
            }
        }
    }

    public ParametroCancelamento getPc() {
        return pc;
    }

    public void setPc(ParametroCancelamento pc) {
        this.pc = pc;
    }
}
