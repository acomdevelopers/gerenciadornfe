package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.nota.NotaControler;
import acom.gerenciadornfe.modelo.entidades.Filial;
import acom.gerenciadornfe.modelo.entidades.Inutilizacao;
import acom.gerenciadornfe.modelo.entidades.NotaFiscal;
import acom.gerenciadornfe.modelo.entidades.NotaFiscalCartaCorrecao;
import acom.gerenciadornfe.modelo.entidades.ObjetoRetornoCartaCorrecao;
import acom.gerenciadornfe.modelo.enuns.EnumImagens;
import acom.gerenciadornfe.modelo.repositorio.FilialDAO;
import acom.gerenciadornfe.modelo.repositorio.NotaFiscalCartaCorrecaoDAO;
import acom.gerenciadornfe.modelo.repositorio.NotaFiscalDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import acom.gerenciadornfe.util.Util;
import acom.gerenciadornfe.util.XmlFormatter;
import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author allison
 */
public class FXMLCartaCorrecaoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private AnchorPane ap;

    @FXML
    private Button btnIncluir;

    @FXML
    private Button btnAlterar;

    @FXML
    private Button btnExcluir;

    @FXML
    private Button btnConsulta;

    @FXML
    private Button btnSalvar;

    @FXML
    private Button btnImprimir;

    @FXML
    private Button btnCancelar;

    @FXML
    private Separator separador1;

    @FXML
    private Button btnFechar;

    @FXML
    private TabPane tabPaneGeral;

    @FXML
    private Tab tabGeralConsulta;

    @FXML
    private TableView<NotaFiscalCartaCorrecao> tabelaCartaCorrecao;

    @FXML
    private TableColumn<?, ?> colunaItem;

    @FXML
    private TableColumn<NotaFiscalCartaCorrecao, String> colunaSituacao;

    @FXML
    private TableColumn<NotaFiscalCartaCorrecao, String> colunaChaveNota;

    @FXML
    private TableColumn<NotaFiscalCartaCorrecao, String> colunaProtocolo;

    @FXML
    private TableColumn<NotaFiscalCartaCorrecao, String> colunaNome;

    @FXML
    private TableColumn<NotaFiscalCartaCorrecao, String> colunaRetornoSefaz;

    @FXML
    private TextField txtPesquisa;

    @FXML
    private Button btnPesquisar;

    @FXML
    private Button btnEnviar;

    @FXML
    private Button btnImprimirCC;

    @FXML
    private Button btnVizualizarXml;
    
    @FXML
    private Button btnEnviarEmail;

    @FXML
    private Tab tabGeralCadastro;

    @FXML
    private TextArea txtRetornoNfeTabela;

    @FXML
    private TextArea txtRetornoXmlTabela;

    @FXML
    private TextField txtIdNfe;

    @FXML
    private Button btnImportar;

    @FXML
    private TextField txtSerie;

    @FXML
    private TextField txtNumeroNfe;

    @FXML
    private TextField txtFilial;

    @FXML
    private TextField txtTotalNota;

    @FXML
    private TextField txtChaveAcesso;

    @FXML
    private TextField txtNome;

    @FXML
    private TextField txtNumeroSequencial;

    @FXML
    private TextField txtData;

    @FXML
    private TextField txtHora;

    @FXML
    private TextField txtModelo;

    @FXML
    private TextArea txtCorrecao;

    @FXML
    private AnchorPane painelMsg;

    @FXML
    private TextField txtCopiarChaveAcesso;

    @FXML
    private Label lblMsg;

    static NotaFiscal nf;

    private NotaFiscalCartaCorrecao nfCC;

    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private NotaFiscalCartaCorrecaoDAO nfccDAO;

    private NotaFiscalDAO nfDAO;

    private String operacao = "";

    private Notifications notification;

    private Node grafics;

    private List<NotaFiscalCartaCorrecao> listaNfcc;

    private ObservableList<NotaFiscalCartaCorrecao> obsListNFcc;

    private NotaControler nControler;

    private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

    private NotaFiscalCartaCorrecao nfcc;

    private Filial filial;
    private FilialDAO fDAO;
    private ParametrosDAO pDAO;

    private static final ImageView ICON_SUCCESS = new ImageView(EnumImagens.ICON_SUCCESS.getIcon());
    private static final ImageView ICON_ERROR = new ImageView(EnumImagens.ICON_ERROR.getIcon());

    public FXMLCartaCorrecaoController() {

    }

    public FXMLCartaCorrecaoController(NotaFiscal notaFiscal) {
//        this.xnotaFiscal = xnotaFiscal;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	tabPaneGeral.setStyle("-fx-open-tab-animation: NONE; -fx-close-tab-animation: NONE;"); // css que tira o efeito das abas ao trocar-las, dando a impressão que uma sobrepõe a outra.
	btnFechar.setVisible(false);
	separador1.setVisible(false);
	nfccDAO = new NotaFiscalCartaCorrecaoDAO();
	pDAO = new ParametrosDAO();
	nfDAO = new NotaFiscalDAO();
	nfCC = new NotaFiscalCartaCorrecao();
	nControler = new NotaControler();
	fDAO = new FilialDAO();
	filial = fDAO.getFilial(pDAO.getParFilial().replace("=", ""));
	iniciarTela();
	carregarTabelaNotasCC();
	funcoesTabela();
    }

    private void iniciarTela() {
	tabPaneGeral.getTabs().remove(tabGeralCadastro);
	btnIncluir.setDisable(false);
	btnConsulta.setDisable(true);
	btnSalvar.setDisable(true);
	btnCancelar.setDisable(true);
	btnExcluir.setDisable(true);
	btnAlterar.setDisable(true);
	btnImprimir.setDisable(true);

	btnEnviar.setDisable(true);
	btnVizualizarXml.setDisable(true);
	btnImprimirCC.setDisable(true);
	btnEnviarEmail.setDisable(true);

    }

    private void carregarTabelaNotasCC() {
	try {
	    colunaItem.setCellValueFactory(new PropertyValueFactory<>("Nfcc_item"));
	    colunaChaveNota.setCellValueFactory(new PropertyValueFactory<>("Nfcc_chavenfe"));
	    colunaProtocolo.setCellValueFactory(new PropertyValueFactory<>("Nfcc_protocolo"));
	    colunaSituacao.setCellValueFactory(new PropertyValueFactory<>("Nfcc_enviada"));
	    colunaNome.setCellValueFactory(new PropertyValueFactory<>("nf_nome"));
	    colunaRetornoSefaz.setCellValueFactory(new PropertyValueFactory<>("nfcc_retorno_sefaz"));
	    listaNfcc = nfccDAO.listar(parametrosPEsquisa());
	    obsListNFcc = FXCollections.observableArrayList(listaNfcc);
	    tabelaCartaCorrecao.setItems(obsListNFcc);
	    tabelaCartaCorrecao.refresh();
	    desabilitarBotesControleTabela();
	} catch (Exception e) {
	}
    }

    private String parametrosPEsquisa() {
	String parametro = txtPesquisa.getText().trim().replace("*", "%");
	return "%" + parametro + "%";
    }

    private void funcoesTabela() {
	colunaSituacao.setCellFactory(tc -> new TableCell<NotaFiscalCartaCorrecao, String>() {
	    @Override
	    protected void updateItem(String item, boolean empty) {
		super.updateItem(item, empty);
		TableRow linha = getTableRow();
		if (empty) {
//                    setText("Erro");
//                    linha.setStyle("-fx-background-color: #FFF;"); //branco
		} else {
		    if (item.equals("S")) {
			setText("Enviada");
			linha.setStyle("-fx-background-color: #D4EDDA;"); //verde
		    } else if (item.equals("N")) {
			setText("Não Enviada");
			linha.setStyle("-fx-background-color: #E2E3E5;"); //cinza
		    } else if (item.equals("R")) {
			setText("Rejeitada");
			linha.setStyle("-fx-background-color: #FFF3CD;"); //amarelo
		    }
		}
	    }
	});

	tabelaCartaCorrecao.setRowFactory(tv -> {
	    TableRow<NotaFiscalCartaCorrecao> row = new TableRow<>();
	    row.setOnMouseClicked(event -> {
		if (event.getClickCount() == 2 && (!row.isEmpty())) {
		    NotaFiscalCartaCorrecao cc = row.getItem();
		    if (!cc.getNfcc_enviada().equals("S")) {
			ccSelecionada(cc);
		    } else {
			Alert a = new Alert(Alert.AlertType.INFORMATION);
			a.setTitle("Acom Informa");
			a.setHeaderText("Atenção!");
			a.setContentText("Carta de Correção enviada, não pode ser editada.");
			a.setResizable(false);
			a.initModality(Modality.WINDOW_MODAL);
			a.show();
		    }
		}
	    });
	    return row;
	});
    }

    private void selecionarLinha() {
	desabilitarBotesControleTabela();
	txtCopiarChaveAcesso.setText("");
	NotaFiscalCartaCorrecao nota = tabelaCartaCorrecao.getSelectionModel().getSelectedItem();
	if (nota == null) {
	    System.out.println("nenhuma nota selecionada");
	} else {
	    System.out.println("NFCC Chave + Item.....: " + nota.getNfcc_chavenfe() + " - " + nota.getNfcc_item());
	    if (nota.getNfcc_enviada().equals("N") || nota.getNfcc_enviada() == null) {
		txtCopiarChaveAcesso.setText(nota.getNfcc_chavenfe());
		btnEnviar.setDisable(false);
		btnImprimirCC.setDisable(true);
		btnEnviarEmail.setDisable(true);
	    } else if (nota.getNfcc_enviada().equals("S")) {
		txtCopiarChaveAcesso.setText(nota.getNfcc_chavenfe());
		btnImprimirCC.setDisable(false);
                btnEnviarEmail.setDisable(false);
	    } else if (nota.getNfcc_enviada().equals("R")) {
		btnEnviar.setDisable(false);
		btnEnviarEmail.setDisable(true);
	    }
	}
    }

    private void ccSelecionada(NotaFiscalCartaCorrecao cc) {
	tabPaneGeral.getTabs().add(tabGeralCadastro);
	tabPaneGeral.getTabs().remove(tabGeralConsulta);
	btnAlterar.setDisable(false);
	btnExcluir.setDisable(false);
	btnIncluir.setDisable(true);
	btnSalvar.setDisable(true);
	btnConsulta.setDisable(false);
	btnCancelar.setDisable(true);
	operacao = "alterando";
	txtCorrecao.setEditable(false);
	desabilitarCampos();
	this.nfCC = cc;
	NotaFiscal nf = nfDAO.getNotaFiscalChave(cc.getNfcc_chavenfe());
	txtIdNfe.setText(nf.getNf_numero().trim());
	txtSerie.setText(nf.getNf_serie().trim());
	txtModelo.setText(nf.getNf_modelo().trim());
	txtNumeroNfe.setText(nf.getNf_nrnota().trim());
	txtTotalNota.setText(currencyFormat.format(nf.getNf_totnota()).trim());
	txtChaveAcesso.setText(cc.getNfcc_chavenfe().trim());
	txtNome.setText(cc.getNf_nome().trim());
	txtNumeroSequencial.setText(cc.getNfcc_item());
	txtData.setText(Util.formatarData(cc.getNfcc_data().toString().trim()));
	txtHora.setText(Util.formatarHora(cc.getNfcc_hora().toString().trim()));
	txtCorrecao.setText(cc.getNfcc_correcao());
	txtFilial.setText(cc.getFilia());
    }

    @FXML
    void clickBtnAlterar(ActionEvent event) {
	habilitarCampos();
	btnAlterar.setDisable(true);
	btnCancelar.setDisable(false);
	btnSalvar.setDisable(false);
	btnConsulta.setDisable(true);
	btnExcluir.setDisable(true);
	btnImportar.setDisable(true);
    }

    @FXML
    void clickBtnCancelar(ActionEvent event) {
	if (tabGeralCadastro.isSelected()) {
	    if (operacao.equals("incluindo")) {
		operacao = "";
		this.nfCC = null;
		limparCamposInclusão();
		btnConsulta.setDisable(false);
		btnSalvar.setDisable(true);
		btnCancelar.setDisable(true);
		btnIncluir.setDisable(false);
		tabPaneGeral.getTabs().remove(tabGeralCadastro);
		tabPaneGeral.getTabs().add(tabGeralConsulta);
		tabelaCartaCorrecao.getSelectionModel().select(0);
		tabelaCartaCorrecao.getFocusModel().focus(0);
		carregarTabelaNotasCC();
	    } else if (operacao.equals("alterando")) {
		recuperarCamposAlteracao();
		operacao = "";
		this.nfCC = null;
		limparCamposInclusão();
		btnSalvar.setDisable(true);
		btnCancelar.setDisable(false);
		btnIncluir.setDisable(false);
		btnExcluir.setDisable(true);
		btnAlterar.setDisable(true);
		tabPaneGeral.getTabs().remove(tabGeralCadastro);
		tabPaneGeral.getTabs().add(tabGeralConsulta);
		tabelaCartaCorrecao.getSelectionModel().select(0);
		tabelaCartaCorrecao.getFocusModel().focus(0);
		carregarTabelaNotasCC();
	    }
	}
    }

    @FXML
    void clickBtnImprimirCc(ActionEvent event) {
	String chave = tabelaCartaCorrecao.getSelectionModel().getSelectedItem().getNfcc_chavenfe();
	String item = tabelaCartaCorrecao.getSelectionModel().getSelectedItem().getNfcc_item();
	nControler.imprimirCartaCorrecao(chave, item);
    }

    @FXML
    void clickBtnExcluir(ActionEvent event) {
	NotaFiscalCartaCorrecao carta = tabelaCartaCorrecao.getSelectionModel().getSelectedItem();
	Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	alert.setTitle("ATENÇÃO");
	alert.initModality(Modality.APPLICATION_MODAL);
	alert.setHeaderText("ATENÇÃO");
	alert.setContentText("Deseja realmente excluir este item?\n\n");

	Optional<ButtonType> result = alert.showAndWait();
	if (result.get() == ButtonType.OK) {
	    boolean retorno = nfccDAO.excluir(carta);
	    if (retorno == true) {
		grafics = ICON_SUCCESS;
		createNotification(Pos.BOTTOM_RIGHT, grafics, "Operação realizada com sucesso!");
	    } else {
		grafics = ICON_ERROR;
		createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao realizar a operação");
	    }
	    tabPaneGeral.getTabs().remove(tabGeralCadastro);
	    tabPaneGeral.getTabs().add(tabGeralConsulta);
	    btnExcluir.setDisable(true);
	    btnConsulta.setDisable(true);
	    btnAlterar.setDisable(true);
	    btnIncluir.setDisable(false);
	    carregarTabelaNotasCC();
	} else {
	    alert.close();
	}
    }

    @FXML
    void clickBtnEnviar(ActionEvent event) {
	ObjetoRetornoCartaCorrecao retorno = nControler.cartaCorrecao(tabelaCartaCorrecao.getSelectionModel().getSelectedItem());
	if (retorno.isStatusTransmissao() == true) {
	    txtRetornoNfeTabela.setText(retorno.getStatus() + " - " + retorno.getMotivo());
	    XmlFormatter f = new XmlFormatter();
	    if (retorno.getXml() != null) {
		txtRetornoXmlTabela.setText(f.format(retorno.getXml()));
	    }
	    carregarTabelaNotasCC();
	} else {
	    txtRetornoNfeTabela.setText(retorno.getStatus() + " - " + retorno.getMotivo());
	    XmlFormatter f = new XmlFormatter();
	    if (retorno.getXml() != null) {
		txtRetornoXmlTabela.setText(f.format(retorno.getXml()));
	    }
	    grafics = ICON_ERROR;
	    createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao executar a operação!");
	    notification.show();
	    carregarTabelaNotasCC();
	}
    }

    @FXML
    void clickBtnIncluir(ActionEvent event) {
	tabPaneGeral.getTabs().add(tabGeralCadastro);
	tabPaneGeral.getTabs().remove(tabGeralConsulta);
	operacao = "incluindo";
	habilitarCampos();
	limparCamposInclusão();
	btnIncluir.setDisable(true);
	btnSalvar.setDisable(false);
	btnConsulta.setDisable(true);
	btnCancelar.setDisable(false);

    }

    @FXML
    void clickBtnVizualizarXml(ActionEvent event) {

    }

    @FXML
    void cliqueBtnConsulta(ActionEvent event) {
	tabPaneGeral.getTabs().add(tabGeralConsulta);
	tabPaneGeral.getTabs().remove(tabGeralCadastro);
	btnExcluir.setDisable(true);
	btnAlterar.setDisable(true);
	btnConsulta.setDisable(true);
	btnIncluir.setDisable(false);
    }

    @FXML
    void cliqueBtnImportar(ActionEvent event) {
	try {
	    Stage dialogStage = new Stage();
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLPesquisarNota.fxml"));
	    loader.setController(new FXMLPesquisarNotaController(dialogStage));
	    Parent page = (Parent) loader.load();
	    dialogStage.initModality(Modality.NONE);
	    dialogStage.setTitle("Importar Nf-e");
	    dialogStage.setResizable(false);
	    Scene scene = new Scene(page);
	    dialogStage.setScene(scene);
	    notifyAllListeners("solicitacao", null);
	    dialogStage.showAndWait();
	    preencherCampos();
	} catch (IOException ex) {
	    Logger.getLogger(FXMLCartaCorrecaoController.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    @FXML
    void cliqueBtnSalvar(ActionEvent event) {
	try {
	    if (tabGeralCadastro.isSelected()) {
		if (validarCCe() == true) {
		    if (operacao.equals("incluindo")) {
			NotaFiscalCartaCorrecao nfcc = new NotaFiscalCartaCorrecao();
			nfcc.setNfcc_chavenfe(Util.removeAcentos(txtChaveAcesso.getText().trim()));
			nfcc.setNfcc_cnpj(nf.getPes_cnpj());
			nfcc.setNfcc_correcao(Util.removeAcentos(txtCorrecao.getText().trim()));
			nfcc.setNfcc_data(Util.convertStringToTimestamp(txtData.getText().trim()));
			nfcc.setNfcc_hora(Util.convertStringHoraToTimestamp(txtHora.getText().trim()));
			nfcc.setNfcc_item(txtNumeroSequencial.getText().trim());
			nfcc.setNfcc_enviada("N");

			nfcc.setNfcc_modelo(txtModelo.getText().trim());
			nfcc.setNfcc_serie(txtSerie.getText().trim());
			nfcc.setNfcc_numero_nota(txtNumeroNfe.getText().trim());
			nfcc.setFilia(filial.getFil_cod());

			boolean retorno = nfccDAO.inserir(nfcc);
			if (retorno == true) {
			    grafics = ICON_SUCCESS;
			    createNotification(Pos.BOTTOM_RIGHT, grafics, "Dados inseridos com sucesso!");
			    notification.show();
			} else {
			    grafics = ICON_ERROR;
			    createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao inserir os dados.");
			    notification.show();
			}
			limparCamposInclusão();
		    } else if (operacao.equals("alterando")) {
			NotaFiscal nota = nfDAO.getNotaFiscalChave(Util.removeAcentos(txtChaveAcesso.getText().trim()));
			NotaFiscalCartaCorrecao nfcc = new NotaFiscalCartaCorrecao();
			nfcc.setNfcc_chavenfe(Util.removeAcentos(txtChaveAcesso.getText().trim()));
			nfcc.setNfcc_cnpj(nota.getFil_cnpj());
			nfcc.setNfcc_correcao(Util.removeAcentos(txtCorrecao.getText().trim()));
			nfcc.setNfcc_data(Util.convertStringToTimestamp(txtData.getText().trim()));
			nfcc.setNfcc_hora(Util.convertStringHoraToTimestamp(txtHora.getText().trim()));
			nfcc.setNfcc_item(txtNumeroSequencial.getText().trim());
			nfcc.setNfcc_enviada("N");

			nfcc.setNfcc_modelo(txtModelo.getText().trim());
			nfcc.setNfcc_serie(txtSerie.getText().trim());
			nfcc.setNfcc_numero_nota(txtNumeroNfe.getText().trim());
			nfcc.setFilia(filial.getFil_cod());

			boolean retorno = nfccDAO.alterar(nfcc);
			if (retorno == true) {
			    grafics = ICON_SUCCESS;
			    createNotification(Pos.BOTTOM_RIGHT, grafics, "Dados inseridos com sucesso!");
			    notification.show();
			} else {
			    grafics = ICON_ERROR;
			    createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao inserir os dados.");
			    notification.show();
			}
			recuperarCamposAlteracao();
		    }
		    tabPaneGeral.getTabs().remove(tabGeralCadastro);
		    tabPaneGeral.getTabs().add(tabGeralConsulta);
		    tabelaCartaCorrecao.getSelectionModel().select(0);
		    tabelaCartaCorrecao.getFocusModel().focus(0);
		    carregarTabelaNotasCC();
		    btnSalvar.setDisable(true);
		    btnCancelar.setDisable(true);
		    btnExcluir.setDisable(true);
		    btnIncluir.setDisable(false);
		    btnAlterar.setDisable(true);
		    this.nfCC = null;
		}else{
		    Alert a = new Alert(Alert.AlertType.INFORMATION);
			a.setTitle("Acom Informa");
			a.setHeaderText("Texto de correção precisa ser maior que 15 caracteres.");
			a.setResizable(false);
			a.initModality(Modality.WINDOW_MODAL);
			a.show();
		}
	    }

	} catch (Exception ex) {
	    System.out.println(ex.getMessage());
	}
    }

    @FXML
    private void cliqueBtnPesquisar(ActionEvent event) {
	carregarTabelaNotasCC();
    }

    @FXML
    private void cliqueBtnEnviarEmail(){
	email(operacao);
    }
    
    private void email(String tipo) {
	try {
	    Stage dialogStage = new Stage();
	    NotaFiscalCartaCorrecao nfcc = tabelaCartaCorrecao.getSelectionModel().getSelectedItem();
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLConfirmacaoDadosEmail.fxml"));
	    loader.setController(new FXMLConfirmacaoDadosEmailController(nfcc, dialogStage, tipo));
	    Parent page;
	    page = (Parent) loader.load();
	    dialogStage.initModality(Modality.APPLICATION_MODAL);
	    dialogStage.setMaximized(false);
	    dialogStage.setTitle("Confirmação de dados do e-mail do " + tipo);
	    dialogStage.setResizable(false);
	    Scene scene = new Scene(page);
	    dialogStage.setScene(scene);
	    dialogStage.showAndWait();
	    
	} catch (IOException ex) {
	    Logger.getLogger(FXMLNotaFiscal.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
    
    public static void changeScreen(String string, NotaFiscal notaFiscal) {
	nf = notaFiscal;
    }

    private void preencherCampos() {
	if (operacao.equals("incluindo")) {
	    String sequencialString;
	    if (nfccDAO.getNumeroSequencial(nf.getNfeChaveAcesso().trim()) == null || nfccDAO.getNumeroSequencial(nf.getNfeChaveAcesso().trim()).equals("null")) {
		sequencialString = "0";
	    } else {
		sequencialString = nfccDAO.getNumeroSequencial(nf.getNfeChaveAcesso().trim());
	    }
	    int sequencialInt = Integer.parseInt(sequencialString) + 1;
	    txtIdNfe.setText(nf.getNf_numero());
	    txtSerie.setText(nf.getNf_serie());
	    txtNumeroNfe.setText(nf.getNf_nrnota());
	    txtFilial.setText(nf.getFil_cod());
	    txtTotalNota.setText(currencyFormat.format(nf.getNf_totnota()));
	    txtChaveAcesso.setText(nf.getNfeChaveAcesso());
	    txtNome.setText(nf.getPes_nome45());
	    txtModelo.setText(nf.getNf_modelo());

	    txtNumeroSequencial.setText(String.valueOf(sequencialInt));
	    txtData.setText(getdata());
	    txtHora.setText(getHora());
	    txtCorrecao.requestFocus();
	} else if (operacao.equals("alterando")) {
	    txtIdNfe.setText(nf.getNf_numero());
	    txtSerie.setText(nf.getNf_serie());
	    txtNumeroNfe.setText(nf.getNf_nrnota());
	    txtFilial.setText(nf.getFil_cod());
	    txtTotalNota.setText(currencyFormat.format(nf.getNf_totnota()));
	    txtChaveAcesso.setText(nf.getNfeChaveAcesso());
	    txtNome.setText(nf.getPes_nome45());

	    txtNumeroSequencial.setText(nfCC.getNfcc_item());
	    txtData.setText(Util.formatarData(nfCC.getNfcc_data().toString()));
	    txtHora.setText(Util.formatarHora(nfCC.getNfcc_hora().toString()));
	    txtCorrecao.requestFocus();
	}
    }

    private String getdata() {
	LocalDate hoje = LocalDate.now();
	DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	return hoje.format(formatador); //08/04/2014
    }

    private String getHora() {
	SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
	return sdf.format(hora);
    }

    private void limparCamposInclusão() {
	txtIdNfe.setText("");
	txtSerie.setText("");
	txtNumeroNfe.setText("");
	txtFilial.setText("");
	txtTotalNota.setText("");
	txtChaveAcesso.setText("");
	txtNome.setText("");
	txtNumeroSequencial.setText("");
	txtData.setText("");
	txtHora.setText("");
	txtCorrecao.setText("");
	txtModelo.setText("");
    }

    private void recuperarCamposAlteracao() {

    }

    private void createNotification(Pos position, Node grafics, String text) {
	notification = Notifications.create()
		.title("Acom Informa\n\n")
		.text(" " + text)
		.graphic(grafics)
		.hideAfter(Duration.seconds(5))
		.position(position);
    }

    private void desabilitarBotesControleTabela() {
	btnEnviar.setDisable(true);
	btnCancelar.setDisable(true);
	btnVizualizarXml.setDisable(true);
	btnEnviarEmail.setDisable(true);
    }

    @FXML
    void onKeyReleased(KeyEvent event) {
	selecionarLinha();
    }

    @FXML
    void onMouseClicked(MouseEvent event) {
	selecionarLinha();
    }

    /**
     * *****************************************************************************
     */
    private static ArrayList<OnChangeScreen> listeners = new ArrayList<>();

    private void habilitarCampos() {
	txtIdNfe.setDisable(false);
	txtSerie.setDisable(false);
	txtNumeroNfe.setDisable(false);
	txtFilial.setDisable(false);
	txtModelo.setDisable(false);
	txtTotalNota.setDisable(false);
	txtChaveAcesso.setDisable(false);
	txtNome.setDisable(false);
	txtNumeroSequencial.setDisable(false);
	txtData.setDisable(false);
	txtHora.setDisable(false);
	txtCorrecao.setDisable(false);
	txtCorrecao.setEditable(true);
	txtCorrecao.positionCaret(txtCorrecao.getText().length());
	txtCorrecao.requestFocus();
	if (operacao.equals("incluindo")) {
	    btnImportar.setDisable(false);
	} else if (operacao.equals("alterando")) {
	    btnImportar.setDisable(true);
	}
    }

    private void desabilitarCampos() {
	txtIdNfe.setDisable(true);
	txtSerie.setDisable(true);
	txtNumeroNfe.setDisable(true);
	txtFilial.setDisable(true);
	txtModelo.setDisable(true);
	txtTotalNota.setDisable(true);
	txtChaveAcesso.setDisable(true);
	txtNome.setDisable(true);
	txtNumeroSequencial.setDisable(true);
	txtData.setDisable(true);
	txtHora.setDisable(true);
	txtCorrecao.setDisable(true);
	if (operacao.equals("incluindo")) {
	    btnImportar.setDisable(false);
	} else if (operacao.equals("alterando")) {
	    btnImportar.setDisable(true);
	}
    }

    private boolean validarCCe() {
	if (txtCorrecao.getText().length() <= 15) {
	    return false;
	} else {
	    return true;
	}
    }

    public static interface OnChangeScreen {

	void onScreenChanged(String string, NotaFiscal notaFiscal);
    }

    public static void addOnChangeScreenListeners(OnChangeScreen newListeners) {
	listeners.add(newListeners);
    }

    private static void notifyAllListeners(String string, NotaFiscal notaFiscal) {
	for (OnChangeScreen l : listeners) {
	    l.onScreenChanged(string, notaFiscal);
	}
    }

}
