/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.nota.NFeConfigAcom;
import acom.gerenciadornfe.modelo.entidades.Parametros;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import acom.gerenciadornfe.util.Util;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author allison
 */
public class FXMLConfigSqLiteController implements Initializable {

    /*variaveis de componentes da tela*/
    @FXML
    private TableView<Parametros> tabelaParametros;

    @FXML
    private TableColumn<Parametros, String> colunaNome;

    @FXML
    private TableColumn<Parametros, String> colunaDescricao;

    @FXML
    private TableColumn<Parametros, String> colunaValor;

    @FXML
    private Button btnGerarCertificado;

    @FXML
    private Button btnFechar;

    @FXML
    private Label lblMsg;
    
    @FXML
    private Label labelRetorno;
    
    @FXML
    private ProgressIndicator barraProgresso;
    

    private Stage dialogStage;

    /*variaveis de uso*/
    ParametrosDAO parametrosDAO;

    private List<Parametros> listaParametros;

    private ObservableList<Parametros> obsParametros;

    private Task processoGerarCadeia;
    
    public FXMLConfigSqLiteController(Stage dialogStage) {
	this.dialogStage = dialogStage;
    }

    public FXMLConfigSqLiteController() {
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
	// TODO
	parametrosDAO = new ParametrosDAO();
	lblMsg.setText("");
	carregarTabela();
	tabelaParametros.getSelectionModel().selectedItemProperty().addListener((observador, valorAntigo, valor) -> selecionarItem(valor));
	/*adicionando um textfield a celula da tabela*/
	colunaValor.setCellFactory(TextFieldTableCell.forTableColumn());
	edicaoTabela();
    }

    public void carregarTabela() {
	colunaNome.setCellValueFactory(new PropertyValueFactory<>("par_nome"));
	colunaDescricao.setCellValueFactory(new PropertyValueFactory<>("par_descricao"));
	colunaValor.setCellValueFactory(new PropertyValueFactory<>("par_valor"));
	listaParametros = parametrosDAO.listarParametros();
	obsParametros = FXCollections.observableArrayList(listaParametros);
	tabelaParametros.setItems(obsParametros);
    }

    private void selecionarItem(Parametros parametro) {
	if (parametro != null) {
	    System.out.println("Parametro selecionado-----> " + parametro.getPar_nome() + " - " + "Valor-----> " + parametro.getPar_valor());
	}
    }

    private void selecionarItemTabelaCertificado(Parametros parametro) {
	if (parametro != null) {
	    System.out.println("Parametro selecionado-----> " + parametro.getPar_nome() + " - " + "Valor-----> " + parametro.getPar_valor());
	}
    }

//    @FXML
//    private void setOnEditCommit(CellEditEvent<Parametros, String> valor) {
//        Parametros parametros = tabelaParametros.getSelectionModel().getSelectedItem();
//        parametros.setPar_valor(valor.getNewValue().toString());
//        parametrosDAO.alterar(parametros);
//        carregarTabela();
//    }
    private void edicaoTabela() {
	colunaValor.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<Parametros, String>>() {
	    @Override
	    public void handle(TableColumn.CellEditEvent<Parametros, String> event) {
		Parametros parametros = tabelaParametros.getSelectionModel().getSelectedItem();
		parametros.setPar_valor(event.getNewValue().toString());
		boolean retorno = parametrosDAO.alterar(parametros);
		if (retorno == true) {
		    lblMsg.setText("Alterado com Sucesso!.");
		    lblMsg.setTextFill(Color.web("#33a546"));
		} else {
		    lblMsg.setText("Erro ao alterar.");
		    lblMsg.setTextFill(Color.web("#db4437"));
		}
		carregarTabela();
	    }
	});
    }

    @FXML
    private void cliqueBtnFechar() {
	Alert alert = new Alert(AlertType.WARNING);
	alert.setTitle("ATENÇÃO");
	alert.initModality(Modality.APPLICATION_MODAL);
	alert.setHeaderText("Aplicação será reiniciada.");
	alert.setContentText("A reinicialização será realizada automaticamente.\nCaso não ocorra, executar a aplicação.\n\nDeseja continuar?\n");

	Optional<ButtonType> result = alert.showAndWait();
	if (result.get() == ButtonType.OK) {
//            primaryStage.close();
//            Platform.runLater( () -> new ReloadApp().start( new Stage() ) );
	    Runnable runBeforeRestart = null;
	    try {
		Util.restartApplication(runBeforeRestart);
	    } catch (IOException ex) {
		Logger.getLogger(FXMLConfigSqLiteController.class.getName()).log(Level.SEVERE, null, ex);
	    }
	} else {
	    // ... user chose CANCEL or closed the dialog
	}
    }

    @FXML
    void cliqueBtnGerarCertificado(ActionEvent event) {
	gerarCadeia();
//	NFeConfigAcom config = new NFeConfigAcom();
//	boolean retorno = config.gerarCadeiaCertificado();
//	 while (true) {
//	    barraProgresso.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
//	    barraProgresso.progressProperty().unbind();
//	    lblMsg.setText("Aguarde, gerando cadeia de Certificados...");
//	    lblMsg.setTextFill(Color.web("#F0AD4E"));
//	    if (homologacao.exists() && producao.exists()) {
//		barraProgresso.setProgress(100);
//		if (retorno == true) {
//		    lblMsg.setText("Cadeia de Certificados Gerada com Sucesso!");
//		    lblMsg.setTextFill(Color.web("#33a546"));
//		} else {
//		    lblMsg.setText("Erro ao gerar a Cadeia de Certificados");
//		    lblMsg.setTextFill(Color.web("#db4437"));
//		}
//	    }
//	}
    }

    private void gerarCadeia() {
	processoGerarCadeia = new Task() {
	    @Override
	    protected Object call() throws Exception {
		apagarArquivos();
		while (verificarArquivos() == false) {
		    updateMessage("Gerando cadeia de certificados, aguarde...");
		    labelRetorno.setTextFill(Color.web("#0182AC"));
		    lblMsg.setTextFill(Color.web("#F0AD4E"));
		    NFeConfigAcom config = new NFeConfigAcom();
		    boolean retorno = config.gerarCadeiaCertificado();
		    if (retorno == true) {
			updateMessage("Cadeias de Certificado geradas com sucesso!");
			labelRetorno.setTextFill(Color.web("#33a546"));
			updateProgress(Double.MAX_VALUE, Double.MAX_VALUE);
		    } else {
			updateMessage("Erro ao gerar a Cadeia de Certificados");
			labelRetorno.setTextFill(Color.web("#db4437"));
			updateProgress(Double.MAX_VALUE, Double.MAX_VALUE);
		    }
		}
		return null;
	    }
	};
	barraProgresso.progressProperty().unbind();
	barraProgresso.progressProperty().bind(processoGerarCadeia.progressProperty());
	labelRetorno.textProperty().unbind();
	labelRetorno.textProperty().bind(processoGerarCadeia.messageProperty());
	Thread threadGeraCadeia = new Thread(processoGerarCadeia);
	threadGeraCadeia.setName("threadGeraCadeia");
	threadGeraCadeia.setDaemon(true);
	threadGeraCadeia.start();
    }
    
    private boolean verificarArquivos() {
	boolean retorno;
	String diretorioProjeto = parametrosDAO.getDiretorioProjeto();
	File homologacao = new File(diretorioProjeto + File.separator + "cadeia_certificados" + File.separator + "homologacao.cacerts");
	File producao = new File(diretorioProjeto + File.separator + "cadeia_certificados" + File.separator + "producao.cacerts");

	if (homologacao.exists() && producao.exists()) {
	    retorno = true;
	} else {
	    retorno = false;
	}
	return retorno;
    }
    
    private void apagarArquivos() {
	String diretorioProjeto = parametrosDAO.getDiretorioProjeto();
	File homologacao = new File(diretorioProjeto + File.separator + "cadeia_certificados" + File.separator + "homologacao.cacerts");
	File producao = new File(diretorioProjeto + File.separator + "cadeia_certificados" + File.separator + "producao.cacerts");
	if (homologacao.exists()) {
	    homologacao.delete();
	}
	if (producao.exists()) {
	    producao.delete();
	}
    }
}
