/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.HttpResponse;
import acom.gerenciadornfe.controle.nota.NFDanfeReport;
import acom.gerenciadornfe.controle.nota.NotaControler;
import acom.gerenciadornfe.modelo.entidades.NotaFiscal;
import acom.gerenciadornfe.modelo.entidades.NotaFiscalCartaCorrecao;
import acom.gerenciadornfe.modelo.entidades.PostEmail;
import acom.gerenciadornfe.modelo.repositorio.ParametrosFiscalDAO;
import acom.gerenciadornfe.util.Util;
import com.fincatto.documentofiscal.nfe400.classes.evento.cartacorrecao.NFEnviaEventoCartaCorrecao;
import com.fincatto.documentofiscal.nfe400.classes.nota.NFNotaProcessada;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.xml.sax.SAXException;

/**
 * FXML Controller class
 *
 * @author Allison
 */
public class FXMLConfirmacaoDadosEmailController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TextField txtDestinatario;

    @FXML
    private TextField txtAssunto;

    @FXML
    private TextArea txtMsg;

    @FXML
    private Button btnEnviar;

    @FXML
    private Button btnFechar;

    @FXML
    private Label labelMsgRetorno;

    private NotaControler nControler;
    private ParametrosFiscalDAO pfDao;

    private NotaFiscal notaFiscal;
    NotaFiscalCartaCorrecao notafiscalCc;
    private Stage stage;
    private String tipo;

    public FXMLConfirmacaoDadosEmailController(NotaFiscal notaFiscal, Stage stage, String tipo) {
	this.notaFiscal = notaFiscal;
	this.stage = stage;
	this.tipo = tipo;
    }
    
    public FXMLConfirmacaoDadosEmailController(NotaFiscalCartaCorrecao notaFiscalCc, Stage stage, String tipo) {
	this.notafiscalCc = notaFiscalCc;
	this.stage = stage;
	this.tipo = tipo;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	// TODO
	nControler = new NotaControler();
	pfDao = new ParametrosFiscalDAO();
	preencherCampos();
	txtDestinatario.requestFocus();
    }

    @FXML
    void cliqueBtnEnviar(ActionEvent event) {
	try {
	    if (notaFiscal != null) {
		PostEmail postEmail = new PostEmail();
		String documento = Util.getNumeros(notaFiscal.getFil_cnpj());
		String chave = notaFiscal.getNfeChaveAcesso();
		NFNotaProcessada np = nControler.carregarXmlNFNotaProcessada(notaFiscal.getNfeChaveAcesso(), nControler.formatarData(notaFiscal.getNf_data().toString()), "processadas", "_nfe");
		String xmlBase64;
		xmlBase64 = Base64.getEncoder().encodeToString(np.toString().getBytes("utf-8"));
		postEmail.setNf_documento(documento);
		postEmail.setNf_chave(chave);
		postEmail.setNf_destinatario(txtDestinatario.getText().trim());
		postEmail.setNf_assunto(txtAssunto.getText().trim());
		postEmail.setNf_menssagem(txtMsg.getText().trim());
		postEmail.setNf_xml(xmlBase64);
		postEmail.setNf_danfe(gerarDanfePdf(notaFiscal, "E"));
		Gson gson = new Gson();
		String json = gson.toJson(postEmail);
		System.out.println("Json post E-mail...: " + json);
		String resposta = HttpResponse.sendEmail(json, "normal");
		if (resposta.equals("finalizou")) {
		    labelMsgRetorno.setText("E-mail enviado com sucesso!");
		    labelMsgRetorno.setTextFill(Color.web("#28A745"));
		} else {
		    labelMsgRetorno.setText("Erro ao enviar o e-mail!");
		    labelMsgRetorno.setTextFill(Color.web("#DC3545"));
		}
	    } else if (notafiscalCc != null) {
		PostEmail postEmail = new PostEmail();
		String documento = Util.getNumeros(notafiscalCc.getNfcc_cnpj());
		String chave = notafiscalCc.getNfcc_chavenfe();
		File np = nControler.carregarXmlEventos(notafiscalCc.getNfcc_chavenfe(), nControler.formatarData(notafiscalCc.getNfcc_data().toString()), "eventos", "cc", "_cc");
		String xmlBase64;
		xmlBase64 = Base64.getEncoder().encodeToString(np.toString().getBytes("utf-8"));
		postEmail.setNf_documento(documento);
		postEmail.setNf_chave(chave);
		postEmail.setNf_destinatario(txtDestinatario.getText().trim());
		postEmail.setNf_assunto(txtAssunto.getText().trim());
		postEmail.setNf_menssagem(txtMsg.getText().trim());
		postEmail.setNf_xml(xmlBase64);
		postEmail.setNf_danfe(gerarDanfePdfCC(notafiscalCc));
		Gson gson = new Gson();
		String json = gson.toJson(postEmail);
		System.out.println("Json post E-mail...: " + json);
		String resposta = HttpResponse.sendEmail(json, "normal");
		if (resposta.equals("finalizou")) {
		    labelMsgRetorno.setText("E-mail enviado com sucesso!");
		    labelMsgRetorno.setTextFill(Color.web("#28A745"));
		} else {
		    labelMsgRetorno.setText("Erro ao enviar o e-mail!");
		    labelMsgRetorno.setTextFill(Color.web("#DC3545"));
		}
	    }
	} catch (UnsupportedEncodingException ex) {
	    Logger.getLogger(FXMLConfirmacaoDadosEmailController.class.getName()).log(Level.SEVERE, null, ex);
	} catch (Exception ex) {
	    Logger.getLogger(FXMLConfirmacaoDadosEmailController.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    @FXML
    void cliqueBtnFechar(ActionEvent event) {
	this.stage.close();
    }

    //	try {
//	    PostEmail postEmail = new PostEmail();
//	    NotaFiscal nf = tabelaNotaFiscal.getSelectionModel().getSelectedItem();
//	    String destinatario = "";
//	    if (tipo.equals("cliente")) {
//		destinatario = nf.getPes_email();
//
//	    } else if (tipo.equals("contador")) {
//		destinatario = nf.getFil_emailcont();
//	    }
//	    String documento = Util.getNumeros(nf.getFil_cnpj());
//	    String chave = nf.getNfeChaveAcesso();
//	    NFNotaProcessada np = nControler.carregarXmlNFNotaProcessada(nf.getNfeChaveAcesso(), nControler.formatarData(nf.getNf_data().toString()), "processadas", "_nfe");
//	    String xmlBase64 = Base64.getEncoder().encodeToString(np.toString().getBytes("utf-8"));
//	    postEmail.setNf_documento(documento);
//	    postEmail.setNf_chave(chave);
//	    postEmail.setNf_destinatario(destinatario);
//	    postEmail.setNf_xml(xmlBase64);
//	    Gson gson = new Gson();
//	    String json = gson.toJson(postEmail);
//	    System.out.println("Json post E-mail...: " + json);
//	    String resposta = HttpResponse.sendEmail(json);
//	    if (resposta.equals("finalizou")) {
//		Alert alert = new Alert(AlertType.INFORMATION);
//		alert.setTitle("Acom Informa");
//		alert.setHeaderText("Sucesso!");
//		alert.setContentText("E-mail enviado com sucesso!");
//		alert.showAndWait();
//	    } else {
//		Alert alert = new Alert(AlertType.ERROR);
//		alert.setTitle("Acom Informa");
//		alert.setHeaderText("Erro!");
//		alert.setContentText("Erro ao tentar enviar o e-mail.");
//		alert.showAndWait();
//	    }
//	} catch (UnsupportedEncodingException ex) {
//	    Logger.getLogger(FXMLNotaFiscal.class.getName()).log(Level.SEVERE, null, ex);
//	} catch (Exception ex) {
//	    Logger.getLogger(FXMLNotaFiscal.class.getName()).log(Level.SEVERE, null, ex);
//	}
    private void preencherCampos() {
	if (notaFiscal != null) {
	    if (tipo.equals("cliente")) {
		txtDestinatario.setText(notaFiscal.getPes_email());
	    } else if (tipo.equals("contador")) {
		txtDestinatario.setText(notaFiscal.getFil_emailcont());
	    }
	    txtAssunto.setText("NF-e Numero:  " + notaFiscal.getNf_nrnota() + " - " + notaFiscal.getFil_nome());
	    txtMsg.setText("Mensagem gerada automaticamente. Nao responder.");
	} else {
	    txtDestinatario.setText("");
	    txtAssunto.setText("Chave da Carta de Correção:  " + notafiscalCc.getNfcc_chavenfe());
	    txtMsg.setText("Mensagem gerada automaticamente. Nao responder.");
	}
    }

    private String gerarDanfePdf(NotaFiscal nf, String status) {
	String arquivo64 = "";
	try {
	    String diretorioLogoXml = "";
	    String diretorioDanfe = "";
	    String pasta = "processadas";
	    String sufixo = "_nfe";
	    if (File.separator.equals("\\")) {
		diretorioLogoXml = pfDao.getDiretorioLogomarcaDanfe();
		diretorioDanfe = pfDao.getDiretorioPdf();
	    } else {
		diretorioLogoXml = "";
	    }

	    NFNotaProcessada np = nControler.carregarXmlNFNotaProcessada(nf.getNfeChaveAcesso().trim(), nControler.formatarData(nf.getNf_data().toString()), pasta, sufixo);
	    NFDanfeReport danfe = new NFDanfeReport(np);
	    JasperPrint print = null;
	    if (diretorioLogoXml.equals("")) {
		print = danfe.createJasperPrintNFe(null);
	    } else {
		print = danfe.createJasperPrintNFe(Util.getBytesLogo(diretorioLogoXml), status);
	    }

	    /*Código usado para salvar o documento no computador sem abrir caixa de diálogo*/
	    File arquivo = new File(diretorioDanfe + File.separator + nf.getNfeChaveAcesso() + "_nfe.pdf");
	    if (arquivo.exists()) {
		arquivo64 = converterBase64Binario(arquivo);
	    } else {
		JasperExportManager.exportReportToPdfFile(print, diretorioDanfe + File.separator + nf.getNfeChaveAcesso() + "_nfe.pdf");
		arquivo = new File(diretorioDanfe + File.separator + nf.getNfeChaveAcesso() + "_nfe.pdf");
		arquivo64 = converterBase64Binario(arquivo);
	    }

	} catch (JRException ex) {
	    System.out.println(ex.getMessage());
	} catch (IOException ex) {
	    Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
	} catch (ParserConfigurationException ex) {
	    Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
	} catch (SAXException ex) {
	    Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
	}
	return arquivo64;
    }

    private String gerarDanfePdfCC(NotaFiscalCartaCorrecao nf) {
	String arquivo64 = "";
	try {
	    String diretorioLogoXml = "";
	    String diretorioDanfe = "";
	    String pasta = "eventos";
	    String subpasta = "cc";
	    String sufixo = "_cc";
	    
	    
	    String array[] = formatarData(nf.getNfcc_data().toString()).split("/");
	    String mes = array[1];//dd/mm/yyyy
	    String ano = array[2];
	    
	    if (File.separator.equals("\\")) {
		diretorioLogoXml = pfDao.getDiretorioLogomarcaDanfe();
		diretorioDanfe = pfDao.getDiretorioPdf();
	    } else {
		diretorioLogoXml = "";
	    }

	    String diretorio = diretorioDanfe + File.separator + pasta + File.separator + subpasta + File.separator + mes + File.separator + ano + File.separator + nf.getNfcc_chavenfe() + sufixo + ".xml";
	    
	    nControler.gerarPdfCC(nf.getNfcc_chavenfe(), nf.getNfcc_item(), diretorio);

	    File arquivo = new File(diretorioDanfe + File.separator + notafiscalCc.getNfcc_chavenfe() + "_nfe.pdf");
	    arquivo64 = converterBase64Binario(arquivo);

	} catch (Exception ex) {
	    Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
	}
	return arquivo64;
    }
    
    private byte[] carregarArquivo(File file) throws IOException {
	byte[] bytes;
	try (InputStream is = new FileInputStream(file)) {
	    long length = file.length();
	    if (length > Integer.MAX_VALUE) {
		throw new IOException("File to large " + file.getName());
	    }
	    bytes = new byte[(int) length];
	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
		offset += numRead;
	    }
	    if (offset < bytes.length) {
		throw new IOException("Não foi possível ler completamente o arquivo " + file.getName());
	    }
	}
	return bytes;
    }

    private String converterBase64Binario(File file)
	    throws IOException {

	byte[] bytes = carregarArquivo(file);
	byte[] encoded = Base64.getEncoder().encode(bytes);
	String encodedString = new String(encoded);

	return encodedString;
    }

    public String formatarData(String dataSefaz) {
	String retorno = "";
	String data = "";
	SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
	try {
	    String dataWsSefaz = dataSefaz;
	    if (dataWsSefaz.contains("T")) {
		data = dataWsSefaz.substring(0, dataWsSefaz.indexOf("T"));
	    }
	    data = dataWsSefaz;
	    Date date = formato.parse(data);
	    formato.applyPattern("dd/MM/yyyy");
	    String dataFormatada = formato.format(date);
	    retorno = dataFormatada;
	} catch (ParseException ex) {
	    Logger.getLogger(NotaControler.class.getName()).log(Level.SEVERE, null, ex);
	}
	return retorno;
    }
    
}
