/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.modelo.entidades.Filial;
import acom.gerenciadornfe.modelo.entidades.Parametros;
import acom.gerenciadornfe.modelo.repositorio.FilialDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import acom.gerenciadornfe.util.Util;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Allison
 */
public class FXMLEmpresaController implements Initializable {

    @FXML
    private TableView<Filial> tabelaEmpresa;

    @FXML
    private TableColumn<?, ?> colunaCodigo;

    @FXML
    private TableColumn<?, ?> colunaRazaoSocial;

    @FXML
    private TableColumn<?, ?> colunaCnpj;

    @FXML
    private Button btnSelecionar;

    @FXML
    private Button btnCancelar;

    private Stage dialogStage;

    private FilialDAO filialDAO;
    private ParametrosDAO pDAO;

    private List<Filial> listaEmpresas;

    private ObservableList<Filial> obsEmpresas;

    /**
     * Initializes the controller class.
     */
    public FXMLEmpresaController() {

    }

    public FXMLEmpresaController(Stage dialogstage) {
	this.dialogStage = dialogstage;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	// TODO
	filialDAO = new FilialDAO();
	pDAO = new ParametrosDAO();
	carregarTabela();
    }

    private void carregarTabela() {
	colunaCodigo.setCellValueFactory(new PropertyValueFactory<>("fil_cod"));
	colunaRazaoSocial.setCellValueFactory(new PropertyValueFactory<>("fil_nreduz"));
	colunaCnpj.setCellValueFactory(new PropertyValueFactory<>("Fil_cnpj"));
	listaEmpresas = filialDAO.listarFilialResumido();
	obsEmpresas = FXCollections.observableArrayList(listaEmpresas);
	tabelaEmpresa.setItems(obsEmpresas);
    }

    @FXML
    void cliqueBtnCancelar(ActionEvent event) {
	
    }

    @FXML
    void cliqueBtnSelecionar(ActionEvent event) {
	Alert alert = new Alert(Alert.AlertType.WARNING);
	alert.setTitle("ATENÇÃO");
	alert.initModality(Modality.APPLICATION_MODAL);
	alert.setHeaderText("Aplicação será reiniciada.");
	alert.setContentText("A reinicialização será realizada automaticamente.\nCaso não ocorra, executar a aplicação.\n\nDeseja continuar?\n");

	Optional<ButtonType> result = alert.showAndWait();
	if (result.get() == ButtonType.OK) {
	    Filial f = new Filial();
	    f = tabelaEmpresa.getSelectionModel().getSelectedItem();
	    pDAO.alterar(new Parametros("par_filial", "filial selecionada", "=" + f.getFil_cod()));
	    Runnable runBeforeRestart = null;
	    try {
		Util.restartApplication(runBeforeRestart);
	    } catch (IOException ex) {
		Logger.getLogger(FXMLConfigSqLiteController.class.getName()).log(Level.SEVERE, null, ex);
	    }
	} else {
	    // ... user chose CANCEL or closed the dialog
	}
    }

}
