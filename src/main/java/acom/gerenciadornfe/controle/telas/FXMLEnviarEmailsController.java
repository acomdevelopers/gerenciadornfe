/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.HttpResponse;
import acom.gerenciadornfe.controle.nota.NotaControler;
import acom.gerenciadornfe.modelo.entidades.Filial;
import acom.gerenciadornfe.modelo.entidades.NotaFiscal;
import acom.gerenciadornfe.modelo.entidades.ParametrosFiscal;
import acom.gerenciadornfe.modelo.entidades.PostEmail;
import acom.gerenciadornfe.modelo.repositorio.FilialDAO;
import acom.gerenciadornfe.modelo.repositorio.NotaFiscalDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosFiscalDAO;
import acom.gerenciadornfe.util.Compactador;
import acom.gerenciadornfe.util.Util;
import com.fincatto.documentofiscal.nfe400.classes.nota.NFNotaProcessada;
import com.google.gson.Gson;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Allison
 */
public class FXMLEnviarEmailsController implements Initializable {

    @FXML
    private DatePicker dpDataInicial;

    @FXML
    private DatePicker dpDataFinal;

    @FXML
    private TextField txtEmailDestinatario;

    @FXML
    private ComboBox<String> boxSituacao;

    @FXML
    private Button btnPesquisar;

    @FXML
    private Button btnEnviar;

    @FXML
    private Button btnCancelar;

    @FXML
    private TableView<NotaFiscal> tabelaNotasFiscais;

    @FXML
    private TableColumn<?, ?> colunaNumero;

    @FXML
    private TableColumn<?, ?> colunaSerie;

    @FXML
    private TableColumn<NotaFiscal, Date> colunaEmissao;

    @FXML
    private TableColumn<?, ?> colunaChave;

    @FXML
    private TableColumn<NotaFiscal, Double> colunaValor;

    @FXML
    private TableColumn<NotaFiscal, String> colunaSituacao;

    @FXML
    private Label labelMsgRetorno;

    private ToggleGroup grupoBotoes = new ToggleGroup();

    private Stage stage;

    private ObservableList<String> situacoes;

    private List<NotaFiscal> listaNotas;

    private ObservableList<NotaFiscal> obsListNotas;

    private NotaFiscalDAO nfDAO = new NotaFiscalDAO();

    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private Filial filial;
    private FilialDAO fDAO;
    private ParametrosDAO pDAO;
    private ParametrosFiscalDAO pfDao;

    private NotaControler notaControler;

    public FXMLEnviarEmailsController() {
    }

    public FXMLEnviarEmailsController(Stage stage) {
	this.stage = stage;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
	// TODO
	fDAO = new FilialDAO();
	pDAO = new ParametrosDAO();
	pfDao = new ParametrosFiscalDAO();
	notaControler = new NotaControler();
	preencherFilial();
	situacoes = FXCollections.observableArrayList("Enviados", "Cancelados", "Denegados");
	boxSituacao.setItems(situacoes);
	boxSituacao.getSelectionModel().select(0);
	dpDataInicial.setValue(LocalDate.parse(getdata(), dateFormatter));
	dpDataFinal.setValue(LocalDate.parse(getdata(), dateFormatter));
	carregarTabela(getCamposPesquisa());
    }

    @FXML
    void cliqueBtnCancelar(ActionEvent event) {
	this.stage.close();
    }

    @FXML
    void cliqueBtnEnviar(ActionEvent event) throws Exception {
	System.out.println("botão enviar clicado...");
	btnEnviar.setDisable(false);
	boolean retorno = prepararCompactacao();
	if (retorno == true) {
	    boolean retorno2 = gerarZip();
	    if (retorno2 == true) {
		try {
		    File file = new File(pfDao.getDiretorioXml() + File.separator + "documentofiscal" + File.separator + "xmls.zip");
		    String zipEncoded = "";
		    zipEncoded = converterBase64Binario(file);
		    System.out.println(zipEncoded.toString());
		    PostEmail email = new PostEmail();
		    email.setNf_assunto("Xmls " + boxSituacao.getValue() + " da empresa " + filial.getFil_nome().trim().toUpperCase());
		    email.setNf_destinatario(txtEmailDestinatario.getText().trim());
		    email.setNf_empresa(filial.getFil_nome().trim().toUpperCase());
		    email.setNf_menssagem_zip("Xmls " + boxSituacao.getValue() + " do periodo " + Util.formatarData(dpDataInicial.getValue().toString()) + " ate " + Util.formatarData(dpDataFinal.getValue().toString()) + " da empresa " + filial.getFil_nome().trim().toUpperCase() + ".");
		    email.setNf_zip(zipEncoded);
		    Gson gson = new Gson();
		    String json = gson.toJson(email);
		    System.out.println("Json post E-mail...: " + json);
		    String resposta = HttpResponse.sendEmail(json, "zip");
		    if (resposta.equals("finalizou")) {
			labelMsgRetorno.setText("E-mail enviado com sucesso!");
			labelMsgRetorno.setTextFill(Color.web("#28A745"));
		    } else {
			labelMsgRetorno.setText("Erro ao enviar o e-mail!");
			labelMsgRetorno.setTextFill(Color.web("#DC3545"));
		    }
		} catch (IOException ex) {
		    Logger.getLogger(FXMLEnviarEmailsController.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }
	}
    }

    @FXML
    void cliqueBtnPesquisar(ActionEvent event) {
	String filtros = getCamposPesquisa();
	tabelaNotasFiscais.setItems(null);
	carregarTabela(filtros);
	if (tabelaNotasFiscais.getItems().size() > 0) {
	    btnEnviar.setDisable(false);
	} else {
	    btnEnviar.setDisable(true);
	}
    }

    private void preencherFilial() {
	filial = new Filial();
	System.out.println(pDAO.getParFilial().replace("=", ""));
	filial = fDAO.getDadosFilial(pDAO.getParFilial().replace("=", ""));
	txtEmailDestinatario.setText(filial.getFil_emailcont().trim());
    }

    private String getdata() {
	LocalDate hoje = LocalDate.now();
	DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	return hoje.format(formatador); //08/04/2014
    }

    private void carregarTabela(String filtros) {
	listaNotas = nfDAO.listar(filtros);
	obsListNotas = FXCollections.observableArrayList(listaNotas);
	tabelaNotasFiscais.setItems(obsListNotas);
	colunaNumero.setCellValueFactory(new PropertyValueFactory<>("nf_nrnota"));
	colunaSerie.setCellValueFactory(new PropertyValueFactory<>("nf_serie"));
	colunaEmissao.setCellValueFactory(new PropertyValueFactory<>("nf_dtemissao"));
	colunaChave.setCellValueFactory(new PropertyValueFactory<>("nfeChaveAcesso"));
	colunaValor.setCellValueFactory(new PropertyValueFactory<>("nf_totnota"));
	colunaSituacao.setCellValueFactory(new PropertyValueFactory<>("nfestatus"));
	funcoesTabela();
    }

    private String getCamposPesquisa() {
	LocalDate dtInicio = dpDataInicial.getValue();
	LocalDate dtFinal = dpDataFinal.getValue();
	RadioButton button = (RadioButton) grupoBotoes.getSelectedToggle();
	String situacao = "";
	if (boxSituacao.getSelectionModel().getSelectedItem().equals("Enviados")) {
	    situacao = "E";
	} else if (boxSituacao.getSelectionModel().getSelectedItem().equals("Cancelados")) {
	    situacao = "C";
	} else if (boxSituacao.getSelectionModel().getSelectedItem().equals("Denegados")) {
	    situacao = "D";
	}
	String where = "AND fil_cod = '" + filial.getFil_cod() + "' and nf_dtemissao >= CONVERT(DATETIME,'" + dtInicio + "',102) AND nf_dtemissao <= CONVERT(DATETIME,'" + dtFinal + "',102) and nfestatus = '" + situacao + "'";
	System.out.println(where);

	return where;
    }

    private void funcoesTabela() {

	colunaEmissao.setCellFactory(column -> {
	    TableCell<NotaFiscal, Date> cell = new TableCell<NotaFiscal, Date>() {
		private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		@Override
		protected void updateItem(Date item, boolean empty) {
		    super.updateItem(item, empty);
		    if (empty) {
			setText(null);
		    } else {
			setText(format.format(item));
		    }
		}
	    };

	    return cell;
	});

	colunaValor.setCellFactory(tc -> {
	    TableCell<NotaFiscal, Double> cell = new TableCell<NotaFiscal, Double>() {
		private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

		@Override
		protected void updateItem(Double price, boolean empty) {
		    super.updateItem(price, empty);
		    if (empty) {
			setText(null);
		    } else {
			setText(currencyFormat.format(price));
		    }
		}
	    };
	    cell.setStyle("-fx-alignment: CENTER_RIGHT;");
	    return cell;
	});

	colunaSituacao.setCellFactory(tc -> new TableCell<NotaFiscal, String>() {
	    @Override
	    protected void updateItem(String item, boolean empty) {
		super.updateItem(item, empty);
		TableRow linha = getTableRow();
		if (empty) {
		    setText(null);
		    linha.setStyle("-fx-background-color: #FFF;"); //branco
		} else {
		    if (item.equals("C")) {
			setText("Cancelada");
			linha.setStyle("-fx-background-color: #F8D7DA;"); //vermelho
		    } else if (item.equals("E")) {
			setText("Enviada");
			linha.setStyle("-fx-background-color: #D4EDDA;"); //verde
//                        setTextFill(Color.web("#721c24"));
		    } else if (item.equals("D")) {
			setText("Denegada");
			linha.setStyle("-fx-background-color: #FFDAB9;"); //roxo
//                        setTextFill(Color.web("#721c24"));
		    }
		}
	    }
	});
    }

    private boolean prepararCompactacao() {
	boolean retorno = false;
	String situacao = boxSituacao.getSelectionModel().getSelectedItem();
	String pasta = "";
	String sufixo = "";
	String diretorioArquivos = "";

	diretorioArquivos = pfDao.getDiretorioXml() + File.separator + "documentofiscal";

	if (situacao.equals("Enviados")) {
	    pasta = "processadas";
	    sufixo = "_nfe";
	} else if (situacao.equals("Cancelados")) {
	    pasta = "canceladas";
	    sufixo = "_canc";
	} else if (situacao.equals("Denegados")) {
	    pasta = "denegadas";
	    sufixo = "_den";
	}

	List<NotaFiscal> listaNotas = new ArrayList<>();
	try {
	    listaNotas = tabelaNotasFiscais.getItems();
	    for (NotaFiscal nf : listaNotas) {
		String array[] = notaControler.formatarData(nf.getNf_data().toString()).split("/");
		String mes = array[1];//dd//mm/yyy
		String ano = array[2];
		NFNotaProcessada np = notaControler.carregarXmlNFNotaProcessada(nf.getNfeChaveAcesso().trim(), notaControler.formatarData(nf.getNf_data().toString()), pasta, sufixo);
		File origem = new File(diretorioArquivos + File.separator + ano + File.separator + mes + File.separator + pasta + File.separator + nf.getNfeChaveAcesso() + sufixo + ".xml");
		File destino = new File(diretorioArquivos + File.separator + "xmls" + File.separator + nf.getNfeChaveAcesso() + sufixo + ".xml");
		if (!destino.exists()) {
		    destino.mkdirs();
		}
		Util.copiarArquivo(origem, destino);
	    }
	    retorno = true;
	} catch (IOException ex) {
	    Logger.getLogger(FXMLEnviarEmailsController.class.getName()).log(Level.SEVERE, null, ex);
	}
	return retorno;
    }

    private byte[] carregarArquivo(File file) throws IOException {
	byte[] bytes;
	try (InputStream is = new FileInputStream(file)) {
	    long length = file.length();
	    if (length > Integer.MAX_VALUE) {
		throw new IOException("File to large " + file.getName());
	    }
	    bytes = new byte[(int) length];
	    int offset = 0;
	    int numRead = 0;
	    while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
		offset += numRead;
	    }
	    if (offset < bytes.length) {
		throw new IOException("Não foi possível ler completamente o arquivo " + file.getName());
	    }
	}
	return bytes;
    }

    private String converterBase64Binario(File file)
	    throws IOException {

	byte[] bytes = carregarArquivo(file);
	byte[] encoded = Base64.getEncoder().encode(bytes);
	String encodedString = new String(encoded);

	return encodedString;
    }

    private boolean gerarZip() {
	Compactador c = new Compactador();
	boolean retorno = false;
	try {
	    c.ziparPasta(pfDao.getDiretorioXml() + File.separator + "documentofiscal" + File.separatorChar + "xmls", pfDao.getDiretorioXml() + File.separator + "documentofiscal" + File.separator + "xmls.zip");
	    retorno = true;
	} catch (Exception ex) {
	    retorno = false;
	    Logger.getLogger(FXMLEnviarEmailsController.class.getName()).log(Level.SEVERE, null, ex);
	}
	return retorno;
    }

}
