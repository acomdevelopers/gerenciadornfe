/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.nota.NFeConfigAcom;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author allison
 */
public class FXMLGerarCadeiaCertificadoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML
    private ProgressIndicator progressBar;

    @FXML
    private Label txtLabel;
    
    private Stage dialogStage;
    
    private NFeConfigAcom config;
    
    @FXML
    private Button btnGerar;
    
    public FXMLGerarCadeiaCertificadoController(Stage stage){
        this.dialogStage = stage;
    }
    
    public FXMLGerarCadeiaCertificadoController(){
        
    }
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        boolean retorno = config.gerarCadeiaCertificado();
//        if(retorno == true){
//            System.out.println("chamou");
//        }else{
//            System.out.println("erro");
//        }
    }    
    
    @FXML
    void cliqueBtnGerar(ActionEvent event) {
        progressBar.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        config = new NFeConfigAcom();
        boolean retorno = config.gerarCadeiaCertificado();
        if(retorno == true){
            System.out.println("terminou");
        }else{
            System.out.println("deu merda");
        }
    }
    
}
