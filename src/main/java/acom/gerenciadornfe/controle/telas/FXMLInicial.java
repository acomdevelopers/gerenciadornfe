package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.nota.NFeConfigAcom;
import acom.gerenciadornfe.modelo.entidades.Filial;
import acom.gerenciadornfe.modelo.enuns.EnumImagens;
import acom.gerenciadornfe.modelo.repositorio.FilialDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import acom.gerenciadornfe.util.MaskFieldUtil;
import com.fincatto.documentofiscal.DFModelo;
import com.fincatto.documentofiscal.DFUnidadeFederativa;
import com.fincatto.documentofiscal.nfe400.classes.statusservico.consulta.NFStatusServicoConsultaRetorno;
import com.fincatto.documentofiscal.nfe400.webservices.WSFacade;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import net.sourceforge.barbecue.Main;

public class FXMLInicial implements Initializable {

    private Stage stage;

    public Stage getStage() {
	return stage;
    }

    public void setStage(Stage stage) {
	this.stage = stage;
    }

    @FXML
    private Label label;

    @FXML
    private TreeView<String> treeViewAtualizacoes;

    @FXML
    private TreeView<String> treeViewUltilitarios;

    @FXML
    private TreeView<String> treeViewConfig;

    @FXML
    private TreeView<String> treeViewAjuda;

    @FXML
    private Label labelSefaz;

    @FXML
    private TabPane tabPane = new TabPane();

    @FXML
    private Label labelVersao;

    @FXML
    private Label labelInstancia;

    @FXML
    private Label labelHost;

    @FXML
    private Label labelTerminal;

    @FXML
    private Label labelDataHora;

    @FXML
    private Label labelEmpresa;

    @FXML
    private Label labelCertificado;

    @FXML
    private Label labelValidadeCertificado;

    @FXML
    private Tab tabVeiculos;

    private Tab tabNotaFiscal;

    private NFeConfigAcom nfeConfig = new NFeConfigAcom();

    private ParametrosDAO pDAO;

    private FilialDAO fDAO;

    private Main main;
//    
//    @FXML
//    private Tab tabInicio;

    private SingleSelectionModel<Tab> selecionModel;

    private static final String stringVersao = "3.0.29";

    private Filial filial;

    @FXML
    private Button btnTeste;

    private static final SimpleDateFormat dataHoraFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	// TODO
//      nfeConfig.gerarCadeiaCertificado();
	pDAO = new ParametrosDAO();
	fDAO = new FilialDAO();
	filial = new Filial();
	filial = fDAO.getDadosFilial(pDAO.getParFilial().replace("=", ""));
	initTreeViewAtualizacoes();
	initTreeViewConfig();
	initTreeViewUtilitario();
	selecionModel = tabPane.getSelectionModel();
//	statusDoServico();
	carregarBarraStatus();
    }

    private void initTreeViewAtualizacoes() {
	TreeItem<String> root = new TreeItem<>();

//        TreeItem<String> cadastros = new TreeItem<>("Cadastros", new ImageView(EnumImagens.ICON_CADASTROS.getIcon()));
//        TreeItem<String> cad_veiculo = new TreeItem<>("Veículos", new ImageView(EnumImagens.ICON_CAD_VEICULOS.getIcon()));
	TreeItem<String> nf = new TreeItem<>("Nota Fiscal", new ImageView(EnumImagens.ICON_NFE.getIcon()));
	TreeItem<String> gerenciador = new TreeItem<>("Gerenciador Nfe", new ImageView(EnumImagens.ICON_GERENCIADOR.getIcon()));
//        TreeItem<String> mdf = new TreeItem<>("MDF", new ImageView(EnumImagens.ICON_MDF.getIcon()));
	TreeItem<String> inutilizaNota = new TreeItem<>("Inutilizador", new ImageView(EnumImagens.ICON_INUTILIZACAO.getIcon()));
	TreeItem<String> cc = new TreeItem<>("Carta de Correção", new ImageView(EnumImagens.ICON_CC.getIcon()));
        
        TreeItem<String> md = new TreeItem<>("Manifesto do Destinatário", new ImageView(EnumImagens.ICON_MANIFESTO_DESTINATARIO.getIcon()));

	root.getChildren().addAll(nf);

//        cadastros.getChildren().addAll(cad_veiculo);
	nf.getChildren().addAll(gerenciador, cc, inutilizaNota, md);
	treeViewAtualizacoes.setRoot(root);

	treeViewAtualizacoes.setOnMouseClicked(new EventHandler<MouseEvent>() {
	    @Override
	    public void handle(MouseEvent event) {
		try {
		    if (treeViewAtualizacoes.getSelectionModel().getSelectedItem().getValue() != "Cadastros" || treeViewAtualizacoes.getSelectionModel().getSelectedItem().getValue() != "Nota Fiscal") {
			openTabs(treeViewAtualizacoes.getSelectionModel().getSelectedItem().getValue());
		    }
		} catch (Exception e) {
		}
	    }
	});
    }

    private void initTreeViewConfig() {
	TreeItem<String> root = new TreeItem<>();

	TreeItem<String> config = new TreeItem<>("Configurações", new ImageView(EnumImagens.ICON_CONFIG.getIcon()));
	TreeItem<String> selecionarFilial = new TreeItem<>("Empresa", new ImageView(EnumImagens.ICON_EMPRESA.getIcon()));

	root.getChildren().addAll(config, selecionarFilial);

	treeViewConfig.setRoot(root);
	treeViewConfig.setOnMouseClicked(new EventHandler<MouseEvent>() {
	    @Override
	    public void handle(MouseEvent event) {
		try {
		    System.out.println(treeViewConfig.getSelectionModel().getSelectedItem().getValue());
//                    if (treeViewConfig.getSelectionModel().getSelectedItem().getValue() != "Configurações") {
		    if (treeViewConfig.getSelectionModel().getSelectedItem().getValue().equals("Configurações")) {
			Stage dialogStage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLConfigSqLite.fxml"));
			loader.setController(new FXMLConfigSqLiteController(dialogStage));
			Parent page = (Parent) loader.load();
			dialogStage.initModality(Modality.APPLICATION_MODAL);
			dialogStage.setTitle("Configuraçoes do Gerenciador NF-e");
			dialogStage.setResizable(false);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.show();
//                        }else if(treeViewConfig.getSelectionModel().getSelectedItem().getValue().equals("Cadeia de Certificados")){
//                            Stage dialogStage = new Stage();
//                            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLGerarCadeiaCertificado.fxml"));
//                            loader.setController(new FXMLGerarCadeiaCertificadoController(dialogStage));
//                            Parent page = (Parent) loader.load();
//                            dialogStage.initModality(Modality.APPLICATION_MODAL);
//                            dialogStage.setTitle("Gerar Cadeia de Certificados");
//                            dialogStage.setResizable(false);
//                            Scene scene = new Scene(page);
//                            dialogStage.setScene(scene);
//                            dialogStage.show();
//                        }
		    } else if (treeViewConfig.getSelectionModel().getSelectedItem().getValue().equals("Empresa")) {
			Stage dialogStage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLEmpresa.fxml"));
			loader.setController(new FXMLEmpresaController(dialogStage));
			Parent page = (Parent) loader.load();
			dialogStage.initModality(Modality.APPLICATION_MODAL);
			dialogStage.setTitle("Selecionar Empresa");
			dialogStage.setResizable(false);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.show();
		    }
		} catch (Exception ex) {
		}
	    }
	});

    }

    private void initTreeViewUtilitario() {
	TreeItem<String> root = new TreeItem<>();
	TreeItem<String> statusServico = new TreeItem<>("Status do Serviço", new ImageView(EnumImagens.ICON_STATUS_SERVICO.getIcon()));

	TreeItem<String> emails = new TreeItem<>("E-mails", new ImageView(EnumImagens.ICON_EMAILS.getIcon()));

	root.getChildren().addAll(statusServico, emails);
	treeViewUltilitarios.setRoot(root);

	treeViewUltilitarios.setOnMouseClicked(new EventHandler<MouseEvent>() {
	    @Override
	    public void handle(MouseEvent event) {
		try {
		    if (treeViewUltilitarios.getSelectionModel().getSelectedItem().getValue().equals("E-mails")) {
			System.out.println("Entrou e-mails");
			Stage dialogStage = new Stage();
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLEnviarEmails.fxml"));
			loader.setController(new FXMLEnviarEmailsController(dialogStage));
			Parent page = (Parent) loader.load();
			dialogStage.initModality(Modality.APPLICATION_MODAL);
			dialogStage.setTitle("E-mails");
			dialogStage.setResizable(false);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.show();
		    } else if (treeViewUltilitarios.getSelectionModel().getSelectedItem().getValue().equals("Status do Serviço")) {
			openTabs(treeViewUltilitarios.getSelectionModel().getSelectedItem().getValue());
		    }
		} catch (Exception e) {
		}
	    }
	});

    }

    private void openTabs(String tab) {
	try {
	    ObservableList<Tab> tabs = tabPane.getTabs();
	    Tab aba = null;
	    boolean existe = false;
	    for (int index = 0; index < tabs.size(); index++) {
		if (tab.equals(tabs.get(index).getText())) {
		    aba = tabs.get(index);
		    existe = true;
		}
	    }
	    if (existe == true) {
		selecionModel.select(aba);
	    } else {
		Node node = null;
		if (tab.equals("Veículos")) {
		    tabVeiculos = new Tab();
		    tabVeiculos.setText(tab);
		    tabVeiculos.setClosable(true);
		    tabVeiculos.setId(tab);
		    node = (AnchorPane) FXMLLoader.load(getClass().getResource("/fxml/FXMLCadastroVeiculos.fxml"));
		    AnchorPane.setTopAnchor(node, 0.0);
		    AnchorPane.setRightAnchor(node, 0.0);
		    AnchorPane.setLeftAnchor(node, 0.0);
		    AnchorPane.setBottomAnchor(node, 0.0);
		    tabVeiculos.setContent(node);
		    tabPane.getTabs().add(tabVeiculos);
		    selecionModel.select(tabVeiculos);
		} else if (tab.equals("MDF")) {
		    Tab newTab = new Tab();
		    node = (AnchorPane) FXMLLoader.load(getClass().getResource("/fxml/FXMLManifesto.fxml"));
		    AnchorPane.setTopAnchor(node, 0.0);
		    AnchorPane.setRightAnchor(node, 0.0);
		    AnchorPane.setLeftAnchor(node, 0.0);
		    AnchorPane.setBottomAnchor(node, 0.0);
		    newTab.setContent(node);
		    tabPane.getTabs().add(newTab);
		    selecionModel.select(newTab);
		} else if (tab.equals("Gerenciador Nfe")) {
		    tabNotaFiscal = new Tab();
		    tabNotaFiscal.setText(tab);
		    tabNotaFiscal.setClosable(true);
		    tabNotaFiscal.setId(tab);
		    node = (AnchorPane) FXMLLoader.load(getClass().getResource("/fxml/FXMLNotaFiscal.fxml"));
		    AnchorPane.setTopAnchor(node, 0.0);
		    AnchorPane.setRightAnchor(node, 0.0);
		    AnchorPane.setLeftAnchor(node, 0.0);
		    AnchorPane.setBottomAnchor(node, 0.0);
		    tabNotaFiscal.setContent(node);
		    tabPane.getTabs().add(tabNotaFiscal);
		    selecionModel.select(tabNotaFiscal);
		} else if (tab.equals("Carta de Correção")) {
		    Tab newTab = new Tab("Carta de Correção");
		    node = (AnchorPane) FXMLLoader.load(getClass().getResource("/fxml/FXMLCartaCorrecao.fxml"));
		    AnchorPane.setTopAnchor(node, 0.0);
		    AnchorPane.setRightAnchor(node, 0.0);
		    AnchorPane.setLeftAnchor(node, 0.0);
		    AnchorPane.setBottomAnchor(node, 0.0);
		    newTab.setContent(node);
		    tabPane.getTabs().add(newTab);
		    selecionModel.select(newTab);
		} else if (tab.equals("Inutilizador")) {
		    Tab newTab = new Tab("Inutilizador");
		    node = (AnchorPane) FXMLLoader.load(getClass().getResource("/fxml/FXMLInutilizacao.fxml"));
		    AnchorPane.setTopAnchor(node, 0.0);
		    AnchorPane.setRightAnchor(node, 0.0);
		    AnchorPane.setLeftAnchor(node, 0.0);
		    AnchorPane.setBottomAnchor(node, 0.0);
		    newTab.setContent(node);
		    tabPane.getTabs().add(newTab);
		    selecionModel.select(newTab);
		} else if (tab.equals("Cadastros")) {
		    System.out.println("menu cadastro precionado");
		} else if (tab.equals("Nota Fiscal")) {
		    System.out.println("menu nota fiscal selecionado");
		} else if (tab.equals("Status do Serviço")) {
		    Tab newTab = new Tab("Status do Serviço");
		    node = (AnchorPane) FXMLLoader.load(getClass().getResource("/fxml/FXMLStatusServico.fxml"));
		    AnchorPane.setTopAnchor(node, 0.0);
		    AnchorPane.setRightAnchor(node, 0.0);
		    AnchorPane.setLeftAnchor(node, 0.0);
		    AnchorPane.setBottomAnchor(node, 0.0);
		    newTab.setContent(node);
		    tabPane.getTabs().add(newTab);
		    selecionModel.select(newTab);
		} else if(tab.equals("Manifesto do Destinatário")){
                    Tab newTab = new Tab("Manifesto do Destinatário");
		    node = (AnchorPane) FXMLLoader.load(getClass().getResource("/fxml/FXMLManifestoDestinatario.fxml"));
		    AnchorPane.setTopAnchor(node, 0.0);
		    AnchorPane.setRightAnchor(node, 0.0);
		    AnchorPane.setLeftAnchor(node, 0.0);
		    AnchorPane.setBottomAnchor(node, 0.0);
		    newTab.setContent(node);
		    tabPane.getTabs().add(newTab);
		    selecionModel.select(newTab);
                }
		System.out.println("tab vai ser berta");
	    }
	} catch (IOException ex) {
	    Logger.getLogger(FXMLInicial.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    private String statusDoServico() {
	String retornoString = "";
	/* --- VER O QUE FAZER COM ESSE IF LOGO ABAIXO DEPOIS --- */
//	    if (!Arrays.asList(NFRetornoStatus.CODIGO_102, NFRetornoStatus.CODIGO_563).contains(NFRetornoStatus.valueOfCodigo(retornoEventoInutilizacaoDados.getStatus()))) {
//		JOptionPane.showMessageDialog(null, "Nota enviada");
//
//		throw new Exception(String.format("NF[%s,%s] - Nao autorizado a inutilizacao da NF na sefaz: %s: %s", cnpjEmitente, numeroInicial, numeroFinal, retornoEventoInutilizacaoDados.getStatus(), retornoEventoInutilizacaoDados.getMotivo()));
//	    }
	try {
	    NFStatusServicoConsultaRetorno retorno = new WSFacade(nfeConfig).consultaStatus(DFUnidadeFederativa.PE, DFModelo.NFE);
	    System.out.println(retorno.getStatus());
	    System.out.println(retorno.getMotivo());
	    System.out.println(retorno.toString());
	    retornoString = retorno.getStatus() + " - " + retorno.getMotivo();
	    JOptionPane.showMessageDialog(null, "Estado: " + retorno.getUf() + " - " + retornoString);
	} catch (IOException ex) {
	    retornoString = "Consulta de status indisponível no momento!";
	    Logger.getLogger(FXMLInicial.class.getName()).log(Level.SEVERE, null, ex);
	} catch (KeyManagementException ex) {
	    retornoString = "Consulta de status indisponível no momento!";
	    Logger.getLogger(FXMLInicial.class.getName()).log(Level.SEVERE, null, ex);
	} catch (UnrecoverableKeyException ex) {
	    retornoString = "Consulta de status indisponível no momento!";
	    Logger.getLogger(FXMLInicial.class.getName()).log(Level.SEVERE, null, ex);
	} catch (KeyStoreException ex) {
	    retornoString = "Consulta de status indisponível no momento!";
	    Logger.getLogger(FXMLInicial.class.getName()).log(Level.SEVERE, null, ex);
	} catch (NoSuchAlgorithmException ex) {
	    retornoString = "Consulta de status indisponível no momento!";
	    Logger.getLogger(FXMLInicial.class.getName()).log(Level.SEVERE, null, ex);
	} catch (CertificateException ex) {
	    retornoString = "Consulta de status indisponível no momento!";
	    Logger.getLogger(FXMLInicial.class.getName()).log(Level.SEVERE, null, ex);
	} catch (Exception ex) {
	    retornoString = "Consulta de status indisponível no momento!";
	    Logger.getLogger(FXMLInicial.class.getName()).log(Level.SEVERE, null, ex);
	}
	return retornoString;
    }

    private void carregarBarraStatus() {
	String certificado = filial.getFil_certificado_serie();
	if (certificado.equals("A1")) {
	    X509Certificate cert = new NFeConfigAcom().getInfoA1();
	    try {
		labelVersao.setText(stringVersao);

		labelInstancia.setText(pDAO.getInstancia());
		labelHost.setText(pDAO.getHost());
		labelTerminal.setText(InetAddress.getLocalHost().getHostName());
		labelDataHora.setText(dateFormat.format(System.currentTimeMillis()));
		labelEmpresa.setText(filial.getFil_cod() + " - " + filial.getFil_nome());

		System.out.println(cert.getSubjectDN().getName());

		String subjectName[] = cert.getSubjectDN().getName().split(",");
		String nome[] = subjectName[0].split(":");

		String razaoSocial = nome[0].replace("CN=", "");
//                String cnpj = MaskFieldUtil.mascaraCpfCnpj(nome[1]);

//                labelCertificado.setText(razaoSocial + " - " + cnpj);
		labelCertificado.setText(razaoSocial);

		labelValidadeCertificado.setText(dataHoraFormat.format(cert.getNotAfter()));
	    } catch (UnknownHostException ex) {
		Logger.getLogger(FXMLInicial.class.getName()).log(Level.SEVERE, null, ex);
	    }
	} else if (certificado.equals("A3")) {
	    X509Certificate cert = new NFeConfigAcom().getInfoA3();
	    try {
		labelVersao.setText(stringVersao);
		labelInstancia.setText(pDAO.getInstancia());
		labelHost.setText(pDAO.getHost());
		labelTerminal.setText(InetAddress.getLocalHost().getHostName());
		labelDataHora.setText(dateFormat.format(System.currentTimeMillis()));
		labelEmpresa.setText(filial.getFil_cod() + " - " + filial.getFil_nome());

//System.out.println(cert.getSubjectDN().getName());
//
//		String subjectName[] = cert.getSubjectDN().getName().split(",");
//		String nome[] = subjectName[0].split(":");
//
//		String razaoSocial = nome[0].replace("CN=", "");
////                String cnpj = MaskFieldUtil.mascaraCpfCnpj(nome[1]);
//
////                labelCertificado.setText(razaoSocial + " - " + cnpj);
//		labelCertificado.setText(razaoSocial);
//
//		labelValidadeCertificado.setText(dataHoraFormat.format(cert.getNotAfter()));
	    } catch (UnknownHostException ex) {
		Logger.getLogger(FXMLInicial.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public Main getMain() {
	return main;
    }

    public void setMain(Main main) {
	this.main = main;
    }

}
