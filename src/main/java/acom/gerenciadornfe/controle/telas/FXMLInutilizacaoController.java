/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.nota.NotaControler;
import acom.gerenciadornfe.modelo.entidades.Filial;
import acom.gerenciadornfe.modelo.entidades.Inutilizacao;
import acom.gerenciadornfe.modelo.entidades.ObjetoRetornoCartaCorrecao;
import acom.gerenciadornfe.modelo.entidades.ObjetoRetornoTransmissao;
import acom.gerenciadornfe.modelo.enuns.EnumImagens;
import acom.gerenciadornfe.modelo.repositorio.FilialDAO;
import acom.gerenciadornfe.modelo.repositorio.InutilizacaoDAO;
import acom.gerenciadornfe.util.MaskFieldUtil;
import acom.gerenciadornfe.util.XmlFormatter;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author allison
 */
public class FXMLInutilizacaoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Button btnIncluir;

    @FXML
    private Button btnAlterar;

    @FXML
    private Button btnExcluir;

    @FXML
    private Button btnConsulta;

    @FXML
    private Button btnSalvar;

    @FXML
    private Button btnImprimir;

    @FXML
    private Button btnCancelar;

    @FXML
    private TabPane tabPaneGeral;

    @FXML
    private Tab tabGeralConsulta;

    @FXML
    private TableView<Inutilizacao> tabelaInutilizacao;

    @FXML
    private TableColumn<?, ?> colunaIdNota;

    @FXML
    private TableColumn<Inutilizacao, String> colunaFilial;

    @FXML
    private TableColumn<?, ?> colunaAno;

    @FXML
    private TableColumn<?, ?> colunaSerie;

    @FXML
    private TableColumn<Inutilizacao, String> colunaStatus;

    @FXML
    private TableColumn<?, ?> colunaNumeroInicial;

    @FXML
    private TableColumn<?, ?> colunaNumeroFinal;

    @FXML
    private TableColumn<?, ?> colunaProtocolo;

    @FXML
    private TableColumn<?, ?> colunaDataRecebimento;

    @FXML
    private TextField txtPesquisa;

    @FXML
    private Button btnPesquisar;

    @FXML
    private Button btnEnviar;

    @FXML
    private Button btnCancelarCc;

    @FXML
    private TextArea txtRetornoNfeTabela;

    @FXML
    private TextArea txtRetornoXmlTabela;

    @FXML
    private Tab tabGeralCadastro;

    @FXML
    private TextField txtIdSequencial;

    @FXML
    private TextField txtAno;

    @FXML
    private TextField txtSerie;

    @FXML
    private TextField txtNumeroNotaInicial;

    @FXML
    private TextField txtNumeroNotaFinal;

    @FXML
    private TextArea txtJustificativa;

    @FXML
    private ComboBox<Filial> comboEmitente;

    @FXML
    private AnchorPane painelMsg;

    @FXML
    private Label lblMsg;

    private String operacao = "";

    private List<Inutilizacao> listaInutilizadas;

    private ObservableList<Inutilizacao> obsListInutulizadas;
    
    private List<Filial> listaFiliais = new ArrayList();

    private ObservableList<Filial> observableListFiliais;

    private FilialDAO filDAO;

    private InutilizacaoDAO iDAO;

    private Inutilizacao inutilizacao;
    
    private Notifications notification;

    private Node grafics;
    private static final ImageView ICON_SUCCESS = new ImageView(EnumImagens.ICON_SUCCESS.getIcon());
    private static final ImageView ICON_ERROR = new ImageView(EnumImagens.ICON_ERROR.getIcon());
    
    private NotaControler notaControler;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btnCancelarCc.setVisible(false);
        notaControler = new NotaControler();
        filDAO = new FilialDAO();
        iDAO = new InutilizacaoDAO();tabPaneGeral.setStyle("-fx-open-tab-animation: NONE; -fx-close-tab-animation: NONE;"); // css que tira o efeito das abas ao trocar-las, dando a impressão que uma sobrepõe a outra.
        iniciarTela();
        carregarTabelaInutilizacao();
        funcoesTabela();
        carregarComboBoxFiliais();
    }

    private void iniciarTela() {
        tabPaneGeral.getTabs().remove(tabGeralCadastro);
        btnIncluir.setDisable(false);
        btnConsulta.setDisable(true);
        btnSalvar.setDisable(true);
        btnCancelar.setDisable(true);
        btnExcluir.setDisable(true);
        btnAlterar.setDisable(true);
        btnImprimir.setDisable(true);

        btnEnviar.setDisable(true);
        btnCancelarCc.setDisable(true);
        mascaras();
    }

    private void carregarTabelaInutilizacao() {
        try {
            colunaIdNota.setCellValueFactory(new PropertyValueFactory<>("Inu_id"));
            colunaStatus.setCellValueFactory(new PropertyValueFactory<>("Inu_enviada"));
            colunaFilial.setCellValueFactory(new PropertyValueFactory<>("Inu_filial"));
            colunaAno.setCellValueFactory(new PropertyValueFactory<>("Inu_ano"));
            colunaSerie.setCellValueFactory(new PropertyValueFactory<>("Inu_serie"));
            colunaNumeroInicial.setCellValueFactory(new PropertyValueFactory<>("Inu_numeronotainicial"));
            colunaNumeroFinal.setCellValueFactory(new PropertyValueFactory<>("Inu_numeronotafinal"));
            colunaProtocolo.setCellValueFactory(new PropertyValueFactory<>("Inu_numeroprotocolo"));
            colunaDataRecebimento.setCellValueFactory(new PropertyValueFactory<>("Inu_datahora"));

            listaInutilizadas = iDAO.listar();
            obsListInutulizadas = FXCollections.observableArrayList(listaInutilizadas);
            tabelaInutilizacao.setItems(obsListInutulizadas);

            tabelaInutilizacao.getSelectionModel().clearSelection();
            tabelaInutilizacao.refresh();
            desabilitarBotesControleTabela();
        } catch (Exception e) {
            System.out.println("erro ao carregar os dados");
        }
    }

    private void funcoesTabela() {
        colunaStatus.setCellFactory(tc -> new TableCell<Inutilizacao, String>() {
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                TableRow linha = getTableRow();
                if (empty) {
                    setText("");
                    linha.setStyle("-fx-background-color: #FFF;"); //branco
                } else {
                    if (item.equals("E")) {
                        setText("Enviada");
                        linha.setStyle("-fx-background-color: #D4EDDA;"); //verde
                    } else if (item.equals("N")) {
                        setText("Não Enviada");
                        linha.setStyle("-fx-background-color: #E2E3E5;"); //cinza
                    } else if (item.equals("R")) {
                        setText("Rejeitada");
                        linha.setStyle("-fx-background-color: #FFF3CD;"); //amarelo
                    }
                }
            }
        });
        
        colunaFilial.setCellFactory(tc -> new TableCell<Inutilizacao, String>() {
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                } else {
                    Filial f = filDAO.getFilial(item);
                    setText(f.getFil_cod() + " - " + f.getFil_nreduz());
                }
            }
        });

        tabelaInutilizacao.setRowFactory(tv -> {
            TableRow<Inutilizacao> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (!row.isEmpty())) {
                    Inutilizacao i = row.getItem();
                    if (!i.getInu_enviada().equals("E")) {
                        inutilizacaSelecionada(i);
                    } else {
                        Alert a = new Alert(Alert.AlertType.ERROR);
                        a.setTitle("Acom Informa");
                        a.setContentText("Carta de Correção enviada, não pode ser editada.");
                        a.setResizable(false);
                        a.initModality(Modality.WINDOW_MODAL);
                        a.show();
                    }
                }
            });
            return row;
        });

    }

    private void inutilizacaSelecionada(Inutilizacao inu) {
        tabPaneGeral.getTabs().add(tabGeralCadastro);
        tabPaneGeral.getTabs().remove(tabGeralConsulta);
        btnAlterar.setDisable(false);
        btnExcluir.setDisable(false);
        btnIncluir.setDisable(true);
        btnSalvar.setDisable(true);
        btnConsulta.setDisable(false);
        btnCancelar.setDisable(true);
        operacao = "alterando";
        this.inutilizacao = inu;
        txtIdSequencial.setText(String.valueOf(inu.getInu_id()));
        comboEmitente.setValue(filDAO.getFilial(inu.getInu_filial()));
        comboEmitente.getSelectionModel().select(0);
        txtAno.setText(inu.getInu_ano());
        txtSerie.setText(inu.getInu_serie());
        txtNumeroNotaInicial.setText(String.valueOf(inu.getInu_numeronotainicial()));
        txtNumeroNotaFinal.setText(String.valueOf(inu.getInu_numeronotafinal()));
        txtJustificativa.setText(inu.getInu_justificativa());
        desabilitarCampos();
    }

    private void selecionarLinha() {
        desabilitarBotesControleTabela();
        txtRetornoNfeTabela.setText("");
        txtRetornoXmlTabela.setText("");
        Inutilizacao inutilizacao = tabelaInutilizacao.getSelectionModel().getSelectedItem();
        if (inutilizacao == null) {
            System.out.println("nenhuma nota selecionada");
        } else {
            if (inutilizacao.getInu_enviada().equals("N") || inutilizacao.getInu_enviada() == null) {
                btnEnviar.setDisable(false);
            } else if (inutilizacao.getInu_enviada().equals("E")) {
//                btnVizualizarXml.setDisable(false);
            }
        }
    }

    private void desabilitarBotesControleTabela() {
        btnEnviar.setDisable(true);
        btnCancelar.setDisable(true);
    }

    @FXML
    void clickBtnAlterar(ActionEvent event) {
        habilitarCampos();
        btnAlterar.setDisable(true);
        btnCancelar.setDisable(false);
        btnSalvar.setDisable(false);
        btnConsulta.setDisable(true);
        btnExcluir.setDisable(true);
    }

    @FXML
    void clickBtnCancelar(ActionEvent event) {
        if (tabGeralCadastro.isSelected()) {
            if (operacao.equals("incluindo")) {
                operacao = "";
                this.inutilizacao = null;
                limparCampos();
                btnConsulta.setDisable(false);
                btnSalvar.setDisable(true);
                btnCancelar.setDisable(true);
                btnIncluir.setDisable(false);
                tabPaneGeral.getTabs().remove(tabGeralCadastro);
                tabPaneGeral.getTabs().add(tabGeralConsulta);
                tabelaInutilizacao.getSelectionModel().select(0);
                tabelaInutilizacao.getFocusModel().focus(0);
                carregarTabelaInutilizacao();
            } else if (operacao.equals("alterando")) {
                recuperarCamposAlteracao();
                operacao = "";
                this.inutilizacao = null;
                btnSalvar.setDisable(true);
                btnCancelar.setDisable(false);
                btnIncluir.setDisable(false);
                btnExcluir.setDisable(true);
                btnAlterar.setDisable(true);
                tabPaneGeral.getTabs().remove(tabGeralCadastro);
                tabPaneGeral.getTabs().add(tabGeralConsulta);
                tabelaInutilizacao.getSelectionModel().select(0);
                tabelaInutilizacao.getFocusModel().focus(0);
                carregarTabelaInutilizacao();
            }
        }
    }

//    @FXML
//    void clickBtnCancelarCc(ActionEvent event) {
//
//    }

    @FXML
    void clickBtnEnviar(ActionEvent event) {

        ObjetoRetornoTransmissao retorno = notaControler.inutilizarNota(tabelaInutilizacao.getSelectionModel().getSelectedItem());
        if (retorno.isStatusTransmissao() == true) {
            txtRetornoNfeTabela.setText(retorno.getCodStatus() + " - " + retorno.getMotivo());
            XmlFormatter f = new XmlFormatter();
            if (retorno.getXmlRetorno() != null) {
                txtRetornoXmlTabela.setText(f.format(retorno.getXmlRetorno()));
            }
            carregarTabelaInutilizacao();
        } else {
            txtRetornoNfeTabela.setText(retorno.getCodStatus() + " - " + retorno.getMotivo());
            XmlFormatter f = new XmlFormatter();
            if (retorno.getXmlRetorno() != null) {
                txtRetornoXmlTabela.setText(f.format(retorno.getXmlRetorno()));
            }
            carregarTabelaInutilizacao();
        }
        
    }

    @FXML
    void clickBtnExcluir(ActionEvent event) {
        Inutilizacao inu = tabelaInutilizacao.getSelectionModel().getSelectedItem();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("ATENÇÃO");
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.setHeaderText("ATENÇÃO");
        alert.setContentText("Deseja realmente excluir este item?\n\n");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            boolean retorno = iDAO.excluir(inu);
            if (retorno == true) {
                grafics = ICON_SUCCESS;
                createNotification(Pos.BOTTOM_RIGHT, grafics, "Operação realizada com sucesso!");
            } else {
                grafics = ICON_ERROR;
                createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao realizar a operação");
            }
            tabPaneGeral.getTabs().remove(tabGeralCadastro);
            tabPaneGeral.getTabs().add(tabGeralConsulta);
            btnExcluir.setDisable(true);
            btnConsulta.setDisable(true);
            btnAlterar.setDisable(true);
            btnIncluir.setDisable(false);
            carregarTabelaInutilizacao();
        } else {
            alert.close();
        }
    }

    @FXML
    void clickBtnIncluir(ActionEvent event) {
        tabPaneGeral.getTabs().add(tabGeralCadastro);
        tabPaneGeral.getTabs().remove(tabGeralConsulta);
        habilitarCampos();
        operacao = "incluindo";
        txtIdSequencial.setText(String.valueOf(iDAO.ultimoId()));
        btnIncluir.setDisable(true);
        btnSalvar.setDisable(false);
        btnConsulta.setDisable(true);
        btnCancelar.setDisable(false);
    }

    @FXML
    void cliqueBtnConsulta(ActionEvent event) {
        tabPaneGeral.getTabs().add(tabGeralConsulta);
        tabPaneGeral.getTabs().remove(tabGeralCadastro);
        btnExcluir.setDisable(true);
        btnAlterar.setDisable(true);
        btnConsulta.setDisable(true);
        btnIncluir.setDisable(false);
    }

    @FXML
    void cliqueBtnSalvar(ActionEvent event) {
        try {
            Inutilizacao i = new Inutilizacao();
            i.setInu_id(Integer.parseInt(txtIdSequencial.getText().trim()));
            i.setInu_filial(comboEmitente.getValue().getFil_cod());
            i.setInu_ano(txtAno.getText().trim());
            i.setInu_serie(txtSerie.getText().trim());
            i.setInu_numeronotainicial(Integer.parseInt(txtNumeroNotaInicial.getText().trim()));
            i.setInu_numeronotafinal(Integer.parseInt(txtNumeroNotaFinal.getText().trim()));
            i.setInu_justificativa(txtJustificativa.getText().trim());
            i.setInu_enviada("N");
            i.setInu_numeroprotocolo("");
            i.setInu_datahora("");
            if (tabGeralCadastro.isSelected()) {
                if (operacao.equals("incluindo")) {
                    boolean retorno = iDAO.inserir(i);
                    if (retorno == true) {
                        grafics = ICON_SUCCESS;
                        createNotification(Pos.BOTTOM_RIGHT, grafics, "Dados inseridos com sucesso!");
                        notification.show();
                    } else {
                        grafics = ICON_ERROR;
                        createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao inserir os dados.");
                        notification.show();
                    }
                } else if (operacao.equals("alterando")) {
                    boolean retorno = iDAO.alterar(i);
                    if (retorno == true) {
                        grafics = ICON_SUCCESS;
                        createNotification(Pos.BOTTOM_RIGHT, grafics, "Dados alterados com sucesso!");
                        notification.show();
                    } else {
                        grafics = ICON_ERROR;
                        createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao alterados os dados.");
                        notification.show();
                    }
                }
                limparCampos();
                tabPaneGeral.getTabs().remove(tabGeralCadastro);
                tabPaneGeral.getTabs().add(tabGeralConsulta);
                tabelaInutilizacao.getSelectionModel().select(0);
                tabelaInutilizacao.getFocusModel().focus(0);
                carregarTabelaInutilizacao();
                btnSalvar.setDisable(true);
                btnCancelar.setDisable(true);
                btnExcluir.setDisable(true);
                btnIncluir.setDisable(false);
                btnAlterar.setDisable(true);
                this.inutilizacao = null;
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        
    }

    @FXML
    void onKeyReleased(KeyEvent event) {
        selecionarLinha();
    }

    @FXML
    void onMouseClicked(MouseEvent event) {
        selecionarLinha();
    }

    private void limparCampos() {
        txtIdSequencial.setText("");
        txtAno.setText("");
        txtSerie.setText("");
        txtNumeroNotaInicial.setText("");
        txtNumeroNotaFinal.setText("");
        txtJustificativa.setText("");
    }

    private void recuperarCamposAlteracao() {
        txtIdSequencial.setText(String.valueOf(inutilizacao.getInu_id()));
        comboEmitente.setValue(filDAO.getFilial(inutilizacao.getInu_filial()));
        txtAno.setText(inutilizacao.getInu_ano());
        txtSerie.setText(inutilizacao.getInu_serie());
        txtNumeroNotaInicial.setText(String.valueOf(inutilizacao.getInu_numeronotainicial()));
        txtNumeroNotaFinal.setText(String.valueOf(inutilizacao.getInu_numeronotafinal()));
        txtJustificativa.setText(inutilizacao.getInu_justificativa());
    }

    private void carregarComboBoxFiliais(){
       listaFiliais = filDAO.listar();
        observableListFiliais = FXCollections.observableArrayList(listaFiliais);
        comboEmitente.setItems(observableListFiliais); 
    }
    
    private void desabilitarCampos(){
        txtIdSequencial.setDisable(true);
        comboEmitente.setDisable(true);
        txtAno.setDisable(true);
        txtSerie.setDisable(true);
        txtNumeroNotaInicial.setDisable(true);
        txtNumeroNotaFinal.setDisable(true);
        txtJustificativa.setDisable(true);
    }
    
    private void habilitarCampos(){
        txtIdSequencial.setDisable(false);
        comboEmitente.setDisable(false);
        txtAno.setDisable(false);
        txtSerie.setDisable(false);
        txtNumeroNotaInicial.setDisable(false);
        txtNumeroNotaFinal.setDisable(false);
        txtJustificativa.setDisable(false);
    }
    
    private void mascaras(){
        MaskFieldUtil.numericFieldWhitLength(txtAno, 2);
        MaskFieldUtil.numericFieldWhitLength(txtSerie, 3);
        MaskFieldUtil.numericFieldWhitLength(txtNumeroNotaInicial, 10);
        MaskFieldUtil.numericFieldWhitLength(txtNumeroNotaFinal, 10);
    }
    
    private void createNotification(Pos position, Node grafics, String text) {
        notification = Notifications.create()
                .title("Acom Informa\n\n")
                .text(" " + text)
                .graphic(grafics)
                .hideAfter(Duration.seconds(5))
                .position(position);
    }
}
