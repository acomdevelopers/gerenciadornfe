/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author allison
 */
public class FXMLManifestoController implements Initializable {

    @FXML
    private AnchorPane ap;

    @FXML
    private AnchorPane painelMsg;

    @FXML
    private Label lblMsg;

    @FXML
    private TabPane tabPaneGeral;

    @FXML
    private Tab tabGeralConsulta;

    @FXML
    private TableView<?> tabelaVeiculos;

    @FXML
    private TableColumn<?, ?> colunaNumero;

    @FXML
    private TableColumn<?, ?> colunaSituacao;

    @FXML
    private TableColumn<?, ?> colunaEmissao;

    @FXML
    private TableColumn<?, ?> colunaUfOrigem;

    @FXML
    private TableColumn<?, ?> colunaUfDestino;

    @FXML
    private TableColumn<?, ?> colunaQtdNfe;

    @FXML
    private TableColumn<?, ?> colunaValorCarga;

    @FXML
    private TableColumn<?, ?> colunaUnidadeCarga;

    @FXML
    private TableColumn<?, ?> colunaQtdCarga;

    @FXML
    private TableColumn<?, ?> colunaPlaca;

    @FXML
    private TableColumn<?, ?> colunaChaveAcesso;

    @FXML
    private ComboBox<?> comboCampos;

    @FXML
    private TextField txtPesquisa;

    @FXML
    private Button btnPesquisar;

    @FXML
    private Button btnLimpar;

    @FXML
    private ComboBox<?> comboFilial;

    @FXML
    private Button btnGerar;

    @FXML
    private Button btnEnviar;

    @FXML
    private Button btnConsultar;

    @FXML
    private Button btnCancelaMdf;

    @FXML
    private Button btnEncerrar;

    @FXML
    private Button btnVizualizarXml;

    @FXML
    private Tab tabGeralCadastro;

    @FXML
    private TextField txtNumero;

    @FXML
    private ComboBox<?> comboOrigem;

    @FXML
    private ComboBox<?> comboDestino;

    @FXML
    private ComboBox<?> comboVeiculo;

    @FXML
    private TextField txtPlaca;

    @FXML
    private ComboBox<?> comboMotorista;

    @FXML
    private ComboBox<?> comboReboque;

    @FXML
    private TextField txtCfMotorista;

    @FXML
    private TextField txtChaveNfe;

    @FXML
    private TextField txtEmissao;

    @FXML
    private TextField txtQtdNfs;

    @FXML
    private TextField txtTotalCargaKg;

    @FXML
    private TextField txtValorCarga;

    @FXML
    private TableView<?> tabelaNfs;

    @FXML
    private TableColumn<?, ?> colunaNome;

    @FXML
    private TableColumn<?, ?> colunaUF;

    @FXML
    private TableColumn<?, ?> colunaValor;

    @FXML
    private TableColumn<?, ?> colunaNfLiquido;

    @FXML
    private TableColumn<?, ?> colunaMdfi_Chnfe;

    @FXML
    private TableColumn<?, ?> colunaCidade;

    @FXML
    private Button btnIncluir;

    @FXML
    private Button btnAlterar;

    @FXML
    private Button btnExcluir;

    @FXML
    private Button btnConsulta;

    @FXML
    private Button btnSalvar;

    @FXML
    private Button btnImprimir;

    @FXML
    private Button btnCancelar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lblMsg.setText("");
    }

    @FXML
    void clickBtnAlterar(ActionEvent event) {

    }

    @FXML
    void clickBtnCancelar(ActionEvent event) {

    }

    @FXML
    void clickBtnCancelarMdf(ActionEvent event) {

    }

    @FXML
    void clickBtnConsultar(ActionEvent event) {

    }

    @FXML
    void clickBtnEncerrar(ActionEvent event) {

    }

    @FXML
    void clickBtnEnviar(ActionEvent event) {

    }

    @FXML
    void clickBtnExcluir(ActionEvent event) {

    }

    @FXML
    void clickBtnGerar(ActionEvent event) {

    }

    @FXML
    void clickBtnIncluir(ActionEvent event) {

    }

    @FXML
    void clickBtnVizualizarXML(ActionEvent event) {

    }

    @FXML
    void cliqueBtnConsulta(ActionEvent event) {

    }

    @FXML
    void cliqueBtnSalvar(ActionEvent event) {

    }

}
