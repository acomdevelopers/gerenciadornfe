/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.nota.NotaControler;
import acom.gerenciadornfe.modelo.entidades.Filial;
import acom.gerenciadornfe.modelo.entidades.NFDestinatario;
import acom.gerenciadornfe.modelo.entidades.NotaFiscal;
import acom.gerenciadornfe.modelo.entidades.ParametroCancelamento;
import acom.gerenciadornfe.modelo.repositorio.FilialDAO;
import acom.gerenciadornfe.modelo.repositorio.NFDestinatarioDAO;
import acom.gerenciadornfe.modelo.repositorio.NotaFiscalDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Allison
 */
public class FXMLManifestoDestinatarioController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private AnchorPane painelMsg;

    @FXML
    private Label lblMsg;

    @FXML
    private Button btnObterNotas;

    @FXML
    private Button btnManifestar;

    @FXML
    private Button btnDownload;

    @FXML
    private Button btnDanfe;

    @FXML
    private Button btnPdf;

    @FXML
    private TabPane tabPaneGeral;

    @FXML
    private Tab tabGeralConsulta;

    @FXML
    private TableView<?> tabelaManifestoDestinatario;

    @FXML
    private TableColumn<?, ?> colunaChkBox;

    @FXML
    private TableColumn<?, ?> colunaDataEmissao;

    @FXML
    private TableColumn<?, ?> colunaNumero;

    @FXML
    private TableColumn<?, ?> colunaSerie;

    @FXML
    private TableColumn<?, ?> colunaNsu;

    @FXML
    private TableColumn<?, ?> colunaChave;

    @FXML
    private TableColumn<?, ?> colunaCnpj;

    @FXML
    private TableColumn<?, ?> colunaIe;

    @FXML
    private TableColumn<?, ?> colunaNome;

    @FXML
    private TableColumn<?, ?> colunaValor;

    @FXML
    private TableColumn<?, ?> colunaTipo;

    @FXML
    private TableColumn<?, ?> colunaStatus;

    @FXML
    private Button btnPesquisar;

    @FXML
    private DatePicker dpPeriodoInicio;

    @FXML
    private DatePicker dpPeriodoFim;

    @FXML
    private RadioButton radioTodas;

    @FXML
    private RadioButton radioBaixadas;

    @FXML
    private Label labelFilial;

    @FXML
    private RadioButton radioManifestadas;

    @FXML
    private TableView<?> tabelaHistorico;

    @FXML
    private TableColumn<?, ?> colunaDataHoraHistorico;

    @FXML
    private TableColumn<?, ?> colunaHistorico;

    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private ToggleGroup grupoBotoes = new ToggleGroup();

    private Filial filial;
    private String operacao = "";

    private FilialDAO fDAO;
    private ParametrosDAO pDAO;
    private NFDestinatarioDAO nfdDAO = new NFDestinatarioDAO();
    private FilialDAO filDAO = new FilialDAO();

    private NotaControler nControler;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	System.out.println("chamou aba nova");

	dpPeriodoInicio.setValue(LocalDate.parse(getdata(), dateFormatter));
	dpPeriodoFim.setValue(LocalDate.parse(getdata(), dateFormatter));
	lblMsg.setText("");
	nfdDAO = new NFDestinatarioDAO();
	nControler = new NotaControler();
	fDAO = new FilialDAO();
	pDAO = new ParametrosDAO();
	filial = new Filial();
	filial = fDAO.getFilial(pDAO.getParFilial().replace("=", ""));
	labelFilial.setText(filial.getFil_cod() + " - " + filial.getFil_nreduz());

	iniciarTela();

    }

    @FXML
    void clickBtnDanfe(ActionEvent event) {

    }

    @FXML
    void clickBtnDownload(ActionEvent event) {

    }

    @FXML
    void clickBtnManifestar(ActionEvent event) {

    }

    @FXML
    void clickBtnObterNotas(ActionEvent event) {
	try {
	    Stage dialogStage = new Stage();
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLObterNotas.fxml"));
	    loader.setController(new FXMLObterNotasController(dialogStage, filial));
	    Parent page = (Parent) loader.load();
	    dialogStage.initModality(Modality.APPLICATION_MODAL);
	    dialogStage.setTitle("Selecionar Empresa");
	    dialogStage.setResizable(false);
	    Scene scene = new Scene(page);
	    dialogStage.setScene(scene);
	    dialogStage.show();
	} catch (IOException ex) {
	    Logger.getLogger(FXMLManifestoDestinatarioController.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    @FXML
    void cliqueBtnPdf(ActionEvent event) {

    }

    @FXML
    void cliqueBtnPesquisar(ActionEvent event) {

    }

    @FXML
    void keyReleased(KeyEvent event) {

    }

    @FXML
    void mouseClique(MouseEvent event) {

    }

    /*metodos de uso da classe*/
    private void iniciarTela() {

	radioTodas.setToggleGroup(grupoBotoes);
	radioBaixadas.setToggleGroup(grupoBotoes);
	radioManifestadas.setToggleGroup(grupoBotoes);

	btnManifestar.setDisable(true);
	btnDownload.setDisable(true);
	btnDanfe.setDisable(true);
	btnPdf.setDisable(true);

	operacao = "consultando";
    }

    private String getdata() {
	LocalDate hoje = LocalDate.now();
	DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	return hoje.format(formatador); //08/04/2014
    }
}
