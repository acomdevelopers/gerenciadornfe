/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.nota.NotaControler;
import acom.gerenciadornfe.modelo.entidades.Filial;
import acom.gerenciadornfe.modelo.entidades.NotaFiscal;
import acom.gerenciadornfe.modelo.entidades.ObjetoRetornoEnviarMota;
import acom.gerenciadornfe.modelo.entidades.ParametroCancelamento;
import acom.gerenciadornfe.modelo.enuns.EnumImagens;
import acom.gerenciadornfe.modelo.repositorio.FilialDAO;
import acom.gerenciadornfe.modelo.repositorio.NotaFiscalDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import acom.gerenciadornfe.util.XmlFormatter;
import java.io.IOException;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * FXML Controller class
 *
 * @author allison
 */
public class FXMLNotaFiscal implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private AnchorPane ap;

    @FXML
    private AnchorPane painelMsg;

    @FXML
    private Label lblMsg;

    @FXML
    private Label labelFilial;

    @FXML
    private TabPane tabPaneGeral;

    @FXML
    private Tab tabGeralConsulta;

    @FXML
    private TableView<NotaFiscal> tabelaNotaFiscal;

    @FXML
    private TableColumn<NotaFiscal, CheckBox> colunaChkBox;

    @FXML
    private TableColumn<?, ?> colunaId;

    @FXML
    private TableColumn<NotaFiscal, String> colunaStatus;

//    @FXML
//    private TableColumn<NotaFiscal, Boolean> colunaEnviada;
//
//    @FXML
//    private TableColumn<NotaFiscal, Boolean> colunaCancelada;
    @FXML
    private TableColumn<NotaFiscal, String> colunaNatureza;

    @FXML
    private TableColumn<NotaFiscal, String> colunaDestinatario;

    @FXML
    private TableColumn<?, ?> colunaSerie;

    @FXML
    private TableColumn<?, ?> colunaNotaNumero;

    @FXML
    private TableColumn<?, ?> colunaEstado;

    @FXML
    private TableColumn<NotaFiscal, Date> colunaDataEmissao;

    @FXML
    private TableColumn<NotaFiscal, Date> colunaDataEntrada;

    @FXML
    private TableColumn<NotaFiscal, Double> colunaTotalNota;

    @FXML
    private TableColumn<?, ?> colunaTipoPagamento;

    @FXML
    private TableColumn<?, ?> colunaChaveAcesso;

    @FXML
    private TableColumn<NotaFiscal, Long> colunaRecibo;

    @FXML
    private TableColumn<NotaFiscal, Long> colunaProtocolo;

    @FXML
    private TableColumn<NotaFiscal, Date> colunarotocoloDataHora;

    @FXML
    private TableColumn<?, ?> colunaProtocoloCancelado;

    @FXML
    private TableColumn<?, ?> colunaFinalidade;

    @FXML
    private TableColumn<?, ?> colunaEmail;

    @FXML
    private TableColumn<?, ?> colunaCfop;

//    @FXML
//    private ComboBox<Filial> comboFilial;
    @FXML
    private DatePicker dpPeriodoInicio;

    @FXML
    private DatePicker dpPeriodoFim;

    @FXML
    private Button btnGerar;

    @FXML
    private Button btnEnviar;

    @FXML
    private Button btnConsultar;

    @FXML
    private Button btnCancelarNota;

    @FXML
    private Button btnConsultaNotaChave;

    @FXML
    private Button btnVizualizarXml;

    @FXML
    private Button btnIncluir;

    @FXML
    private Button btnAlterar;

    @FXML
    private Button btnExcluir;

    @FXML
    private Button btnConsulta;

    @FXML
    private Button btnSalvar;

    @FXML
    private Button btnImprimir;

    @FXML
    private Button btnCancelar;

    @FXML
    private Button btnEmailContador;

    @FXML
    private Button btnEmailCliente;

    @FXML
    private Button btnImprimirDanfe;

    @FXML
    private TextField txtCopiarChaveAcesso;

    @FXML
    private RadioButton radioEntrada;

    @FXML
    private RadioButton radioSaida;

    @FXML
    private TextArea txtRetornoNfe;

    @FXML
    private TextArea txtRetornoXml;


    /*variaveis de uso na tela*/
    private NotaFiscal nf = new NotaFiscal();

    private List<NotaFiscal> listaNotas;

    private ObservableList<NotaFiscal> obsListNotas;

    private NotaFiscalDAO nfDAO = new NotaFiscalDAO();
    private FilialDAO filDAO = new FilialDAO();

    private NotaControler nControler;

    private String operacao = "";

    private List<Filial> listaFiliais = new ArrayList();

    private ObservableList<Filial> observableListFiliais;

    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private ToggleGroup grupoBotoes = new ToggleGroup();

    private Notifications notification;

    private Node grafics;

    private Filial filial;
    private FilialDAO fDAO;
    private ParametrosDAO pDAO;

    private static final ImageView ICON_SUCCESS = new ImageView(EnumImagens.ICON_SUCCESS.getIcon());
    private static final ImageView ICON_ERROR = new ImageView(EnumImagens.ICON_ERROR.getIcon());

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	nfDAO = new NotaFiscalDAO();
	nControler = new NotaControler();
	fDAO = new FilialDAO();
	pDAO = new ParametrosDAO();
	filial = new Filial();
	filial = fDAO.getFilial(pDAO.getParFilial().replace("=", ""));
	lblMsg.setText("");
	tabPaneGeral.setStyle("-fx-open-tab-animation: NONE; -fx-close-tab-animation: NONE;"); // css que tira o efeito das abas ao trocar-las, dando a impressão que uma sobrepõe a outra.tabPaneGeral.setStyle("-fx-open-tab-animation: NONE; -fx-close-tab-animation: NONE;"); // css que tira o efeito das abas ao trocar-las, dando a impressão que uma sobrepõe a outra.
//	carregarBoxFiliais();
	radioSaida.setSelected(true);
//	comboFilial.getSelectionModel().select(0);
	iniciarTela();
	labelFilial.setText(filial.getFil_cod() + " - " + filial.getFil_nreduz());
	dpPeriodoInicio.setValue(LocalDate.parse(getdata(), dateFormatter));
	dpPeriodoFim.setValue(LocalDate.parse(getdata(), dateFormatter));
	tabelaNotaFiscal.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
	carregarTabelaNotas(getCamposPesquisa());
	funcoesTabela();
    }

    private void iniciarTela() {

	radioEntrada.setToggleGroup(grupoBotoes);
	radioSaida.setToggleGroup(grupoBotoes);

//        tabPaneGeral.getTabs().remove(tabGeralCadastro);
	btnIncluir.setDisable(true);
	btnConsulta.setDisable(true);
	btnSalvar.setDisable(true);
	btnCancelar.setDisable(true);
	btnExcluir.setDisable(true);
	btnAlterar.setDisable(true);
	btnImprimir.setDisable(true);
	operacao = "consultando";

	btnGerar.setDisable(true);
	btnEnviar.setDisable(true);
	btnConsultar.setDisable(true);
	btnCancelarNota.setDisable(true);
	btnConsultaNotaChave.setDisable(true);
	btnVizualizarXml.setDisable(true);
	btnEmailCliente.setDisable(true);
	btnEmailContador.setDisable(true);
	btnImprimirDanfe.setDisable(true);

    }

//    private void carregarBoxFiliais() {
//	listaFiliais = filDAO.listar();
//	observableListFiliais = FXCollections.observableArrayList(listaFiliais);
//	comboFilial.setItems(observableListFiliais);
//    }
    private void carregarTabelaNotas(String filtros) {
	try {
	    listaNotas = nfDAO.listar(filtros);
	    obsListNotas = FXCollections.observableArrayList();
	    for (NotaFiscal n : listaNotas) {
		CheckBox ch = new CheckBox();
		ch.setDisable(true);
		if (n.getNfestatus().equals("N") || n.getNfestatus().equals("G") || n.getNfestatus() == null) {
		    ch.setDisable(false);
		}
		obsListNotas.add(new NotaFiscal(ch, n));
	    }
	    tabelaNotaFiscal.setItems(obsListNotas);

	    colunaChkBox.setCellValueFactory(new PropertyValueFactory<NotaFiscal, CheckBox>("checkBox"));
	    colunaId.setCellValueFactory(new PropertyValueFactory<>("nf_numero"));
	    colunaSerie.setCellValueFactory(new PropertyValueFactory<>("nf_serie"));
	    colunaNotaNumero.setCellValueFactory(new PropertyValueFactory<>("nf_nrnota"));
	    colunaChaveAcesso.setCellValueFactory(new PropertyValueFactory<>("nfeChaveAcesso"));
	    colunaStatus.setCellValueFactory(new PropertyValueFactory<>("nfestatus"));
//            colunaEnviada.setCellValueFactory(new PropertyValueFactory<>("nfeEnviada"));
//            colunaCancelada.setCellValueFactory(new PropertyValueFactory<>("nfeCancelada"));
	    colunaNatureza.setCellValueFactory(new PropertyValueFactory<>("nf_tipomov"));
	    colunaDestinatario.setCellValueFactory(new PropertyValueFactory<>("nf_nome"));
	    colunaEstado.setCellValueFactory(new PropertyValueFactory<>("nf_uf"));
	    colunaDataEmissao.setCellValueFactory(new PropertyValueFactory<>("nf_dtemissao"));
	    colunaDataEntrada.setCellValueFactory(new PropertyValueFactory<>("nf_data"));
	    colunaTotalNota.setCellValueFactory(new PropertyValueFactory<>("nf_totnota"));
	    colunaTipoPagamento.setCellValueFactory(new PropertyValueFactory<>("nfeIndicadorFormaPagamento"));
	    colunaRecibo.setCellValueFactory(new PropertyValueFactory<>("nfeRecibo"));
	    colunaProtocolo.setCellValueFactory(new PropertyValueFactory<>("nfeProtocolo"));
	    colunarotocoloDataHora.setCellValueFactory(new PropertyValueFactory<>("nfeProtocoloDataHora"));
	    colunaProtocoloCancelado.setCellValueFactory(new PropertyValueFactory<>("nfeCanceladaProtocolo"));
	    colunaFinalidade.setCellValueFactory(new PropertyValueFactory<>("nfefinalidade"));
	    colunaEmail.setCellValueFactory(new PropertyValueFactory<>("nfeemail"));
	    colunaCfop.setCellValueFactory(new PropertyValueFactory<>("nf_cfop"));

	    tabelaNotaFiscal.getSelectionModel().clearSelection();

	    tabelaNotaFiscal.getColumns().get(0).setVisible(false);
	    tabelaNotaFiscal.getColumns().get(0).setVisible(true);

	    txtCopiarChaveAcesso.setText("");
	    desabilitarBotesControleTabela();
	} catch (Exception ex) {
	    System.out.println("Erro ao carregar dados da tabela veiculos" + ex.getMessage());
	}
    }

    private void selecionarLinha() {
	desabilitarBotesControleTabela();
	txtRetornoNfe.setText("");
	txtRetornoXml.setText("");
	NotaFiscal nota = tabelaNotaFiscal.getSelectionModel().getSelectedItem();
	if (nota == null) {
	    System.out.println("nenhuma nota selecionada");
	} else {
	    System.out.println("Nota Número.....: " + nota.getNf_numero());
	    txtCopiarChaveAcesso.setText(nota.getNfeChaveAcesso());
	    if (nota.getNfestatus().equals("N") || nota.getNfestatus() == null) {
		btnGerar.setDisable(false);
	    } else if (nota.getNfestatus().equals("G")) {
		btnGerar.setDisable(false);
		btnEnviar.setDisable(false);
		btnImprimirDanfe.setDisable(false);
		btnVizualizarXml.setDisable(false);
	    } else if (nota.getNfestatus().equals("E")) {
		btnEmailCliente.setDisable(false);
		btnEmailContador.setDisable(false);
		btnImprimirDanfe.setDisable(false);
		btnConsultar.setDisable(true);
		btnCancelarNota.setDisable(false);
		btnConsultaNotaChave.setDisable(false);
		btnVizualizarXml.setDisable(false);
	    } else if (nota.getNfestatus().equals("R")) {
		btnGerar.setDisable(false);
		btnConsultar.setDisable(false);
		btnVizualizarXml.setDisable(false);
		btnConsultaNotaChave.setDisable(false);
		btnImprimirDanfe.setDisable(false);
	    } else if (nota.getNfestatus().equals("C")) {
		btnCancelarNota.setDisable(false);
		btnConsultaNotaChave.setDisable(false);
		btnImprimirDanfe.setDisable(false);
		btnVizualizarXml.setDisable(false);
	    } else if (nota.getNfestatus().equals("D")) {
		btnVizualizarXml.setDisable(true);
		btnConsultaNotaChave.setDisable(false);
		btnImprimirDanfe.setDisable(false);
		btnVizualizarXml.setDisable(false);
	    } else if (nota.getNfestatus().equals("A")) {
		btnConsultar.setDisable(false);
		btnGerar.setDisable(false);
		btnConsultaNotaChave.setDisable(false);
		btnVizualizarXml.setDisable(false);
	    }
	}
    }

    @FXML
    private void keyReleased(KeyEvent event) {
	selecionarLinha();
    }

    @FXML
    private void mouseClique(MouseEvent event) {
	selecionarLinha();
    }

    private void funcoesTabela() {

//        colunaChkBox.setCellFactory(tc -> new CheckBoxTableCell<NotaFiscal, CheckBox>() {
//            @Override
//            public void updateItem(CheckBox item, boolean empty) {
//                super.updateItem(item, empty);
//                TableRow linha = getTableRow();
//                if (empty) {
//                    setText(null);
//                } else {
//                    if (item.isSelected()) {
//                        linha.setStyle("-fx-background-color: #B53CB5;"); //branco
//                    } else{
//                        setText("Não");
//                    }
//                }
//            }
//        });
	colunaStatus.setCellFactory(tc -> new TableCell<NotaFiscal, String>() {
	    @Override
	    protected void updateItem(String item, boolean empty) {
		super.updateItem(item, empty);
		TableRow linha = getTableRow();
		if (empty) {
		    setText(null);
		    linha.setStyle("-fx-background-color: #FFF;"); //branco
		} else {
		    if (item == null || item.equals("N")) {
			linha.setStyle("-fx-background-color: #FFF;"); //branco
			setText("Digitada");
		    } else if (item.equals("C")) {
			setText("Cancelada");
			linha.setStyle("-fx-background-color: #F8D7DA;"); //vermelho
		    } else if (item.equals("E")) {
			setText("Enviada");
			linha.setStyle("-fx-background-color: #D4EDDA;"); //verde
//                        setTextFill(Color.web("#721c24"));
		    } else if (item.equals("G")) {
			setText("Gerada");
			linha.setStyle("-fx-background-color: #E2E3E5;"); //cinza
//                        setTextFill(Color.web("#721c24"));
		    } else if (item.equals("R")) {
			setText("Rejeitada");
			linha.setStyle("-fx-background-color: #FFF3CD;"); // amarelo
//                        setTextFill(Color.web("#721c24"));
		    } else if (item.equals("A")) {
			setText("Aguardando");
			linha.setStyle("-fx-background-color: #CCE5FF;"); //azul
//                        setTextFill(Color.web("#721c24"));
		    } else if (item.equals("D")) {
			setText("Denegada");
			linha.setStyle("-fx-background-color: #FFDAB9;"); //roxo
//                        setTextFill(Color.web("#721c24"));
		    }
		}
	    }
	});

//        colunaEnviada.setCellFactory(tc -> new TableCell<NotaFiscal, Boolean>() {
//            @Override
//            protected void updateItem(Boolean item, boolean empty) {
//                super.updateItem(item, empty);
//                if (empty) {
//                    setText(null);
//                } else {
//                    if (item.booleanValue() == true) {
//                        setText("Sim");
//                    } else if (item.booleanValue() == false) {
//                        setText("Não");
//                    }
//                }
//            }
//        });
//
//        colunaCancelada.setCellFactory(tc -> new TableCell<NotaFiscal, Boolean>() {
//            @Override
//            protected void updateItem(Boolean item, boolean empty) {
//                super.updateItem(item, empty);
//                setText(empty ? null : item.booleanValue() ? "Sim" : "Não");
//            }
//        });
	colunaDataEmissao.setCellFactory(column -> {
	    TableCell<NotaFiscal, Date> cell = new TableCell<NotaFiscal, Date>() {
		private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		@Override
		protected void updateItem(Date item, boolean empty) {
		    super.updateItem(item, empty);
		    if (empty) {
			setText(null);
		    } else {
			setText(format.format(item));
		    }
		}
	    };

	    return cell;
	});

	colunaDataEntrada.setCellFactory(column -> {
	    TableCell<NotaFiscal, Date> cell = new TableCell<NotaFiscal, Date>() {
		private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

		@Override
		protected void updateItem(Date item, boolean empty) {
		    super.updateItem(item, empty);
		    if (empty) {
			setText(null);
		    } else {
			setText(format.format(item));
		    }
		}
	    };
	    return cell;
	});

	colunaTotalNota.setCellFactory(tc -> {
	    TableCell<NotaFiscal, Double> cell = new TableCell<NotaFiscal, Double>() {
		private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

		@Override
		protected void updateItem(Double price, boolean empty) {
		    super.updateItem(price, empty);
		    if (empty) {
			setText(null);
		    } else {
			setText(currencyFormat.format(price));
		    }
		}
	    };
	    cell.setStyle("-fx-alignment: CENTER_RIGHT;");
	    return cell;
	});

    }

    private String getdata() {
	LocalDate hoje = LocalDate.now();
	DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	return hoje.format(formatador); //08/04/2014
    }

    @FXML
    void clickBtnAlterar(ActionEvent event) {

    }

    @FXML
    void clickBtnCancelar(ActionEvent event) {

    }

    @FXML
    void clickBtnCancelarNota(ActionEvent event) throws IOException {
	ParametroCancelamento pc = new ParametroCancelamento();
	Stage dialogStage = new Stage();
	pc.setDialogStage(dialogStage);
	NotaFiscal nf = tabelaNotaFiscal.getSelectionModel().getSelectedItem();
	pc.setNf(tabelaNotaFiscal.getSelectionModel().getSelectedItem());
	FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLCancelamento.fxml"));
	loader.setController(new FXMLCancelamentoController(pc));
	Parent page = (Parent) loader.load();
	dialogStage.initModality(Modality.APPLICATION_MODAL);
	dialogStage.setTitle("Cancelamento de NFe");
	dialogStage.setResizable(false);
	Scene scene = new Scene(page);
	dialogStage.setScene(scene);
	dialogStage.showAndWait();
    }

    /*consultar nota*/
    @FXML
    void clickBtnConsultar(ActionEvent event) {
	System.out.println("click consultar nota clicado");
	ObjetoRetornoEnviarMota retorno = nControler.consultarNfe(tabelaNotaFiscal.getSelectionModel().getSelectedItem().getNfeChaveAcesso());
	if (retorno.isStatusTransmissao() == true) {
	    txtRetornoNfe.setText(retorno.getStatus() + " - " + retorno.getMotivo());
	    XmlFormatter f = new XmlFormatter();
	    if (retorno.getXml() != null) {
		txtRetornoXml.setText(f.format(retorno.getXml()));
	    }
	    carregarTabelaNotas(getCamposPesquisa());
	} else {
	    txtRetornoNfe.setText(retorno.getStatus() + " - " + retorno.getMotivo());
	    XmlFormatter f = new XmlFormatter();
	    if (retorno.getXml() != null) {
		txtRetornoXml.setText(f.format(retorno.getXml()));
	    }
	    grafics = ICON_ERROR;
	    createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao executar a operação!");
	    notification.show();
	    carregarTabelaNotas(getCamposPesquisa());
	}
    }

    @FXML
    void clickBtnConsultaNotaChave(ActionEvent event) {
	System.out.println("click consultar nota clicado");
	ObjetoRetornoEnviarMota retorno = nControler.consultarNfePelaChave(tabelaNotaFiscal.getSelectionModel().getSelectedItem().getNfeChaveAcesso());
	if (retorno.isStatusTransmissao() == true) {
	    txtRetornoNfe.setText(retorno.getStatus() + " - " + retorno.getMotivo());
	    XmlFormatter f = new XmlFormatter();
	    if (retorno.getXml() != null) {
		txtRetornoXml.setText(f.format(retorno.getXml()));
	    }
	    carregarTabelaNotas(getCamposPesquisa());
	} else {
	    txtRetornoNfe.setText(retorno.getStatus() + " - " + retorno.getMotivo());
	    XmlFormatter f = new XmlFormatter();
	    if (retorno.getXml() != null) {
		txtRetornoXml.setText(f.format(retorno.getXml()));
	    }
	    grafics = ICON_ERROR;
	    createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao executar a operação!");
	    notification.show();
	    carregarTabelaNotas(getCamposPesquisa());
	}
    }

    @FXML
    void clickBtnEnviar(ActionEvent event) {
	System.out.println("clickBtnEnviar() precinado...");
//        List<NotaFiscal> lista = new ArrayList<>();
//            if(listaSeleciodados().isEmpty()){
	NotaFiscal nf = new NotaFiscal();
	nf = tabelaNotaFiscal.getSelectionModel().getSelectedItem();
//                lista.add(nf);
//            }else{
//                lista = listaSeleciodados();
//            }
	ObjetoRetornoEnviarMota retorno = nControler.enviarLote(nf.getNfeChaveAcesso());
	if (retorno.isStatusTransmissao() == true) {
	    txtRetornoNfe.setText(retorno.getStatus() + " - " + retorno.getMotivo());
	    XmlFormatter f = new XmlFormatter();
	    if (retorno.getXml() != null) {
		txtRetornoXml.setText(f.format(retorno.getXml()));
	    }
	    carregarTabelaNotas(getCamposPesquisa());
	} else {
	    txtRetornoNfe.setText(retorno.getStatus() + " - " + retorno.getMotivo());
	    XmlFormatter f = new XmlFormatter();
	    if (retorno.getXml() != null) {
		txtRetornoXml.setText(f.format(retorno.getXml()));
	    }
	    grafics = ICON_ERROR;
	    createNotification(Pos.BOTTOM_RIGHT, grafics, "Erro ao executar a operação!");
	    notification.show();
	    carregarTabelaNotas(getCamposPesquisa());
	}
    }

    @FXML
    void clickBtnExcluir(ActionEvent event) {

    }

    @FXML
    void clickBtnGerar(ActionEvent event) throws Exception {
	System.out.println("clickBtnGerar() precinado...");
	System.out.println(tabelaNotaFiscal.getSelectionModel().getSelectedItem().getnFeAmbiente());
	List<NotaFiscal> lista = new ArrayList<NotaFiscal>();
	if (listaSeleciodados().isEmpty()) {
	    NotaFiscal notaFiscal;
	    notaFiscal = tabelaNotaFiscal.getSelectionModel().getSelectedItem();
	    lista.add(notaFiscal);
	} else {
	    lista = listaSeleciodados();
	}
	ObjetoRetornoGerarNota retorno = nControler.gerarXml(lista);
	if (retorno.isStatusGeracao() == true) {
	    txtRetornoNfe.setText("Nota gerada com sucesso!");
	    carregarTabelaNotas(getCamposPesquisa());
	} else {
	    txtRetornoNfe.setText("Erro ao executar a operação!");
//	    XmlFormatter f = new XmlFormatter();
//	    txtRetornoXml.setText(f.format(retorno.getXmlLote()));
//	    notification.show();
	    carregarTabelaNotas(getCamposPesquisa());
	}
    }

    @FXML
    void clickBtnIncluir(ActionEvent event) {

    }

    @FXML
    void cliqueBtnEmailCliente(ActionEvent event) {
	email("cliente");
    }

    @FXML
    void cliqueBtnEmailContador(ActionEvent event) {
	email("contador");
    }

    @FXML
    void cliqueBtnImprimirDanfe(ActionEvent event) {
	System.out.println("cliqueBtnImprimirDanfe()...");
	NotaFiscal nf = tabelaNotaFiscal.getSelectionModel().getSelectedItem();
	if (nf.getNfestatus().equals("E") || nf.getNfestatus().equals("C") || nf.getNfestatus().equals("D")) {
	    nControler.imprimirDanfe(nf, nf.getNfestatus());
	} else if (nf.getNfestatus().equals("G") || nf.getNfestatus().equals("A") || nf.getNfestatus().equals("R")) {
	    nControler.vizualizarImpressao(nf, nf.getNfestatus());
	}

//	nControler.vizualizarImpressao(tabelaNotaFiscal.getSelectionModel().getSelectedItem());
    }

    @FXML
    void clickBtnVizualizarXML(ActionEvent event) throws IOException {
	Stage dialogStage = new Stage();
	NotaFiscal nf = tabelaNotaFiscal.getSelectionModel().getSelectedItem();
	FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLVizualizarXML.fxml"));
	loader.setController(new FXMLVizualizarXML(nf, dialogStage));
	Parent page = (Parent) loader.load();
	dialogStage.initModality(Modality.APPLICATION_MODAL);
	dialogStage.setMaximized(true);
	dialogStage.setTitle("Vizualizar XML");
	dialogStage.setResizable(false);
	Scene scene = new Scene(page);
	dialogStage.setScene(scene);
	dialogStage.showAndWait();
	carregarTabelaNotas(getCamposPesquisa());
    }

    @FXML
    void cliqueBtnConsulta(ActionEvent event) {

    }

    @FXML
    void cliqueBtnSalvar(ActionEvent event) {

    }

    @FXML
    private void cliqueBtnPesquisar(ActionEvent event) {
	String filtros = getCamposPesquisa();
	tabelaNotaFiscal.setItems(null);
	carregarTabelaNotas(filtros);
    }

    private String getCamposPesquisa() {
	LocalDate dtInicio = dpPeriodoInicio.getValue();
	LocalDate dtFinal = dpPeriodoFim.getValue();
	RadioButton button = (RadioButton) grupoBotoes.getSelectedToggle();
	String radio = "";
	if (button.getText().equals("Entrada")) {
	    radio = "0";
	} else if (button.getText().equals("Saída")) {
	    radio = "1";
	}
	String where = "AND fil_cod = '" + filial.getFil_cod() + "' and nf_dtemissao >= CONVERT(DATETIME,'" + dtInicio + "',102) AND nf_dtemissao <= CONVERT(DATETIME,'" + dtFinal + "',102) and nf_tipo = '" + radio + "'";
	System.out.println(where);

	return where;
    }

    private void desabilitarBotesControleTabela() {
	btnGerar.setDisable(true);
	btnEnviar.setDisable(true);
	btnConsultar.setDisable(true);
	btnCancelarNota.setDisable(true);
	btnConsultaNotaChave.setDisable(true);
	btnVizualizarXml.setDisable(true);
	btnEmailCliente.setDisable(true);
	btnEmailContador.setDisable(true);
	btnImprimirDanfe.setDisable(true);
    }

    private void createNotification(Pos position, Node grafics, String text) {
	notification = Notifications.create()
		.title("Acom Informa\n\n")
		.text(" " + text)
		.graphic(grafics)
		.hideAfter(Duration.seconds(5))
		.position(position);
    }

    private List<NotaFiscal> listaSeleciodados() {
	List<NotaFiscal> lista = new ArrayList<>();
	NotaFiscal nf = null;
	for (int i = 0; i < tabelaNotaFiscal.getItems().size(); i++) {
	    if (tabelaNotaFiscal.getItems().get(i).getCheckBox().isSelected()) {
		nf = tabelaNotaFiscal.getItems().get(i);
		lista.add(nf);
	    }
	}

	return lista;
    }

    private void email(String tipo) {
	try {
	    Stage dialogStage = new Stage();
	    NotaFiscal nf = tabelaNotaFiscal.getSelectionModel().getSelectedItem();
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLConfirmacaoDadosEmail.fxml"));
	    loader.setController(new FXMLConfirmacaoDadosEmailController(nf, dialogStage, tipo));
	    Parent page;
	    page = (Parent) loader.load();
	    dialogStage.initModality(Modality.APPLICATION_MODAL);
	    dialogStage.setMaximized(false);
	    dialogStage.setTitle("Confirmação de dados do e-mail do " + tipo);
	    dialogStage.setResizable(false);
	    Scene scene = new Scene(page);
	    dialogStage.setScene(scene);
	    dialogStage.showAndWait();
	    carregarTabelaNotas(getCamposPesquisa());
	} catch (IOException ex) {
	    Logger.getLogger(FXMLNotaFiscal.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

}
