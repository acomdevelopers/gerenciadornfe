/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.nota.NotaControler;
import acom.gerenciadornfe.modelo.entidades.Filial;
import acom.gerenciadornfe.modelo.entidades.NFDestinatario;
import acom.gerenciadornfe.modelo.notas.ResNfe;
import acom.gerenciadornfe.modelo.repositorio.FilialDAO;
import acom.gerenciadornfe.modelo.repositorio.NFDestinatarioDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import acom.gerenciadornfe.util.Util;
import com.fincatto.documentofiscal.DFUnidadeFederativa;
import com.fincatto.documentofiscal.nfe.classes.distribuicao.NFDistribuicaoDFeLote;
import com.fincatto.documentofiscal.nfe.classes.distribuicao.NFDistribuicaoDocumentoZip;
import com.fincatto.documentofiscal.nfe.classes.distribuicao.NFDistribuicaoIntRetorno;
import com.fincatto.documentofiscal.nfe.webservices.distribuicao.WSDistribuicaoNFe;
import com.fincatto.documentofiscal.utils.DFPersister;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Allison
 */
public class FXMLObterNotasController implements Initializable {

    @FXML
    private TextArea txtRetorno;

    @FXML
    private Button btnFechar;
    private Stage stage;
    private NotaControler notaControler = new NotaControler();
    private FilialDAO fDAO = new FilialDAO();
    private ParametrosDAO pDAO = new ParametrosDAO();
    private Filial filial = new Filial();
    private NFDestinatarioDAO nfdDAO = new NFDestinatarioDAO();

    /**
     * Initializes the controller class.
     */
    public FXMLObterNotasController(Stage stage, Filial filial) {
	this.stage = stage;
	this.filial = filial;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	System.out.println("iniciou a tela ObterNotasController");
	filial = fDAO.getDadosFilial(pDAO.getParFilial().replace("=", ""));
	obterNotas();
    }

    @FXML
    void cliqueBtnFechar(ActionEvent event) {
	stage.close();
    }

    private void obterNotas() {
	NFDistribuicaoIntRetorno nfd = new NFDistribuicaoIntRetorno();
	NFDistribuicaoDFeLote lote = new NFDistribuicaoDFeLote();
	String cpfCnpj = Util.getNumeros(filial.getFil_cnpj());
	String codMunicipio = String.valueOf(filial.getFil_cod_municipio()).substring(0, 2);
	int nsu = nfdDAO.getMaxNsu();
	NFDestinatario notaDestinatario;
	nfd = notaControler.obterNotas("10621284000197", DFUnidadeFederativa.valueOfCodigo(codMunicipio), "", "", "000000000000013");//String.valueOf(nsu)
	if (nfd != null) {
	    String text = "";
	    int cont = 0;
	    ResNfe resnfe;
	    for (NFDistribuicaoDocumentoZip zip : nfd.getLote().getDocZip()) {
		try {
		    System.out.println(zip.getValue());
		    resnfe = new ResNfe();
		    resnfe = new DFPersister().read(ResNfe.class, WSDistribuicaoNFe.decodeGZipToXml(zip.getValue()));
		    notaDestinatario = new NFDestinatario();
		    notaDestinatario = preencherObjeto(resnfe, zip.getNsu());
		    System.out.println("----------------------------------------------------------");
		    System.out.println(resnfe.getChNFe());
		    System.out.println(WSDistribuicaoNFe.decodeGZipToXml(zip.getValue()));
		    cont++;
		} catch (IOException ex) {
		    Logger.getLogger(FXMLObterNotasController.class.getName()).log(Level.SEVERE, null, ex);
		} catch (Exception ex) {
		    Logger.getLogger(FXMLObterNotasController.class.getName()).log(Level.SEVERE, null, ex);
		}
	    }
	    System.out.println("TOTAL DE NOTAS RECEBIDAS-----> " + cont);
	}
    }

    private NFDestinatario preencherObjeto(ResNfe resNfe, String nsu) {
	NFDestinatario nfd = new NFDestinatario();
	nfd.setNf_chave(resNfe.getChNFe());
	nfd.setNf_cnpj(resNfe.getCnpj());
	nfd.setNf_data_recibo(formatarDataEHora(resNfe.getDhRecbto()));
	nfd.setNf_emissao(formatarDataEHora(resNfe.getDhRecbto()));
	nfd.setNf_ie(resNfe.getIe());
	nfd.setNf_nome(resNfe.getxNome());
	nfd.setNf_nsu(nsu);
	nfd.setNf_numero(Integer.parseInt(resNfe.getChNFe().substring(25, 34)));
	nfd.setNf_protocolo(resNfe.getnProt());
	nfd.setNf_serie(Integer.parseInt(resNfe.getChNFe().substring(22, 25)));;

	return nfd;
    }

    public Timestamp formatarDataEHora(String dataSefaz) {
	Timestamp timeStampDate = null;
	try {
	    String dataWsSefaz = dataSefaz;
	    String data = dataWsSefaz.substring(0, dataWsSefaz.indexOf("T"));
	    DateFormat formatter;
	    formatter = new SimpleDateFormat("dd/MM/yyyy");
	    Date date = formatter.parse(data);
	    timeStampDate = new Timestamp(date.getTime());
	} catch (ParseException e) {
	    System.out.println("Exception :" + e);
	}
	return timeStampDate;
    }
}
