/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.modelo.entidades.Filial;
import acom.gerenciadornfe.modelo.entidades.NotaFiscal;
import acom.gerenciadornfe.modelo.repositorio.FilialDAO;
import acom.gerenciadornfe.modelo.repositorio.NotaFiscalDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * FXML Controller class
 *
 * @author allison
 */
public class FXMLPesquisarNotaController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Button btnPesquisar;

    @FXML
    private Label labelFilial;

    @FXML
    private DatePicker dpPeriodoInicio;

    @FXML
    private DatePicker dpPeriodoFim;

    @FXML
    private RadioButton radioEntrada;

    @FXML
    private ToggleGroup grupoBotoes;

    @FXML
    private RadioButton radioSaida;

    @FXML
    private TableView<NotaFiscal> tabelaSelecionarNotas;

    @FXML
    private TableColumn<?, ?> colunaNumero;

    @FXML
    private TableColumn<?, ?> colunaChave;

    @FXML
    private TableColumn<?, ?> colunaCliente;

    @FXML
    private TableColumn<?, ?> colunaSerie;

    @FXML
    private TableColumn<NotaFiscal, Date> colunaData;

    @FXML
    private TableColumn<NotaFiscal, Double> colunaTotal;

    @FXML
    private Button btnSelecionar;

    @FXML
    private Button btnCancelar;

    private Stage dialogStage;
    
    public NotaFiscal xnotaFiscal = new NotaFiscal();
    
    public String teste;

    private List<NotaFiscal> listaNotas;

    private ObservableList<NotaFiscal> obsListNotas;
    
    private List<Filial> listaFiliais = new ArrayList();

    private ObservableList<Filial> observableListFiliais;

    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private FilialDAO filDAO;
    private NotaFiscalDAO nfDAO;
    
    private Filial filial;
    private ParametrosDAO pDAO;
    
    public FXMLPesquisarNotaController(Stage stage){
        this.dialogStage = stage;
        
    }
    
    public FXMLPesquisarNotaController(){
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        FXMLCartaCorrecaoController.addOnChangeScreenListeners(new FXMLCartaCorrecaoController.OnChangeScreen() {
            @Override
            public void onScreenChanged(String string, NotaFiscal notaFiscal) {
                System.out.println("Tela de pesquisa:tipo--> " + string);
            }
        });
        
        // TODO
        filDAO = new FilialDAO();
        nfDAO = new NotaFiscalDAO();
	pDAO = new ParametrosDAO();
        radioSaida.setSelected(true);
	filial = new Filial();
	filial = filDAO.getFilial(pDAO.getParFilial().replace("=", ""));
	labelFilial.setText(filial.getFil_cod() + " - " + filial.getFil_nreduz());
	iniciarTela();
        dpPeriodoInicio.setValue(LocalDate.parse(getdata(), dateFormatter));
        dpPeriodoFim.setValue(LocalDate.parse(getdata(), dateFormatter));
        tabelaSelecionarNotas.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        carregarTabelaNotas(getCamposPesquisa());
        funcoesTabela();
    }

    private void iniciarTela() {
        radioEntrada.setToggleGroup(grupoBotoes);
        radioSaida.setToggleGroup(grupoBotoes);
        btnSelecionar.setDisable(true);
    }
    
    private void carregarTabelaNotas(String filtros) {
        try {
            listaNotas = nfDAO.listar(filtros);
            obsListNotas = FXCollections.observableArrayList();
            for (NotaFiscal n : listaNotas) {
                CheckBox ch = new CheckBox();
                ch.setDisable(true);
                if (n.getNfestatus().equals("N") || n.getNfestatus().equals("G") || n.getNfestatus() == null) {
                    ch.setDisable(false);
                }
                obsListNotas.add(new NotaFiscal(ch, n));
            }
            tabelaSelecionarNotas.setItems(obsListNotas);

            colunaNumero.setCellValueFactory(new PropertyValueFactory<>("nf_nrnota"));
            colunaChave.setCellValueFactory(new PropertyValueFactory<>("nfeChaveAcesso"));
            colunaCliente.setCellValueFactory(new PropertyValueFactory<>("nf_nome"));
            colunaSerie.setCellValueFactory(new PropertyValueFactory<>("nf_serie"));
            colunaData.setCellValueFactory(new PropertyValueFactory<>("nf_dtemissao"));
            colunaTotal.setCellValueFactory(new PropertyValueFactory<>("nf_totnota"));
            tabelaSelecionarNotas.getSelectionModel().clearSelection();
        } catch (Exception ex) {
            System.out.println("Erro ao carregar dados da tabela veiculos" + ex.getMessage());
        }
    }

    private void selecionarLinha() {
        NotaFiscal nota = tabelaSelecionarNotas.getSelectionModel().getSelectedItem();
        if (nota == null) {
            System.out.println("nenhuma nota selecionada");
        } else {
            System.out.println("Nota Número.....: " + nota.getNf_numero());
            btnSelecionar.setDisable(false);
            }
        }
    

    @FXML
    private void keyReleased(KeyEvent event) {
        selecionarLinha();
    }

    @FXML
    private void mouseClique(MouseEvent event) {
        selecionarLinha();
    }

    private void funcoesTabela() {

//        colunaStatus.setCellFactory(tc -> new TableCell<NotaFiscal, String>() {
//            @Override
//            protected void updateItem(String item, boolean empty) {
//                super.updateItem(item, empty);
//                TableRow linha = getTableRow();
//                if (empty) {
//                    setText(null);
//                    linha.setStyle("-fx-background-color: #FFF;"); //branco
//                } else {
//                    if (item == null || item.equals("N")) {
//                        linha.setStyle("-fx-background-color: #FFF;"); //branco
//                        setText("Digitada");
//                    } else if (item.equals("C")) {
//                        setText("Cancelada");
//                        linha.setStyle("-fx-background-color: #F8D7DA;"); //vermelho
//                    } else if (item.equals("E")) {
//                        setText("Enviada");
//                        linha.setStyle("-fx-background-color: #D4EDDA;"); //verde
////                        setTextFill(Color.web("#721c24"));
//                    } else if (item.equals("G")) {
//                        setText("Gerada");
//                        linha.setStyle("-fx-background-color: #E2E3E5;"); //cinza
////                        setTextFill(Color.web("#721c24"));
//                    } else if (item.equals("R")) {
//                        setText("Rejeitada");
//                        linha.setStyle("-fx-background-color: #FFF3CD;"); // amarelo
////                        setTextFill(Color.web("#721c24"));
//                    } else if (item.equals("A")) {
//                        setText("Aguardando");
//                        linha.setStyle("-fx-background-color: #CCE5FF;"); //azul
////                        setTextFill(Color.web("#721c24"));
//                    } else if (item.equals("D")) {
//                        setText("Denegada");
//                        linha.setStyle("-fx-background-color: #FFDAB9;"); //roxo
////                        setTextFill(Color.web("#721c24"));
//                    }
//                }
//            }
//        });

        colunaData.setCellFactory(column -> {
            TableCell<NotaFiscal, Date> cell = new TableCell<NotaFiscal, Date>() {
                private SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

                @Override
                protected void updateItem(Date item, boolean empty) {
                    super.updateItem(item, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        setText(format.format(item));
                    }
                }
            };

            return cell;
        });


        colunaTotal.setCellFactory(tc -> {
            TableCell<NotaFiscal, Double> cell = new TableCell<NotaFiscal, Double>() {
                private NumberFormat currencyFormat = NumberFormat.getCurrencyInstance();

                @Override
                protected void updateItem(Double price, boolean empty) {
                    super.updateItem(price, empty);
                    if (empty) {
                        setText(null);
                    } else {
                        setText(currencyFormat.format(price));
                    }
                }
            };
            cell.setStyle("-fx-alignment: CENTER_RIGHT;");
            return cell;
        });

    }
    
    private String getdata() {
        LocalDate hoje = LocalDate.now();
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        return hoje.format(formatador); //08/04/2014
    }
    
    private String getCamposPesquisa() {
        LocalDate dtInicio = dpPeriodoInicio.getValue();
        LocalDate dtFinal = dpPeriodoFim.getValue();
        RadioButton button = (RadioButton) grupoBotoes.getSelectedToggle();
        String radio = "";
        if (button.getText().equals("Entrada")) {
            radio = "0";
        } else if (button.getText().equals("Saída")) {
            radio = "1";
        }
        String where = "AND nfestatus = 'E' AND fil_cod = '" + filial.getFil_cod() + "' and nf_dtemissao >= CONVERT(DATETIME,'" + dtInicio + "',102) AND nf_dtemissao <= CONVERT(DATETIME,'" + dtFinal + "',102) and nf_tipo = '" + radio + "'";
        System.out.println(where);

        return where;
    }
    
    
    @FXML
    void cliqueBtnCancelar(ActionEvent event) {
        dialogStage.close();
    }

    @FXML
    void cliqueBtnPesquisar(ActionEvent event) {
        String filtros = getCamposPesquisa();
        tabelaSelecionarNotas.setItems(null);
        carregarTabelaNotas(filtros);
    }

    @FXML
    void cliqueBtnSelecionar(ActionEvent event) {
        xnotaFiscal = tabelaSelecionarNotas.getSelectionModel().getSelectedItem();
        teste = "teste tela b";
        FXMLCartaCorrecaoController.changeScreen("devolução", xnotaFiscal);
        dialogStage.hide();
    }

     public NotaFiscal getXnotaFiscal() {
        return xnotaFiscal;
    }

    public void setXnotaFiscal(NotaFiscal xnotaFiscal) {
        this.xnotaFiscal = xnotaFiscal;
    }
    
    
}
