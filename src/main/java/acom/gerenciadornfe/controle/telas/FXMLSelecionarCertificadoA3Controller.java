/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.nota.NFeConfigAcom;
import acom.gerenciadornfe.modelo.entidades.Parametros;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import com.fincatto.documentofiscal.DFModelo;
import com.fincatto.documentofiscal.DFUnidadeFederativa;
import com.fincatto.documentofiscal.nfe400.classes.statusservico.consulta.NFStatusServicoConsultaRetorno;
import com.fincatto.documentofiscal.nfe400.webservices.WSFacade;
import java.io.IOException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author allison
 */
public class FXMLSelecionarCertificadoA3Controller implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private ComboBox<String> comboCertificado;

    @FXML
    private PasswordField txtSenha;

    @FXML
    private Button btnOk;
    
    private List<String> listaCertificados = new ArrayList();

    private ObservableList<String> observableListCertificados;

    private Stage stage;

    private ParametrosDAO pDAO;
    
    public FXMLSelecionarCertificadoA3Controller(Stage stage){
        this.stage = stage;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        pDAO = new ParametrosDAO();
        carregarComboCertificado();
    }

    @FXML
    void cliqueBtnOk(ActionEvent event) throws IOException, KeyManagementException, UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, Exception {
        String aliasSelecionado = comboCertificado.getSelectionModel().getSelectedItem().toString();
        Parametros p = new Parametros("par_alias_certificado", "Alias do certificado (nao preencher).", aliasSelecionado);
        pDAO.alterar(p);
        NFeConfigAcom config = new NFeConfigAcom();
//        config.selecionarAlias(aliasSelecionado[0]);
//        NFStatusServicoConsultaRetorno retorno = new WSFacade(config).consultaStatus(DFUnidadeFederativa.PE, DFModelo.NFE);
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/FXMLInicial.fxml"));
        Scene scene = new Scene(root);
        stage.setTitle("Gerenciador de Nota Fiscal");
        Image iconeApp = new Image(getClass().getResourceAsStream("/image/icon.png"));
        stage.getIcons().add(iconeApp);
        stage.initStyle(StageStyle.UNIFIED);
        stage.centerOnScreen();
        stage.setMaximized(true);
        stage.setScene(scene);
        stage.showAndWait();
	this.stage.close();
    }

    
    private void carregarComboCertificado(){
        listaCertificados = new NFeConfigAcom().listarCertificados();
        observableListCertificados = FXCollections.observableArrayList(listaCertificados);
        comboCertificado.setItems(observableListCertificados);
    }
    


    
}
