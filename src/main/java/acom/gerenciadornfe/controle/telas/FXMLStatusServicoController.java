/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.controle.telas;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * FXML Controller class
 *
 * @author Allison
 */
public class FXMLStatusServicoController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private WebView browser;

    private WebEngine webengine;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
	System.out.println("iniciando o browser...");
	startBrowser();
//	getHtml();
//	extractDiv();
    }

    private void startBrowser() {
	this.webengine = this.browser.getEngine();
	this.webengine.load("http://www.nfe.fazenda.gov.br/portal/disponibilidade.aspx?versao=0.00&tipoConteudo=Skeuqr8PQBY=#conteudoDinamico");
    }

    private String getHtml() {
	String content = null;
	URLConnection connection = null;
	try {
	    connection = new URL("http://www.nfe.fazenda.gov.br/portal/disponibilidade.aspx?versao=0.00&tipoConteudo=Skeuqr8PQBY=").openConnection();
	    Scanner scanner = new Scanner(connection.getInputStream());
	    scanner.useDelimiter("\\Z");
	    content = scanner.next();
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
	return content;
    }

    private void extractDiv() {
	try {
	    Document doc = Jsoup.connect("http://www.nfe.fazenda.gov.br/portal/disponibilidade.aspx?versao=0.00&tipoConteudo=Skeuqr8PQBY=").get();

	    Elements text = doc.select("div[id=conteudoDinamico]");
	    System.out.println("CONTEUDO.....: " + text.html());
//	    startBrowser(text.html());
	} catch (IOException ex) {
	    Logger.getLogger(FXMLStatusServicoController.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

}
