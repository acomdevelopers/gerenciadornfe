package acom.gerenciadornfe.controle.telas;

import acom.gerenciadornfe.controle.nota.NotaControler;
import acom.gerenciadornfe.modelo.entidades.NotaFiscal;
import acom.gerenciadornfe.util.Util;
import acom.gerenciadornfe.util.XmlFormatter;
import com.fincatto.documentofiscal.nfe400.classes.lote.envio.NFLoteEnvio;
import com.fincatto.documentofiscal.nfe400.classes.nota.NFNotaProcessada;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import org.apache.axiom.mime.ContentType;

/**
 *
 * @author allison
 */
public class FXMLVizualizarXML implements Initializable {

    @FXML
    private AnchorPane ap;

    @FXML
    private Tab abaRetornoXml;

    @FXML
    private TextArea txtRetornoXml;

    @FXML
    private Button btnFechar;
    
    private Stage dialogstage;

    private NotaFiscal notaFiscal = new NotaFiscal();
    private NotaControler nc;

    public FXMLVizualizarXML() {

    }

    public FXMLVizualizarXML(NotaFiscal nf, Stage stage) {
	this.notaFiscal = nf;
	this.dialogstage = stage;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
	nc = new NotaControler();
	inicioTela(notaFiscal);
    }

    public Stage getDialogstage() {
	return dialogstage;
    }

    public void setDialogstage(Stage dialogstage) {
	this.dialogstage = dialogstage;
    }
 
    @FXML
    void clickBtnFechar(ActionEvent event) {
	dialogstage.close();
    }

    public NotaFiscal getNotaFiscal() {
	return notaFiscal;
    }

    public void setNotaFiscal(NotaFiscal notaFiscal) {
	this.notaFiscal = notaFiscal;
    }

    private void inicioTela(NotaFiscal nf) {
	System.out.println("Nota Numero: " + nf.getNf_numero());
	if (nf.getNfestatus().equals("E")) {
	    NFNotaProcessada np = nc.carregarXmlNFNotaProcessada(nf.getNfeChaveAcesso(), Util.formatarData(nf.getNf_data().toString()), "processadas", "_nfe");
	    XmlFormatter f = new XmlFormatter();
	    String xml = np.toString();
	    if (np.toString() != null) {
		txtRetornoXml.setText(f.format(xml));
	    } else {
		txtRetornoXml.setText("Erro ao carregar xml ou arquivo não encontrado.");
	    }
	} else if (nf.getNfestatus().equals("C")) {
	    NFNotaProcessada np = nc.carregarXmlNFNotaProcessada(nf.getNfeChaveAcesso(), Util.formatarData(nf.getNf_data().toString()), "canceladas", "_canc");
	    XmlFormatter f = new XmlFormatter();
	    String xml = np.toString();
	    if (np.toString() != null) {
		txtRetornoXml.setText(f.format(xml));
	    } else {
		txtRetornoXml.setText("Erro ao carregar xml ou arquivo não encontrado.");
	    }
	} else if (nf.getNfestatus().equals("D")) {
	    NFNotaProcessada np = nc.carregarXmlNFNotaProcessada(nf.getNfeChaveAcesso(), Util.formatarData(nf.getNf_data().toString()), "denegadas", "_den");
	    XmlFormatter f = new XmlFormatter();
	    String xml = np.toString();
	    if (np.toString() != null) {
		txtRetornoXml.setText(f.format(xml));
	    } else {
		txtRetornoXml.setText("Erro ao carregar xml ou arquivo não encontrado.");
	    }
	} else {
	    NFLoteEnvio nota = nc.carregarXmlPastaGeradas(nf.getNfeChaveAcesso());
	    XmlFormatter f = new XmlFormatter();
	    String xml = nota.toString();
	    if (nota.toString() != null) {
		txtRetornoXml.setText(f.format(xml));
	    } else {
		txtRetornoXml.setText("Erro ao carregar xml ou arquivo não encontrado.");
	    }
	}
    }

}
