package acom.gerenciadornfe.controle.telas;

/**
 * @author allison
 */
public class ObjetoRetornoGerarNota {

    boolean statusGeracao;
    String XmlLote;
    String xmlAssinado;

    public ObjetoRetornoGerarNota() {
    }

    public ObjetoRetornoGerarNota(boolean statusGeracao, String XmlLote, String xmlAssinado) {
        this.statusGeracao = statusGeracao;
        this.XmlLote = XmlLote;
        this.xmlAssinado = xmlAssinado;
    }

    public boolean isStatusGeracao() {
        return statusGeracao;
    }

    public void setStatusGeracao(boolean statusGeracao) {
        this.statusGeracao = statusGeracao;
    }

    public String getXmlLote() {
        return XmlLote;
    }

    public void setXmlLote(String XmlLote) {
        this.XmlLote = XmlLote;
    }

    public String getXmlAssinado() {
        return xmlAssinado;
    }

    public void setXmlAssinado(String xmlAssinado) {
        this.xmlAssinado = xmlAssinado;
    }

}
