package acom.gerenciadornfe.inicio;

import acom.gerenciadornfe.controle.telas.FXMLConfigSqLiteController;
import acom.gerenciadornfe.modelo.conexao.ConexaoSqLite;
import acom.gerenciadornfe.modelo.entidades.Parametros;
import acom.gerenciadornfe.modelo.repositorio.ConfigSqliteDAO;
import acom.gerenciadornfe.modelo.repositorio.FilialDAO;
import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import java.io.IOException;
import java.net.ServerSocket;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Inicio extends Application {

    private static ServerSocket serverSocket;
    private ConexaoSqLite connection = new ConexaoSqLite();
    private ConfigSqliteDAO csDAO = new ConfigSqliteDAO();
    private ParametrosDAO pDAO = new ParametrosDAO();
    private FilialDAO fDAO;

    @Override
    public void start(Stage stage) throws Exception {
//        setUserAgentStylesheet(STYLESHEET_MODENA);
        
        csDAO.criarTabelaSqlite();
        if (csDAO.consultaBanco() == false) {
	    csDAO.inserirHost();
	    csDAO.inserirInstancia();
	    csDAO.inserirBanco();
	    csDAO.inserirPorta();
	    csDAO.inserirDiretorioProjeto();
	    csDAO.inserirParAlias();
	    csDAO.inserirParFilial();
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLConfigSqLite.fxml"));
	    loader.setController(new FXMLConfigSqLiteController());
	    Parent page = (Parent) loader.load();
	    Scene scene = new Scene(page);
	    Image iconeApp = new Image(getClass().getResourceAsStream("/image/icon.png"));
	    stage.getIcons().add(iconeApp);
//	    stage.initStyle(StageStyle.UNIFIED);
	    stage.setResizable(false);
	    stage.setScene(scene);
	    stage.show();
	} else if (pDAO.getHost().equals("") || pDAO.getBanco().equals("") || pDAO.getInstancia().equals("") || pDAO.getPorta().equals("") || pDAO.getDiretorioProjeto().equals("")) {
	    FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLConfigSqLite.fxml"));
	    loader.setController(new FXMLConfigSqLiteController());
	    Parent page = (Parent) loader.load();
	    Scene scene = new Scene(page);
	    Image iconeApp = new Image(getClass().getResourceAsStream("/image/icon.png"));
	    stage.getIcons().add(iconeApp);
//	    stage.initStyle(StageStyle.UNIFIED);
	    stage.setResizable(false);
	    stage.setScene(scene);
	    stage.show();
	} else if (!pDAO.getHost().equals("") || !pDAO.getBanco().equals("") || !pDAO.getInstancia().equals("") || !pDAO.getPorta().equals("") || !pDAO.getDiretorioProjeto().equals("")) {
//	    String certificado = pDAO.getTipoCertificado();
//	    if (certificado.equals("A3")) {
//		int size = new NFeConfigAcom().listarCertificados().size();
//		if (size > 0) {
//		    try {
//			Stage mStage = new Stage();
//			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FXMLSelecionarCertificadoA3.fxml"));
//			Image iconeApp = new Image(getClass().getResourceAsStream("/image/icon.png"));
//			loader.setController(new FXMLSelecionarCertificadoA3Controller(mStage));
//			Parent page = (Parent) loader.load();
//			mStage.initModality(Modality.APPLICATION_MODAL);
//			mStage.getIcons().add(iconeApp);
//			mStage.initStyle(StageStyle.UNIFIED);
//			mStage.centerOnScreen();
//			mStage.setAlwaysOnTop(true);
//			Scene scene = new Scene(page);
//			mStage.setScene(scene);
//			mStage.showAndWait();
//		    } catch (IOException ex) {
//			Logger.getLogger(FXMLInicial.class.getName()).log(Level.SEVERE, null, ex);
//			ExceptionDialog exceptionDialog = new ExceptionDialog(ex);
//			exceptionDialog.setTitle("Erro");
//			exceptionDialog.setContentText("as");
//			exceptionDialog.setHeaderText("Erro ao carregar informações: ");
//			exceptionDialog.show();
//		    }
//		}
//		System.out.println("Else da chezagem se tem alias no repositorio do windows");
//	    } else {
		verificacao();
		Parent root = FXMLLoader.load(getClass().getResource("/fxml/FXMLInicial.fxml"));
		Scene scene = new Scene(root);
		stage.setTitle("Gerenciador de Nota Fiscal");
		Image iconeApp = new Image(getClass().getResourceAsStream("/image/icon.png"));
		stage.getIcons().add(iconeApp);
//		stage.initStyle(StageStyle.DECORATED);
		stage.setMinWidth(1024);
		stage.setMinHeight(768);
		stage.centerOnScreen();
		Screen screen = Screen.getPrimary();
		Rectangle2D bounds = screen.getVisualBounds();
		stage.setHeight(bounds.getHeight());
		stage.setWidth(bounds.getWidth());
		stage.setMaximized(true);
		stage.setScene(scene);
//            CertificadoUtil util = new CertificadoUtil();
//            util.validadeCertificado();
		stage.show();
	    }
        }
//    }

    private void verificacao() {
	if (csDAO.verificarParFilial().equals("")) {
	    fDAO = new FilialDAO();
	    csDAO.inserirParFilial();
	    pDAO.alterar(new Parametros("par_filial", "filial selecionada", "=" + fDAO.getCodFilialTop1()));
	    fDAO.inserirNomeCertificado(pDAO.getNomeCertificado(), pDAO.getSenhaCertificado(), pDAO.getTipoCertificado(), pDAO.getParFilial().replace("=", ""));
	    csDAO.apagarParametrosAntigos();
	}
    }
    
    public static void main(String[] args) {
        try {
            serverSocket = new ServerSocket(9581);
            launch(args);
        } catch (IOException ex) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Aplicação já está sendo executada!");
            alert.setTitle(".:. ACOM INFORMA .:.");
            alert.showAndWait();
        }
    }

}
