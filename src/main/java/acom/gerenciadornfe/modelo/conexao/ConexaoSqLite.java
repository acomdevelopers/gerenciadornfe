package acom.gerenciadornfe.modelo.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author allison
 */
public class ConexaoSqLite {

    private Connection connection;
    private String host = "jdbc:sqlite:gnfe_dados.sqlite";
    private static ConexaoSqLite instance;

    public static ConexaoSqLite getInstance() {
        if (instance == null) {
            instance = new ConexaoSqLite();
        }
        return instance;
    }

    public ConexaoSqLite() {
        try {
            Class.forName("org.sqlite.JDBC");
            this.connection = DriverManager.getConnection(host);
            System.out.println("Sqlite.......: " + "Conectado com sucesso!");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ConexaoSqLite.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ConexaoSqLite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void desconectar(Connection con) {
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ConexaoSqLite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Connection getConexao(){
        return this.connection;
    }
    
}
