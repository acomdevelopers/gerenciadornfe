package acom.gerenciadornfe.modelo.conexao;

import acom.gerenciadornfe.modelo.repositorio.ParametrosDAO;
import java.sql.Connection;
import java.sql.DriverManager;

public class ConexaoSqlServer {

    private ParametrosDAO parametrosDAO;

    private Connection conexao;
    private String User = "acom";
    private String Pass = "disktorkio"; //disktokio
    private static ConexaoSqlServer instacia;

    public static ConexaoSqlServer getInstancia() {
        if (instacia == null) {
            instacia = new ConexaoSqlServer();
        }
        return instacia;
    }

    public ConexaoSqlServer() {

        try {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            this.conexao = DriverManager.getConnection(getUrlConexao(), User, Pass);
            System.out.println("estou dentro de kaio");
        } catch (Exception e) {
            System.err.println(e);

        }
    }

    public Connection getConexao() {
        return this.conexao;
    }

    public String getUrlConexao() {
        parametrosDAO = new ParametrosDAO();

        String banco = parametrosDAO.getBanco().trim();
        String instancia = parametrosDAO.getInstancia().trim();
        String porta = parametrosDAO.getPorta().trim();
        String host = parametrosDAO.getHost().trim();
        /*
        jdbc:jtds:sqlserver://192.168.25.139:1433;instance=ACOM;DatabaseName=SGEDADOS;namedPipers=true", "ACOM", "1"
         */
        String urlConexao = "jdbc:jtds:sqlserver:" + host + ":" + porta + ";" + "instance=" + instancia + ";DatabaseName=" + banco + ";namedPipers=true";
//        System.out.println("**********************************************************************************************************************");
//        System.out.println("jdbc:jtds:sqlserver://192.168.25.139:1433;instance=ACOM;DatabaseName=SGEDADOS;namedPipers=true");
//        System.out.println("**********************************************************************************************************************");
        System.out.println(urlConexao);
        return urlConexao;
    }

}
