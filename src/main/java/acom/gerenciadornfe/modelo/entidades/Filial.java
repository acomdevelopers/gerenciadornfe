
package acom.gerenciadornfe.modelo.entidades;

import java.sql.Timestamp;

/**
 * @author allison
 */
public class Filial {

    private String fil_cod;
    private String fil_nome;
    private String fil_nreduz;
    private String fil_endereço;
    private String fil_numero;
    private String fil_compl;
    private String fil_bairro;
    private String fil_cep;
    private String fil_cidade;
    private String fil_estado;
    private String fil_cnpj;
    private String fil_insc;
    private String fil_fone;
    private String fil_fax;
    private String fil_contato;
    private String fil_foncontato;
    private String fil_cxpostal;
    private String fil_email;
    private String fil_socio;
    private String fil_cpf;
    private String fil_emailsocio;
    private String fil_contador;
    private String fil_cpfcont;
    private String fil_crc;
    private String fil_fonecont;
    private String fil_cxpostcont;
    private String fil_emailcont;
    private String fil_serienf;
    private long fil_nrnota;
    private long fil_nrnota_nfe_2;
    private String fil_serieselo;
    private long fil_nrselo;
    private String fil_serie_nfe_entrada;
    private long fil_nrnota_nfe_entrada;
    private String fil_almoxarifado_venda;
    private String fil_almoxarifado_nota;
    private String fil_clifor;
    private String incluido;
    private Timestamp dtinc;
    private Timestamp hora;
    private String alterado;
    private Timestamp dtalt;
    private Timestamp horaalt;
    private String ctaent;
    private String ctasai;
    private String filial;
    private String fil_cnae;
    private int fil_destaca_substituicao;
    private String fil_obs_padrao;
    private String fil_atividade;
    private String fil_tipo_substituicao;
    private String fil_crt;
    private String fil_crt_sn;
    private double fil_pcredsn;
    private String fil_certificado;
    private String fil_certificado_senha;
    private String fil_certificado_serie;
    private String fil_forma_tributacao;
    private String fil_insc_mun;
    private String fil_suframa;
    private String fil_natureza;
    private String fil_tipo_atividade;
    private String fil_cnpj_cont;
    private String fil_end_cont;
    private String fil_num_cont;
    private String fil_cep_cont;
    private String fil_cidade_cont;
    private String fil_bairro_cont;
    private String fil_estado_cont;
    private String fil_servidor_smtp;
    private String fil_usuario_autenticacao;
    private String fil_senha_autenticacao;
    private String fil_assunto_email;
    private String fil_corpo_email;
    private int fil_email_porta;
    private boolean fil_email_autenticacao;
    private String fil_nire;
    private int fil_cod_municipio;
    private int fil_cod_mun_cont;
    private String fil_qualificacao_assinante;
    private double fil_aliquota_pis;
    private double fil_aliquota_cofins;
    private int fil_destaca_ipi;
    private String fil_tipo_destaque_ipi;
    private String fil_cfop_vendas_tributadas_ecf;
    private String fil_cfop_vendas_subst_ecf;
    private String fil_cfop_vendas_canceladas_ecf;
    private String fil_beneficiario_prodepe;
    private String fil_enquadramento_beneficio;
    private int fil_nr_decreto_beneficio;
    private Timestamp fil_dt_decreto;
    private String fil_natureza_beneficio;
    private String fil_ind_cobranca_icms;
    private String fil_tipo_ligacao_energia;
    private String fil_grupo_tensao;
    private Timestamp fil_data_certificado;

    public Filial() {
    }

    public Filial(String fil_cod, String fil_nome, String fil_nreduz, String fil_endereço, String fil_numero, String fil_compl, String fil_bairro, String fil_cep, String fil_cidade, String fil_estado, String fil_cnpj, String fil_insc, String fil_fone, String fil_fax, String fil_contato, String fil_foncontato, String fil_cxpostal, String fil_email, String fil_socio, String fil_cpf, String fil_emailsocio, String fil_contador, String fil_cpfcont, String fil_crc, String fil_fonecont, String fil_cxpostcont, String fil_emailcont, String fil_serienf, long fil_nrnota, long fil_nrnota_nfe_2, String fil_serieselo, long fil_nrselo, String fil_serie_nfe_entrada, long fil_nrnota_nfe_entrada, String fil_almoxarifado_venda, String fil_almoxarifado_nota, String fil_clifor, String incluido, Timestamp dtinc, Timestamp hora, String alterado, Timestamp dtalt, Timestamp horaalt, String ctaent, String ctasai, String filial, String fil_cnae, int fil_destaca_substituicao, String fil_obs_padrao, String fil_atividade, String fil_tipo_substituicao, String fil_crt, String fil_crt_sn, double fil_pcredsn, String fil_certificado, String fil_certificado_senha, String fil_certificado_serie, String fil_forma_tributacao, String fil_insc_mun, String fil_suframa, String fil_natureza, String fil_tipo_atividade, String fil_cnpj_cont, String fil_end_cont, String fil_num_cont, String fil_cep_cont, String fil_cidade_cont, String fil_bairro_cont, String fil_estado_cont, String fil_servidor_smtp, String fil_usuario_autenticacao, String fil_senha_autenticacao, String fil_assunto_email, String fil_corpo_email, int fil_email_porta, boolean fil_email_autenticacao, String fil_nire, int fil_cod_municipio, int fil_cod_mun_cont, String fil_qualificacao_assinante, double fil_aliquota_pis, double fil_aliquota_cofins, int fil_destaca_ipi, String fil_tipo_destaque_ipi, String fil_cfop_vendas_tributadas_ecf, String fil_cfop_vendas_subst_ecf, String fil_cfop_vendas_canceladas_ecf, String fil_beneficiario_prodepe, String fil_enquadramento_beneficio, int fil_nr_decreto_beneficio, Timestamp fil_dt_decreto, String fil_natureza_beneficio, String fil_ind_cobranca_icms, String fil_tipo_ligacao_energia, String fil_grupo_tensao, Timestamp fil_data_certificado) {
	this.fil_cod = fil_cod;
	this.fil_nome = fil_nome;
	this.fil_nreduz = fil_nreduz;
	this.fil_endereço = fil_endereço;
	this.fil_numero = fil_numero;
	this.fil_compl = fil_compl;
	this.fil_bairro = fil_bairro;
	this.fil_cep = fil_cep;
	this.fil_cidade = fil_cidade;
	this.fil_estado = fil_estado;
	this.fil_cnpj = fil_cnpj;
	this.fil_insc = fil_insc;
	this.fil_fone = fil_fone;
	this.fil_fax = fil_fax;
	this.fil_contato = fil_contato;
	this.fil_foncontato = fil_foncontato;
	this.fil_cxpostal = fil_cxpostal;
	this.fil_email = fil_email;
	this.fil_socio = fil_socio;
	this.fil_cpf = fil_cpf;
	this.fil_emailsocio = fil_emailsocio;
	this.fil_contador = fil_contador;
	this.fil_cpfcont = fil_cpfcont;
	this.fil_crc = fil_crc;
	this.fil_fonecont = fil_fonecont;
	this.fil_cxpostcont = fil_cxpostcont;
	this.fil_emailcont = fil_emailcont;
	this.fil_serienf = fil_serienf;
	this.fil_nrnota = fil_nrnota;
	this.fil_nrnota_nfe_2 = fil_nrnota_nfe_2;
	this.fil_serieselo = fil_serieselo;
	this.fil_nrselo = fil_nrselo;
	this.fil_serie_nfe_entrada = fil_serie_nfe_entrada;
	this.fil_nrnota_nfe_entrada = fil_nrnota_nfe_entrada;
	this.fil_almoxarifado_venda = fil_almoxarifado_venda;
	this.fil_almoxarifado_nota = fil_almoxarifado_nota;
	this.fil_clifor = fil_clifor;
	this.incluido = incluido;
	this.dtinc = dtinc;
	this.hora = hora;
	this.alterado = alterado;
	this.dtalt = dtalt;
	this.horaalt = horaalt;
	this.ctaent = ctaent;
	this.ctasai = ctasai;
	this.filial = filial;
	this.fil_cnae = fil_cnae;
	this.fil_destaca_substituicao = fil_destaca_substituicao;
	this.fil_obs_padrao = fil_obs_padrao;
	this.fil_atividade = fil_atividade;
	this.fil_tipo_substituicao = fil_tipo_substituicao;
	this.fil_crt = fil_crt;
	this.fil_crt_sn = fil_crt_sn;
	this.fil_pcredsn = fil_pcredsn;
	this.fil_certificado = fil_certificado;
	this.fil_certificado_senha = fil_certificado_senha;
	this.fil_certificado_serie = fil_certificado_serie;
	this.fil_forma_tributacao = fil_forma_tributacao;
	this.fil_insc_mun = fil_insc_mun;
	this.fil_suframa = fil_suframa;
	this.fil_natureza = fil_natureza;
	this.fil_tipo_atividade = fil_tipo_atividade;
	this.fil_cnpj_cont = fil_cnpj_cont;
	this.fil_end_cont = fil_end_cont;
	this.fil_num_cont = fil_num_cont;
	this.fil_cep_cont = fil_cep_cont;
	this.fil_cidade_cont = fil_cidade_cont;
	this.fil_bairro_cont = fil_bairro_cont;
	this.fil_estado_cont = fil_estado_cont;
	this.fil_servidor_smtp = fil_servidor_smtp;
	this.fil_usuario_autenticacao = fil_usuario_autenticacao;
	this.fil_senha_autenticacao = fil_senha_autenticacao;
	this.fil_assunto_email = fil_assunto_email;
	this.fil_corpo_email = fil_corpo_email;
	this.fil_email_porta = fil_email_porta;
	this.fil_email_autenticacao = fil_email_autenticacao;
	this.fil_nire = fil_nire;
	this.fil_cod_municipio = fil_cod_municipio;
	this.fil_cod_mun_cont = fil_cod_mun_cont;
	this.fil_qualificacao_assinante = fil_qualificacao_assinante;
	this.fil_aliquota_pis = fil_aliquota_pis;
	this.fil_aliquota_cofins = fil_aliquota_cofins;
	this.fil_destaca_ipi = fil_destaca_ipi;
	this.fil_tipo_destaque_ipi = fil_tipo_destaque_ipi;
	this.fil_cfop_vendas_tributadas_ecf = fil_cfop_vendas_tributadas_ecf;
	this.fil_cfop_vendas_subst_ecf = fil_cfop_vendas_subst_ecf;
	this.fil_cfop_vendas_canceladas_ecf = fil_cfop_vendas_canceladas_ecf;
	this.fil_beneficiario_prodepe = fil_beneficiario_prodepe;
	this.fil_enquadramento_beneficio = fil_enquadramento_beneficio;
	this.fil_nr_decreto_beneficio = fil_nr_decreto_beneficio;
	this.fil_dt_decreto = fil_dt_decreto;
	this.fil_natureza_beneficio = fil_natureza_beneficio;
	this.fil_ind_cobranca_icms = fil_ind_cobranca_icms;
	this.fil_tipo_ligacao_energia = fil_tipo_ligacao_energia;
	this.fil_grupo_tensao = fil_grupo_tensao;
	this.fil_data_certificado = fil_data_certificado;
    }

    public String getFil_cod() {
	return fil_cod;
    }

    public void setFil_cod(String fil_cod) {
	this.fil_cod = fil_cod;
    }

    public String getFil_nome() {
	return fil_nome;
    }

    public void setFil_nome(String fil_nome) {
	this.fil_nome = fil_nome;
    }

    public String getFil_nreduz() {
	return fil_nreduz;
    }

    public void setFil_nreduz(String fil_nreduz) {
	this.fil_nreduz = fil_nreduz;
    }

    public String getFil_endereço() {
	return fil_endereço;
    }

    public void setFil_endereço(String fil_endereço) {
	this.fil_endereço = fil_endereço;
    }

    public String getFil_numero() {
	return fil_numero;
    }

    public void setFil_numero(String fil_numero) {
	this.fil_numero = fil_numero;
    }

    public String getFil_compl() {
	return fil_compl;
    }

    public void setFil_compl(String fil_compl) {
	this.fil_compl = fil_compl;
    }

    public String getFil_bairro() {
	return fil_bairro;
    }

    public void setFil_bairro(String fil_bairro) {
	this.fil_bairro = fil_bairro;
    }

    public String getFil_cep() {
	return fil_cep;
    }

    public void setFil_cep(String fil_cep) {
	this.fil_cep = fil_cep;
    }

    public String getFil_cidade() {
	return fil_cidade;
    }

    public void setFil_cidade(String fil_cidade) {
	this.fil_cidade = fil_cidade;
    }

    public String getFil_estado() {
	return fil_estado;
    }

    public void setFil_estado(String fil_estado) {
	this.fil_estado = fil_estado;
    }

    public String getFil_cnpj() {
	return fil_cnpj;
    }

    public void setFil_cnpj(String fil_cnpj) {
	this.fil_cnpj = fil_cnpj;
    }

    public String getFil_insc() {
	return fil_insc;
    }

    public void setFil_insc(String fil_insc) {
	this.fil_insc = fil_insc;
    }

    public String getFil_fone() {
	return fil_fone;
    }

    public void setFil_fone(String fil_fone) {
	this.fil_fone = fil_fone;
    }

    public String getFil_fax() {
	return fil_fax;
    }

    public void setFil_fax(String fil_fax) {
	this.fil_fax = fil_fax;
    }

    public String getFil_contato() {
	return fil_contato;
    }

    public void setFil_contato(String fil_contato) {
	this.fil_contato = fil_contato;
    }

    public String getFil_foncontato() {
	return fil_foncontato;
    }

    public void setFil_foncontato(String fil_foncontato) {
	this.fil_foncontato = fil_foncontato;
    }

    public String getFil_cxpostal() {
	return fil_cxpostal;
    }

    public void setFil_cxpostal(String fil_cxpostal) {
	this.fil_cxpostal = fil_cxpostal;
    }

    public String getFil_email() {
	return fil_email;
    }

    public void setFil_email(String fil_email) {
	this.fil_email = fil_email;
    }

    public String getFil_socio() {
	return fil_socio;
    }

    public void setFil_socio(String fil_socio) {
	this.fil_socio = fil_socio;
    }

    public String getFil_cpf() {
	return fil_cpf;
    }

    public void setFil_cpf(String fil_cpf) {
	this.fil_cpf = fil_cpf;
    }

    public String getFil_emailsocio() {
	return fil_emailsocio;
    }

    public void setFil_emailsocio(String fil_emailsocio) {
	this.fil_emailsocio = fil_emailsocio;
    }

    public String getFil_contador() {
	return fil_contador;
    }

    public void setFil_contador(String fil_contador) {
	this.fil_contador = fil_contador;
    }

    public String getFil_cpfcont() {
	return fil_cpfcont;
    }

    public void setFil_cpfcont(String fil_cpfcont) {
	this.fil_cpfcont = fil_cpfcont;
    }

    public String getFil_crc() {
	return fil_crc;
    }

    public void setFil_crc(String fil_crc) {
	this.fil_crc = fil_crc;
    }

    public String getFil_fonecont() {
	return fil_fonecont;
    }

    public void setFil_fonecont(String fil_fonecont) {
	this.fil_fonecont = fil_fonecont;
    }

    public String getFil_cxpostcont() {
	return fil_cxpostcont;
    }

    public void setFil_cxpostcont(String fil_cxpostcont) {
	this.fil_cxpostcont = fil_cxpostcont;
    }

    public String getFil_emailcont() {
	return fil_emailcont;
    }

    public void setFil_emailcont(String fil_emailcont) {
	this.fil_emailcont = fil_emailcont;
    }

    public String getFil_serienf() {
	return fil_serienf;
    }

    public void setFil_serienf(String fil_serienf) {
	this.fil_serienf = fil_serienf;
    }

    public long getFil_nrnota() {
	return fil_nrnota;
    }

    public void setFil_nrnota(long fil_nrnota) {
	this.fil_nrnota = fil_nrnota;
    }

    public long getFil_nrnota_nfe_2() {
	return fil_nrnota_nfe_2;
    }

    public void setFil_nrnota_nfe_2(long fil_nrnota_nfe_2) {
	this.fil_nrnota_nfe_2 = fil_nrnota_nfe_2;
    }

    public String getFil_serieselo() {
	return fil_serieselo;
    }

    public void setFil_serieselo(String fil_serieselo) {
	this.fil_serieselo = fil_serieselo;
    }

    public long getFil_nrselo() {
	return fil_nrselo;
    }

    public void setFil_nrselo(long fil_nrselo) {
	this.fil_nrselo = fil_nrselo;
    }

    public String getFil_serie_nfe_entrada() {
	return fil_serie_nfe_entrada;
    }

    public void setFil_serie_nfe_entrada(String fil_serie_nfe_entrada) {
	this.fil_serie_nfe_entrada = fil_serie_nfe_entrada;
    }

    public long getFil_nrnota_nfe_entrada() {
	return fil_nrnota_nfe_entrada;
    }

    public void setFil_nrnota_nfe_entrada(long fil_nrnota_nfe_entrada) {
	this.fil_nrnota_nfe_entrada = fil_nrnota_nfe_entrada;
    }

    public String getFil_almoxarifado_venda() {
	return fil_almoxarifado_venda;
    }

    public void setFil_almoxarifado_venda(String fil_almoxarifado_venda) {
	this.fil_almoxarifado_venda = fil_almoxarifado_venda;
    }

    public String getFil_almoxarifado_nota() {
	return fil_almoxarifado_nota;
    }

    public void setFil_almoxarifado_nota(String fil_almoxarifado_nota) {
	this.fil_almoxarifado_nota = fil_almoxarifado_nota;
    }

    public String getFil_clifor() {
	return fil_clifor;
    }

    public void setFil_clifor(String fil_clifor) {
	this.fil_clifor = fil_clifor;
    }

    public String getIncluido() {
	return incluido;
    }

    public void setIncluido(String incluido) {
	this.incluido = incluido;
    }

    public Timestamp getDtinc() {
	return dtinc;
    }

    public void setDtinc(Timestamp dtinc) {
	this.dtinc = dtinc;
    }

    public Timestamp getHora() {
	return hora;
    }

    public void setHora(Timestamp hora) {
	this.hora = hora;
    }

    public String getAlterado() {
	return alterado;
    }

    public void setAlterado(String alterado) {
	this.alterado = alterado;
    }

    public Timestamp getDtalt() {
	return dtalt;
    }

    public void setDtalt(Timestamp dtalt) {
	this.dtalt = dtalt;
    }

    public Timestamp getHoraalt() {
	return horaalt;
    }

    public void setHoraalt(Timestamp horaalt) {
	this.horaalt = horaalt;
    }

    public String getCtaent() {
	return ctaent;
    }

    public void setCtaent(String ctaent) {
	this.ctaent = ctaent;
    }

    public String getCtasai() {
	return ctasai;
    }

    public void setCtasai(String ctasai) {
	this.ctasai = ctasai;
    }

    public String getFilial() {
	return filial;
    }

    public void setFilial(String filial) {
	this.filial = filial;
    }

    public String getFil_cnae() {
	return fil_cnae;
    }

    public void setFil_cnae(String fil_cnae) {
	this.fil_cnae = fil_cnae;
    }

    public int getFil_destaca_substituicao() {
	return fil_destaca_substituicao;
    }

    public void setFil_destaca_substituicao(int fil_destaca_substituicao) {
	this.fil_destaca_substituicao = fil_destaca_substituicao;
    }

    public String getFil_obs_padrao() {
	return fil_obs_padrao;
    }

    public void setFil_obs_padrao(String fil_obs_padrao) {
	this.fil_obs_padrao = fil_obs_padrao;
    }

    public String getFil_atividade() {
	return fil_atividade;
    }

    public void setFil_atividade(String fil_atividade) {
	this.fil_atividade = fil_atividade;
    }

    public String getFil_tipo_substituicao() {
	return fil_tipo_substituicao;
    }

    public void setFil_tipo_substituicao(String fil_tipo_substituicao) {
	this.fil_tipo_substituicao = fil_tipo_substituicao;
    }

    public String getFil_crt() {
	return fil_crt;
    }

    public void setFil_crt(String fil_crt) {
	this.fil_crt = fil_crt;
    }

    public String getFil_crt_sn() {
	return fil_crt_sn;
    }

    public void setFil_crt_sn(String fil_crt_sn) {
	this.fil_crt_sn = fil_crt_sn;
    }

    public double getFil_pcredsn() {
	return fil_pcredsn;
    }

    public void setFil_pcredsn(double fil_pcredsn) {
	this.fil_pcredsn = fil_pcredsn;
    }

    public String getFil_certificado() {
	return fil_certificado;
    }

    public void setFil_certificado(String fil_certificado) {
	this.fil_certificado = fil_certificado;
    }

    public String getFil_certificado_senha() {
	return fil_certificado_senha;
    }

    public void setFil_certificado_senha(String fil_certificado_senha) {
	this.fil_certificado_senha = fil_certificado_senha;
    }

    public String getFil_certificado_serie() {
	return fil_certificado_serie;
    }

    public void setFil_certificado_serie(String fil_certificado_serie) {
	this.fil_certificado_serie = fil_certificado_serie;
    }

    public String getFil_forma_tributacao() {
	return fil_forma_tributacao;
    }

    public void setFil_forma_tributacao(String fil_forma_tributacao) {
	this.fil_forma_tributacao = fil_forma_tributacao;
    }

    public String getFil_insc_mun() {
	return fil_insc_mun;
    }

    public void setFil_insc_mun(String fil_insc_mun) {
	this.fil_insc_mun = fil_insc_mun;
    }

    public String getFil_suframa() {
	return fil_suframa;
    }

    public void setFil_suframa(String fil_suframa) {
	this.fil_suframa = fil_suframa;
    }

    public String getFil_natureza() {
	return fil_natureza;
    }

    public void setFil_natureza(String fil_natureza) {
	this.fil_natureza = fil_natureza;
    }

    public String getFil_tipo_atividade() {
	return fil_tipo_atividade;
    }

    public void setFil_tipo_atividade(String fil_tipo_atividade) {
	this.fil_tipo_atividade = fil_tipo_atividade;
    }

    public String getFil_cnpj_cont() {
	return fil_cnpj_cont;
    }

    public void setFil_cnpj_cont(String fil_cnpj_cont) {
	this.fil_cnpj_cont = fil_cnpj_cont;
    }

    public String getFil_end_cont() {
	return fil_end_cont;
    }

    public void setFil_end_cont(String fil_end_cont) {
	this.fil_end_cont = fil_end_cont;
    }

    public String getFil_num_cont() {
	return fil_num_cont;
    }

    public void setFil_num_cont(String fil_num_cont) {
	this.fil_num_cont = fil_num_cont;
    }

    public String getFil_cep_cont() {
	return fil_cep_cont;
    }

    public void setFil_cep_cont(String fil_cep_cont) {
	this.fil_cep_cont = fil_cep_cont;
    }

    public String getFil_cidade_cont() {
	return fil_cidade_cont;
    }

    public void setFil_cidade_cont(String fil_cidade_cont) {
	this.fil_cidade_cont = fil_cidade_cont;
    }

    public String getFil_bairro_cont() {
	return fil_bairro_cont;
    }

    public void setFil_bairro_cont(String fil_bairro_cont) {
	this.fil_bairro_cont = fil_bairro_cont;
    }

    public String getFil_estado_cont() {
	return fil_estado_cont;
    }

    public void setFil_estado_cont(String fil_estado_cont) {
	this.fil_estado_cont = fil_estado_cont;
    }

    public String getFil_servidor_smtp() {
	return fil_servidor_smtp;
    }

    public void setFil_servidor_smtp(String fil_servidor_smtp) {
	this.fil_servidor_smtp = fil_servidor_smtp;
    }

    public String getFil_usuario_autenticacao() {
	return fil_usuario_autenticacao;
    }

    public void setFil_usuario_autenticacao(String fil_usuario_autenticacao) {
	this.fil_usuario_autenticacao = fil_usuario_autenticacao;
    }

    public String getFil_senha_autenticacao() {
	return fil_senha_autenticacao;
    }

    public void setFil_senha_autenticacao(String fil_senha_autenticacao) {
	this.fil_senha_autenticacao = fil_senha_autenticacao;
    }

    public String getFil_assunto_email() {
	return fil_assunto_email;
    }

    public void setFil_assunto_email(String fil_assunto_email) {
	this.fil_assunto_email = fil_assunto_email;
    }

    public String getFil_corpo_email() {
	return fil_corpo_email;
    }

    public void setFil_corpo_email(String fil_corpo_email) {
	this.fil_corpo_email = fil_corpo_email;
    }

    public int getFil_email_porta() {
	return fil_email_porta;
    }

    public void setFil_email_porta(int fil_email_porta) {
	this.fil_email_porta = fil_email_porta;
    }

    public boolean isFil_email_autenticacao() {
	return fil_email_autenticacao;
    }

    public void setFil_email_autenticacao(boolean fil_email_autenticacao) {
	this.fil_email_autenticacao = fil_email_autenticacao;
    }

    public String getFil_nire() {
	return fil_nire;
    }

    public void setFil_nire(String fil_nire) {
	this.fil_nire = fil_nire;
    }

    public int getFil_cod_municipio() {
	return fil_cod_municipio;
    }

    public void setFil_cod_municipio(int fil_cod_municipio) {
	this.fil_cod_municipio = fil_cod_municipio;
    }

    public int getFil_cod_mun_cont() {
	return fil_cod_mun_cont;
    }

    public void setFil_cod_mun_cont(int fil_cod_mun_cont) {
	this.fil_cod_mun_cont = fil_cod_mun_cont;
    }

    public String getFil_qualificacao_assinante() {
	return fil_qualificacao_assinante;
    }

    public void setFil_qualificacao_assinante(String fil_qualificacao_assinante) {
	this.fil_qualificacao_assinante = fil_qualificacao_assinante;
    }

    public double getFil_aliquota_pis() {
	return fil_aliquota_pis;
    }

    public void setFil_aliquota_pis(double fil_aliquota_pis) {
	this.fil_aliquota_pis = fil_aliquota_pis;
    }

    public double getFil_aliquota_cofins() {
	return fil_aliquota_cofins;
    }

    public void setFil_aliquota_cofins(double fil_aliquota_cofins) {
	this.fil_aliquota_cofins = fil_aliquota_cofins;
    }

    public int getFil_destaca_ipi() {
	return fil_destaca_ipi;
    }

    public void setFil_destaca_ipi(int fil_destaca_ipi) {
	this.fil_destaca_ipi = fil_destaca_ipi;
    }

    public String getFil_tipo_destaque_ipi() {
	return fil_tipo_destaque_ipi;
    }

    public void setFil_tipo_destaque_ipi(String fil_tipo_destaque_ipi) {
	this.fil_tipo_destaque_ipi = fil_tipo_destaque_ipi;
    }

    public String getFil_cfop_vendas_tributadas_ecf() {
	return fil_cfop_vendas_tributadas_ecf;
    }

    public void setFil_cfop_vendas_tributadas_ecf(String fil_cfop_vendas_tributadas_ecf) {
	this.fil_cfop_vendas_tributadas_ecf = fil_cfop_vendas_tributadas_ecf;
    }

    public String getFil_cfop_vendas_subst_ecf() {
	return fil_cfop_vendas_subst_ecf;
    }

    public void setFil_cfop_vendas_subst_ecf(String fil_cfop_vendas_subst_ecf) {
	this.fil_cfop_vendas_subst_ecf = fil_cfop_vendas_subst_ecf;
    }

    public String getFil_cfop_vendas_canceladas_ecf() {
	return fil_cfop_vendas_canceladas_ecf;
    }

    public void setFil_cfop_vendas_canceladas_ecf(String fil_cfop_vendas_canceladas_ecf) {
	this.fil_cfop_vendas_canceladas_ecf = fil_cfop_vendas_canceladas_ecf;
    }

    public String getFil_beneficiario_prodepe() {
	return fil_beneficiario_prodepe;
    }

    public void setFil_beneficiario_prodepe(String fil_beneficiario_prodepe) {
	this.fil_beneficiario_prodepe = fil_beneficiario_prodepe;
    }

    public String getFil_enquadramento_beneficio() {
	return fil_enquadramento_beneficio;
    }

    public void setFil_enquadramento_beneficio(String fil_enquadramento_beneficio) {
	this.fil_enquadramento_beneficio = fil_enquadramento_beneficio;
    }

    public int getFil_nr_decreto_beneficio() {
	return fil_nr_decreto_beneficio;
    }

    public void setFil_nr_decreto_beneficio(int fil_nr_decreto_beneficio) {
	this.fil_nr_decreto_beneficio = fil_nr_decreto_beneficio;
    }

    public Timestamp getFil_dt_decreto() {
	return fil_dt_decreto;
    }

    public void setFil_dt_decreto(Timestamp fil_dt_decreto) {
	this.fil_dt_decreto = fil_dt_decreto;
    }

    public String getFil_natureza_beneficio() {
	return fil_natureza_beneficio;
    }

    public void setFil_natureza_beneficio(String fil_natureza_beneficio) {
	this.fil_natureza_beneficio = fil_natureza_beneficio;
    }

    public String getFil_ind_cobranca_icms() {
	return fil_ind_cobranca_icms;
    }

    public void setFil_ind_cobranca_icms(String fil_ind_cobranca_icms) {
	this.fil_ind_cobranca_icms = fil_ind_cobranca_icms;
    }

    public String getFil_tipo_ligacao_energia() {
	return fil_tipo_ligacao_energia;
    }

    public void setFil_tipo_ligacao_energia(String fil_tipo_ligacao_energia) {
	this.fil_tipo_ligacao_energia = fil_tipo_ligacao_energia;
    }

    public String getFil_grupo_tensao() {
	return fil_grupo_tensao;
    }

    public void setFil_grupo_tensao(String fil_grupo_tensao) {
	this.fil_grupo_tensao = fil_grupo_tensao;
    }

    public Timestamp getFil_data_certificado() {
	return fil_data_certificado;
    }

    public void setFil_data_certificado(Timestamp fil_data_certificado) {
	this.fil_data_certificado = fil_data_certificado;
    }

    @Override
    public String toString() {
        return this.getFil_cod() + " - " + this.getFil_nreduz();
    }
    
}
