/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.entidades;

/**
 *
 * @author allison
 */
public class Inutilizacao {

    private int inu_id;
    private String inu_ano;
    private int inu_numeronotainicial;
    private int inu_numeronotafinal;
    private String inu_justificativa;
    private String inu_enviada;
    private String inu_serie;
    private String inu_numeroprotocolo;
    private String inu_datahora;
    private String inu_filial;

    public Inutilizacao() {
    }

    public Inutilizacao(int inu_id, String inu_ano, int inu_numeronotainicial, int inu_numeronotafinal, String inu_justificativa, String inu_enviada, String inu_serie, String inu_numeroprotocolo, String inu_datahora, String inu_filial) {
        this.inu_id = inu_id;
        this.inu_ano = inu_ano;
        this.inu_numeronotainicial = inu_numeronotainicial;
        this.inu_numeronotafinal = inu_numeronotafinal;
        this.inu_justificativa = inu_justificativa;
        this.inu_enviada = inu_enviada;
        this.inu_serie = inu_serie;
        this.inu_numeroprotocolo = inu_numeroprotocolo;
        this.inu_datahora = inu_datahora;
        this.inu_filial = inu_filial;
    }

    public int getInu_id() {
        return inu_id;
    }

    public void setInu_id(int inu_id) {
        this.inu_id = inu_id;
    }

    public String getInu_ano() {
        return inu_ano;
    }

    public void setInu_ano(String inu_ano) {
        this.inu_ano = inu_ano;
    }

    public int getInu_numeronotainicial() {
        return inu_numeronotainicial;
    }

    public void setInu_numeronotainicial(int inu_numeronotainicial) {
        this.inu_numeronotainicial = inu_numeronotainicial;
    }

    public int getInu_numeronotafinal() {
        return inu_numeronotafinal;
    }

    public void setInu_numeronotafinal(int inu_numeronotafinal) {
        this.inu_numeronotafinal = inu_numeronotafinal;
    }

    public String getInu_justificativa() {
        return inu_justificativa;
    }

    public void setInu_justificativa(String inu_justificativa) {
        this.inu_justificativa = inu_justificativa;
    }

    public String getInu_enviada() {
        return inu_enviada;
    }

    public void setInu_enviada(String inu_enviada) {
        this.inu_enviada = inu_enviada;
    }

    public String getInu_serie() {
        return inu_serie;
    }

    public void setInu_serie(String inu_serie) {
        this.inu_serie = inu_serie;
    }

    public String getInu_numeroprotocolo() {
        return inu_numeroprotocolo;
    }

    public void setInu_numeroprotocolo(String inu_numeroprotocolo) {
        this.inu_numeroprotocolo = inu_numeroprotocolo;
    }

    public String getInu_datahora() {
        return inu_datahora;
    }

    public void setInu_datahora(String inu_datahora) {
        this.inu_datahora = inu_datahora;
    }

    public String getInu_filial() {
        return inu_filial;
    }

    public void setInu_filial(String inu_filial) {
        this.inu_filial = inu_filial;
    }
}
