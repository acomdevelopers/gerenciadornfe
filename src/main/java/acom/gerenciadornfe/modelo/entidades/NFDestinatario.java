package acom.gerenciadornfe.modelo.entidades;

import java.sql.Timestamp;

/**
 * @author Allison
 */
public class NFDestinatario {
    
    private int nf_id;
    private int emp_id;
    private int nf_serie;
    private int nf_numero;
    private String nf_cnpj;
    private String nf_nome;
    private Timestamp nf_emissao;
    private double nf_valor;
    private String nf_tipo;
    private String nf_situacao;
    private String nf_chave;
    private String nf_ie;
    private String nf_protocolo;
    private Timestamp nf_data_recibo;
    private String nf_manifestacao;
    private String nf_download;
    private String nf_ambiente;
    private String nf_nsu;
    private String nf_visivel;

    public NFDestinatario() {
    }

    public NFDestinatario(int nf_id, int emp_id, int nf_serie, int nf_numero, String nf_cnpj, String nf_nome, Timestamp nf_emissao, double nf_valor, String nf_tipo, String nf_situacao, String nf_chave, String nf_ie, String nf_protocolo, Timestamp nf_data_recibo, String nf_manifestacao, String nf_download, String nf_ambiente, String nf_nsu, String nf_visivel) {
	this.nf_id = nf_id;
	this.emp_id = emp_id;
	this.nf_serie = nf_serie;
	this.nf_numero = nf_numero;
	this.nf_cnpj = nf_cnpj;
	this.nf_nome = nf_nome;
	this.nf_emissao = nf_emissao;
	this.nf_valor = nf_valor;
	this.nf_tipo = nf_tipo;
	this.nf_situacao = nf_situacao;
	this.nf_chave = nf_chave;
	this.nf_ie = nf_ie;
	this.nf_protocolo = nf_protocolo;
	this.nf_data_recibo = nf_data_recibo;
	this.nf_manifestacao = nf_manifestacao;
	this.nf_download = nf_download;
	this.nf_ambiente = nf_ambiente;
	this.nf_nsu = nf_nsu;
	this.nf_visivel = nf_visivel;
    }

    public int getNf_id() {
	return nf_id;
    }

    public void setNf_id(int nf_id) {
	this.nf_id = nf_id;
    }

    public int getEmp_id() {
	return emp_id;
    }

    public void setEmp_id(int emp_id) {
	this.emp_id = emp_id;
    }

    public int getNf_serie() {
	return nf_serie;
    }

    public void setNf_serie(int nf_serie) {
	this.nf_serie = nf_serie;
    }

    public int getNf_numero() {
	return nf_numero;
    }

    public void setNf_numero(int nf_numero) {
	this.nf_numero = nf_numero;
    }

    public String getNf_cnpj() {
	return nf_cnpj;
    }

    public void setNf_cnpj(String nf_cnpj) {
	this.nf_cnpj = nf_cnpj;
    }

    public String getNf_nome() {
	return nf_nome;
    }

    public void setNf_nome(String nf_nome) {
	this.nf_nome = nf_nome;
    }

    public Timestamp getNf_emissao() {
	return nf_emissao;
    }

    public void setNf_emissao(Timestamp nf_emissao) {
	this.nf_emissao = nf_emissao;
    }

    public double getNf_valor() {
	return nf_valor;
    }

    public void setNf_valor(double nf_valor) {
	this.nf_valor = nf_valor;
    }

    public String getNf_tipo() {
	return nf_tipo;
    }

    public void setNf_tipo(String nf_tipo) {
	this.nf_tipo = nf_tipo;
    }

    public String getNf_situacao() {
	return nf_situacao;
    }

    public void setNf_situacao(String nf_situacao) {
	this.nf_situacao = nf_situacao;
    }

    public String getNf_chave() {
	return nf_chave;
    }

    public void setNf_chave(String nf_chave) {
	this.nf_chave = nf_chave;
    }

    public String getNf_ie() {
	return nf_ie;
    }

    public void setNf_ie(String nf_ie) {
	this.nf_ie = nf_ie;
    }

    public String getNf_protocolo() {
	return nf_protocolo;
    }

    public void setNf_protocolo(String nf_protocolo) {
	this.nf_protocolo = nf_protocolo;
    }

    public Timestamp getNf_data_recibo() {
	return nf_data_recibo;
    }

    public void setNf_data_recibo(Timestamp nf_data_recibo) {
	this.nf_data_recibo = nf_data_recibo;
    }

    public String getNf_manifestacao() {
	return nf_manifestacao;
    }

    public void setNf_manifestacao(String nf_manifestacao) {
	this.nf_manifestacao = nf_manifestacao;
    }

    public String getNf_download() {
	return nf_download;
    }

    public void setNf_download(String nf_download) {
	this.nf_download = nf_download;
    }

    public String getNf_ambiente() {
	return nf_ambiente;
    }

    public void setNf_ambiente(String nf_ambiente) {
	this.nf_ambiente = nf_ambiente;
    }

    public String getNf_nsu() {
	return nf_nsu;
    }

    public void setNf_nsu(String nf_nsu) {
	this.nf_nsu = nf_nsu;
    }

    public String getNf_visivel() {
	return nf_visivel;
    }

    public void setNf_visivel(String nf_visivel) {
	this.nf_visivel = nf_visivel;
    }

}
