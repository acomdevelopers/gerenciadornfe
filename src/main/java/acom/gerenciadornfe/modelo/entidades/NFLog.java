package acom.gerenciadornfe.modelo.entidades;

import java.sql.Timestamp;

/**
 * @author Allison
 */
public class NFLog {

    private int nflog_id;
    private int nflog_id_nf;
    private int nflog_operacao;
    private Timestamp nflog_data_hora;
    private String nflog_historico;

    public NFLog() {
    }

    public NFLog(int nflog_id, int nflog_id_nf, int nflog_operacao, Timestamp nflog_data_hora, String nflog_historico) {
	this.nflog_id = nflog_id;
	this.nflog_id_nf = nflog_id_nf;
	this.nflog_operacao = nflog_operacao;
	this.nflog_data_hora = nflog_data_hora;
	this.nflog_historico = nflog_historico;
    }

    public int getNflog_id() {
	return nflog_id;
    }

    public void setNflog_id(int nflog_id) {
	this.nflog_id = nflog_id;
    }

    public int getNflog_id_nf() {
	return nflog_id_nf;
    }

    public void setNflog_id_nf(int nflog_id_nf) {
	this.nflog_id_nf = nflog_id_nf;
    }

    public int getNflog_operacao() {
	return nflog_operacao;
    }

    public void setNflog_operacao(int nflog_operacao) {
	this.nflog_operacao = nflog_operacao;
    }

    public Timestamp getNflog_data_hora() {
	return nflog_data_hora;
    }

    public void setNflog_data_hora(Timestamp nflog_data_hora) {
	this.nflog_data_hora = nflog_data_hora;
    }

    public String getNflog_historico() {
	return nflog_historico;
    }

    public void setNflog_historico(String nflog_historico) {
	this.nflog_historico = nflog_historico;
    }

}
