package acom.gerenciadornfe.modelo.entidades;

import java.sql.Timestamp;
import javafx.scene.control.CheckBox;

/**
 *
 * @author allison
 */
public class NotaFiscal {

    private String nf_numero;
    private String nf_tipomov;
    private String nf_formulario;
    private String nf_serie;
    private String nf_nrnota;
    private String nf_filial;
    private String nf_clifor;
    private String nf_uf;
    private Timestamp nf_dtemissao;
    private Timestamp nf_data;
    private String nf_nome;
    private double nf_base_n;
    private double nf_picms_n;
    private double nf_vicms_n;
    private double nf_isento;
    private double nf_outros;
    private double nf_piss;
    private double nf_viss;
    private double nf_base_s;
    private double nf_vicms_s;
    private double nf_vipi;
    private double nf_vproduto;
    private double nf_totnota;
    private String nf_obs;
    private int nf_volume;
    private double nf_pbruto;
    private double nf_pliquido;
    private String nf_serieselo;
    private String nf_nrselo;
    private String nf_tes;
    private String nf_cfop;
    private String nf_cancelada;
    private double nf_frete;
    private double nf_despesas;
    private double nf_desconto;
    private int nf_tipo;
    private String nf_transportadora;
    private int nf_tipofrete;
    private String nf_flag;
    private String filial;
    private String nf_dup1_numero;
    private Timestamp nf_dup1_vencimento;
    private double nf_dup1_valor;
    private String nf_dup2_numero;
    private Timestamp nf_dup2_vencimento;
    private double nf_dup2_valor;
    private String nf_dup3_numero;
    private Timestamp nf_dup3_vencimento;
    private double nf_dup3_valor;
    private String nf_dup4_numero;
    private Timestamp nf_dup4_vencimento;
    private double nf_dup4_valor;
    private String nf_dup5_numero;
    private Timestamp nf_dup5_vencimento;
    private double nf_dup5_valor;
    private String nf_dup6_numero;
    private Timestamp nf_dup6_vencimento;
    private double nf_dup6_valor;
    private String nf_almoxarifado;
    private String codlan;
    private double nf_baseipi;
    private String nf_modelo;
    private double nf_seguro;
    private String nf_msg_corpo;
    private String nf_marca;
    private String nf_cod_energia;
    private String nf_cfop_nota;
    private String nf_placa;
    private String nf_especie;
    private String nfe_numeracao_volume;
    private String nfeIndicadorFormaPagamento;
    private String nfeFormatoImpressaoDANFe;
    private String nfeFormatoEmissao;
    private boolean nfeEnviada;
    private int nfeCodigoNota;
    private String nfeChaveAcesso;
    private String nfeChaveAdicional;
    private long nfeRecibo;
    private long nfeProtocolo;
    private String nfeProtocoloDataHora;
    private long nfeCanceladaProtocolo;
    private String nfeCanceladaJustificativa;
    private boolean nfeCancelada;
    private String nfefinalidade;
    private String nfeemail;
    private String nfecontingenciadatahora;
    private String nfecontingenciajustificativa;
    private String nfe_crt;
    private boolean marcado;
    private boolean nf_tipo_importacao;
    private double nf_imposto_importacao;
    private double nf_valor_pis;
    private double nf_valor_cofins;
    private double nf_outros_impostos_importacao;
    private double nf_despesas_adjuaneiras;
    private double nf_subtotal_importacao;
    private String nFeAmbiente;
    private String nf_caixa;
    private boolean nf_imprime_dtsaida;
    private boolean nf_denegada;
    private String nfeChaveNFeoriginal;
    private double nf_icms_antecipado;
    private boolean nf_altman;
    private String nf_dmi_numero;
    private Timestamp nf_dmi_data;
    private String nf_dmi_cidade;
    private String nf_dmi_uf;
    private String nf_dmi_codigo_exportador;
    private double nf_tributos_valor;
    private double nf_tributos_aliquota;
    private String nf_parcelado;
    private int nf_qtd_parcelas;
    private int nf_dias_prazo;
    private long nf_protocolo;
    private long seq_sincronizacao;
    private double nf_icm_creditado;
    private double nf_base_icm_creditado;
    private String nf_efd_mes_ano;
    private String nf_dmi_viatransporte;
    private double nf_dmi_valor_afrmm;
    private String nf_incentivo_prodepe;
    private String nf_ecf_numero;
    private String controle_alteracao;
    private String nf_tipo_operacao;
    private double nf_opi_vicms_destino;
    private double nf_opi_vicms_origem;
    private double nf_opi_vicms_fcp;
    private double nf_vicms_interestadual;
    private String nf_importacao_deferido;
    private double nf_importacao_deferido_p;
    private double nf_valor_importacao;
    private String nf_modelo_importacao;
    private String nfestatus;
    private double nf_vfcp;
    private double nf_vicmsufdest;
    private double nf_vicmsufremet;
    private double nf_fcpst_basecalculo;
    private double nf_fcpst_valor;
    private double nf_fcpst_valorretido;
    private String nf_infadicionaisfisco;
    private String nf_documento;
    private Timestamp hora;
    private String fil_cod;
    private String fil_nome;
    private String fil_bairro;
    private String fil_cidade;
    private String fil_compl;
    private String fil_endereço;
    private String fil_estado;
    private String fil_fone;
    private String fil_nreduz;
    private String fil_numero;
    private String fil_cep;
    private String fil_cnpj;
    private String fil_insc;
    private String fil_cnae;
    private String fil_insc_mun;
    private String fil_obs_padrao;
    private String fil_crt;
    private String fil_crt_sn;
    private String fil_forma_tributacao;
    private double fil_pcredsn;
    private String fil_tipo_atividade;
    private String fil_emailcont;

    private String pes_bai;
    private String pes_cidade;
    private String pes_compl;
    private String pes_end;
    private String pes_fone1;
    private String pes_nome45;
    private String pes_nrend;
    private String pes_uf;
    private String pes_cep;
    private String pes_cnpj;
    private String pes_cpf;
    private String pes_ie;
    private String pes_indicador_consumidor;
    private String pes_indicador_ie;
    private int pes_fj;
    private String pes_email;
    private double pes_aliq_subst;
    private String pes_suframa;
    private int transp_fj;
    private String transp_nome45;
    private String transp_cnpj;
    private String transp_ie;
    private String transp_cpf;
    private String transp_end;
    private String transp_nrend;
    private String transp_bai;
    private String transp_cidade;
    private String transp_uf;
    private String transp_codigo_antt;
    private String cfo_texto;
    private String tes_texto;
    private CheckBox checkBox;

    public NotaFiscal() {
    }

    public NotaFiscal(CheckBox checkBox, NotaFiscal nf) {
	this.nf_numero = nf.getNf_numero();
	this.nf_tipomov = nf.getNf_tipomov();
	this.nf_formulario = nf.getNf_formulario();
	this.nf_serie = nf.getNf_serie();
	this.nf_nrnota = nf.getNf_nrnota();
	this.nf_filial = nf.getNf_filial();
	this.nf_clifor = nf.getNf_clifor();
	this.nf_uf = nf.getNf_uf();
	this.nf_dtemissao = nf.getNf_dtemissao();
	this.nf_data = nf.getNf_data();
	this.nf_nome = nf.getNf_nome();
	this.nf_base_n = nf.getNf_base_n();
	this.nf_picms_n = nf.getNf_picms_n();
	this.nf_vicms_n = nf.getNf_vicms_n();
	this.nf_isento = nf.getNf_isento();
	this.nf_outros = nf.getNf_outros();
	this.nf_piss = nf.getNf_piss();
	this.nf_viss = nf.getNf_viss();
	this.nf_base_s = nf.getNf_base_s();
	this.nf_vicms_s = nf.getNf_vicms_s();
	this.nf_vipi = nf.getNf_vipi();
	this.nf_vproduto = nf.getNf_vproduto();
	this.nf_totnota = nf.getNf_totnota();
	this.nf_obs = nf.getNf_obs();
	this.nf_volume = nf.getNf_volume();
	this.nf_pbruto = nf.getNf_pbruto();
	this.nf_pliquido = nf.getNf_pliquido();
	this.nf_serieselo = nf.getNf_serieselo();
	this.nf_nrselo = nf.getNf_nrselo();
	this.nf_tes = nf.getNf_tes();
	this.nf_cfop = nf.getNf_cfop();
	this.nf_cancelada = nf.getNf_cancelada();
	this.nf_frete = nf.getNf_frete();
	this.nf_despesas = nf.getNf_despesas();
	this.nf_desconto = nf.getNf_desconto();
	this.nf_tipo = nf.getNf_tipo();
	this.nf_transportadora = nf.getNf_transportadora();
	this.nf_tipofrete = nf.getNf_tipofrete();
	this.nf_flag = nf.getNf_flag();
	this.filial = nf.getFilial();
	this.nf_dup1_numero = nf.getNf_dup1_numero();
	this.nf_dup1_vencimento = nf.getNf_dup1_vencimento();
	this.nf_dup1_valor = nf.getNf_dup1_valor();
	this.nf_dup2_numero = nf.getNf_dup2_numero();
	this.nf_dup2_vencimento = nf.getNf_dup2_vencimento();
	this.nf_dup2_valor = nf.getNf_dup2_valor();
	this.nf_dup3_numero = nf.getNf_dup3_numero();
	this.nf_dup3_vencimento = nf.getNf_dup3_vencimento();
	this.nf_dup3_valor = nf.getNf_dup3_valor();
	this.nf_dup4_numero = nf.getNf_dup4_numero();
	this.nf_dup4_vencimento = nf.getNf_dup4_vencimento();
	this.nf_dup4_valor = nf.getNf_dup4_valor();
	this.nf_dup5_numero = nf.getNf_dup5_numero();
	this.nf_dup5_vencimento = nf.getNf_dup5_vencimento();
	this.nf_dup5_valor = nf.getNf_dup5_valor();
	this.nf_dup6_numero = nf.getNf_dup6_numero();
	this.nf_dup6_vencimento = nf.getNf_dup6_vencimento();
	this.nf_dup6_valor = nf.getNf_dup6_valor();
	this.nf_almoxarifado = nf.getNf_almoxarifado();
	this.codlan = nf.getCodlan();
	this.nf_baseipi = nf.getNf_baseipi();
	this.nf_modelo = nf.getNf_modelo();
	this.nf_seguro = nf.getNf_seguro();
	this.nf_msg_corpo = nf.getNf_msg_corpo();
	this.nf_marca = nf.getNf_marca();
	this.nf_cod_energia = nf.getNf_cod_energia();
	this.nf_cfop_nota = nf.getNf_cfop_nota();
	this.nf_placa = nf.getNf_placa();
	this.nf_especie = nf.getNf_especie();
	this.nfe_numeracao_volume = nf.getNfe_numeracao_volume();
	this.nfeIndicadorFormaPagamento = nf.getNfeIndicadorFormaPagamento();
	this.nfeFormatoImpressaoDANFe = nf.getNfeFormatoImpressaoDANFe();
	this.nfeFormatoEmissao = nf.getNfeFormatoEmissao();
	this.nfeEnviada = nf.isNfeEnviada();
	this.nfeCodigoNota = nf.getNfeCodigoNota();
	this.nfeChaveAcesso = nf.getNfeChaveAcesso();
	this.nfeChaveAdicional = nf.getNfeChaveAdicional();
	this.nfeRecibo = nf.getNfeRecibo();
	this.nfeProtocolo = nf.getNfeProtocolo();
	this.nfeProtocoloDataHora = nf.getNfeProtocoloDataHora();
	this.nfeCanceladaProtocolo = nf.getNfeCanceladaProtocolo();
	this.nfeCanceladaJustificativa = nf.getNfeCanceladaJustificativa();
	this.nfeCancelada = nf.isNfeCancelada();
	this.nfefinalidade = nf.getNfefinalidade();
	this.nfeemail = nf.getNfeemail();
	this.nfecontingenciadatahora = nf.getNfecontingenciadatahora();
	this.nfecontingenciajustificativa = nf.getNfecontingenciajustificativa();
	this.nfe_crt = nf.getNfe_crt();
	this.marcado = nf.isMarcado();
	this.nf_tipo_importacao = nf.isNf_tipo_importacao();
	this.nf_imposto_importacao = nf.getNf_imposto_importacao();
	this.nf_valor_pis = nf.getNf_valor_pis();
	this.nf_valor_cofins = nf.getNf_valor_cofins();
	this.nf_outros_impostos_importacao = nf.getNf_outros_impostos_importacao();
	this.nf_despesas_adjuaneiras = nf.getNf_despesas_adjuaneiras();
	this.nf_subtotal_importacao = nf.getNf_subtotal_importacao();
	this.nFeAmbiente = nf.getnFeAmbiente();
	this.nf_caixa = nf.getNf_caixa();
	this.nf_imprime_dtsaida = nf.isNf_imprime_dtsaida();
	this.nf_denegada = nf.isNf_denegada();
	this.nfeChaveNFeoriginal = nf.getNfeChaveNFeoriginal();
	this.nf_icms_antecipado = nf.getNf_icms_antecipado();
	this.nf_altman = nf.isNf_altman();
	this.nf_dmi_numero = nf.getNf_dmi_numero();
	this.nf_dmi_data = nf.getNf_dmi_data();
	this.nf_dmi_cidade = nf.getNf_dmi_cidade();
	this.nf_dmi_uf = nf.getNf_dmi_uf();
	this.nf_dmi_codigo_exportador = nf.getNf_dmi_codigo_exportador();
	this.nf_tributos_valor = nf.getNf_tributos_valor();
	this.nf_tributos_aliquota = nf.getNf_tributos_aliquota();
	this.nf_parcelado = nf.getNf_parcelado();
	this.nf_qtd_parcelas = nf.getNf_qtd_parcelas();
	this.nf_dias_prazo = nf.getNf_dias_prazo();
	this.nf_protocolo = nf.getNf_protocolo();
	this.seq_sincronizacao = nf.getSeq_sincronizacao();
	this.nf_icm_creditado = nf.getNf_icm_creditado();
	this.nf_base_icm_creditado = nf.getNf_base_icm_creditado();
	this.nf_efd_mes_ano = nf.getNf_efd_mes_ano();
	this.nf_dmi_viatransporte = nf.getNf_dmi_viatransporte();
	this.nf_dmi_valor_afrmm = nf.getNf_dmi_valor_afrmm();
	this.nf_incentivo_prodepe = nf.getNf_incentivo_prodepe();
	this.nf_ecf_numero = nf.getNf_ecf_numero();
	this.controle_alteracao = nf.getControle_alteracao();
	this.nf_tipo_operacao = nf.getNf_tipo_operacao();
	this.nf_opi_vicms_destino = nf.getNf_opi_vicms_destino();
	this.nf_opi_vicms_origem = nf.getNf_opi_vicms_origem();
	this.nf_opi_vicms_fcp = nf.getNf_opi_vicms_fcp();
	this.nf_vicms_interestadual = nf.getNf_vicms_interestadual();
	this.nf_importacao_deferido = nf.getNf_importacao_deferido();
	this.nf_importacao_deferido_p = nf.getNf_importacao_deferido_p();
	this.nf_valor_importacao = nf.getNf_valor_importacao();
	this.nf_modelo_importacao = nf.getNf_modelo_importacao();
	this.nfestatus = nf.getNfestatus();
	this.nf_vfcp = nf.getNf_vfcp();
	this.nf_vicmsufdest = nf.getNf_vicmsufdest();
	this.nf_vicmsufremet = nf.getNf_vicmsufremet();
	this.nf_fcpst_basecalculo = nf.getNf_fcpst_basecalculo();
	this.nf_fcpst_valor = nf.getNf_fcpst_valor();
	this.nf_fcpst_valorretido = nf.getNf_fcpst_valorretido();
	this.nf_infadicionaisfisco = nf.getNf_infadicionaisfisco();
	this.nf_documento = nf.getNf_documento();
	this.hora = nf.getHora();
	this.fil_cod = nf.getFil_cod();
	this.fil_nome = nf.getFil_nome();
	this.fil_bairro = nf.getFil_bairro();
	this.fil_cidade = nf.getFil_cidade();
	this.fil_compl = nf.getFil_compl();
	this.fil_endereço = nf.getFil_endereço();
	this.fil_estado = nf.getFil_estado();
	this.fil_fone = nf.getFil_fone();
	this.fil_nreduz = nf.getFil_nreduz();
	this.fil_numero = nf.getFil_numero();
	this.fil_cep = nf.getFil_cep();
	this.fil_cnpj = nf.getFil_cnpj();
	this.fil_insc = nf.getFil_insc();
	this.fil_cnae = nf.getFil_cnae();
	this.fil_insc_mun = nf.getFil_insc_mun();
	this.fil_obs_padrao = nf.getFil_obs_padrao();
	this.fil_crt = nf.getFil_crt();
	this.fil_crt_sn = nf.getFil_crt_sn();
	this.fil_forma_tributacao = nf.getFil_forma_tributacao();
	this.fil_pcredsn = nf.getFil_pcredsn();
	this.fil_tipo_atividade = nf.getFil_tipo_atividade();
	this.fil_emailcont = nf.getFil_emailcont();
	this.pes_bai = nf.getPes_bai();
	this.pes_cidade = nf.getPes_cidade();
	this.pes_compl = nf.getPes_compl();
	this.pes_end = nf.getPes_end();
	this.pes_fone1 = nf.getPes_fone1();
	this.pes_nome45 = nf.getPes_nome45();
	this.pes_nrend = nf.getPes_nrend();
	this.pes_uf = nf.getPes_uf();
	this.pes_cep = nf.getPes_cep();
	this.pes_cnpj = nf.getPes_cnpj();
	this.pes_cpf = nf.getPes_cpf();
	this.pes_ie = nf.getPes_ie();
	this.pes_fj = nf.getPes_fj();
	this.pes_email = nf.getPes_email();
	this.pes_indicador_consumidor = nf.getPes_indicador_consumidor();
	this.pes_indicador_ie = nf.getPes_indicador_ie();
	this.pes_aliq_subst = nf.getPes_aliq_subst();
	this.pes_suframa = nf.getPes_suframa();
	this.transp_fj = nf.getTransp_fj();
	this.transp_nome45 = nf.getTransp_nome45();
	this.transp_cnpj = nf.getTransp_cnpj();
	this.transp_ie = nf.getTransp_ie();
	this.transp_cpf = nf.getTransp_cpf();
	this.transp_end = nf.getTransp_end();
	this.transp_nrend = nf.getTransp_nrend();
	this.transp_bai = nf.getTransp_bai();
	this.transp_cidade = nf.getTransp_cidade();
	this.transp_uf = nf.getTransp_uf();
	this.transp_codigo_antt = nf.getTransp_codigo_antt();
	this.cfo_texto = nf.getCfo_texto();
	this.tes_texto = nf.getTes_texto();
	this.checkBox = checkBox;
    }

    public NotaFiscal(String nf_numero, String nf_tipomov, String nf_formulario, String nf_serie, String nf_nrnota, String nf_filial, String nf_clifor, String nf_uf, Timestamp nf_dtemissao, Timestamp nf_data, String nf_nome, double nf_base_n, double nf_picms_n, double nf_vicms_n, double nf_isento, double nf_outros, double nf_piss, double nf_viss, double nf_base_s, double nf_vicms_s, double nf_vipi, double nf_vproduto, double nf_totnota, String nf_obs, int nf_volume, double nf_pbruto, double nf_pliquido, String nf_serieselo, String nf_nrselo, String nf_tes, String nf_cfop, String nf_cancelada, double nf_frete, double nf_despesas, double nf_desconto, int nf_tipo, String nf_transportadora, int nf_tipofrete, String nf_flag, String filial, String nf_dup1_numero, Timestamp nf_dup1_vencimento, double nf_dup1_valor, String nf_dup2_numero, Timestamp nf_dup2_vencimento, double nf_dup2_valor, String nf_dup3_numero, Timestamp nf_dup3_vencimento, double nf_dup3_valor, String nf_dup4_numero, Timestamp nf_dup4_vencimento, double nf_dup4_valor, String nf_dup5_numero, Timestamp nf_dup5_vencimento, double nf_dup5_valor, String nf_dup6_numero, Timestamp nf_dup6_vencimento, double nf_dup6_valor, String nf_almoxarifado, String codlan, double nf_baseipi, String nf_modelo, double nf_seguro, String nf_msg_corpo, String nf_marca, String nf_cod_energia, String nf_cfop_nota, String nf_placa, String nf_especie, String nfe_numeracao_volume, String nfeIndicadorFormaPagamento, String nfeFormatoImpressaoDANFe, String nfeFormatoEmissao, boolean nfeEnviada, int nfeCodigoNota, String nfeChaveAcesso, String nfeChaveAdicional, long nfeRecibo, long nfeProtocolo, String nfeProtocoloDataHora, long nfeCanceladaProtocolo, String nfeCanceladaJustificativa, boolean nfeCancelada, String nfefinalidade, String nfeemail, String nfecontingenciadatahora, String nfecontingenciajustificativa, String nfe_crt, boolean marcado, boolean nf_tipo_importacao, double nf_imposto_importacao, double nf_valor_pis, double nf_valor_cofins, double nf_outros_impostos_importacao, double nf_despesas_adjuaneiras, double nf_subtotal_importacao, String nFeAmbiente, String nf_caixa, boolean nf_imprime_dtsaida, boolean nf_denegada, String nfeChaveNFeoriginal, double nf_icms_antecipado, boolean nf_altman, String nf_dmi_numero, Timestamp nf_dmi_data, String nf_dmi_cidade, String nf_dmi_uf, String nf_dmi_codigo_exportador, double nf_tributos_valor, double nf_tributos_aliquota, String nf_parcelado, int nf_qtd_parcelas, int nf_dias_prazo, long nf_protocolo, long seq_sincronizacao, double nf_icm_creditado, double nf_base_icm_creditado, String nf_efd_mes_ano, String nf_dmi_viatransporte, double nf_dmi_valor_afrmm, String nf_incentivo_prodepe, String nf_ecf_numero, String controle_alteracao, String nf_tipo_operacao, double nf_opi_vicms_destino, double nf_opi_vicms_origem, double nf_opi_vicms_fcp, double nf_vicms_interestadual, String nf_importacao_deferido, double nf_importacao_deferido_p, double nf_valor_importacao, String nf_modelo_importacao, String nfestatus, double nf_vfcp, double nf_vicmsufdest, double nf_vicmsufremet, double nf_fcpst_basecalculo, double nf_fcpst_valor, double nf_fcpst_valorretido, String nf_infadicionaisfisco, String nf_documento, Timestamp hora, String fil_cod, String fil_nome, String fil_bairro, String fil_cidade, String fil_compl, String fil_endereço, String fil_estado, String fil_fone, String fil_nreduz, String fil_numero, String fil_cep, String fil_cnpj, String fil_insc, String fil_cnae, String fil_insc_mun, String fil_obs_padrao, String fil_crt, String fil_crt_sn, String fil_forma_tributacao, double fil_pcredsn, String fil_tipo_atividade, String fil_emailcont, String pes_bai, String pes_cidade, String pes_compl, String pes_end, String pes_fone1, String pes_nome45, String pes_nrend, String pes_uf, String pes_cep, String pes_cnpj, String pes_cpf, String pes_ie, String pes_indicador_consumidor, String pes_indicador_ie, int pes_fj, String pes_email, double pes_aliq_subst, String pes_suframa, int transp_fj, String transp_nome45, String transp_cnpj, String transp_ie, String transp_cpf, String transp_end, String transp_nrend, String transp_bai, String transp_cidade, String transp_uf, String transp_codigo_antt, String cfo_texto, String tes_texto, CheckBox checkBox) {
	this.nf_numero = nf_numero;
	this.nf_tipomov = nf_tipomov;
	this.nf_formulario = nf_formulario;
	this.nf_serie = nf_serie;
	this.nf_nrnota = nf_nrnota;
	this.nf_filial = nf_filial;
	this.nf_clifor = nf_clifor;
	this.nf_uf = nf_uf;
	this.nf_dtemissao = nf_dtemissao;
	this.nf_data = nf_data;
	this.nf_nome = nf_nome;
	this.nf_base_n = nf_base_n;
	this.nf_picms_n = nf_picms_n;
	this.nf_vicms_n = nf_vicms_n;
	this.nf_isento = nf_isento;
	this.nf_outros = nf_outros;
	this.nf_piss = nf_piss;
	this.nf_viss = nf_viss;
	this.nf_base_s = nf_base_s;
	this.nf_vicms_s = nf_vicms_s;
	this.nf_vipi = nf_vipi;
	this.nf_vproduto = nf_vproduto;
	this.nf_totnota = nf_totnota;
	this.nf_obs = nf_obs;
	this.nf_volume = nf_volume;
	this.nf_pbruto = nf_pbruto;
	this.nf_pliquido = nf_pliquido;
	this.nf_serieselo = nf_serieselo;
	this.nf_nrselo = nf_nrselo;
	this.nf_tes = nf_tes;
	this.nf_cfop = nf_cfop;
	this.nf_cancelada = nf_cancelada;
	this.nf_frete = nf_frete;
	this.nf_despesas = nf_despesas;
	this.nf_desconto = nf_desconto;
	this.nf_tipo = nf_tipo;
	this.nf_transportadora = nf_transportadora;
	this.nf_tipofrete = nf_tipofrete;
	this.nf_flag = nf_flag;
	this.filial = filial;
	this.nf_dup1_numero = nf_dup1_numero;
	this.nf_dup1_vencimento = nf_dup1_vencimento;
	this.nf_dup1_valor = nf_dup1_valor;
	this.nf_dup2_numero = nf_dup2_numero;
	this.nf_dup2_vencimento = nf_dup2_vencimento;
	this.nf_dup2_valor = nf_dup2_valor;
	this.nf_dup3_numero = nf_dup3_numero;
	this.nf_dup3_vencimento = nf_dup3_vencimento;
	this.nf_dup3_valor = nf_dup3_valor;
	this.nf_dup4_numero = nf_dup4_numero;
	this.nf_dup4_vencimento = nf_dup4_vencimento;
	this.nf_dup4_valor = nf_dup4_valor;
	this.nf_dup5_numero = nf_dup5_numero;
	this.nf_dup5_vencimento = nf_dup5_vencimento;
	this.nf_dup5_valor = nf_dup5_valor;
	this.nf_dup6_numero = nf_dup6_numero;
	this.nf_dup6_vencimento = nf_dup6_vencimento;
	this.nf_dup6_valor = nf_dup6_valor;
	this.nf_almoxarifado = nf_almoxarifado;
	this.codlan = codlan;
	this.nf_baseipi = nf_baseipi;
	this.nf_modelo = nf_modelo;
	this.nf_seguro = nf_seguro;
	this.nf_msg_corpo = nf_msg_corpo;
	this.nf_marca = nf_marca;
	this.nf_cod_energia = nf_cod_energia;
	this.nf_cfop_nota = nf_cfop_nota;
	this.nf_placa = nf_placa;
	this.nf_especie = nf_especie;
	this.nfe_numeracao_volume = nfe_numeracao_volume;
	this.nfeIndicadorFormaPagamento = nfeIndicadorFormaPagamento;
	this.nfeFormatoImpressaoDANFe = nfeFormatoImpressaoDANFe;
	this.nfeFormatoEmissao = nfeFormatoEmissao;
	this.nfeEnviada = nfeEnviada;
	this.nfeCodigoNota = nfeCodigoNota;
	this.nfeChaveAcesso = nfeChaveAcesso;
	this.nfeChaveAdicional = nfeChaveAdicional;
	this.nfeRecibo = nfeRecibo;
	this.nfeProtocolo = nfeProtocolo;
	this.nfeProtocoloDataHora = nfeProtocoloDataHora;
	this.nfeCanceladaProtocolo = nfeCanceladaProtocolo;
	this.nfeCanceladaJustificativa = nfeCanceladaJustificativa;
	this.nfeCancelada = nfeCancelada;
	this.nfefinalidade = nfefinalidade;
	this.nfeemail = nfeemail;
	this.nfecontingenciadatahora = nfecontingenciadatahora;
	this.nfecontingenciajustificativa = nfecontingenciajustificativa;
	this.nfe_crt = nfe_crt;
	this.marcado = marcado;
	this.nf_tipo_importacao = nf_tipo_importacao;
	this.nf_imposto_importacao = nf_imposto_importacao;
	this.nf_valor_pis = nf_valor_pis;
	this.nf_valor_cofins = nf_valor_cofins;
	this.nf_outros_impostos_importacao = nf_outros_impostos_importacao;
	this.nf_despesas_adjuaneiras = nf_despesas_adjuaneiras;
	this.nf_subtotal_importacao = nf_subtotal_importacao;
	this.nFeAmbiente = nFeAmbiente;
	this.nf_caixa = nf_caixa;
	this.nf_imprime_dtsaida = nf_imprime_dtsaida;
	this.nf_denegada = nf_denegada;
	this.nfeChaveNFeoriginal = nfeChaveNFeoriginal;
	this.nf_icms_antecipado = nf_icms_antecipado;
	this.nf_altman = nf_altman;
	this.nf_dmi_numero = nf_dmi_numero;
	this.nf_dmi_data = nf_dmi_data;
	this.nf_dmi_cidade = nf_dmi_cidade;
	this.nf_dmi_uf = nf_dmi_uf;
	this.nf_dmi_codigo_exportador = nf_dmi_codigo_exportador;
	this.nf_tributos_valor = nf_tributos_valor;
	this.nf_tributos_aliquota = nf_tributos_aliquota;
	this.nf_parcelado = nf_parcelado;
	this.nf_qtd_parcelas = nf_qtd_parcelas;
	this.nf_dias_prazo = nf_dias_prazo;
	this.nf_protocolo = nf_protocolo;
	this.seq_sincronizacao = seq_sincronizacao;
	this.nf_icm_creditado = nf_icm_creditado;
	this.nf_base_icm_creditado = nf_base_icm_creditado;
	this.nf_efd_mes_ano = nf_efd_mes_ano;
	this.nf_dmi_viatransporte = nf_dmi_viatransporte;
	this.nf_dmi_valor_afrmm = nf_dmi_valor_afrmm;
	this.nf_incentivo_prodepe = nf_incentivo_prodepe;
	this.nf_ecf_numero = nf_ecf_numero;
	this.controle_alteracao = controle_alteracao;
	this.nf_tipo_operacao = nf_tipo_operacao;
	this.nf_opi_vicms_destino = nf_opi_vicms_destino;
	this.nf_opi_vicms_origem = nf_opi_vicms_origem;
	this.nf_opi_vicms_fcp = nf_opi_vicms_fcp;
	this.nf_vicms_interestadual = nf_vicms_interestadual;
	this.nf_importacao_deferido = nf_importacao_deferido;
	this.nf_importacao_deferido_p = nf_importacao_deferido_p;
	this.nf_valor_importacao = nf_valor_importacao;
	this.nf_modelo_importacao = nf_modelo_importacao;
	this.nfestatus = nfestatus;
	this.nf_vfcp = nf_vfcp;
	this.nf_vicmsufdest = nf_vicmsufdest;
	this.nf_vicmsufremet = nf_vicmsufremet;
	this.nf_fcpst_basecalculo = nf_fcpst_basecalculo;
	this.nf_fcpst_valor = nf_fcpst_valor;
	this.nf_fcpst_valorretido = nf_fcpst_valorretido;
	this.nf_infadicionaisfisco = nf_infadicionaisfisco;
	this.nf_documento = nf_documento;
	this.hora = hora;
	this.fil_cod = fil_cod;
	this.fil_nome = fil_nome;
	this.fil_bairro = fil_bairro;
	this.fil_cidade = fil_cidade;
	this.fil_compl = fil_compl;
	this.fil_endereço = fil_endereço;
	this.fil_estado = fil_estado;
	this.fil_fone = fil_fone;
	this.fil_nreduz = fil_nreduz;
	this.fil_numero = fil_numero;
	this.fil_cep = fil_cep;
	this.fil_cnpj = fil_cnpj;
	this.fil_insc = fil_insc;
	this.fil_cnae = fil_cnae;
	this.fil_insc_mun = fil_insc_mun;
	this.fil_obs_padrao = fil_obs_padrao;
	this.fil_crt = fil_crt;
	this.fil_crt_sn = fil_crt_sn;
	this.fil_forma_tributacao = fil_forma_tributacao;
	this.fil_pcredsn = fil_pcredsn;
	this.fil_tipo_atividade = fil_tipo_atividade;
	this.fil_emailcont = fil_emailcont;
	this.pes_bai = pes_bai;
	this.pes_cidade = pes_cidade;
	this.pes_compl = pes_compl;
	this.pes_end = pes_end;
	this.pes_fone1 = pes_fone1;
	this.pes_nome45 = pes_nome45;
	this.pes_nrend = pes_nrend;
	this.pes_uf = pes_uf;
	this.pes_cep = pes_cep;
	this.pes_cnpj = pes_cnpj;
	this.pes_cpf = pes_cpf;
	this.pes_ie = pes_ie;
	this.pes_indicador_consumidor = pes_indicador_consumidor;
	this.pes_indicador_ie = pes_indicador_ie;
	this.pes_fj = pes_fj;
	this.pes_email = pes_email;
	this.pes_aliq_subst = pes_aliq_subst;
	this.pes_suframa = pes_suframa;
	this.transp_fj = transp_fj;
	this.transp_nome45 = transp_nome45;
	this.transp_cnpj = transp_cnpj;
	this.transp_ie = transp_ie;
	this.transp_cpf = transp_cpf;
	this.transp_end = transp_end;
	this.transp_nrend = transp_nrend;
	this.transp_bai = transp_bai;
	this.transp_cidade = transp_cidade;
	this.transp_uf = transp_uf;
	this.transp_codigo_antt = transp_codigo_antt;
	this.cfo_texto = cfo_texto;
	this.tes_texto = tes_texto;
	this.checkBox = checkBox;
    }

    public String getNf_numero() {
	return nf_numero;
    }

    public void setNf_numero(String nf_numero) {
	this.nf_numero = nf_numero;
    }

    public String getNf_tipomov() {
	return nf_tipomov;
    }

    public void setNf_tipomov(String nf_tipomov) {
	this.nf_tipomov = nf_tipomov;
    }

    public String getNf_formulario() {
	return nf_formulario;
    }

    public void setNf_formulario(String nf_formulario) {
	this.nf_formulario = nf_formulario;
    }

    public String getNf_serie() {
	return nf_serie;
    }

    public void setNf_serie(String nf_serie) {
	this.nf_serie = nf_serie;
    }

    public String getNf_nrnota() {
	return nf_nrnota;
    }

    public void setNf_nrnota(String nf_nrnota) {
	this.nf_nrnota = nf_nrnota;
    }

    public String getNf_filial() {
	return nf_filial;
    }

    public void setNf_filial(String nf_filial) {
	this.nf_filial = nf_filial;
    }

    public String getNf_clifor() {
	return nf_clifor;
    }

    public void setNf_clifor(String nf_clifor) {
	this.nf_clifor = nf_clifor;
    }

    public String getNf_uf() {
	return nf_uf;
    }

    public void setNf_uf(String nf_uf) {
	this.nf_uf = nf_uf;
    }

    public Timestamp getNf_dtemissao() {
	return nf_dtemissao;
    }

    public void setNf_dtemissao(Timestamp nf_dtemissao) {
	this.nf_dtemissao = nf_dtemissao;
    }

    public Timestamp getNf_data() {
	return nf_data;
    }

    public void setNf_data(Timestamp nf_data) {
	this.nf_data = nf_data;
    }

    public String getNf_nome() {
	return nf_nome;
    }

    public void setNf_nome(String nf_nome) {
	this.nf_nome = nf_nome;
    }

    public double getNf_base_n() {
	return nf_base_n;
    }

    public void setNf_base_n(double nf_base_n) {
	this.nf_base_n = nf_base_n;
    }

    public double getNf_picms_n() {
	return nf_picms_n;
    }

    public void setNf_picms_n(double nf_picms_n) {
	this.nf_picms_n = nf_picms_n;
    }

    public double getNf_vicms_n() {
	return nf_vicms_n;
    }

    public void setNf_vicms_n(double nf_vicms_n) {
	this.nf_vicms_n = nf_vicms_n;
    }

    public double getNf_isento() {
	return nf_isento;
    }

    public void setNf_isento(double nf_isento) {
	this.nf_isento = nf_isento;
    }

    public double getNf_outros() {
	return nf_outros;
    }

    public void setNf_outros(double nf_outros) {
	this.nf_outros = nf_outros;
    }

    public double getNf_piss() {
	return nf_piss;
    }

    public void setNf_piss(double nf_piss) {
	this.nf_piss = nf_piss;
    }

    public double getNf_viss() {
	return nf_viss;
    }

    public void setNf_viss(double nf_viss) {
	this.nf_viss = nf_viss;
    }

    public double getNf_base_s() {
	return nf_base_s;
    }

    public void setNf_base_s(double nf_base_s) {
	this.nf_base_s = nf_base_s;
    }

    public double getNf_vicms_s() {
	return nf_vicms_s;
    }

    public void setNf_vicms_s(double nf_vicms_s) {
	this.nf_vicms_s = nf_vicms_s;
    }

    public double getNf_vipi() {
	return nf_vipi;
    }

    public void setNf_vipi(double nf_vipi) {
	this.nf_vipi = nf_vipi;
    }

    public double getNf_vproduto() {
	return nf_vproduto;
    }

    public void setNf_vproduto(double nf_vproduto) {
	this.nf_vproduto = nf_vproduto;
    }

    public double getNf_totnota() {
	return nf_totnota;
    }

    public void setNf_totnota(double nf_totnota) {
	this.nf_totnota = nf_totnota;
    }

    public String getNf_obs() {
	return nf_obs;
    }

    public void setNf_obs(String nf_obs) {
	this.nf_obs = nf_obs;
    }

    public int getNf_volume() {
	return nf_volume;
    }

    public void setNf_volume(int nf_volume) {
	this.nf_volume = nf_volume;
    }

    public double getNf_pbruto() {
	return nf_pbruto;
    }

    public void setNf_pbruto(double nf_pbruto) {
	this.nf_pbruto = nf_pbruto;
    }

    public double getNf_pliquido() {
	return nf_pliquido;
    }

    public void setNf_pliquido(double nf_pliquido) {
	this.nf_pliquido = nf_pliquido;
    }

    public String getNf_serieselo() {
	return nf_serieselo;
    }

    public void setNf_serieselo(String nf_serieselo) {
	this.nf_serieselo = nf_serieselo;
    }

    public String getNf_nrselo() {
	return nf_nrselo;
    }

    public void setNf_nrselo(String nf_nrselo) {
	this.nf_nrselo = nf_nrselo;
    }

    public String getNf_tes() {
	return nf_tes;
    }

    public void setNf_tes(String nf_tes) {
	this.nf_tes = nf_tes;
    }

    public String getNf_cfop() {
	return nf_cfop;
    }

    public void setNf_cfop(String nf_cfop) {
	this.nf_cfop = nf_cfop;
    }

    public String getNf_cancelada() {
	return nf_cancelada;
    }

    public void setNf_cancelada(String nf_cancelada) {
	this.nf_cancelada = nf_cancelada;
    }

    public double getNf_frete() {
	return nf_frete;
    }

    public void setNf_frete(double nf_frete) {
	this.nf_frete = nf_frete;
    }

    public double getNf_despesas() {
	return nf_despesas;
    }

    public void setNf_despesas(double nf_despesas) {
	this.nf_despesas = nf_despesas;
    }

    public double getNf_desconto() {
	return nf_desconto;
    }

    public void setNf_desconto(double nf_desconto) {
	this.nf_desconto = nf_desconto;
    }

    public int getNf_tipo() {
	return nf_tipo;
    }

    public void setNf_tipo(int nf_tipo) {
	this.nf_tipo = nf_tipo;
    }

    public String getNf_transportadora() {
	return nf_transportadora;
    }

    public void setNf_transportadora(String nf_transportadora) {
	this.nf_transportadora = nf_transportadora;
    }

    public int getNf_tipofrete() {
	return nf_tipofrete;
    }

    public void setNf_tipofrete(int nf_tipofrete) {
	this.nf_tipofrete = nf_tipofrete;
    }

    public String getNf_flag() {
	return nf_flag;
    }

    public void setNf_flag(String nf_flag) {
	this.nf_flag = nf_flag;
    }

    public String getFilial() {
	return filial;
    }

    public void setFilial(String filial) {
	this.filial = filial;
    }

    public String getNf_dup1_numero() {
	return nf_dup1_numero;
    }

    public void setNf_dup1_numero(String nf_dup1_numero) {
	this.nf_dup1_numero = nf_dup1_numero;
    }

    public Timestamp getNf_dup1_vencimento() {
	return nf_dup1_vencimento;
    }

    public void setNf_dup1_vencimento(Timestamp nf_dup1_vencimento) {
	this.nf_dup1_vencimento = nf_dup1_vencimento;
    }

    public double getNf_dup1_valor() {
	return nf_dup1_valor;
    }

    public void setNf_dup1_valor(double nf_dup1_valor) {
	this.nf_dup1_valor = nf_dup1_valor;
    }

    public String getNf_dup2_numero() {
	return nf_dup2_numero;
    }

    public void setNf_dup2_numero(String nf_dup2_numero) {
	this.nf_dup2_numero = nf_dup2_numero;
    }

    public Timestamp getNf_dup2_vencimento() {
	return nf_dup2_vencimento;
    }

    public void setNf_dup2_vencimento(Timestamp nf_dup2_vencimento) {
	this.nf_dup2_vencimento = nf_dup2_vencimento;
    }

    public double getNf_dup2_valor() {
	return nf_dup2_valor;
    }

    public void setNf_dup2_valor(double nf_dup2_valor) {
	this.nf_dup2_valor = nf_dup2_valor;
    }

    public String getNf_dup3_numero() {
	return nf_dup3_numero;
    }

    public void setNf_dup3_numero(String nf_dup3_numero) {
	this.nf_dup3_numero = nf_dup3_numero;
    }

    public Timestamp getNf_dup3_vencimento() {
	return nf_dup3_vencimento;
    }

    public void setNf_dup3_vencimento(Timestamp nf_dup3_vencimento) {
	this.nf_dup3_vencimento = nf_dup3_vencimento;
    }

    public double getNf_dup3_valor() {
	return nf_dup3_valor;
    }

    public void setNf_dup3_valor(double nf_dup3_valor) {
	this.nf_dup3_valor = nf_dup3_valor;
    }

    public String getNf_dup4_numero() {
	return nf_dup4_numero;
    }

    public void setNf_dup4_numero(String nf_dup4_numero) {
	this.nf_dup4_numero = nf_dup4_numero;
    }

    public Timestamp getNf_dup4_vencimento() {
	return nf_dup4_vencimento;
    }

    public void setNf_dup4_vencimento(Timestamp nf_dup4_vencimento) {
	this.nf_dup4_vencimento = nf_dup4_vencimento;
    }

    public double getNf_dup4_valor() {
	return nf_dup4_valor;
    }

    public void setNf_dup4_valor(double nf_dup4_valor) {
	this.nf_dup4_valor = nf_dup4_valor;
    }

    public String getNf_dup5_numero() {
	return nf_dup5_numero;
    }

    public void setNf_dup5_numero(String nf_dup5_numero) {
	this.nf_dup5_numero = nf_dup5_numero;
    }

    public Timestamp getNf_dup5_vencimento() {
	return nf_dup5_vencimento;
    }

    public void setNf_dup5_vencimento(Timestamp nf_dup5_vencimento) {
	this.nf_dup5_vencimento = nf_dup5_vencimento;
    }

    public double getNf_dup5_valor() {
	return nf_dup5_valor;
    }

    public void setNf_dup5_valor(double nf_dup5_valor) {
	this.nf_dup5_valor = nf_dup5_valor;
    }

    public String getNf_dup6_numero() {
	return nf_dup6_numero;
    }

    public void setNf_dup6_numero(String nf_dup6_numero) {
	this.nf_dup6_numero = nf_dup6_numero;
    }

    public Timestamp getNf_dup6_vencimento() {
	return nf_dup6_vencimento;
    }

    public void setNf_dup6_vencimento(Timestamp nf_dup6_vencimento) {
	this.nf_dup6_vencimento = nf_dup6_vencimento;
    }

    public double getNf_dup6_valor() {
	return nf_dup6_valor;
    }

    public void setNf_dup6_valor(double nf_dup6_valor) {
	this.nf_dup6_valor = nf_dup6_valor;
    }

    public String getNf_almoxarifado() {
	return nf_almoxarifado;
    }

    public void setNf_almoxarifado(String nf_almoxarifado) {
	this.nf_almoxarifado = nf_almoxarifado;
    }

    public String getCodlan() {
	return codlan;
    }

    public void setCodlan(String codlan) {
	this.codlan = codlan;
    }

    public double getNf_baseipi() {
	return nf_baseipi;
    }

    public void setNf_baseipi(double nf_baseipi) {
	this.nf_baseipi = nf_baseipi;
    }

    public String getNf_modelo() {
	return nf_modelo;
    }

    public void setNf_modelo(String nf_modelo) {
	this.nf_modelo = nf_modelo;
    }

    public double getNf_seguro() {
	return nf_seguro;
    }

    public void setNf_seguro(double nf_seguro) {
	this.nf_seguro = nf_seguro;
    }

    public String getNf_msg_corpo() {
	return nf_msg_corpo;
    }

    public void setNf_msg_corpo(String nf_msg_corpo) {
	this.nf_msg_corpo = nf_msg_corpo;
    }

    public String getNf_marca() {
	return nf_marca;
    }

    public void setNf_marca(String nf_marca) {
	this.nf_marca = nf_marca;
    }

    public String getNf_cod_energia() {
	return nf_cod_energia;
    }

    public void setNf_cod_energia(String nf_cod_energia) {
	this.nf_cod_energia = nf_cod_energia;
    }

    public String getNf_cfop_nota() {
	return nf_cfop_nota;
    }

    public void setNf_cfop_nota(String nf_cfop_nota) {
	this.nf_cfop_nota = nf_cfop_nota;
    }

    public String getNf_placa() {
	return nf_placa;
    }

    public void setNf_placa(String nf_placa) {
	this.nf_placa = nf_placa;
    }

    public String getNf_especie() {
	return nf_especie;
    }

    public void setNf_especie(String nf_especie) {
	this.nf_especie = nf_especie;
    }

    public String getNfe_numeracao_volume() {
	return nfe_numeracao_volume;
    }

    public void setNfe_numeracao_volume(String nfe_numeracao_volume) {
	this.nfe_numeracao_volume = nfe_numeracao_volume;
    }

    public String getNfeIndicadorFormaPagamento() {
	return nfeIndicadorFormaPagamento;
    }

    public void setNfeIndicadorFormaPagamento(String nfeIndicadorFormaPagamento) {
	this.nfeIndicadorFormaPagamento = nfeIndicadorFormaPagamento;
    }

    public String getNfeFormatoImpressaoDANFe() {
	return nfeFormatoImpressaoDANFe;
    }

    public void setNfeFormatoImpressaoDANFe(String nfeFormatoImpressaoDANFe) {
	this.nfeFormatoImpressaoDANFe = nfeFormatoImpressaoDANFe;
    }

    public String getNfeFormatoEmissao() {
	return nfeFormatoEmissao;
    }

    public void setNfeFormatoEmissao(String nfeFormatoEmissao) {
	this.nfeFormatoEmissao = nfeFormatoEmissao;
    }

    public boolean isNfeEnviada() {
	return nfeEnviada;
    }

    public void setNfeEnviada(boolean nfeEnviada) {
	this.nfeEnviada = nfeEnviada;
    }

    public int getNfeCodigoNota() {
	return nfeCodigoNota;
    }

    public void setNfeCodigoNota(int nfeCodigoNota) {
	this.nfeCodigoNota = nfeCodigoNota;
    }

    public String getNfeChaveAcesso() {
	return nfeChaveAcesso;
    }

    public void setNfeChaveAcesso(String nfeChaveAcesso) {
	this.nfeChaveAcesso = nfeChaveAcesso;
    }

    public String getNfeChaveAdicional() {
	return nfeChaveAdicional;
    }

    public void setNfeChaveAdicional(String nfeChaveAdicional) {
	this.nfeChaveAdicional = nfeChaveAdicional;
    }

    public long getNfeRecibo() {
	return nfeRecibo;
    }

    public void setNfeRecibo(long nfeRecibo) {
	this.nfeRecibo = nfeRecibo;
    }

    public long getNfeProtocolo() {
	return nfeProtocolo;
    }

    public void setNfeProtocolo(long nfeProtocolo) {
	this.nfeProtocolo = nfeProtocolo;
    }

    public String getNfeProtocoloDataHora() {
	return nfeProtocoloDataHora;
    }

    public void setNfeProtocoloDataHora(String nfeProtocoloDataHora) {
	this.nfeProtocoloDataHora = nfeProtocoloDataHora;
    }

    public long getNfeCanceladaProtocolo() {
	return nfeCanceladaProtocolo;
    }

    public void setNfeCanceladaProtocolo(long nfeCanceladaProtocolo) {
	this.nfeCanceladaProtocolo = nfeCanceladaProtocolo;
    }

    public String getNfeCanceladaJustificativa() {
	return nfeCanceladaJustificativa;
    }

    public void setNfeCanceladaJustificativa(String nfeCanceladaJustificativa) {
	this.nfeCanceladaJustificativa = nfeCanceladaJustificativa;
    }

    public boolean isNfeCancelada() {
	return nfeCancelada;
    }

    public void setNfeCancelada(boolean nfeCancelada) {
	this.nfeCancelada = nfeCancelada;
    }

    public String getNfefinalidade() {
	return nfefinalidade;
    }

    public void setNfefinalidade(String nfefinalidade) {
	this.nfefinalidade = nfefinalidade;
    }

    public String getNfeemail() {
	return nfeemail;
    }

    public void setNfeemail(String nfeemail) {
	this.nfeemail = nfeemail;
    }

    public String getNfecontingenciadatahora() {
	return nfecontingenciadatahora;
    }

    public void setNfecontingenciadatahora(String nfecontingenciadatahora) {
	this.nfecontingenciadatahora = nfecontingenciadatahora;
    }

    public String getNfecontingenciajustificativa() {
	return nfecontingenciajustificativa;
    }

    public void setNfecontingenciajustificativa(String nfecontingenciajustificativa) {
	this.nfecontingenciajustificativa = nfecontingenciajustificativa;
    }

    public String getNfe_crt() {
	return nfe_crt;
    }

    public void setNfe_crt(String nfe_crt) {
	this.nfe_crt = nfe_crt;
    }

    public boolean isMarcado() {
	return marcado;
    }

    public void setMarcado(boolean marcado) {
	this.marcado = marcado;
    }

    public boolean isNf_tipo_importacao() {
	return nf_tipo_importacao;
    }

    public void setNf_tipo_importacao(boolean nf_tipo_importacao) {
	this.nf_tipo_importacao = nf_tipo_importacao;
    }

    public double getNf_imposto_importacao() {
	return nf_imposto_importacao;
    }

    public void setNf_imposto_importacao(double nf_imposto_importacao) {
	this.nf_imposto_importacao = nf_imposto_importacao;
    }

    public double getNf_valor_pis() {
	return nf_valor_pis;
    }

    public void setNf_valor_pis(double nf_valor_pis) {
	this.nf_valor_pis = nf_valor_pis;
    }

    public double getNf_valor_cofins() {
	return nf_valor_cofins;
    }

    public void setNf_valor_cofins(double nf_valor_cofins) {
	this.nf_valor_cofins = nf_valor_cofins;
    }

    public double getNf_outros_impostos_importacao() {
	return nf_outros_impostos_importacao;
    }

    public void setNf_outros_impostos_importacao(double nf_outros_impostos_importacao) {
	this.nf_outros_impostos_importacao = nf_outros_impostos_importacao;
    }

    public double getNf_despesas_adjuaneiras() {
	return nf_despesas_adjuaneiras;
    }

    public void setNf_despesas_adjuaneiras(double nf_despesas_adjuaneiras) {
	this.nf_despesas_adjuaneiras = nf_despesas_adjuaneiras;
    }

    public double getNf_subtotal_importacao() {
	return nf_subtotal_importacao;
    }

    public void setNf_subtotal_importacao(double nf_subtotal_importacao) {
	this.nf_subtotal_importacao = nf_subtotal_importacao;
    }

    public String getnFeAmbiente() {
	return nFeAmbiente;
    }

    public void setnFeAmbiente(String nFeAmbiente) {
	this.nFeAmbiente = nFeAmbiente;
    }

    public String getNf_caixa() {
	return nf_caixa;
    }

    public void setNf_caixa(String nf_caixa) {
	this.nf_caixa = nf_caixa;
    }

    public boolean isNf_imprime_dtsaida() {
	return nf_imprime_dtsaida;
    }

    public void setNf_imprime_dtsaida(boolean nf_imprime_dtsaida) {
	this.nf_imprime_dtsaida = nf_imprime_dtsaida;
    }

    public boolean isNf_denegada() {
	return nf_denegada;
    }

    public void setNf_denegada(boolean nf_denegada) {
	this.nf_denegada = nf_denegada;
    }

    public String getNfeChaveNFeoriginal() {
	return nfeChaveNFeoriginal;
    }

    public void setNfeChaveNFeoriginal(String nfeChaveNFeoriginal) {
	this.nfeChaveNFeoriginal = nfeChaveNFeoriginal;
    }

    public double getNf_icms_antecipado() {
	return nf_icms_antecipado;
    }

    public void setNf_icms_antecipado(double nf_icms_antecipado) {
	this.nf_icms_antecipado = nf_icms_antecipado;
    }

    public boolean isNf_altman() {
	return nf_altman;
    }

    public void setNf_altman(boolean nf_altman) {
	this.nf_altman = nf_altman;
    }

    public String getNf_dmi_numero() {
	return nf_dmi_numero;
    }

    public void setNf_dmi_numero(String nf_dmi_numero) {
	this.nf_dmi_numero = nf_dmi_numero;
    }

    public Timestamp getNf_dmi_data() {
	return nf_dmi_data;
    }

    public void setNf_dmi_data(Timestamp nf_dmi_data) {
	this.nf_dmi_data = nf_dmi_data;
    }

    public String getNf_dmi_cidade() {
	return nf_dmi_cidade;
    }

    public void setNf_dmi_cidade(String nf_dmi_cidade) {
	this.nf_dmi_cidade = nf_dmi_cidade;
    }

    public String getNf_dmi_uf() {
	return nf_dmi_uf;
    }

    public void setNf_dmi_uf(String nf_dmi_uf) {
	this.nf_dmi_uf = nf_dmi_uf;
    }

    public String getNf_dmi_codigo_exportador() {
	return nf_dmi_codigo_exportador;
    }

    public void setNf_dmi_codigo_exportador(String nf_dmi_codigo_exportador) {
	this.nf_dmi_codigo_exportador = nf_dmi_codigo_exportador;
    }

    public double getNf_tributos_valor() {
	return nf_tributos_valor;
    }

    public void setNf_tributos_valor(double nf_tributos_valor) {
	this.nf_tributos_valor = nf_tributos_valor;
    }

    public double getNf_tributos_aliquota() {
	return nf_tributos_aliquota;
    }

    public void setNf_tributos_aliquota(double nf_tributos_aliquota) {
	this.nf_tributos_aliquota = nf_tributos_aliquota;
    }

    public String getNf_parcelado() {
	return nf_parcelado;
    }

    public void setNf_parcelado(String nf_parcelado) {
	this.nf_parcelado = nf_parcelado;
    }

    public int getNf_qtd_parcelas() {
	return nf_qtd_parcelas;
    }

    public void setNf_qtd_parcelas(int nf_qtd_parcelas) {
	this.nf_qtd_parcelas = nf_qtd_parcelas;
    }

    public int getNf_dias_prazo() {
	return nf_dias_prazo;
    }

    public void setNf_dias_prazo(int nf_dias_prazo) {
	this.nf_dias_prazo = nf_dias_prazo;
    }

    public long getNf_protocolo() {
	return nf_protocolo;
    }

    public void setNf_protocolo(long nf_protocolo) {
	this.nf_protocolo = nf_protocolo;
    }

    public long getSeq_sincronizacao() {
	return seq_sincronizacao;
    }

    public void setSeq_sincronizacao(long seq_sincronizacao) {
	this.seq_sincronizacao = seq_sincronizacao;
    }

    public double getNf_icm_creditado() {
	return nf_icm_creditado;
    }

    public void setNf_icm_creditado(double nf_icm_creditado) {
	this.nf_icm_creditado = nf_icm_creditado;
    }

    public double getNf_base_icm_creditado() {
	return nf_base_icm_creditado;
    }

    public void setNf_base_icm_creditado(double nf_base_icm_creditado) {
	this.nf_base_icm_creditado = nf_base_icm_creditado;
    }

    public String getNf_efd_mes_ano() {
	return nf_efd_mes_ano;
    }

    public void setNf_efd_mes_ano(String nf_efd_mes_ano) {
	this.nf_efd_mes_ano = nf_efd_mes_ano;
    }

    public String getNf_dmi_viatransporte() {
	return nf_dmi_viatransporte;
    }

    public void setNf_dmi_viatransporte(String nf_dmi_viatransporte) {
	this.nf_dmi_viatransporte = nf_dmi_viatransporte;
    }

    public double getNf_dmi_valor_afrmm() {
	return nf_dmi_valor_afrmm;
    }

    public void setNf_dmi_valor_afrmm(double nf_dmi_valor_afrmm) {
	this.nf_dmi_valor_afrmm = nf_dmi_valor_afrmm;
    }

    public String getNf_incentivo_prodepe() {
	return nf_incentivo_prodepe;
    }

    public void setNf_incentivo_prodepe(String nf_incentivo_prodepe) {
	this.nf_incentivo_prodepe = nf_incentivo_prodepe;
    }

    public String getNf_ecf_numero() {
	return nf_ecf_numero;
    }

    public void setNf_ecf_numero(String nf_ecf_numero) {
	this.nf_ecf_numero = nf_ecf_numero;
    }

    public String getControle_alteracao() {
	return controle_alteracao;
    }

    public void setControle_alteracao(String controle_alteracao) {
	this.controle_alteracao = controle_alteracao;
    }

    public String getNf_tipo_operacao() {
	return nf_tipo_operacao;
    }

    public void setNf_tipo_operacao(String nf_tipo_operacao) {
	this.nf_tipo_operacao = nf_tipo_operacao;
    }

    public double getNf_opi_vicms_destino() {
	return nf_opi_vicms_destino;
    }

    public void setNf_opi_vicms_destino(double nf_opi_vicms_destino) {
	this.nf_opi_vicms_destino = nf_opi_vicms_destino;
    }

    public double getNf_opi_vicms_origem() {
	return nf_opi_vicms_origem;
    }

    public void setNf_opi_vicms_origem(double nf_opi_vicms_origem) {
	this.nf_opi_vicms_origem = nf_opi_vicms_origem;
    }

    public double getNf_opi_vicms_fcp() {
	return nf_opi_vicms_fcp;
    }

    public void setNf_opi_vicms_fcp(double nf_opi_vicms_fcp) {
	this.nf_opi_vicms_fcp = nf_opi_vicms_fcp;
    }

    public double getNf_vicms_interestadual() {
	return nf_vicms_interestadual;
    }

    public void setNf_vicms_interestadual(double nf_vicms_interestadual) {
	this.nf_vicms_interestadual = nf_vicms_interestadual;
    }

    public String getNf_importacao_deferido() {
	return nf_importacao_deferido;
    }

    public void setNf_importacao_deferido(String nf_importacao_deferido) {
	this.nf_importacao_deferido = nf_importacao_deferido;
    }

    public double getNf_importacao_deferido_p() {
	return nf_importacao_deferido_p;
    }

    public void setNf_importacao_deferido_p(double nf_importacao_deferido_p) {
	this.nf_importacao_deferido_p = nf_importacao_deferido_p;
    }

    public double getNf_valor_importacao() {
	return nf_valor_importacao;
    }

    public void setNf_valor_importacao(double nf_valor_importacao) {
	this.nf_valor_importacao = nf_valor_importacao;
    }

    public String getNf_modelo_importacao() {
	return nf_modelo_importacao;
    }

    public void setNf_modelo_importacao(String nf_modelo_importacao) {
	this.nf_modelo_importacao = nf_modelo_importacao;
    }

    public String getNfestatus() {
	return nfestatus;
    }

    public void setNfestatus(String nfestatus) {
	this.nfestatus = nfestatus;
    }

    public double getNf_vfcp() {
	return nf_vfcp;
    }

    public void setNf_vfcp(double nf_vfcp) {
	this.nf_vfcp = nf_vfcp;
    }

    public double getNf_vicmsufdest() {
	return nf_vicmsufdest;
    }

    public void setNf_vicmsufdest(double nf_vicmsufdest) {
	this.nf_vicmsufdest = nf_vicmsufdest;
    }

    public double getNf_vicmsufremet() {
	return nf_vicmsufremet;
    }

    public void setNf_vicmsufremet(double nf_vicmsufremet) {
	this.nf_vicmsufremet = nf_vicmsufremet;
    }

    public double getNf_fcpst_basecalculo() {
	return nf_fcpst_basecalculo;
    }

    public void setNf_fcpst_basecalculo(double nf_fcpst_basecalculo) {
	this.nf_fcpst_basecalculo = nf_fcpst_basecalculo;
    }

    public double getNf_fcpst_valor() {
	return nf_fcpst_valor;
    }

    public void setNf_fcpst_valor(double nf_fcpst_valor) {
	this.nf_fcpst_valor = nf_fcpst_valor;
    }

    public double getNf_fcpst_valorretido() {
	return nf_fcpst_valorretido;
    }

    public void setNf_fcpst_valorretido(double nf_fcpst_valorretido) {
	this.nf_fcpst_valorretido = nf_fcpst_valorretido;
    }

    public String getNf_infadicionaisfisco() {
	return nf_infadicionaisfisco;
    }

    public void setNf_infadicionaisfisco(String nf_infadicionaisfisco) {
	this.nf_infadicionaisfisco = nf_infadicionaisfisco;
    }

    public String getNf_documento() {
	return nf_documento;
    }

    public void setNf_documento(String nf_documento) {
	this.nf_documento = nf_documento;
    }

    public Timestamp getHora() {
	return hora;
    }

    public void setHora(Timestamp hora) {
	this.hora = hora;
    }

    public String getFil_cod() {
	return fil_cod;
    }

    public void setFil_cod(String fil_cod) {
	this.fil_cod = fil_cod;
    }

    public String getFil_nome() {
	return fil_nome;
    }

    public void setFil_nome(String fil_nome) {
	this.fil_nome = fil_nome;
    }

    public String getFil_bairro() {
	return fil_bairro;
    }

    public void setFil_bairro(String fil_bairro) {
	this.fil_bairro = fil_bairro;
    }

    public String getFil_cidade() {
	return fil_cidade;
    }

    public void setFil_cidade(String fil_cidade) {
	this.fil_cidade = fil_cidade;
    }

    public String getFil_compl() {
	return fil_compl;
    }

    public void setFil_compl(String fil_compl) {
	this.fil_compl = fil_compl;
    }

    public String getFil_endereço() {
	return fil_endereço;
    }

    public void setFil_endereço(String fil_endereço) {
	this.fil_endereço = fil_endereço;
    }

    public String getFil_estado() {
	return fil_estado;
    }

    public void setFil_estado(String fil_estado) {
	this.fil_estado = fil_estado;
    }

    public String getFil_fone() {
	return fil_fone;
    }

    public void setFil_fone(String fil_fone) {
	this.fil_fone = fil_fone;
    }

    public String getFil_nreduz() {
	return fil_nreduz;
    }

    public void setFil_nreduz(String fil_nreduz) {
	this.fil_nreduz = fil_nreduz;
    }

    public String getFil_numero() {
	return fil_numero;
    }

    public void setFil_numero(String fil_numero) {
	this.fil_numero = fil_numero;
    }

    public String getFil_cep() {
	return fil_cep;
    }

    public void setFil_cep(String fil_cep) {
	this.fil_cep = fil_cep;
    }

    public String getFil_cnpj() {
	return fil_cnpj;
    }

    public void setFil_cnpj(String fil_cnpj) {
	this.fil_cnpj = fil_cnpj;
    }

    public String getFil_insc() {
	return fil_insc;
    }

    public void setFil_insc(String fil_insc) {
	this.fil_insc = fil_insc;
    }

    public String getFil_cnae() {
	return fil_cnae;
    }

    public void setFil_cnae(String fil_cnae) {
	this.fil_cnae = fil_cnae;
    }

    public String getFil_insc_mun() {
	return fil_insc_mun;
    }

    public void setFil_insc_mun(String fil_insc_mun) {
	this.fil_insc_mun = fil_insc_mun;
    }

    public String getFil_obs_padrao() {
	return fil_obs_padrao;
    }

    public void setFil_obs_padrao(String fil_obs_padrao) {
	this.fil_obs_padrao = fil_obs_padrao;
    }

    public String getFil_crt() {
	return fil_crt;
    }

    public void setFil_crt(String fil_crt) {
	this.fil_crt = fil_crt;
    }

    public String getFil_crt_sn() {
	return fil_crt_sn;
    }

    public void setFil_crt_sn(String fil_crt_sn) {
	this.fil_crt_sn = fil_crt_sn;
    }

    public String getFil_forma_tributacao() {
	return fil_forma_tributacao;
    }

    public void setFil_forma_tributacao(String fil_forma_tributacao) {
	this.fil_forma_tributacao = fil_forma_tributacao;
    }

    public double getFil_pcredsn() {
	return fil_pcredsn;
    }

    public void setFil_pcredsn(double fil_pcredsn) {
	this.fil_pcredsn = fil_pcredsn;
    }

    public String getFil_tipo_atividade() {
	return fil_tipo_atividade;
    }

    public void setFil_tipo_atividade(String fil_tipo_atividade) {
	this.fil_tipo_atividade = fil_tipo_atividade;
    }

    public String getFil_emailcont() {
	return fil_emailcont;
    }

    public void setFil_emailcont(String fil_emailcont) {
	this.fil_emailcont = fil_emailcont;
    }

    public String getPes_bai() {
	return pes_bai;
    }

    public void setPes_bai(String pes_bai) {
	this.pes_bai = pes_bai;
    }

    public String getPes_cidade() {
	return pes_cidade;
    }

    public void setPes_cidade(String pes_cidade) {
	this.pes_cidade = pes_cidade;
    }

    public String getPes_compl() {
	return pes_compl;
    }

    public void setPes_compl(String pes_compl) {
	this.pes_compl = pes_compl;
    }

    public String getPes_end() {
	return pes_end;
    }

    public void setPes_end(String pes_end) {
	this.pes_end = pes_end;
    }

    public String getPes_fone1() {
	return pes_fone1;
    }

    public void setPes_fone1(String pes_fone1) {
	this.pes_fone1 = pes_fone1;
    }

    public String getPes_nome45() {
	return pes_nome45;
    }

    public void setPes_nome45(String pes_nome45) {
	this.pes_nome45 = pes_nome45;
    }

    public String getPes_nrend() {
	return pes_nrend;
    }

    public void setPes_nrend(String pes_nrend) {
	this.pes_nrend = pes_nrend;
    }

    public String getPes_uf() {
	return pes_uf;
    }

    public void setPes_uf(String pes_uf) {
	this.pes_uf = pes_uf;
    }

    public String getPes_cep() {
	return pes_cep;
    }

    public void setPes_cep(String pes_cep) {
	this.pes_cep = pes_cep;
    }

    public String getPes_cnpj() {
	return pes_cnpj;
    }

    public void setPes_cnpj(String pes_cnpj) {
	this.pes_cnpj = pes_cnpj;
    }

    public String getPes_cpf() {
	return pes_cpf;
    }

    public void setPes_cpf(String pes_cpf) {
	this.pes_cpf = pes_cpf;
    }

    public String getPes_ie() {
	return pes_ie;
    }

    public void setPes_ie(String pes_ie) {
	this.pes_ie = pes_ie;
    }

    public String getPes_indicador_consumidor() {
	return pes_indicador_consumidor;
    }

    public void setPes_indicador_consumidor(String pes_indicador_consumidor) {
	this.pes_indicador_consumidor = pes_indicador_consumidor;
    }

    public String getPes_indicador_ie() {
	return pes_indicador_ie;
    }

    public void setPes_indicador_ie(String pes_indicador_ie) {
	this.pes_indicador_ie = pes_indicador_ie;
    }

    public int getPes_fj() {
	return pes_fj;
    }

    public void setPes_fj(int pes_fj) {
	this.pes_fj = pes_fj;
    }

    public String getPes_email() {
	return pes_email;
    }

    public void setPes_email(String pes_email) {
	this.pes_email = pes_email;
    }

    public double getPes_aliq_subst() {
	return pes_aliq_subst;
    }

    public void setPes_aliq_subst(double pes_aliq_subst) {
	this.pes_aliq_subst = pes_aliq_subst;
    }

    public String getPes_suframa() {
	return pes_suframa;
    }

    public void setPes_suframa(String pes_suframa) {
	this.pes_suframa = pes_suframa;
    }

    public int getTransp_fj() {
	return transp_fj;
    }

    public void setTransp_fj(int transp_fj) {
	this.transp_fj = transp_fj;
    }

    public String getTransp_nome45() {
	return transp_nome45;
    }

    public void setTransp_nome45(String transp_nome45) {
	this.transp_nome45 = transp_nome45;
    }

    public String getTransp_cnpj() {
	return transp_cnpj;
    }

    public void setTransp_cnpj(String transp_cnpj) {
	this.transp_cnpj = transp_cnpj;
    }

    public String getTransp_ie() {
	return transp_ie;
    }

    public void setTransp_ie(String transp_ie) {
	this.transp_ie = transp_ie;
    }

    public String getTransp_cpf() {
	return transp_cpf;
    }

    public void setTransp_cpf(String transp_cpf) {
	this.transp_cpf = transp_cpf;
    }

    public String getTransp_end() {
	return transp_end;
    }

    public void setTransp_end(String transp_end) {
	this.transp_end = transp_end;
    }

    public String getTransp_nrend() {
	return transp_nrend;
    }

    public void setTransp_nrend(String transp_nrend) {
	this.transp_nrend = transp_nrend;
    }

    public String getTransp_bai() {
	return transp_bai;
    }

    public void setTransp_bai(String transp_bai) {
	this.transp_bai = transp_bai;
    }

    public String getTransp_cidade() {
	return transp_cidade;
    }

    public void setTransp_cidade(String transp_cidade) {
	this.transp_cidade = transp_cidade;
    }

    public String getTransp_uf() {
	return transp_uf;
    }

    public void setTransp_uf(String transp_uf) {
	this.transp_uf = transp_uf;
    }

    public String getTransp_codigo_antt() {
	return transp_codigo_antt;
    }

    public void setTransp_codigo_antt(String transp_codigo_antt) {
	this.transp_codigo_antt = transp_codigo_antt;
    }

    public String getCfo_texto() {
	return cfo_texto;
    }

    public void setCfo_texto(String cfo_texto) {
	this.cfo_texto = cfo_texto;
    }

    public String getTes_texto() {
	return tes_texto;
    }

    public void setTes_texto(String tes_texto) {
	this.tes_texto = tes_texto;
    }

    public CheckBox getCheckBox() {
	return checkBox;
    }

    public void setCheckBox(CheckBox checkBox) {
	this.checkBox = checkBox;
    }
}
