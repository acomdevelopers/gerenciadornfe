package acom.gerenciadornfe.modelo.entidades;

/**
 * @author allison
 */
public class NotaFiscalCancelamento {

    private int nfec_id;
    private String nfec_chaveacesso;
    private int nfec_seqevento;
    private long nfec_protocolo_nfe;
    private String nfec_justificativa;
    private long nfec_lote;
    private int nfec_cstat;
    private String nfec_motivo_resposta;
    private String nfec_descricao_resposta;
    private String nfec_data_hora;
    private long nfec_protocolo_evento;
    private String nfec_cnpj;

    public NotaFiscalCancelamento() {
    }

    public NotaFiscalCancelamento(int nfec_id, String nfec_chaveacesso, int nfec_seqevento, long nfec_protocolo_nfe, String nfec_justificativa, long nfec_lote, int nfec_cstat, String nfec_motivo_resposta, String nfec_descricao_resposta, String nfec_data_hora, long nfec_protocolo_evento, String nfec_cnpj) {
        this.nfec_id = nfec_id;
        this.nfec_chaveacesso = nfec_chaveacesso;
        this.nfec_seqevento = nfec_seqevento;
        this.nfec_protocolo_nfe = nfec_protocolo_nfe;
        this.nfec_justificativa = nfec_justificativa;
        this.nfec_lote = nfec_lote;
        this.nfec_cstat = nfec_cstat;
        this.nfec_motivo_resposta = nfec_motivo_resposta;
        this.nfec_descricao_resposta = nfec_descricao_resposta;
        this.nfec_data_hora = nfec_data_hora;
        this.nfec_protocolo_evento = nfec_protocolo_evento;
        this.nfec_cnpj = nfec_cnpj;
    }

    public int getNfec_id() {
        return nfec_id;
    }

    public void setNfec_id(int nfec_id) {
        this.nfec_id = nfec_id;
    }

    public String getNfec_chaveacesso() {
        return nfec_chaveacesso;
    }

    public void setNfec_chaveacesso(String nfec_chaveacesso) {
        this.nfec_chaveacesso = nfec_chaveacesso;
    }

    public int getNfec_seqevento() {
        return nfec_seqevento;
    }

    public void setNfec_seqevento(int nfec_seqevento) {
        this.nfec_seqevento = nfec_seqevento;
    }

    public long getNfec_protocolo_nfe() {
        return nfec_protocolo_nfe;
    }

    public void setNfec_protocolo_nfe(long nfec_protocolo_nfe) {
        this.nfec_protocolo_nfe = nfec_protocolo_nfe;
    }

    public String getNfec_justificativa() {
        return nfec_justificativa;
    }

    public void setNfec_justificativa(String nfec_justificativa) {
        this.nfec_justificativa = nfec_justificativa;
    }

    public long getNfec_lote() {
        return nfec_lote;
    }

    public void setNfec_lote(long nfec_lote) {
        this.nfec_lote = nfec_lote;
    }

    public int getNfec_cstat() {
        return nfec_cstat;
    }

    public void setNfec_cstat(int nfec_cstat) {
        this.nfec_cstat = nfec_cstat;
    }

    public String getNfec_motivo_resposta() {
        return nfec_motivo_resposta;
    }

    public void setNfec_motivo_resposta(String nfec_motivo_resposta) {
        this.nfec_motivo_resposta = nfec_motivo_resposta;
    }

    public String getNfec_descricao_resposta() {
        return nfec_descricao_resposta;
    }

    public void setNfec_descricao_resposta(String nfec_descricao_resposta) {
        this.nfec_descricao_resposta = nfec_descricao_resposta;
    }

    public String getNfec_data_hora() {
        return nfec_data_hora;
    }

    public void setNfec_data_hora(String nfec_data_hora) {
        this.nfec_data_hora = nfec_data_hora;
    }

    public long getNfec_protocolo_evento() {
        return nfec_protocolo_evento;
    }

    public void setNfec_protocolo_evento(long nfec_protocolo_evento) {
        this.nfec_protocolo_evento = nfec_protocolo_evento;
    }

    public String getNfec_cnpj() {
        return nfec_cnpj;
    }

    public void setNfec_cnpj(String nfec_cnpj) {
        this.nfec_cnpj = nfec_cnpj;
    }

}
