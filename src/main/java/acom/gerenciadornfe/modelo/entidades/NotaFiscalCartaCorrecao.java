package acom.gerenciadornfe.modelo.entidades;

import java.sql.Timestamp;

/**
 * @author allison
 */
public class NotaFiscalCartaCorrecao {

    private String nfcc_chavenfe;
    private String nfcc_item;
    private Timestamp nfcc_data;
    private Timestamp nfcc_hora;
    private String nfcc_correcao;
    private String nfcc_protocolo;
    private String nfcc_enviada;
    private long nfcc_lote;
    private String nfcc_cnpj;
    private String nfcc_orgao;
    private Timestamp nfcc_dataProtocolo;
    private String nf_nome;
    private String nfcc_retorno_sefaz;
    private String nfcc_modelo;
    private String nfcc_serie;
    private String nfcc_numero_nota;
    private String nfcc_numero_evento;
    private String filia;

    public NotaFiscalCartaCorrecao() {
    }

    public NotaFiscalCartaCorrecao(String nfcc_chavenfe, String nfcc_item, Timestamp nfcc_data, Timestamp nfcc_hora, String nfcc_correcao, String nfcc_protocolo, String nfcc_enviada, long nfcc_lote, String nfcc_cnpj, String nfcc_orgao, Timestamp nfcc_dataProtocolo, String nf_nome, String nfcc_retorno_sefaz, String nfcc_modelo, String nfcc_serie, String nfcc_numero_nota, String nfcc_numero_evento, String filia) {
	this.nfcc_chavenfe = nfcc_chavenfe;
	this.nfcc_item = nfcc_item;
	this.nfcc_data = nfcc_data;
	this.nfcc_hora = nfcc_hora;
	this.nfcc_correcao = nfcc_correcao;
	this.nfcc_protocolo = nfcc_protocolo;
	this.nfcc_enviada = nfcc_enviada;
	this.nfcc_lote = nfcc_lote;
	this.nfcc_cnpj = nfcc_cnpj;
	this.nfcc_orgao = nfcc_orgao;
	this.nfcc_dataProtocolo = nfcc_dataProtocolo;
	this.nf_nome = nf_nome;
	this.nfcc_retorno_sefaz = nfcc_retorno_sefaz;
	this.nfcc_modelo = nfcc_modelo;
	this.nfcc_serie = nfcc_serie;
	this.nfcc_numero_nota = nfcc_numero_nota;
	this.nfcc_numero_evento = nfcc_numero_evento;
	this.filia = filia;
    }

    public String getNfcc_chavenfe() {
	return nfcc_chavenfe;
    }

    public void setNfcc_chavenfe(String nfcc_chavenfe) {
	this.nfcc_chavenfe = nfcc_chavenfe;
    }

    public String getNfcc_item() {
	return nfcc_item;
    }

    public void setNfcc_item(String nfcc_item) {
	this.nfcc_item = nfcc_item;
    }

    public Timestamp getNfcc_data() {
	return nfcc_data;
    }

    public void setNfcc_data(Timestamp nfcc_data) {
	this.nfcc_data = nfcc_data;
    }

    public Timestamp getNfcc_hora() {
	return nfcc_hora;
    }

    public void setNfcc_hora(Timestamp nfcc_hora) {
	this.nfcc_hora = nfcc_hora;
    }

    public String getNfcc_correcao() {
	return nfcc_correcao;
    }

    public void setNfcc_correcao(String nfcc_correcao) {
	this.nfcc_correcao = nfcc_correcao;
    }

    public String getNfcc_protocolo() {
	return nfcc_protocolo;
    }

    public void setNfcc_protocolo(String nfcc_protocolo) {
	this.nfcc_protocolo = nfcc_protocolo;
    }

    public String getNfcc_enviada() {
	return nfcc_enviada;
    }

    public void setNfcc_enviada(String nfcc_enviada) {
	this.nfcc_enviada = nfcc_enviada;
    }

    public long getNfcc_lote() {
	return nfcc_lote;
    }

    public void setNfcc_lote(long nfcc_lote) {
	this.nfcc_lote = nfcc_lote;
    }

    public String getNfcc_cnpj() {
	return nfcc_cnpj;
    }

    public void setNfcc_cnpj(String nfcc_cnpj) {
	this.nfcc_cnpj = nfcc_cnpj;
    }

    public String getNfcc_orgao() {
	return nfcc_orgao;
    }

    public void setNfcc_orgao(String nfcc_orgao) {
	this.nfcc_orgao = nfcc_orgao;
    }

    public Timestamp getNfcc_dataProtocolo() {
	return nfcc_dataProtocolo;
    }

    public void setNfcc_dataProtocolo(Timestamp nfcc_dataProtocolo) {
	this.nfcc_dataProtocolo = nfcc_dataProtocolo;
    }

    public String getNf_nome() {
	return nf_nome;
    }

    public void setNf_nome(String nf_nome) {
	this.nf_nome = nf_nome;
    }

    public String getNfcc_retorno_sefaz() {
	return nfcc_retorno_sefaz;
    }

    public void setNfcc_retorno_sefaz(String nfcc_retorno_sefaz) {
	this.nfcc_retorno_sefaz = nfcc_retorno_sefaz;
    }

    public String getNfcc_modelo() {
	return nfcc_modelo;
    }

    public void setNfcc_modelo(String nfcc_modelo) {
	this.nfcc_modelo = nfcc_modelo;
    }

    public String getNfcc_serie() {
	return nfcc_serie;
    }

    public void setNfcc_serie(String nfcc_serie) {
	this.nfcc_serie = nfcc_serie;
    }

    public String getNfcc_numero_nota() {
	return nfcc_numero_nota;
    }

    public void setNfcc_numero_nota(String nfcc_numero_nota) {
	this.nfcc_numero_nota = nfcc_numero_nota;
    }

    public String getNfcc_numero_evento() {
	return nfcc_numero_evento;
    }

    public void setNfcc_numero_evento(String nfcc_numero_evento) {
	this.nfcc_numero_evento = nfcc_numero_evento;
    }

    public String getFilia() {
	return filia;
    }

    public void setFilia(String filia) {
	this.filia = filia;
    }
}
