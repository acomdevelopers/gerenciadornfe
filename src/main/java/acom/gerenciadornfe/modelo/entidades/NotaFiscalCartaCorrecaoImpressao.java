package acom.gerenciadornfe.modelo.entidades;

import java.sql.Timestamp;

public class NotaFiscalCartaCorrecaoImpressao {

    private String nfcc_chavenfe;
    private String nfcc_item;
    private Timestamp nfcc_data;
    private Timestamp nfcc_hora;
    private String nfcc_correcao;
    private String nfcc_protocolo;
    private String nfcc_enviada;
    private long nfcc_lote;
    private String nfcc_cnpj;
    private String nfcc_orgao;
    private Timestamp nfcc_dataProtocolo;
    private String nfcc_nome;
    private String nfcc_retorno_sefaz;
    private String nfcc_modelo;
    private String nfcc_serie;
    private String nfcc_numero_nota;
    private String nfcc_numero_evento;
    private String filia;
    private String fil_nome;
    private String fil_nreduz;
    private String fil_endereco;
    private String fil_numero;
    private String fil_bairro;
    private String fil_cep;
    private String fil_cidade;
    private String fil_estado;
    private String fil_cnpj;
    private String fil_insc;
    private String fil_fone;
    private String pes_nome45;
    private String pes_nome25;
    private String pes_fj;
    private String pes_cep;
    private String pes_end;
    private String pes_nrend;
    private String pes_bai;
    private String pes_cidade;
    private String pes_uf;
    private String pes_cnpj;
    private String pes_ie;
    private String pes_cpf;
    private String pes_fone1;

    public NotaFiscalCartaCorrecaoImpressao() {
    }

    public NotaFiscalCartaCorrecaoImpressao(String nfcc_chavenfe, String nfcc_item, Timestamp nfcc_data, Timestamp nfcc_hora, String nfcc_correcao, String nfcc_protocolo, String nfcc_enviada, long nfcc_lote, String nfcc_cnpj, String nfcc_orgao, Timestamp nfcc_dataProtocolo, String nfcc_nome, String nfcc_retorno_sefaz, String nfcc_modelo, String nfcc_serie, String nfcc_numero_nota, String nfcc_numero_evento, String filia, String fil_nome, String fil_nreduz, String fil_endereco, String fil_numero, String fil_bairro, String fil_cep, String fil_cidade, String fil_estado, String fil_cnpj, String fil_insc, String fil_fone, String pes_nome45, String pes_nome25, String pes_fj, String pes_cep, String pes_end, String pes_nrend, String pes_bai, String pes_cidade, String pes_uf, String pes_cnpj, String pes_ie, String pes_cpf, String pes_fone1) {
	this.nfcc_chavenfe = nfcc_chavenfe;
	this.nfcc_item = nfcc_item;
	this.nfcc_data = nfcc_data;
	this.nfcc_hora = nfcc_hora;
	this.nfcc_correcao = nfcc_correcao;
	this.nfcc_protocolo = nfcc_protocolo;
	this.nfcc_enviada = nfcc_enviada;
	this.nfcc_lote = nfcc_lote;
	this.nfcc_cnpj = nfcc_cnpj;
	this.nfcc_orgao = nfcc_orgao;
	this.nfcc_dataProtocolo = nfcc_dataProtocolo;
	this.nfcc_nome = nfcc_nome;
	this.nfcc_retorno_sefaz = nfcc_retorno_sefaz;
	this.nfcc_modelo = nfcc_modelo;
	this.nfcc_serie = nfcc_serie;
	this.nfcc_numero_nota = nfcc_numero_nota;
	this.nfcc_numero_evento = nfcc_numero_evento;
	this.filia = filia;
	this.fil_nome = fil_nome;
	this.fil_nreduz = fil_nreduz;
	this.fil_endereco = fil_endereco;
	this.fil_numero = fil_numero;
	this.fil_bairro = fil_bairro;
	this.fil_cep = fil_cep;
	this.fil_cidade = fil_cidade;
	this.fil_estado = fil_estado;
	this.fil_cnpj = fil_cnpj;
	this.fil_insc = fil_insc;
	this.fil_fone = fil_fone;
	this.pes_nome45 = pes_nome45;
	this.pes_nome25 = pes_nome25;
	this.pes_fj = pes_fj;
	this.pes_cep = pes_cep;
	this.pes_end = pes_end;
	this.pes_nrend = pes_nrend;
	this.pes_bai = pes_bai;
	this.pes_cidade = pes_cidade;
	this.pes_uf = pes_uf;
	this.pes_cnpj = pes_cnpj;
	this.pes_ie = pes_ie;
	this.pes_cpf = pes_cpf;
	this.pes_fone1 = pes_fone1;
    }

    public String getNfcc_chavenfe() {
	return nfcc_chavenfe;
    }

    public void setNfcc_chavenfe(String nfcc_chavenfe) {
	this.nfcc_chavenfe = nfcc_chavenfe;
    }

    public String getNfcc_item() {
	return nfcc_item;
    }

    public void setNfcc_item(String nfcc_item) {
	this.nfcc_item = nfcc_item;
    }

    public Timestamp getNfcc_data() {
	return nfcc_data;
    }

    public void setNfcc_data(Timestamp nfcc_data) {
	this.nfcc_data = nfcc_data;
    }

    public Timestamp getNfcc_hora() {
	return nfcc_hora;
    }

    public void setNfcc_hora(Timestamp nfcc_hora) {
	this.nfcc_hora = nfcc_hora;
    }

    public String getNfcc_correcao() {
	return nfcc_correcao;
    }

    public void setNfcc_correcao(String nfcc_correcao) {
	this.nfcc_correcao = nfcc_correcao;
    }

    public String getNfcc_protocolo() {
	return nfcc_protocolo;
    }

    public void setNfcc_protocolo(String nfcc_protocolo) {
	this.nfcc_protocolo = nfcc_protocolo;
    }

    public String getNfcc_enviada() {
	return nfcc_enviada;
    }

    public void setNfcc_enviada(String nfcc_enviada) {
	this.nfcc_enviada = nfcc_enviada;
    }

    public long getNfcc_lote() {
	return nfcc_lote;
    }

    public void setNfcc_lote(long nfcc_lote) {
	this.nfcc_lote = nfcc_lote;
    }

    public String getNfcc_cnpj() {
	return nfcc_cnpj;
    }

    public void setNfcc_cnpj(String nfcc_cnpj) {
	this.nfcc_cnpj = nfcc_cnpj;
    }

    public String getNfcc_orgao() {
	return nfcc_orgao;
    }

    public void setNfcc_orgao(String nfcc_orgao) {
	this.nfcc_orgao = nfcc_orgao;
    }

    public Timestamp getNfcc_dataProtocolo() {
	return nfcc_dataProtocolo;
    }

    public void setNfcc_dataProtocolo(Timestamp nfcc_dataProtocolo) {
	this.nfcc_dataProtocolo = nfcc_dataProtocolo;
    }

    public String getNfcc_nome() {
	return nfcc_nome;
    }

    public void setNfcc_nome(String nfcc_nome) {
	this.nfcc_nome = nfcc_nome;
    }

    public String getNfcc_retorno_sefaz() {
	return nfcc_retorno_sefaz;
    }

    public void setNfcc_retorno_sefaz(String nfcc_retorno_sefaz) {
	this.nfcc_retorno_sefaz = nfcc_retorno_sefaz;
    }

    public String getNfcc_modelo() {
	return nfcc_modelo;
    }

    public void setNfcc_modelo(String nfcc_modelo) {
	this.nfcc_modelo = nfcc_modelo;
    }

    public String getNfcc_serie() {
	return nfcc_serie;
    }

    public void setNfcc_serie(String nfcc_serie) {
	this.nfcc_serie = nfcc_serie;
    }

    public String getNfcc_numero_nota() {
	return nfcc_numero_nota;
    }

    public void setNfcc_numero_nota(String nfcc_numero_nota) {
	this.nfcc_numero_nota = nfcc_numero_nota;
    }

    public String getNfcc_numero_evento() {
	return nfcc_numero_evento;
    }

    public void setNfcc_numero_evento(String nfcc_numero_evento) {
	this.nfcc_numero_evento = nfcc_numero_evento;
    }

    public String getFilia() {
	return filia;
    }

    public void setFilia(String filia) {
	this.filia = filia;
    }

    public String getFil_nome() {
	return fil_nome;
    }

    public void setFil_nome(String fil_nome) {
	this.fil_nome = fil_nome;
    }

    public String getFil_nreduz() {
	return fil_nreduz;
    }

    public void setFil_nreduz(String fil_nreduz) {
	this.fil_nreduz = fil_nreduz;
    }

    public String getFil_endereco() {
	return fil_endereco;
    }

    public void setFil_endereco(String fil_endereco) {
	this.fil_endereco = fil_endereco;
    }

    public String getFil_numero() {
	return fil_numero;
    }

    public void setFil_numero(String fil_numero) {
	this.fil_numero = fil_numero;
    }

    public String getFil_bairro() {
	return fil_bairro;
    }

    public void setFil_bairro(String fil_bairro) {
	this.fil_bairro = fil_bairro;
    }

    public String getFil_cep() {
	return fil_cep;
    }

    public void setFil_cep(String fil_cep) {
	this.fil_cep = fil_cep;
    }

    public String getFil_cidade() {
	return fil_cidade;
    }

    public void setFil_cidade(String fil_cidade) {
	this.fil_cidade = fil_cidade;
    }

    public String getFil_estado() {
	return fil_estado;
    }

    public void setFil_estado(String fil_estado) {
	this.fil_estado = fil_estado;
    }

    public String getFil_cnpj() {
	return fil_cnpj;
    }

    public void setFil_cnpj(String fil_cnpj) {
	this.fil_cnpj = fil_cnpj;
    }

    public String getFil_insc() {
	return fil_insc;
    }

    public void setFil_insc(String fil_insc) {
	this.fil_insc = fil_insc;
    }

    public String getFil_fone() {
	return fil_fone;
    }

    public void setFil_fone(String fil_fone) {
	this.fil_fone = fil_fone;
    }

    public String getPes_nome45() {
	return pes_nome45;
    }

    public void setPes_nome45(String pes_nome45) {
	this.pes_nome45 = pes_nome45;
    }

    public String getPes_nome25() {
	return pes_nome25;
    }

    public void setPes_nome25(String pes_nome25) {
	this.pes_nome25 = pes_nome25;
    }

    public String getPes_fj() {
	return pes_fj;
    }

    public void setPes_fj(String pes_fj) {
	this.pes_fj = pes_fj;
    }

    public String getPes_cep() {
	return pes_cep;
    }

    public void setPes_cep(String pes_cep) {
	this.pes_cep = pes_cep;
    }

    public String getPes_end() {
	return pes_end;
    }

    public void setPes_end(String pes_end) {
	this.pes_end = pes_end;
    }

    public String getPes_nrend() {
	return pes_nrend;
    }

    public void setPes_nrend(String pes_nrend) {
	this.pes_nrend = pes_nrend;
    }

    public String getPes_bai() {
	return pes_bai;
    }

    public void setPes_bai(String pes_bai) {
	this.pes_bai = pes_bai;
    }

    public String getPes_cidade() {
	return pes_cidade;
    }

    public void setPes_cidade(String pes_cidade) {
	this.pes_cidade = pes_cidade;
    }

    public String getPes_uf() {
	return pes_uf;
    }

    public void setPes_uf(String pes_uf) {
	this.pes_uf = pes_uf;
    }

    public String getPes_cnpj() {
	return pes_cnpj;
    }

    public void setPes_cnpj(String pes_cnpj) {
	this.pes_cnpj = pes_cnpj;
    }

    public String getPes_ie() {
	return pes_ie;
    }

    public void setPes_ie(String pes_ie) {
	this.pes_ie = pes_ie;
    }

    public String getPes_cpf() {
	return pes_cpf;
    }

    public void setPes_cpf(String pes_cpf) {
	this.pes_cpf = pes_cpf;
    }

    public String getPes_fone1() {
	return pes_fone1;
    }

    public void setPes_fone1(String pes_fone1) {
	this.pes_fone1 = pes_fone1;
    }

}
