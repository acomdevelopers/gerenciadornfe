/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.entidades;

import java.sql.Timestamp;

/**
 * @author allison
 */
public class NotaFiscalDuplicatas {

    private String nfd_numero;
    private String nfd_parcela;
    private Timestamp nfd_vencimento;
    private String nfd_documento;
    private double nfd_valor;

    public NotaFiscalDuplicatas() {
    }

    public NotaFiscalDuplicatas(String nfd_numero, String nfd_parcela, Timestamp nfd_vencimento, String nfd_documento, double nfd_valor) {
        this.nfd_numero = nfd_numero;
        this.nfd_parcela = nfd_parcela;
        this.nfd_vencimento = nfd_vencimento;
        this.nfd_documento = nfd_documento;
        this.nfd_valor = nfd_valor;
    }

    public String getNfd_numero() {
        return nfd_numero;
    }

    public void setNfd_numero(String nfd_numero) {
        this.nfd_numero = nfd_numero;
    }

    public String getNfd_parcela() {
        return nfd_parcela;
    }

    public void setNfd_parcela(String nfd_parcela) {
        this.nfd_parcela = nfd_parcela;
    }

    public Timestamp getNfd_vencimento() {
        return nfd_vencimento;
    }

    public void setNfd_vencimento(Timestamp nfd_vencimento) {
        this.nfd_vencimento = nfd_vencimento;
    }

    public String getNfd_documento() {
        return nfd_documento;
    }

    public void setNfd_documento(String nfd_documento) {
        this.nfd_documento = nfd_documento;
    }

    public double getNfd_valor() {
        return nfd_valor;
    }

    public void setNfd_valor(double nfd_valor) {
        this.nfd_valor = nfd_valor;
    }

}
