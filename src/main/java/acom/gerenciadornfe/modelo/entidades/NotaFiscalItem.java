package acom.gerenciadornfe.modelo.entidades;

import java.sql.Timestamp;

/**
 * @author allison
 */
public class NotaFiscalItem {

    private String nfi_item;
    private String nfi_numero;
    private String nfi_clifor;
    private String nfi_serie;
    private String nfi_nrnota;
    private String nfi_codpro;
    private String nfi_nome;
    private double nfi_qtdfat;
    private double nfi_prunit;
    private double nfi_total;
    private double nfi_picms_n;
    private double nfi_vicms_n;
    private double nfi_vicms_s;
    private String nfi_tes;
    private double nfi_pipi;
    private double nfi_vipi;
    private String nfi_cfop;
    private String nfi_sittrib;
    private double nfi_piss;
    private double nfi_viss;
    private String nfi_destino;
    private Timestamp nfi_dtemissao;
    private Timestamp nfi_data;
    private String nfi_obs;
    private double nfi_pbruto;
    private double nfi_pliquido;
    private int nfi_tipo;
    private String nfi_codint;
    private String filial;
    private String incluido;
    private Timestamp horainc;
    private Timestamp dtinc;
    private String alterado;
    private Timestamp horaalt;
    private Timestamp dtalt;
    private String nfi_filial;
    private String nfi_pedido;
    private String nfi_cupom;
    private String nfi_documento;
    private String nfi_almoxarifado;
    private double nfi_base_calculo;
    private String codlan;
    private double nfi_pipifrete;
    private double nfi_baseipifrete;
    private double nfi_vipifrete;
    private double nfi_baseipi;
    private double nfi_desconto;
    private double nfi_valorfrete;
    private double nfi_valordespesa;
    private double nfi_isento;
    private double nfi_outros;
    private double nfi_valorseguro;
    private double nfi_base_s;
    private long nfi_codigodanfe;
    private double nfi_valor_pis;
    private double nfi_valor_cofins;
    private double nfi_icms_antecipado;
    private String nfi_cst_pis;
    private String nfi_cst_cofins;
    private double nfi_aliquota_pis;
    private double nfi_aliquota_cofins;
    private boolean nfi_atualizado_pis_cofins;
    private String nfi_natureza_receita;
    private double nfi_tributos_valor;
    private double nfi_tributos_aliquota;
    private long seq_sincronizacao;
    private String controle_alteracao;
    private String nfi_ecf_numero;
    private String nfi_ecf_coo;
    private String nfi_chaveacesso_ref;
    private double nfi_icm_creditado;
    private double nfi_base_icm_creditado;
    private double nfi_imposto_importacao;
    private double nfi_opi_vicms_destino;
    private double nfi_opi_vicms_origem;
    private double nfi_opi_picms_destino;
    private double nfi_opi_picms_fcp;
    private double nfi_opi_vicms_fcp;
    private double nfi_opi_vicms_interestadual;
    private String nfi_altman;
    private String nfi_um;
    private double nfi_fatorqtd;
    private double nfi_qtd_entsai;
    private double nfi_fcpst_basecalculo;
    private double nfi_fcpst_valor;
    private double nfi_fcpst_aliquota;
    private String nfi_cst_ipi;
    private String nfi_ipi_cenq;
    private String nfi_codean;
    private String nfi_codeantrib;
    private String pro_cst_ipi_ent;
    private String pro_um;
    private String pro_origem;
    private String pro_codean;
    private int pro_tipocod;
    private String pro_coddun;
    private String pro_ncm;
    private String pro_cest;
    private String pro_cst_ipi;
    private String pro_codigo_anp;
    private String pro_codigo_codif;
    private String tes_gera_piscofins;
    private double nfi_vbcufdest;
    private double nfi_pfcpufdest;
    private double nfi_picmsufdest;
    private double nfi_picmsinterestadual;
    private double nfi_picmsinterpart;
    private double nfi_vfcpufdest;
    private double nfi_vicmsufdest;
    private double nfi_vicmsufremet;

    private double nfi_vfcp;
    private double nfi_pfcp;

    public NotaFiscalItem() {
    }

    public NotaFiscalItem(String nfi_item, String nfi_numero, String nfi_clifor, String nfi_serie, String nfi_nrnota, String nfi_codpro, String nfi_nome, double nfi_qtdfat, double nfi_prunit, double nfi_total, double nfi_picms_n, double nfi_vicms_n, double nfi_vicms_s, String nfi_tes, double nfi_pipi, double nfi_vipi, String nfi_cfop, String nfi_sittrib, double nfi_piss, double nfi_viss, String nfi_destino, Timestamp nfi_dtemissao, Timestamp nfi_data, String nfi_obs, double nfi_pbruto, double nfi_pliquido, int nfi_tipo, String nfi_codint, String filial, String incluido, Timestamp horainc, Timestamp dtinc, String alterado, Timestamp horaalt, Timestamp dtalt, String nfi_filial, String nfi_pedido, String nfi_cupom, String nfi_documento, String nfi_almoxarifado, double nfi_base_calculo, String codlan, double nfi_pipifrete, double nfi_baseipifrete, double nfi_vipifrete, double nfi_baseipi, double nfi_desconto, double nfi_valorfrete, double nfi_valordespesa, double nfi_isento, double nfi_outros, double nfi_valorseguro, double nfi_base_s, long nfi_codigodanfe, double nfi_valor_pis, double nfi_valor_cofins, double nfi_icms_antecipado, String nfi_cst_pis, String nfi_cst_cofins, double nfi_aliquota_pis, double nfi_aliquota_cofins, boolean nfi_atualizado_pis_cofins, String nfi_natureza_receita, double nfi_tributos_valor, double nfi_tributos_aliquota, long seq_sincronizacao, String controle_alteracao, String nfi_ecf_numero, String nfi_ecf_coo, String nfi_chaveacesso_ref, double nfi_icm_creditado, double nfi_base_icm_creditado, double nfi_imposto_importacao, double nfi_opi_vicms_destino, double nfi_opi_vicms_origem, double nfi_opi_picms_destino, double nfi_opi_picms_fcp, double nfi_opi_vicms_fcp, double nfi_opi_vicms_interestadual, String nfi_altman, String nfi_um, double nfi_fatorqtd, double nfi_qtd_entsai, double nfi_fcpst_basecalculo, double nfi_fcpst_valor, double nfi_fcpst_aliquota, String nfi_cst_ipi, String nfi_ipi_cenq, String nfi_codean, String nfi_codeantrib, String pro_cst_ipi_ent, String pro_um, String pro_origem, String pro_codean, int pro_tipocod, String pro_coddun, String pro_ncm, String pro_cest, String pro_cst_ipi, String pro_codigo_anp, String pro_codigo_codif, String tes_gera_piscofins, double nfi_vbcufdest, double nfi_pfcpufdest, double nfi_picmsufdest, double nfi_picmsinterestadual, double nfi_picmsinterpart, double nfi_vfcpufdest, double nfi_vicmsufdest, double nfi_vicmsufremet, double nfi_vfcp, double nfi_pfcp) {
	this.nfi_item = nfi_item;
	this.nfi_numero = nfi_numero;
	this.nfi_clifor = nfi_clifor;
	this.nfi_serie = nfi_serie;
	this.nfi_nrnota = nfi_nrnota;
	this.nfi_codpro = nfi_codpro;
	this.nfi_nome = nfi_nome;
	this.nfi_qtdfat = nfi_qtdfat;
	this.nfi_prunit = nfi_prunit;
	this.nfi_total = nfi_total;
	this.nfi_picms_n = nfi_picms_n;
	this.nfi_vicms_n = nfi_vicms_n;
	this.nfi_vicms_s = nfi_vicms_s;
	this.nfi_tes = nfi_tes;
	this.nfi_pipi = nfi_pipi;
	this.nfi_vipi = nfi_vipi;
	this.nfi_cfop = nfi_cfop;
	this.nfi_sittrib = nfi_sittrib;
	this.nfi_piss = nfi_piss;
	this.nfi_viss = nfi_viss;
	this.nfi_destino = nfi_destino;
	this.nfi_dtemissao = nfi_dtemissao;
	this.nfi_data = nfi_data;
	this.nfi_obs = nfi_obs;
	this.nfi_pbruto = nfi_pbruto;
	this.nfi_pliquido = nfi_pliquido;
	this.nfi_tipo = nfi_tipo;
	this.nfi_codint = nfi_codint;
	this.filial = filial;
	this.incluido = incluido;
	this.horainc = horainc;
	this.dtinc = dtinc;
	this.alterado = alterado;
	this.horaalt = horaalt;
	this.dtalt = dtalt;
	this.nfi_filial = nfi_filial;
	this.nfi_pedido = nfi_pedido;
	this.nfi_cupom = nfi_cupom;
	this.nfi_documento = nfi_documento;
	this.nfi_almoxarifado = nfi_almoxarifado;
	this.nfi_base_calculo = nfi_base_calculo;
	this.codlan = codlan;
	this.nfi_pipifrete = nfi_pipifrete;
	this.nfi_baseipifrete = nfi_baseipifrete;
	this.nfi_vipifrete = nfi_vipifrete;
	this.nfi_baseipi = nfi_baseipi;
	this.nfi_desconto = nfi_desconto;
	this.nfi_valorfrete = nfi_valorfrete;
	this.nfi_valordespesa = nfi_valordespesa;
	this.nfi_isento = nfi_isento;
	this.nfi_outros = nfi_outros;
	this.nfi_valorseguro = nfi_valorseguro;
	this.nfi_base_s = nfi_base_s;
	this.nfi_codigodanfe = nfi_codigodanfe;
	this.nfi_valor_pis = nfi_valor_pis;
	this.nfi_valor_cofins = nfi_valor_cofins;
	this.nfi_icms_antecipado = nfi_icms_antecipado;
	this.nfi_cst_pis = nfi_cst_pis;
	this.nfi_cst_cofins = nfi_cst_cofins;
	this.nfi_aliquota_pis = nfi_aliquota_pis;
	this.nfi_aliquota_cofins = nfi_aliquota_cofins;
	this.nfi_atualizado_pis_cofins = nfi_atualizado_pis_cofins;
	this.nfi_natureza_receita = nfi_natureza_receita;
	this.nfi_tributos_valor = nfi_tributos_valor;
	this.nfi_tributos_aliquota = nfi_tributos_aliquota;
	this.seq_sincronizacao = seq_sincronizacao;
	this.controle_alteracao = controle_alteracao;
	this.nfi_ecf_numero = nfi_ecf_numero;
	this.nfi_ecf_coo = nfi_ecf_coo;
	this.nfi_chaveacesso_ref = nfi_chaveacesso_ref;
	this.nfi_icm_creditado = nfi_icm_creditado;
	this.nfi_base_icm_creditado = nfi_base_icm_creditado;
	this.nfi_imposto_importacao = nfi_imposto_importacao;
	this.nfi_opi_vicms_destino = nfi_opi_vicms_destino;
	this.nfi_opi_vicms_origem = nfi_opi_vicms_origem;
	this.nfi_opi_picms_destino = nfi_opi_picms_destino;
	this.nfi_opi_picms_fcp = nfi_opi_picms_fcp;
	this.nfi_opi_vicms_fcp = nfi_opi_vicms_fcp;
	this.nfi_opi_vicms_interestadual = nfi_opi_vicms_interestadual;
	this.nfi_altman = nfi_altman;
	this.nfi_um = nfi_um;
	this.nfi_fatorqtd = nfi_fatorqtd;
	this.nfi_qtd_entsai = nfi_qtd_entsai;
	this.nfi_fcpst_basecalculo = nfi_fcpst_basecalculo;
	this.nfi_fcpst_valor = nfi_fcpst_valor;
	this.nfi_fcpst_aliquota = nfi_fcpst_aliquota;
	this.nfi_cst_ipi = nfi_cst_ipi;
	this.nfi_ipi_cenq = nfi_ipi_cenq;
	this.nfi_codean = nfi_codean;
	this.nfi_codeantrib = nfi_codeantrib;
	this.pro_cst_ipi_ent = pro_cst_ipi_ent;
	this.pro_um = pro_um;
	this.pro_origem = pro_origem;
	this.pro_codean = pro_codean;
	this.pro_tipocod = pro_tipocod;
	this.pro_coddun = pro_coddun;
	this.pro_ncm = pro_ncm;
	this.pro_cest = pro_cest;
	this.pro_cst_ipi = pro_cst_ipi;
	this.pro_codigo_anp = pro_codigo_anp;
	this.pro_codigo_codif = pro_codigo_codif;
	this.tes_gera_piscofins = tes_gera_piscofins;
	this.nfi_vbcufdest = nfi_vbcufdest;
	this.nfi_pfcpufdest = nfi_pfcpufdest;
	this.nfi_picmsufdest = nfi_picmsufdest;
	this.nfi_picmsinterestadual = nfi_picmsinterestadual;
	this.nfi_picmsinterpart = nfi_picmsinterpart;
	this.nfi_vfcpufdest = nfi_vfcpufdest;
	this.nfi_vicmsufdest = nfi_vicmsufdest;
	this.nfi_vicmsufremet = nfi_vicmsufremet;
	this.nfi_vfcp = nfi_vfcp;
	this.nfi_pfcp = nfi_pfcp;
    }

    public String getNfi_item() {
	return nfi_item;
    }

    public void setNfi_item(String nfi_item) {
	this.nfi_item = nfi_item;
    }

    public String getNfi_numero() {
	return nfi_numero;
    }

    public void setNfi_numero(String nfi_numero) {
	this.nfi_numero = nfi_numero;
    }

    public String getNfi_clifor() {
	return nfi_clifor;
    }

    public void setNfi_clifor(String nfi_clifor) {
	this.nfi_clifor = nfi_clifor;
    }

    public String getNfi_serie() {
	return nfi_serie;
    }

    public void setNfi_serie(String nfi_serie) {
	this.nfi_serie = nfi_serie;
    }

    public String getNfi_nrnota() {
	return nfi_nrnota;
    }

    public void setNfi_nrnota(String nfi_nrnota) {
	this.nfi_nrnota = nfi_nrnota;
    }

    public String getNfi_codpro() {
	return nfi_codpro;
    }

    public void setNfi_codpro(String nfi_codpro) {
	this.nfi_codpro = nfi_codpro;
    }

    public String getNfi_nome() {
	return nfi_nome;
    }

    public void setNfi_nome(String nfi_nome) {
	this.nfi_nome = nfi_nome;
    }

    public double getNfi_qtdfat() {
	return nfi_qtdfat;
    }

    public void setNfi_qtdfat(double nfi_qtdfat) {
	this.nfi_qtdfat = nfi_qtdfat;
    }

    public double getNfi_prunit() {
	return nfi_prunit;
    }

    public void setNfi_prunit(double nfi_prunit) {
	this.nfi_prunit = nfi_prunit;
    }

    public double getNfi_total() {
	return nfi_total;
    }

    public void setNfi_total(double nfi_total) {
	this.nfi_total = nfi_total;
    }

    public double getNfi_picms_n() {
	return nfi_picms_n;
    }

    public void setNfi_picms_n(double nfi_picms_n) {
	this.nfi_picms_n = nfi_picms_n;
    }

    public double getNfi_vicms_n() {
	return nfi_vicms_n;
    }

    public void setNfi_vicms_n(double nfi_vicms_n) {
	this.nfi_vicms_n = nfi_vicms_n;
    }

    public double getNfi_vicms_s() {
	return nfi_vicms_s;
    }

    public void setNfi_vicms_s(double nfi_vicms_s) {
	this.nfi_vicms_s = nfi_vicms_s;
    }

    public String getNfi_tes() {
	return nfi_tes;
    }

    public void setNfi_tes(String nfi_tes) {
	this.nfi_tes = nfi_tes;
    }

    public double getNfi_pipi() {
	return nfi_pipi;
    }

    public void setNfi_pipi(double nfi_pipi) {
	this.nfi_pipi = nfi_pipi;
    }

    public double getNfi_vipi() {
	return nfi_vipi;
    }

    public void setNfi_vipi(double nfi_vipi) {
	this.nfi_vipi = nfi_vipi;
    }

    public String getNfi_cfop() {
	return nfi_cfop;
    }

    public void setNfi_cfop(String nfi_cfop) {
	this.nfi_cfop = nfi_cfop;
    }

    public String getNfi_sittrib() {
	return nfi_sittrib;
    }

    public void setNfi_sittrib(String nfi_sittrib) {
	this.nfi_sittrib = nfi_sittrib;
    }

    public double getNfi_piss() {
	return nfi_piss;
    }

    public void setNfi_piss(double nfi_piss) {
	this.nfi_piss = nfi_piss;
    }

    public double getNfi_viss() {
	return nfi_viss;
    }

    public void setNfi_viss(double nfi_viss) {
	this.nfi_viss = nfi_viss;
    }

    public String getNfi_destino() {
	return nfi_destino;
    }

    public void setNfi_destino(String nfi_destino) {
	this.nfi_destino = nfi_destino;
    }

    public Timestamp getNfi_dtemissao() {
	return nfi_dtemissao;
    }

    public void setNfi_dtemissao(Timestamp nfi_dtemissao) {
	this.nfi_dtemissao = nfi_dtemissao;
    }

    public Timestamp getNfi_data() {
	return nfi_data;
    }

    public void setNfi_data(Timestamp nfi_data) {
	this.nfi_data = nfi_data;
    }

    public String getNfi_obs() {
	return nfi_obs;
    }

    public void setNfi_obs(String nfi_obs) {
	this.nfi_obs = nfi_obs;
    }

    public double getNfi_pbruto() {
	return nfi_pbruto;
    }

    public void setNfi_pbruto(double nfi_pbruto) {
	this.nfi_pbruto = nfi_pbruto;
    }

    public double getNfi_pliquido() {
	return nfi_pliquido;
    }

    public void setNfi_pliquido(double nfi_pliquido) {
	this.nfi_pliquido = nfi_pliquido;
    }

    public int getNfi_tipo() {
	return nfi_tipo;
    }

    public void setNfi_tipo(int nfi_tipo) {
	this.nfi_tipo = nfi_tipo;
    }

    public String getNfi_codint() {
	return nfi_codint;
    }

    public void setNfi_codint(String nfi_codint) {
	this.nfi_codint = nfi_codint;
    }

    public String getFilial() {
	return filial;
    }

    public void setFilial(String filial) {
	this.filial = filial;
    }

    public String getIncluido() {
	return incluido;
    }

    public void setIncluido(String incluido) {
	this.incluido = incluido;
    }

    public Timestamp getHorainc() {
	return horainc;
    }

    public void setHorainc(Timestamp horainc) {
	this.horainc = horainc;
    }

    public Timestamp getDtinc() {
	return dtinc;
    }

    public void setDtinc(Timestamp dtinc) {
	this.dtinc = dtinc;
    }

    public String getAlterado() {
	return alterado;
    }

    public void setAlterado(String alterado) {
	this.alterado = alterado;
    }

    public Timestamp getHoraalt() {
	return horaalt;
    }

    public void setHoraalt(Timestamp horaalt) {
	this.horaalt = horaalt;
    }

    public Timestamp getDtalt() {
	return dtalt;
    }

    public void setDtalt(Timestamp dtalt) {
	this.dtalt = dtalt;
    }

    public String getNfi_filial() {
	return nfi_filial;
    }

    public void setNfi_filial(String nfi_filial) {
	this.nfi_filial = nfi_filial;
    }

    public String getNfi_pedido() {
	return nfi_pedido;
    }

    public void setNfi_pedido(String nfi_pedido) {
	this.nfi_pedido = nfi_pedido;
    }

    public String getNfi_cupom() {
	return nfi_cupom;
    }

    public void setNfi_cupom(String nfi_cupom) {
	this.nfi_cupom = nfi_cupom;
    }

    public String getNfi_documento() {
	return nfi_documento;
    }

    public void setNfi_documento(String nfi_documento) {
	this.nfi_documento = nfi_documento;
    }

    public String getNfi_almoxarifado() {
	return nfi_almoxarifado;
    }

    public void setNfi_almoxarifado(String nfi_almoxarifado) {
	this.nfi_almoxarifado = nfi_almoxarifado;
    }

    public double getNfi_base_calculo() {
	return nfi_base_calculo;
    }

    public void setNfi_base_calculo(double nfi_base_calculo) {
	this.nfi_base_calculo = nfi_base_calculo;
    }

    public String getCodlan() {
	return codlan;
    }

    public void setCodlan(String codlan) {
	this.codlan = codlan;
    }

    public double getNfi_pipifrete() {
	return nfi_pipifrete;
    }

    public void setNfi_pipifrete(double nfi_pipifrete) {
	this.nfi_pipifrete = nfi_pipifrete;
    }

    public double getNfi_baseipifrete() {
	return nfi_baseipifrete;
    }

    public void setNfi_baseipifrete(double nfi_baseipifrete) {
	this.nfi_baseipifrete = nfi_baseipifrete;
    }

    public double getNfi_vipifrete() {
	return nfi_vipifrete;
    }

    public void setNfi_vipifrete(double nfi_vipifrete) {
	this.nfi_vipifrete = nfi_vipifrete;
    }

    public double getNfi_baseipi() {
	return nfi_baseipi;
    }

    public void setNfi_baseipi(double nfi_baseipi) {
	this.nfi_baseipi = nfi_baseipi;
    }

    public double getNfi_desconto() {
	return nfi_desconto;
    }

    public void setNfi_desconto(double nfi_desconto) {
	this.nfi_desconto = nfi_desconto;
    }

    public double getNfi_valorfrete() {
	return nfi_valorfrete;
    }

    public void setNfi_valorfrete(double nfi_valorfrete) {
	this.nfi_valorfrete = nfi_valorfrete;
    }

    public double getNfi_valordespesa() {
	return nfi_valordespesa;
    }

    public void setNfi_valordespesa(double nfi_valordespesa) {
	this.nfi_valordespesa = nfi_valordespesa;
    }

    public double getNfi_isento() {
	return nfi_isento;
    }

    public void setNfi_isento(double nfi_isento) {
	this.nfi_isento = nfi_isento;
    }

    public double getNfi_outros() {
	return nfi_outros;
    }

    public void setNfi_outros(double nfi_outros) {
	this.nfi_outros = nfi_outros;
    }

    public double getNfi_valorseguro() {
	return nfi_valorseguro;
    }

    public void setNfi_valorseguro(double nfi_valorseguro) {
	this.nfi_valorseguro = nfi_valorseguro;
    }

    public double getNfi_base_s() {
	return nfi_base_s;
    }

    public void setNfi_base_s(double nfi_base_s) {
	this.nfi_base_s = nfi_base_s;
    }

    public long getNfi_codigodanfe() {
	return nfi_codigodanfe;
    }

    public void setNfi_codigodanfe(long nfi_codigodanfe) {
	this.nfi_codigodanfe = nfi_codigodanfe;
    }

    public double getNfi_valor_pis() {
	return nfi_valor_pis;
    }

    public void setNfi_valor_pis(double nfi_valor_pis) {
	this.nfi_valor_pis = nfi_valor_pis;
    }

    public double getNfi_valor_cofins() {
	return nfi_valor_cofins;
    }

    public void setNfi_valor_cofins(double nfi_valor_cofins) {
	this.nfi_valor_cofins = nfi_valor_cofins;
    }

    public double getNfi_icms_antecipado() {
	return nfi_icms_antecipado;
    }

    public void setNfi_icms_antecipado(double nfi_icms_antecipado) {
	this.nfi_icms_antecipado = nfi_icms_antecipado;
    }

    public String getNfi_cst_pis() {
	return nfi_cst_pis;
    }

    public void setNfi_cst_pis(String nfi_cst_pis) {
	this.nfi_cst_pis = nfi_cst_pis;
    }

    public String getNfi_cst_cofins() {
	return nfi_cst_cofins;
    }

    public void setNfi_cst_cofins(String nfi_cst_cofins) {
	this.nfi_cst_cofins = nfi_cst_cofins;
    }

    public double getNfi_aliquota_pis() {
	return nfi_aliquota_pis;
    }

    public void setNfi_aliquota_pis(double nfi_aliquota_pis) {
	this.nfi_aliquota_pis = nfi_aliquota_pis;
    }

    public double getNfi_aliquota_cofins() {
	return nfi_aliquota_cofins;
    }

    public void setNfi_aliquota_cofins(double nfi_aliquota_cofins) {
	this.nfi_aliquota_cofins = nfi_aliquota_cofins;
    }

    public boolean isNfi_atualizado_pis_cofins() {
	return nfi_atualizado_pis_cofins;
    }

    public void setNfi_atualizado_pis_cofins(boolean nfi_atualizado_pis_cofins) {
	this.nfi_atualizado_pis_cofins = nfi_atualizado_pis_cofins;
    }

    public String getNfi_natureza_receita() {
	return nfi_natureza_receita;
    }

    public void setNfi_natureza_receita(String nfi_natureza_receita) {
	this.nfi_natureza_receita = nfi_natureza_receita;
    }

    public double getNfi_tributos_valor() {
	return nfi_tributos_valor;
    }

    public void setNfi_tributos_valor(double nfi_tributos_valor) {
	this.nfi_tributos_valor = nfi_tributos_valor;
    }

    public double getNfi_tributos_aliquota() {
	return nfi_tributos_aliquota;
    }

    public void setNfi_tributos_aliquota(double nfi_tributos_aliquota) {
	this.nfi_tributos_aliquota = nfi_tributos_aliquota;
    }

    public long getSeq_sincronizacao() {
	return seq_sincronizacao;
    }

    public void setSeq_sincronizacao(long seq_sincronizacao) {
	this.seq_sincronizacao = seq_sincronizacao;
    }

    public String getControle_alteracao() {
	return controle_alteracao;
    }

    public void setControle_alteracao(String controle_alteracao) {
	this.controle_alteracao = controle_alteracao;
    }

    public String getNfi_ecf_numero() {
	return nfi_ecf_numero;
    }

    public void setNfi_ecf_numero(String nfi_ecf_numero) {
	this.nfi_ecf_numero = nfi_ecf_numero;
    }

    public String getNfi_ecf_coo() {
	return nfi_ecf_coo;
    }

    public void setNfi_ecf_coo(String nfi_ecf_coo) {
	this.nfi_ecf_coo = nfi_ecf_coo;
    }

    public String getNfi_chaveacesso_ref() {
	return nfi_chaveacesso_ref;
    }

    public void setNfi_chaveacesso_ref(String nfi_chaveacesso_ref) {
	this.nfi_chaveacesso_ref = nfi_chaveacesso_ref;
    }

    public double getNfi_icm_creditado() {
	return nfi_icm_creditado;
    }

    public void setNfi_icm_creditado(double nfi_icm_creditado) {
	this.nfi_icm_creditado = nfi_icm_creditado;
    }

    public double getNfi_base_icm_creditado() {
	return nfi_base_icm_creditado;
    }

    public void setNfi_base_icm_creditado(double nfi_base_icm_creditado) {
	this.nfi_base_icm_creditado = nfi_base_icm_creditado;
    }

    public double getNfi_imposto_importacao() {
	return nfi_imposto_importacao;
    }

    public void setNfi_imposto_importacao(double nfi_imposto_importacao) {
	this.nfi_imposto_importacao = nfi_imposto_importacao;
    }

    public double getNfi_opi_vicms_destino() {
	return nfi_opi_vicms_destino;
    }

    public void setNfi_opi_vicms_destino(double nfi_opi_vicms_destino) {
	this.nfi_opi_vicms_destino = nfi_opi_vicms_destino;
    }

    public double getNfi_opi_vicms_origem() {
	return nfi_opi_vicms_origem;
    }

    public void setNfi_opi_vicms_origem(double nfi_opi_vicms_origem) {
	this.nfi_opi_vicms_origem = nfi_opi_vicms_origem;
    }

    public double getNfi_opi_picms_destino() {
	return nfi_opi_picms_destino;
    }

    public void setNfi_opi_picms_destino(double nfi_opi_picms_destino) {
	this.nfi_opi_picms_destino = nfi_opi_picms_destino;
    }

    public double getNfi_opi_picms_fcp() {
	return nfi_opi_picms_fcp;
    }

    public void setNfi_opi_picms_fcp(double nfi_opi_picms_fcp) {
	this.nfi_opi_picms_fcp = nfi_opi_picms_fcp;
    }

    public double getNfi_opi_vicms_fcp() {
	return nfi_opi_vicms_fcp;
    }

    public void setNfi_opi_vicms_fcp(double nfi_opi_vicms_fcp) {
	this.nfi_opi_vicms_fcp = nfi_opi_vicms_fcp;
    }

    public double getNfi_opi_vicms_interestadual() {
	return nfi_opi_vicms_interestadual;
    }

    public void setNfi_opi_vicms_interestadual(double nfi_opi_vicms_interestadual) {
	this.nfi_opi_vicms_interestadual = nfi_opi_vicms_interestadual;
    }

    public String getNfi_altman() {
	return nfi_altman;
    }

    public void setNfi_altman(String nfi_altman) {
	this.nfi_altman = nfi_altman;
    }

    public String getNfi_um() {
	return nfi_um;
    }

    public void setNfi_um(String nfi_um) {
	this.nfi_um = nfi_um;
    }

    public double getNfi_fatorqtd() {
	return nfi_fatorqtd;
    }

    public void setNfi_fatorqtd(double nfi_fatorqtd) {
	this.nfi_fatorqtd = nfi_fatorqtd;
    }

    public double getNfi_qtd_entsai() {
	return nfi_qtd_entsai;
    }

    public void setNfi_qtd_entsai(double nfi_qtd_entsai) {
	this.nfi_qtd_entsai = nfi_qtd_entsai;
    }

    public double getNfi_fcpst_basecalculo() {
	return nfi_fcpst_basecalculo;
    }

    public void setNfi_fcpst_basecalculo(double nfi_fcpst_basecalculo) {
	this.nfi_fcpst_basecalculo = nfi_fcpst_basecalculo;
    }

    public double getNfi_fcpst_valor() {
	return nfi_fcpst_valor;
    }

    public void setNfi_fcpst_valor(double nfi_fcpst_valor) {
	this.nfi_fcpst_valor = nfi_fcpst_valor;
    }

    public double getNfi_fcpst_aliquota() {
	return nfi_fcpst_aliquota;
    }

    public void setNfi_fcpst_aliquota(double nfi_fcpst_aliquota) {
	this.nfi_fcpst_aliquota = nfi_fcpst_aliquota;
    }

    public String getNfi_cst_ipi() {
	return nfi_cst_ipi;
    }

    public void setNfi_cst_ipi(String nfi_cst_ipi) {
	this.nfi_cst_ipi = nfi_cst_ipi;
    }

    public String getNfi_ipi_cenq() {
	return nfi_ipi_cenq;
    }

    public void setNfi_ipi_cenq(String nfi_ipi_cenq) {
	this.nfi_ipi_cenq = nfi_ipi_cenq;
    }

    public String getNfi_codean() {
	return nfi_codean;
    }

    public void setNfi_codean(String nfi_codean) {
	this.nfi_codean = nfi_codean;
    }

    public String getNfi_codeantrib() {
	return nfi_codeantrib;
    }

    public void setNfi_codeantrib(String nfi_codeantrib) {
	this.nfi_codeantrib = nfi_codeantrib;
    }

    public String getPro_cst_ipi_ent() {
	return pro_cst_ipi_ent;
    }

    public void setPro_cst_ipi_ent(String pro_cst_ipi_ent) {
	this.pro_cst_ipi_ent = pro_cst_ipi_ent;
    }

    public String getPro_um() {
	return pro_um;
    }

    public void setPro_um(String pro_um) {
	this.pro_um = pro_um;
    }

    public String getPro_origem() {
	return pro_origem;
    }

    public void setPro_origem(String pro_origem) {
	this.pro_origem = pro_origem;
    }

    public String getPro_codean() {
	return pro_codean;
    }

    public void setPro_codean(String pro_codean) {
	this.pro_codean = pro_codean;
    }

    public int getPro_tipocod() {
	return pro_tipocod;
    }

    public void setPro_tipocod(int pro_tipocod) {
	this.pro_tipocod = pro_tipocod;
    }

    public String getPro_coddun() {
	return pro_coddun;
    }

    public void setPro_coddun(String pro_coddun) {
	this.pro_coddun = pro_coddun;
    }

    public String getPro_ncm() {
	return pro_ncm;
    }

    public void setPro_ncm(String pro_ncm) {
	this.pro_ncm = pro_ncm;
    }

    public String getPro_cest() {
	return pro_cest;
    }

    public void setPro_cest(String pro_cest) {
	this.pro_cest = pro_cest;
    }

    public String getPro_cst_ipi() {
	return pro_cst_ipi;
    }

    public void setPro_cst_ipi(String pro_cst_ipi) {
	this.pro_cst_ipi = pro_cst_ipi;
    }

    public String getPro_codigo_anp() {
	return pro_codigo_anp;
    }

    public void setPro_codigo_anp(String pro_codigo_anp) {
	this.pro_codigo_anp = pro_codigo_anp;
    }

    public String getPro_codigo_codif() {
	return pro_codigo_codif;
    }

    public void setPro_codigo_codif(String pro_codigo_codif) {
	this.pro_codigo_codif = pro_codigo_codif;
    }

    public String getTes_gera_piscofins() {
	return tes_gera_piscofins;
    }

    public void setTes_gera_piscofins(String tes_gera_piscofins) {
	this.tes_gera_piscofins = tes_gera_piscofins;
    }

    public double getNfi_vbcufdest() {
	return nfi_vbcufdest;
    }

    public void setNfi_vbcufdest(double nfi_vbcufdest) {
	this.nfi_vbcufdest = nfi_vbcufdest;
    }

    public double getNfi_pfcpufdest() {
	return nfi_pfcpufdest;
    }

    public void setNfi_pfcpufdest(double nfi_pfcpufdest) {
	this.nfi_pfcpufdest = nfi_pfcpufdest;
    }

    public double getNfi_picmsufdest() {
	return nfi_picmsufdest;
    }

    public void setNfi_picmsufdest(double nfi_picmsufdest) {
	this.nfi_picmsufdest = nfi_picmsufdest;
    }

    public double getNfi_picmsinterestadual() {
	return nfi_picmsinterestadual;
    }

    public void setNfi_picmsinterestadual(double nfi_picmsinterestadual) {
	this.nfi_picmsinterestadual = nfi_picmsinterestadual;
    }

    public double getNfi_picmsinterpart() {
	return nfi_picmsinterpart;
    }

    public void setNfi_picmsinterpart(double nfi_picmsinterpart) {
	this.nfi_picmsinterpart = nfi_picmsinterpart;
    }

    public double getNfi_vfcpufdest() {
	return nfi_vfcpufdest;
    }

    public void setNfi_vfcpufdest(double nfi_vfcpufdest) {
	this.nfi_vfcpufdest = nfi_vfcpufdest;
    }

    public double getNfi_vicmsufdest() {
	return nfi_vicmsufdest;
    }

    public void setNfi_vicmsufdest(double nfi_vicmsufdest) {
	this.nfi_vicmsufdest = nfi_vicmsufdest;
    }

    public double getNfi_vicmsufremet() {
	return nfi_vicmsufremet;
    }

    public void setNfi_vicmsufremet(double nfi_vicmsufremet) {
	this.nfi_vicmsufremet = nfi_vicmsufremet;
    }

    public double getNfi_vfcp() {
	return nfi_vfcp;
    }

    public void setNfi_vfcp(double nfi_vfcp) {
	this.nfi_vfcp = nfi_vfcp;
    }

    public double getNfi_pfcp() {
	return nfi_pfcp;
    }

    public void setNfi_pfcp(double nfi_pfcp) {
	this.nfi_pfcp = nfi_pfcp;
    }

}
