/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.entidades;

import java.sql.Timestamp;

/**
 *
 * @author allison
 */
public class NotaFiscalLote {

    private String nfe_lote;
    private Timestamp nfe_data;
    private String nfe_protocolo;
    private String nfe_protocolodatahora;
    private String nfe_recibo;
    private int nfe_qtdnotas;
    private String nfe_fechado;
    private String nFeFormatoEmissao;
    private String nfe_filial;
    private String inc;
    private Timestamp dtinc;
    private Timestamp horainc;
    private String alt;
    private Timestamp dtalt;
    private Timestamp horaalt;
    private String nfe_tipo;

    public NotaFiscalLote() {
    }

    public NotaFiscalLote(String nfe_lote, Timestamp nfe_data, String nfe_protocolo, String nfe_protocolodatahora, String nfe_recibo, int nfe_qtdnotas, String nfe_fechado, String nFeFormatoEmissao, String nfe_filial, String inc, Timestamp dtinc, Timestamp horainc, String alt, Timestamp dtalt, Timestamp horaalt, String nfe_tipo) {
        this.nfe_lote = nfe_lote;
        this.nfe_data = nfe_data;
        this.nfe_protocolo = nfe_protocolo;
        this.nfe_protocolodatahora = nfe_protocolodatahora;
        this.nfe_recibo = nfe_recibo;
        this.nfe_qtdnotas = nfe_qtdnotas;
        this.nfe_fechado = nfe_fechado;
        this.nFeFormatoEmissao = nFeFormatoEmissao;
        this.nfe_filial = nfe_filial;
        this.inc = inc;
        this.dtinc = dtinc;
        this.horainc = horainc;
        this.alt = alt;
        this.dtalt = dtalt;
        this.horaalt = horaalt;
        this.nfe_tipo = nfe_tipo;
    }

    public String getNfe_lote() {
        return nfe_lote;
    }

    public void setNfe_lote(String nfe_lote) {
        this.nfe_lote = nfe_lote;
    }

    public Timestamp getNfe_data() {
        return nfe_data;
    }

    public void setNfe_data(Timestamp nfe_data) {
        this.nfe_data = nfe_data;
    }

    public String getNfe_protocolo() {
        return nfe_protocolo;
    }

    public void setNfe_protocolo(String nfe_protocolo) {
        this.nfe_protocolo = nfe_protocolo;
    }

    public String getNfe_protocolodatahora() {
        return nfe_protocolodatahora;
    }

    public void setNfe_protocolodatahora(String nfe_protocolodatahora) {
        this.nfe_protocolodatahora = nfe_protocolodatahora;
    }

    public String getNfe_recibo() {
        return nfe_recibo;
    }

    public void setNfe_recibo(String nfe_recibo) {
        this.nfe_recibo = nfe_recibo;
    }

    public int getNfe_qtdnotas() {
        return nfe_qtdnotas;
    }

    public void setNfe_qtdnotas(int nfe_qtdnotas) {
        this.nfe_qtdnotas = nfe_qtdnotas;
    }

    public String getNfe_fechado() {
        return nfe_fechado;
    }

    public void setNfe_fechado(String nfe_fechado) {
        this.nfe_fechado = nfe_fechado;
    }

    public String getnFeFormatoEmissao() {
        return nFeFormatoEmissao;
    }

    public void setnFeFormatoEmissao(String nFeFormatoEmissao) {
        this.nFeFormatoEmissao = nFeFormatoEmissao;
    }

    public String getNfe_filial() {
        return nfe_filial;
    }

    public void setNfe_filial(String nfe_filial) {
        this.nfe_filial = nfe_filial;
    }

    public String getInc() {
        return inc;
    }

    public void setInc(String inc) {
        this.inc = inc;
    }

    public Timestamp getDtinc() {
        return dtinc;
    }

    public void setDtinc(Timestamp dtinc) {
        this.dtinc = dtinc;
    }

    public Timestamp getHorainc() {
        return horainc;
    }

    public void setHorainc(Timestamp horainc) {
        this.horainc = horainc;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public Timestamp getDtalt() {
        return dtalt;
    }

    public void setDtalt(Timestamp dtalt) {
        this.dtalt = dtalt;
    }

    public Timestamp getHoraalt() {
        return horaalt;
    }

    public void setHoraalt(Timestamp horaalt) {
        this.horaalt = horaalt;
    }

    public String getNfe_tipo() {
        return nfe_tipo;
    }

    public void setNfe_tipo(String nfe_tipo) {
        this.nfe_tipo = nfe_tipo;
    }
}
