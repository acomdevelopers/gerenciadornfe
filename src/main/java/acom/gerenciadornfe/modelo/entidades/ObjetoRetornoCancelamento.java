package acom.gerenciadornfe.modelo.entidades;

/**
 * @author allison
 */
public class ObjetoRetornoCancelamento {

    private boolean statusTransmissao;
    private String status;
    private String motivo;
    private String numeroLote;
    private String protocolo;
    private String cnpj;
    private String xml;
    private String motivoJustificativa;
    private Exception ex;

    public ObjetoRetornoCancelamento() {
    }

    public ObjetoRetornoCancelamento(boolean statusTransmissao, String status, String motivo, String numeroLote, String protocolo, String cnpj, String xml, String motivoJustificativa, Exception ex) {
        this.statusTransmissao = statusTransmissao;
        this.status = status;
        this.motivo = motivo;
        this.numeroLote = numeroLote;
        this.protocolo = protocolo;
        this.cnpj = cnpj;
        this.xml = xml;
        this.motivoJustificativa = motivoJustificativa;
        this.ex = ex;
    }

    public boolean isStatusTransmissao() {
        return statusTransmissao;
    }

    public void setStatusTransmissao(boolean statusTransmissao) {
        this.statusTransmissao = statusTransmissao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getNumeroLote() {
        return numeroLote;
    }

    public void setNumeroLote(String numeroLote) {
        this.numeroLote = numeroLote;
    }

    public String getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getMotivoJustificativa() {
        return motivoJustificativa;
    }

    public void setMotivoJustificativa(String motivoJustificativa) {
        this.motivoJustificativa = motivoJustificativa;
    }

    public Exception getEx() {
        return ex;
    }

    public void setEx(Exception ex) {
        this.ex = ex;
    }

}
