/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.entidades;

import java.time.LocalDateTime;

/**
 *
 * @author allison
 */
public class ObjetoRetornoEnviarMota {
    private boolean statusTransmissao;
    private String codigoMenssagem;
    private LocalDateTime dataHoraRecebimento;
    private String menssagem;
    private String motivo;
    private String numeroRecibo;
    private String protocolo;
    private String status;
    private String xml;

    public ObjetoRetornoEnviarMota() {
    }

    public ObjetoRetornoEnviarMota(boolean statusTransmissao, String codigoMenssagem, LocalDateTime dataHoraRecebimento, String menssagem, String motivo, String numeroRecibo, String protocolo, String status, String xml) {
        this.statusTransmissao = statusTransmissao;
        this.codigoMenssagem = codigoMenssagem;
        this.dataHoraRecebimento = dataHoraRecebimento;
        this.menssagem = menssagem;
        this.motivo = motivo;
        this.numeroRecibo = numeroRecibo;
        this.protocolo = protocolo;
        this.status = status;
        this.xml = xml;
    }

    public boolean isStatusTransmissao() {
        return statusTransmissao;
    }

    public void setStatusTransmissao(boolean statusTransmissao) {
        this.statusTransmissao = statusTransmissao;
    }

    public String getCodigoMenssagem() {
        return codigoMenssagem;
    }

    public void setCodigoMenssagem(String codigoMenssagem) {
        this.codigoMenssagem = codigoMenssagem;
    }

    public LocalDateTime getDataHoraRecebimento() {
        return dataHoraRecebimento;
    }

    public void setDataHoraRecebimento(LocalDateTime dataHoraRecebimento) {
        this.dataHoraRecebimento = dataHoraRecebimento;
    }

    public String getMenssagem() {
        return menssagem;
    }

    public void setMenssagem(String menssagem) {
        this.menssagem = menssagem;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getNumeroRecibo() {
        return numeroRecibo;
    }

    public void setNumeroRecibo(String numeroRecibo) {
        this.numeroRecibo = numeroRecibo;
    }

    public String getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }
}
