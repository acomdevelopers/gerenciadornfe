/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.entidades;

/**
 *
 * @author allison
 */
public class ObjetoRetornoTransmissao {

    private boolean statusTransmissao;

    private String xmlRetorno;

    private String codStatus;

    private String motivo;

    private String data;

    private String protocolo;

    public ObjetoRetornoTransmissao() {
    }

    public ObjetoRetornoTransmissao(boolean statusTransmissao, String xmlRetorno, String codStatus, String motivo, String data, String protocolo) {
        this.statusTransmissao = statusTransmissao;
        this.xmlRetorno = xmlRetorno;
        this.codStatus = codStatus;
        this.motivo = motivo;
        this.data = data;
        this.protocolo = protocolo;
    }

    public boolean isStatusTransmissao() {
        return statusTransmissao;
    }

    public void setStatusTransmissao(boolean statusTransmissao) {
        this.statusTransmissao = statusTransmissao;
    }

    public String getXmlRetorno() {
        return xmlRetorno;
    }

    public void setXmlRetorno(String xmlRetorno) {
        this.xmlRetorno = xmlRetorno;
    }

    public String getCodStatus() {
        return codStatus;
    }

    public void setCodStatus(String codStatus) {
        this.codStatus = codStatus;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getProtocolo() {
        return protocolo;
    }

    public void setProtocolo(String protocolo) {
        this.protocolo = protocolo;
    }

}
