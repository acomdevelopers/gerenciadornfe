/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.entidades;

import javafx.stage.Stage;

/**
 *
 * @author allison
 */
public class ParametroCancelamento {

    private Stage dialogStage;
    private NotaFiscal nf;

    public ParametroCancelamento() {
    }

    public ParametroCancelamento(Stage dialogStage, NotaFiscal nf) {
        this.dialogStage = dialogStage;
        this.nf = nf;
    }

    public Stage getDialogStage() {
        return dialogStage;
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public NotaFiscal getNf() {
        return nf;
    }

    public void setNf(NotaFiscal nf) {
        this.nf = nf;
    }

}
