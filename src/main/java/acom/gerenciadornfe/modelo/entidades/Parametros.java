package acom.gerenciadornfe.modelo.entidades;

import java.io.Serializable;
import javafx.beans.property.SimpleStringProperty;

/**
 * @author allison
 */
public class Parametros implements Serializable {

    private String par_nome;
    private String par_descricao;
    private SimpleStringProperty par_valor;

    public Parametros() {
    }

    public Parametros(String par_nome, String par_descricao, String par_valor) {
        this.par_nome = par_nome;
        this.par_descricao = par_descricao;
        this.par_valor = new SimpleStringProperty(par_valor);
    }

    public String getPar_nome() {
        return par_nome;
    }

    public void setPar_nome(String par_nome) {
        this.par_nome = par_nome;
    }

    public String getPar_descricao() {
        return par_descricao;
    }

    public void setPar_descricao(String par_descricao) {
        this.par_descricao = par_descricao;
    }

    public String getPar_valor() {
        return par_valor.get();
    }

    public void setPar_valor(String par_valor) {
        this.par_valor = new SimpleStringProperty(par_valor);
    }
}
