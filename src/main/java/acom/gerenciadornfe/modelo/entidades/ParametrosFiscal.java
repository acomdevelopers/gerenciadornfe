/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.entidades;

/**
 *
 * @author allison
 */
public class ParametrosFiscal {

    private double cfg_aliquota_cofins;
    private double cfg_aliquota_pis;
    private int cfg_beneficiario_prodepe;
    private String cfg_considera_cod_xml_ent;
    private boolean cfg_considera_natureza_geranf;
    private String cfg_dir_xml_destinatario;
    private String cfg_diretoriopdf;
    private String cfg_diretoriotxt;
    private boolean cfg_importa_produto_xml_ent;
    private boolean cfg_importa_produto_xml_saida;
    private boolean cfg_lei_transparencia;
    private double cfg_limite_venda_pessoa_fisica;
    private String cfg_logomarca_danfe;
    private String cfg_modelo_nfe;
    private String cfg_modeloemissaodanfe;
    private boolean cfg_mostraretornonfe;
    private double cfg_percentual_prodepe;
    private String cfg_tipo_volumes_nf;
    private String cfg_versao_leiaute;
    private String cfg_versaoschema;
    private String cfg_webservice;
    private String nfeAmbiente;
    private String nfeCertificadoDigital;
    private String nfeDiretorioXML;
    private String nfeFormatoEmissao;
    private String nfeFormatoImpressaoDANFe;
    private String nfeLicencaDLL;
    private boolean usaNFe;

    public ParametrosFiscal() {
    }

    public ParametrosFiscal(double cfg_aliquota_cofins, double cfg_aliquota_pis, int cfg_beneficiario_prodepe, String cfg_considera_cod_xml_ent, boolean cfg_considera_natureza_geranf, String cfg_dir_xml_destinatario, String cfg_diretoriopdf, String cfg_diretoriotxt, boolean cfg_importa_produto_xml_ent, boolean cfg_importa_produto_xml_saida, boolean cfg_lei_transparencia, double cfg_limite_venda_pessoa_fisica, String cfg_logomarca_danfe, String cfg_modelo_nfe, String cfg_modeloemissaodanfe, boolean cfg_mostraretornonfe, double cfg_percentual_prodepe, String cfg_tipo_volumes_nf, String cfg_versao_leiaute, String cfg_versaoschema, String cfg_webservice, String nfeAmbiente, String nfeCertificadoDigital, String nfeDiretorioXML, String nfeFormatoEmissao, String nfeFormatoImpressaoDANFe, String nfeLicencaDLL, boolean usaNFe) {
        this.cfg_aliquota_cofins = cfg_aliquota_cofins;
        this.cfg_aliquota_pis = cfg_aliquota_pis;
        this.cfg_beneficiario_prodepe = cfg_beneficiario_prodepe;
        this.cfg_considera_cod_xml_ent = cfg_considera_cod_xml_ent;
        this.cfg_considera_natureza_geranf = cfg_considera_natureza_geranf;
        this.cfg_dir_xml_destinatario = cfg_dir_xml_destinatario;
        this.cfg_diretoriopdf = cfg_diretoriopdf;
        this.cfg_diretoriotxt = cfg_diretoriotxt;
        this.cfg_importa_produto_xml_ent = cfg_importa_produto_xml_ent;
        this.cfg_importa_produto_xml_saida = cfg_importa_produto_xml_saida;
        this.cfg_lei_transparencia = cfg_lei_transparencia;
        this.cfg_limite_venda_pessoa_fisica = cfg_limite_venda_pessoa_fisica;
        this.cfg_logomarca_danfe = cfg_logomarca_danfe;
        this.cfg_modelo_nfe = cfg_modelo_nfe;
        this.cfg_modeloemissaodanfe = cfg_modeloemissaodanfe;
        this.cfg_mostraretornonfe = cfg_mostraretornonfe;
        this.cfg_percentual_prodepe = cfg_percentual_prodepe;
        this.cfg_tipo_volumes_nf = cfg_tipo_volumes_nf;
        this.cfg_versao_leiaute = cfg_versao_leiaute;
        this.cfg_versaoschema = cfg_versaoschema;
        this.cfg_webservice = cfg_webservice;
        this.nfeAmbiente = nfeAmbiente;
        this.nfeCertificadoDigital = nfeCertificadoDigital;
        this.nfeDiretorioXML = nfeDiretorioXML;
        this.nfeFormatoEmissao = nfeFormatoEmissao;
        this.nfeFormatoImpressaoDANFe = nfeFormatoImpressaoDANFe;
        this.nfeLicencaDLL = nfeLicencaDLL;
        this.usaNFe = usaNFe;
    }

    public double getCfg_aliquota_cofins() {
        return cfg_aliquota_cofins;
    }

    public void setCfg_aliquota_cofins(double cfg_aliquota_cofins) {
        this.cfg_aliquota_cofins = cfg_aliquota_cofins;
    }

    public double getCfg_aliquota_pis() {
        return cfg_aliquota_pis;
    }

    public void setCfg_aliquota_pis(double cfg_aliquota_pis) {
        this.cfg_aliquota_pis = cfg_aliquota_pis;
    }

    public int getCfg_beneficiario_prodepe() {
        return cfg_beneficiario_prodepe;
    }

    public void setCfg_beneficiario_prodepe(int cfg_beneficiario_prodepe) {
        this.cfg_beneficiario_prodepe = cfg_beneficiario_prodepe;
    }

    public String getCfg_considera_cod_xml_ent() {
        return cfg_considera_cod_xml_ent;
    }

    public void setCfg_considera_cod_xml_ent(String cfg_considera_cod_xml_ent) {
        this.cfg_considera_cod_xml_ent = cfg_considera_cod_xml_ent;
    }

    public boolean isCfg_considera_natureza_geranf() {
        return cfg_considera_natureza_geranf;
    }

    public void setCfg_considera_natureza_geranf(boolean cfg_considera_natureza_geranf) {
        this.cfg_considera_natureza_geranf = cfg_considera_natureza_geranf;
    }

    public String getCfg_dir_xml_destinatario() {
        return cfg_dir_xml_destinatario;
    }

    public void setCfg_dir_xml_destinatario(String cfg_dir_xml_destinatario) {
        this.cfg_dir_xml_destinatario = cfg_dir_xml_destinatario;
    }

    public String getCfg_diretoriopdf() {
        return cfg_diretoriopdf;
    }

    public void setCfg_diretoriopdf(String cfg_diretoriopdf) {
        this.cfg_diretoriopdf = cfg_diretoriopdf;
    }

    public String getCfg_diretoriotxt() {
        return cfg_diretoriotxt;
    }

    public void setCfg_diretoriotxt(String cfg_diretoriotxt) {
        this.cfg_diretoriotxt = cfg_diretoriotxt;
    }

    public boolean isCfg_importa_produto_xml_ent() {
        return cfg_importa_produto_xml_ent;
    }

    public void setCfg_importa_produto_xml_ent(boolean cfg_importa_produto_xml_ent) {
        this.cfg_importa_produto_xml_ent = cfg_importa_produto_xml_ent;
    }

    public boolean isCfg_importa_produto_xml_saida() {
        return cfg_importa_produto_xml_saida;
    }

    public void setCfg_importa_produto_xml_saida(boolean cfg_importa_produto_xml_saida) {
        this.cfg_importa_produto_xml_saida = cfg_importa_produto_xml_saida;
    }

    public boolean isCfg_lei_transparencia() {
        return cfg_lei_transparencia;
    }

    public void setCfg_lei_transparencia(boolean cfg_lei_transparencia) {
        this.cfg_lei_transparencia = cfg_lei_transparencia;
    }

    public double getCfg_limite_venda_pessoa_fisica() {
        return cfg_limite_venda_pessoa_fisica;
    }

    public void setCfg_limite_venda_pessoa_fisica(double cfg_limite_venda_pessoa_fisica) {
        this.cfg_limite_venda_pessoa_fisica = cfg_limite_venda_pessoa_fisica;
    }

    public String getCfg_logomarca_danfe() {
        return cfg_logomarca_danfe;
    }

    public void setCfg_logomarca_danfe(String cfg_logomarca_danfe) {
        this.cfg_logomarca_danfe = cfg_logomarca_danfe;
    }

    public String getCfg_modelo_nfe() {
        return cfg_modelo_nfe;
    }

    public void setCfg_modelo_nfe(String cfg_modelo_nfe) {
        this.cfg_modelo_nfe = cfg_modelo_nfe;
    }

    public String getCfg_modeloemissaodanfe() {
        return cfg_modeloemissaodanfe;
    }

    public void setCfg_modeloemissaodanfe(String cfg_modeloemissaodanfe) {
        this.cfg_modeloemissaodanfe = cfg_modeloemissaodanfe;
    }

    public boolean isCfg_mostraretornonfe() {
        return cfg_mostraretornonfe;
    }

    public void setCfg_mostraretornonfe(boolean cfg_mostraretornonfe) {
        this.cfg_mostraretornonfe = cfg_mostraretornonfe;
    }

    public double getCfg_percentual_prodepe() {
        return cfg_percentual_prodepe;
    }

    public void setCfg_percentual_prodepe(double cfg_percentual_prodepe) {
        this.cfg_percentual_prodepe = cfg_percentual_prodepe;
    }

    public String getCfg_tipo_volumes_nf() {
        return cfg_tipo_volumes_nf;
    }

    public void setCfg_tipo_volumes_nf(String cfg_tipo_volumes_nf) {
        this.cfg_tipo_volumes_nf = cfg_tipo_volumes_nf;
    }

    public String getCfg_versao_leiaute() {
        return cfg_versao_leiaute;
    }

    public void setCfg_versao_leiaute(String cfg_versao_leiaute) {
        this.cfg_versao_leiaute = cfg_versao_leiaute;
    }

    public String getCfg_versaoschema() {
        return cfg_versaoschema;
    }

    public void setCfg_versaoschema(String cfg_versaoschema) {
        this.cfg_versaoschema = cfg_versaoschema;
    }

    public String getCfg_webservice() {
        return cfg_webservice;
    }

    public void setCfg_webservice(String cfg_webservice) {
        this.cfg_webservice = cfg_webservice;
    }

    public String getNfeAmbiente() {
        return nfeAmbiente;
    }

    public void setNfeAmbiente(String nfeAmbiente) {
        this.nfeAmbiente = nfeAmbiente;
    }

    public String getNfeCertificadoDigital() {
        return nfeCertificadoDigital;
    }

    public void setNfeCertificadoDigital(String nfeCertificadoDigital) {
        this.nfeCertificadoDigital = nfeCertificadoDigital;
    }

    public String getNfeDiretorioXML() {
        return nfeDiretorioXML;
    }

    public void setNfeDiretorioXML(String nfeDiretorioXML) {
        this.nfeDiretorioXML = nfeDiretorioXML;
    }

    public String getNfeFormatoEmissao() {
        return nfeFormatoEmissao;
    }

    public void setNfeFormatoEmissao(String nfeFormatoEmissao) {
        this.nfeFormatoEmissao = nfeFormatoEmissao;
    }

    public String getNfeFormatoImpressaoDANFe() {
        return nfeFormatoImpressaoDANFe;
    }

    public void setNfeFormatoImpressaoDANFe(String nfeFormatoImpressaoDANFe) {
        this.nfeFormatoImpressaoDANFe = nfeFormatoImpressaoDANFe;
    }

    public String getNfeLicencaDLL() {
        return nfeLicencaDLL;
    }

    public void setNfeLicencaDLL(String nfeLicencaDLL) {
        this.nfeLicencaDLL = nfeLicencaDLL;
    }

    public boolean isUsaNFe() {
        return usaNFe;
    }

    public void setUsaNFe(boolean usaNFe) {
        this.usaNFe = usaNFe;
    }
}
