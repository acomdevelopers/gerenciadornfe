/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.entidades;

/**
 * @author Allison
 */
public class PostEmail {

    private String nf_documento;
    private String nf_empresa;
    private String nf_chave;
    private String nf_destinatario;
    private String nf_assunto;
    private String nf_menssagem;
    private String nf_menssagem_zip;
    private String nf_xml;
    private String nf_danfe;
    private String nf_zip;

    public PostEmail() {
    }

    public PostEmail(String nf_documento, String nf_empresa, String nf_chave, String nf_destinatario, String nf_assunto, String nf_menssagem, String nf_menssagem_zip, String nf_xml, String nf_danfe, String nf_zip) {
	this.nf_documento = nf_documento;
	this.nf_empresa = nf_empresa;
	this.nf_chave = nf_chave;
	this.nf_destinatario = nf_destinatario;
	this.nf_assunto = nf_assunto;
	this.nf_menssagem = nf_menssagem;
	this.nf_menssagem_zip = nf_menssagem_zip;
	this.nf_xml = nf_xml;
	this.nf_danfe = nf_danfe;
	this.nf_zip = nf_zip;
    }

    public String getNf_documento() {
	return nf_documento;
    }

    public void setNf_documento(String nf_documento) {
	this.nf_documento = nf_documento;
    }

    public String getNf_empresa() {
	return nf_empresa;
    }

    public void setNf_empresa(String nf_empresa) {
	this.nf_empresa = nf_empresa;
    }

    public String getNf_chave() {
	return nf_chave;
    }

    public void setNf_chave(String nf_chave) {
	this.nf_chave = nf_chave;
    }

    public String getNf_destinatario() {
	return nf_destinatario;
    }

    public void setNf_destinatario(String nf_destinatario) {
	this.nf_destinatario = nf_destinatario;
    }

    public String getNf_assunto() {
	return nf_assunto;
    }

    public void setNf_assunto(String nf_assunto) {
	this.nf_assunto = nf_assunto;
    }

    public String getNf_menssagem() {
	return nf_menssagem;
    }

    public void setNf_menssagem(String nf_menssagem) {
	this.nf_menssagem = nf_menssagem;
    }

    public String getNf_menssagem_zip() {
	return nf_menssagem_zip;
    }

    public void setNf_menssagem_zip(String nf_menssagem_zip) {
	this.nf_menssagem_zip = nf_menssagem_zip;
    }

    public String getNf_xml() {
	return nf_xml;
    }

    public void setNf_xml(String nf_xml) {
	this.nf_xml = nf_xml;
    }

    public String getNf_danfe() {
	return nf_danfe;
    }

    public void setNf_danfe(String nf_danfe) {
	this.nf_danfe = nf_danfe;
    }

    public String getNf_zip() {
	return nf_zip;
    }

    public void setNf_zip(String nf_zip) {
	this.nf_zip = nf_zip;
    }

}
