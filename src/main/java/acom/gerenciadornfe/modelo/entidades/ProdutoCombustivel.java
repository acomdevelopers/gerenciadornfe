/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.entidades;

/**
 *
 * @author allison
 */
public class ProdutoCombustivel {

    private String pcom_codint;
    private String pcom_descricao_anp;
    private String pcom_codigo_anp;
    private String pcom_codif;
    private double pcom_pglp;
    private double pcom_pgn;
    private double pcom_pgni;
    private double pcom_vpartida;
    private String pcom_estado;

    private String pcom_unidadetributavel;
    private double pcom_qtdtributavel;

    public ProdutoCombustivel() {
    }

    public ProdutoCombustivel(String pcom_codint, String pcom_descricao_anp, String pcom_codigo_anp, String pcom_codif, double pcom_pglp, double pcom_pgn, double pcom_pgni, double pcom_vpartida, String pcom_estado, String pcom_unidadetributavel, double pcom_qtdtributavel) {
        this.pcom_codint = pcom_codint;
        this.pcom_descricao_anp = pcom_descricao_anp;
        this.pcom_codigo_anp = pcom_codigo_anp;
        this.pcom_codif = pcom_codif;
        this.pcom_pglp = pcom_pglp;
        this.pcom_pgn = pcom_pgn;
        this.pcom_pgni = pcom_pgni;
        this.pcom_vpartida = pcom_vpartida;
        this.pcom_estado = pcom_estado;
        this.pcom_unidadetributavel = pcom_unidadetributavel;
        this.pcom_qtdtributavel = pcom_qtdtributavel;
    }

    public String getPcom_codint() {
        return pcom_codint;
    }

    public void setPcom_codint(String pcom_codint) {
        this.pcom_codint = pcom_codint;
    }

    public String getPcom_descricao_anp() {
        return pcom_descricao_anp;
    }

    public void setPcom_descricao_anp(String pcom_descricao_anp) {
        this.pcom_descricao_anp = pcom_descricao_anp;
    }

    public String getPcom_codigo_anp() {
        return pcom_codigo_anp;
    }

    public void setPcom_codigo_anp(String pcom_codigo_anp) {
        this.pcom_codigo_anp = pcom_codigo_anp;
    }

    public String getPcom_codif() {
        return pcom_codif;
    }

    public void setPcom_codif(String pcom_codif) {
        this.pcom_codif = pcom_codif;
    }

    public double getPcom_pglp() {
        return pcom_pglp;
    }

    public void setPcom_pglp(double pcom_pglp) {
        this.pcom_pglp = pcom_pglp;
    }

    public double getPcom_pgn() {
        return pcom_pgn;
    }

    public void setPcom_pgn(double pcom_pgn) {
        this.pcom_pgn = pcom_pgn;
    }

    public double getPcom_pgni() {
        return pcom_pgni;
    }

    public void setPcom_pgni(double pcom_pgni) {
        this.pcom_pgni = pcom_pgni;
    }

    public double getPcom_vpartida() {
        return pcom_vpartida;
    }

    public void setPcom_vpartida(double pcom_vpartida) {
        this.pcom_vpartida = pcom_vpartida;
    }

    public String getPcom_estado() {
        return pcom_estado;
    }

    public void setPcom_estado(String pcom_estado) {
        this.pcom_estado = pcom_estado;
    }

    public String getPcom_unidadetributavel() {
        return pcom_unidadetributavel;
    }

    public void setPcom_unidadetributavel(String pcom_unidadetributavel) {
        this.pcom_unidadetributavel = pcom_unidadetributavel;
    }

    public double getPcom_qtdtributavel() {
        return pcom_qtdtributavel;
    }

    public void setPcom_qtdtributavel(double pcom_qtdtributavel) {
        this.pcom_qtdtributavel = pcom_qtdtributavel;
    }

}
