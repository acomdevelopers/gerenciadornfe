package acom.gerenciadornfe.modelo.entidades;

import java.io.Serializable;

/**
 * @author allison - 30/04/2018
 */
public class Veiculo implements Serializable {

    private int vei_codigo;
    private String vei_placa;
    private String vei_renavam;
    private int vei_tara;
    private long vei_capkg;
    private long vei_capm3;
    private String vei_tprod;
    private String vei_tpcar;
    private String vei_uf;

    public Veiculo() {
    }

    public Veiculo(int vei_codigo, String vei_placa, String vei_renavam, int vei_tara, long vei_capkg, long vei_capm3, String vei_tprod, String vei_tpcar, String vei_uf) {
        this.vei_codigo = vei_codigo;
        this.vei_placa = vei_placa;
        this.vei_renavam = vei_renavam;
        this.vei_tara = vei_tara;
        this.vei_capkg = vei_capkg;
        this.vei_capm3 = vei_capm3;
        this.vei_tprod = vei_tprod;
        this.vei_tpcar = vei_tpcar;
        this.vei_uf = vei_uf;
    }

    public int getVei_codigo() {
        return vei_codigo;
    }

    public void setVei_codigo(int vei_codigo) {
        this.vei_codigo = vei_codigo;
    }

    public String getVei_placa() {
        return vei_placa;
    }

    public void setVei_placa(String vei_placa) {
        this.vei_placa = vei_placa;
    }

    public String getVei_renavam() {
        return vei_renavam;
    }

    public void setVei_renavam(String vei_renavam) {
        this.vei_renavam = vei_renavam;
    }

    public int getVei_tara() {
        return vei_tara;
    }

    public void setVei_tara(int vei_tara) {
        this.vei_tara = vei_tara;
    }

    public long getVei_capkg() {
        return vei_capkg;
    }

    public void setVei_capkg(long vei_capkg) {
        this.vei_capkg = vei_capkg;
    }

    public long getVei_capm3() {
        return vei_capm3;
    }

    public void setVei_capm3(long vei_capm3) {
        this.vei_capm3 = vei_capm3;
    }

    public String getVei_tprod() {
        return vei_tprod;
    }

    public void setVei_tprod(String vei_tprod) {
        this.vei_tprod = vei_tprod;
    }

    public String getVei_tpcar() {
        return vei_tpcar;
    }

    public void setVei_tpcar(String vei_tpcar) {
        this.vei_tpcar = vei_tpcar;
    }

    public String getVei_uf() {
        return vei_uf;
    }

    public void setVei_uf(String vei_uf) {
        this.vei_uf = vei_uf;
    }

}
