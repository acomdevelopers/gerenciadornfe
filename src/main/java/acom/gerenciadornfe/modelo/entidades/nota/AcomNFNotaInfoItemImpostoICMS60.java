/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.entidades.nota;

import com.fincatto.documentofiscal.DFBase;
import com.fincatto.documentofiscal.nfe400.classes.nota.NFNotaInfoItemImpostoICMS60;
import org.simpleframework.xml.Element;

import java.math.BigDecimal;

public class AcomNFNotaInfoItemImpostoICMS60 extends NFNotaInfoItemImpostoICMS60 {

    @Element(name = "vICMSSubstituto", required = false)
    private String valorICMSSubstituto;

    public void setValorICMSSubstituto(final BigDecimal valorICMSSubstituto) {
        this.valorICMSSubstituto = AcomBigDecimalValidador.tamanho15Com2CasasDecimais(valorICMSSubstituto, "Valor ICMS Substituto ICMS60 Item");
    }

}
