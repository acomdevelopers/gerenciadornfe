/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.enuns;

import javafx.scene.image.Image;

/**
 *
 * @author allison
 */
public enum EnumImagens {
    
    
    /*imagens das notificações*/
    
    ICON_SUCCESS("icon_success", new Image("/image/32/Accept.png")),
    ICON_ERROR("icon_error", new Image("/image/32/delete1.png")),
    ICON_INFO("icon_info", new Image("/image/32/information.png")),
    ICON_WARNING("icon_warning", new Image("/image/32/alert.png")),
    
    ICON_CADASTROS("cadastros", new Image("/image/24/data-add.png")),
    ICON_CAD_VEICULOS("cad_veiculos", new Image("/image/24/truck.png")),
    ICON_NFE("nfe", new Image("/image/32/icons8-nota-fiscal-electrónica-32.png")),
    ICON_GERENCIADOR("gerenciador", new Image("/image/24/application.png")),
    ICON_MDF("mdf", new Image("/image/32/icons8-mdf-32.png")),
    ICON_CC("carta_correcao", new Image("/image/32/icons8-cc-32.png")),
    ICON_INUTILIZACAO("inutilizacao", new Image("/image/32/icons8-inu-32.png")),
    ICON_CONFIG("config", new Image("/image/24/setting.png")),
    ICON_CERTIFICADO("gerar_cadeira", new Image("/image/32/icons8-certificado.png")),
    ICON_EMPRESA("empresa", new Image("/image/24/building.png")),
    
    ICON_MANIFESTO_DESTINATARIO("manifesto_destinatario", new Image("/image/24/Tests.png")),
    
    ICON_STATUS_SERVICO("status_servico", new Image("/image/24/globe-service.png")),
    ICON_EMAILS("emails", new Image("/image/24/email.png"));

    private String nome;
    private Image icon;
    
    private EnumImagens(String nome, Image icon) {
        this.nome = nome;
        this.icon = icon;
    }

    public String getNome() {
        return nome;
    }

    public Image getIcon() {
        return icon;
    }
}
