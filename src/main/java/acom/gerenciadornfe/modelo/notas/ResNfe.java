package acom.gerenciadornfe.modelo.notas;

import com.fincatto.documentofiscal.DFBase;
import java.sql.Timestamp;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.Root;

/**
 * @author Allison
 */
@Root(name = "resNFe")
@Namespace(reference = "http://www.portalfiscal.inf.br/nfe")
public class ResNfe extends DFBase {

    @Attribute(name = "versao")
    private String versao;

    @Element(name = "chNFe")
    private String chNFe;

    @Element(name = "CNPJ")
    private String cnpj;

    @Element(name = "xNome")
    private String xNome;

    @Element(name = "IE")
    private String ie;

    @Element(name = "dhEmi")
    private String dhEmi;

    @Element(name = "tpNF")
    private String tpNF;

    @Element(name = "vNF")
    private double vNF;

    @Element(name = "digVal")
    private String digVal;

    @Element(name = "dhRecbto")
    private String dhRecbto;

    @Element(name = "nProt")
    private String nProt;

    @Element(name = "cSitNFe")
    private String cSitNFe;

    public String getVersao() {
	return versao;
    }

    public void setVersao(String versao) {
	this.versao = versao;
    }

    public String getChNFe() {
	return chNFe;
    }

    public void setChNFe(String chNFe) {
	this.chNFe = chNFe;
    }

    public String getCnpj() {
	return cnpj;
    }

    public void setCnpj(String cnpj) {
	this.cnpj = cnpj;
    }

    public String getxNome() {
	return xNome;
    }

    public void setxNome(String xNome) {
	this.xNome = xNome;
    }

    public String getIe() {
	return ie;
    }

    public void setIe(String ie) {
	this.ie = ie;
    }

    public String getDhEmi() {
	return dhEmi;
    }

    public void setDhEmi(String dhEmi) {
	this.dhEmi = dhEmi;
    }

    public String getTpNF() {
	return tpNF;
    }

    public void setTpNF(String tpNF) {
	this.tpNF = tpNF;
    }

    public double getvNF() {
	return vNF;
    }

    public void setvNF(double vNF) {
	this.vNF = vNF;
    }

    public String getDigVal() {
	return digVal;
    }

    public void setDigVal(String digVal) {
	this.digVal = digVal;
    }

    public String getDhRecbto() {
	return dhRecbto;
    }

    public void setDhRecbto(String dhRecbto) {
	this.dhRecbto = dhRecbto;
    }

    public String getnProt() {
	return nProt;
    }

    public void setnProt(String nProt) {
	this.nProt = nProt;
    }

    public String getcSitNFe() {
	return cSitNFe;
    }

    public void setcSitNFe(String cSitNFe) {
	this.cSitNFe = cSitNFe;
    }

}
