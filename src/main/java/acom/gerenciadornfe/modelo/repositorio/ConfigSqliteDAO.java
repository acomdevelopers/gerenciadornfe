/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqLite;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class ConfigSqliteDAO implements Serializable {

    private ConexaoSqLite conexao = ConexaoSqLite.getInstance();
    private Connection connection = conexao.getConexao();

    public void criarTabelaSqlite() {
	String sql = "CREATE TABLE IF NOT EXISTS parametros (par_nome STRING (255) PRIMARY KEY DEFAULT vazio NOT NULL, par_descricao STRING (255) DEFAULT vazio NOT NULL, par_valor STRING (255) DEFAULT vazio NOT NULL);";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.execute();
	} catch (SQLException ex) {
	    Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public String verificarParFilial() {
	String sql = "SELECT par_valor FROM parametros where par_nome = 'par_filial' and par_valor <> '=';";
	String retorno = "";
	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    ps = connection.prepareStatement(sql);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		retorno = rs.getString("par_valor");
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }
    
    public void inserirHost() {
	String sql = "INSERT INTO parametros (par_nome, par_descricao, par_valor) VALUES (\"par_host\", \"host\", \"\");\n";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.execute();
	} catch (SQLException ex) {
	    Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public void inserirBanco() {
	String sql = "INSERT INTO parametros (par_nome, par_descricao, par_valor) VALUES (\"par_banco\", \"banco de dados\", \"\");";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.execute();
	} catch (SQLException ex) {
	    Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public void inserirPorta() {
	String sql = "INSERT INTO parametros (par_nome, par_descricao, par_valor) VALUES (\"par_porta\", \"porta (padrao 1433)\", \"\");\n";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.execute();
	} catch (SQLException ex) {
	    Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public void inserirInstancia() {
	String sql = "INSERT INTO parametros (par_nome, par_descricao, par_valor) VALUES (\"par_instancia\", \"Instancia\", \"\");\n";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.execute();
	} catch (SQLException ex) {
	    Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public void inserirDiretorioProjeto() {
	String sql = "INSERT INTO parametros (par_nome, par_descricao, par_valor) VALUES (\"par_diretorio_projeto\", \"diretorio onde o projeto esta instalado\", \"\");\n";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.execute();
	} catch (SQLException ex) {
	    Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public void inserirParAlias() {
	String sql = "INSERT INTO parametros (par_nome, par_descricao, par_valor) VALUES (\"par_alias_certificado\", \"alias do certificado\", \"\");\n";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.execute();
	} catch (SQLException ex) {
	    Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public void inserirParFilial() {
	String sql = "INSERT INTO parametros (par_nome, par_descricao, par_valor) VALUES (\"par_filial\", \"Filial selecionada\", \"\");\n";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.execute();
	} catch (SQLException ex) {
	    Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }
    
    public void apagarParametrosAntigos() {//campos de filial e contingencia que n serao mais usados no sqlite
	String sql = "DELETE FROM parametros WHERE par_nome = 'par_nome_certificado' OR par_nome = 'par_senha_certificado'  OR par_nome = 'par_tipo_certificado' OR  par_nome = 'par_contigencia';";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.execute();
	} catch (SQLException ex) {
	    Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public boolean consultaBanco() {
	String sql = "SELECT * FROM parametros;";
	PreparedStatement ps = null;
	ResultSet rs = null;
	boolean retorno = false;
	try {
	    ps = connection.prepareStatement(sql);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		retorno = true;
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ConfigSqliteDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }

}
