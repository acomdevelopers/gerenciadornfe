package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.Filial;
import acom.gerenciadornfe.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class FilialDAO {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public List<Filial> listar() {
	String sql = "SELECT fil_cod, fil_nreduz FROM filiais;";
	PreparedStatement ps = null;
	ResultSet rs = null;
	List<Filial> retorno = new ArrayList<>();
	Filial filial = null;

	try {
	    ps = connection.prepareStatement(sql);
	    rs = ps.executeQuery();
	    while (rs.next()) {
		filial = new Filial();
		filial.setFil_cod(rs.getString("fil_cod"));
		filial.setFil_nreduz(rs.getString("fil_nreduz"));
		retorno.add(filial);
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }

    public List<Filial> listarFilialResumido() {
	String sql = "SELECT fil_cod, fil_nreduz, fil_cnpj FROM filiais;";
	PreparedStatement ps = null;
	ResultSet rs = null;
	List<Filial> retorno = new ArrayList<>();
	Filial filial = null;

	try {
	    ps = connection.prepareStatement(sql);
	    rs = ps.executeQuery();
	    while (rs.next()) {
		filial = new Filial();
		filial.setFil_cod(rs.getString("fil_cod"));
		filial.setFil_nreduz(rs.getString("fil_nreduz"));
		filial.setFil_cnpj(rs.getString("Fil_cnpj"));
		retorno.add(filial);
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }
    
    public Filial getFilial(String filial) {
	String sql = "SELECT fil_cod, fil_nreduz FROM filiais where fil_cod = ?;";
	PreparedStatement ps = null;
	ResultSet rs = null;
	Filial retorno = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setString(1, filial);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		retorno = new Filial();
		retorno.setFil_cod(rs.getString("fil_cod"));
		retorno.setFil_nreduz(rs.getString("fil_nreduz"));
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		System.out.println(ps.toString());
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }

    public String getCnpj(String filialCod) {
	String sql = "select fil_cnpj from filiais where fil_cod = ?";
	PreparedStatement ps = null;
	ResultSet rs = null;
	String retorno = "";
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setString(1, filialCod);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		retorno = Util.getNumeros(rs.getString("fil_cnpj"));
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		System.out.println(ps.toString());
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }

    public String getCodUf() {
	String sql = "select fil_cod_municipio from filiais;";
	PreparedStatement ps = null;
	ResultSet rs = null;
	String retorno = "";
	try {
	    ps = connection.prepareStatement(sql);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		retorno = rs.getString("fil_cod_municipio").substring(0, 2);
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		System.out.println(ps.toString());
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }

    public String getCodFilialTop1() {
	String sql = "select top 1 fil_cod from filiais;";
	PreparedStatement ps = null;
	ResultSet rs = null;
	String retorno = "";
	try {
	    ps = connection.prepareStatement(sql);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		retorno = rs.getString("fil_cod").substring(0, 2);
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		System.out.println(ps.toString());
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }
    
    public Filial getDadosFilial(String fil_cod) {
	String sql = "SELECT * FROM filiais WHERE Filiais.Fil_cod = ?;";
	PreparedStatement ps = null;
	ResultSet rs = null;
	Filial filial = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setString(1, fil_cod);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		filial = new Filial();
		filial.setFil_cod(rs.getString("fil_cod"));
		filial.setFil_nome(rs.getString("fil_nome"));
		filial.setFil_nreduz(rs.getString("fil_nreduz"));
		filial.setFil_endereço(rs.getString("fil_endereço"));
		filial.setFil_numero(rs.getString("fil_numero"));
		filial.setFil_compl(rs.getString("fil_compl"));
		filial.setFil_bairro(rs.getString("fil_bairro"));
		filial.setFil_cep(rs.getString("fil_cep"));
		filial.setFil_cidade(rs.getString("fil_cidade"));
		filial.setFil_estado(rs.getString("fil_estado"));
		filial.setFil_cnpj(rs.getString("fil_cnpj"));
		filial.setFil_insc(rs.getString("fil_insc"));
		filial.setFil_fone(rs.getString("fil_fone"));
		filial.setFil_fax(rs.getString("fil_fax"));
		filial.setFil_contato(rs.getString("fil_contato"));
		filial.setFil_foncontato(rs.getString("fil_foncontato"));
		filial.setFil_cxpostal(rs.getString("fil_cxpostal"));
		filial.setFil_email(rs.getString("fil_email"));
		filial.setFil_socio(rs.getString("fil_socio"));
		filial.setFil_cpf(rs.getString("fil_cpf"));
		filial.setFil_emailsocio(rs.getString("fil_emailsocio"));
		filial.setFil_contador(rs.getString("fil_contador"));
		filial.setFil_cpfcont(rs.getString("fil_cpfcont"));
		filial.setFil_crc(rs.getString("fil_crc"));
		filial.setFil_fonecont(rs.getString("fil_fonecont"));
		filial.setFil_cxpostcont(rs.getString("fil_cxpostcont"));
		filial.setFil_emailcont(rs.getString("fil_emailcont"));
		filial.setFil_serienf(rs.getString("fil_serienf"));
		filial.setFil_nrnota(rs.getLong("fil_nrnota"));
		filial.setFil_nrnota_nfe_2(rs.getLong("fil_nrnota_nfe_2"));
		filial.setFil_serieselo(rs.getString("fil_serieselo"));
		filial.setFil_nrselo(rs.getLong("fil_nrselo"));
		filial.setFil_serie_nfe_entrada(rs.getString("fil_serie_nfe_entrada"));
		filial.setFil_nrnota_nfe_entrada(rs.getLong("fil_nrnota_nfe_entrada"));
		filial.setFil_almoxarifado_venda(rs.getString("fil_almoxarifado_venda"));
		filial.setFil_almoxarifado_nota(rs.getString("fil_almoxarifado_nota"));
		filial.setFil_clifor(rs.getString("fil_clifor"));
		filial.setIncluido(rs.getString("incluido"));
		filial.setDtinc(rs.getTimestamp("dtinc"));
		filial.setHora(rs.getTimestamp("hora"));
		filial.setAlterado(rs.getString("alterado"));
		filial.setDtalt(rs.getTimestamp("dtalt"));
		filial.setHoraalt(rs.getTimestamp("horaalt"));
		filial.setCtaent(rs.getString("ctaent"));
		filial.setCtasai(rs.getString("ctasai"));
		filial.setFilial(rs.getString("filial"));
		filial.setFil_cnae(rs.getString("fil_cnae"));
		filial.setFil_destaca_substituicao(rs.getInt("fil_destaca_substituicao"));
		filial.setFil_obs_padrao(rs.getString("fil_obs_padrao"));
		filial.setFil_atividade(rs.getString("fil_atividade"));
		filial.setFil_tipo_substituicao(rs.getString("fil_tipo_substituicao"));
		filial.setFil_crt(rs.getString("fil_crt"));
		filial.setFil_crt_sn(rs.getString("fil_crt_sn"));
		filial.setFil_pcredsn(rs.getDouble("fil_pcredsn"));
		filial.setFil_certificado(rs.getString("fil_certificado"));
		filial.setFil_certificado_senha(rs.getString("fil_certificado_senha"));
		filial.setFil_certificado_serie(rs.getString("fil_certificado_serie"));
		filial.setFil_forma_tributacao(rs.getString("fil_forma_tributacao"));
		filial.setFil_insc_mun(rs.getString("fil_insc_mun"));
		filial.setFil_suframa(rs.getString("fil_suframa"));
		filial.setFil_natureza(rs.getString("fil_natureza"));
		filial.setFil_tipo_atividade(rs.getString("fil_tipo_atividade"));
		filial.setFil_cnpj_cont(rs.getString("fil_cnpj_cont"));
		filial.setFil_end_cont(rs.getString("fil_end_cont"));
		filial.setFil_num_cont(rs.getString("fil_num_cont"));
		filial.setFil_cep_cont(rs.getString("fil_cep_cont"));
		filial.setFil_cidade_cont(rs.getString("fil_cidade_cont"));
		filial.setFil_bairro_cont(rs.getString("fil_bairro_cont"));
		filial.setFil_estado_cont(rs.getString("fil_estado_cont"));
		filial.setFil_servidor_smtp(rs.getString("fil_servidor_smtp"));
		filial.setFil_usuario_autenticacao(rs.getString("fil_usuario_autenticacao"));
		filial.setFil_senha_autenticacao(rs.getString("fil_senha_autenticacao"));
		filial.setFil_assunto_email(rs.getString("fil_assunto_email"));
		filial.setFil_corpo_email(rs.getString("fil_corpo_email"));
		filial.setFil_email_porta(rs.getInt("fil_email_porta"));
		filial.setFil_email_autenticacao(rs.getBoolean("fil_email_autenticacao"));
		filial.setFil_nire(rs.getString("fil_nire"));
		filial.setFil_cod_municipio(rs.getInt("fil_cod_municipio"));
		filial.setFil_cod_mun_cont(rs.getInt("fil_cod_mun_cont"));
		filial.setFil_qualificacao_assinante(rs.getString("fil_qualificacao_assinante"));
		filial.setFil_aliquota_pis(rs.getDouble("fil_aliquota_pis"));
		filial.setFil_aliquota_cofins(rs.getDouble("fil_aliquota_cofins"));
		filial.setFil_destaca_ipi(rs.getInt("fil_destaca_ipi"));
		filial.setFil_tipo_destaque_ipi(rs.getString("fil_tipo_destaque_ipi"));
		filial.setFil_cfop_vendas_tributadas_ecf(rs.getString("fil_cfop_vendas_tributadas_ecf"));
		filial.setFil_cfop_vendas_subst_ecf(rs.getString("fil_cfop_vendas_subst_ecf"));
		filial.setFil_cfop_vendas_canceladas_ecf(rs.getString("fil_cfop_vendas_canceladas_ecf"));
		filial.setFil_beneficiario_prodepe(rs.getString("fil_beneficiario_prodepe"));
		filial.setFil_enquadramento_beneficio(rs.getString("fil_enquadramento_beneficio"));
		filial.setFil_nr_decreto_beneficio(rs.getInt("fil_nr_decreto_beneficio"));
		filial.setFil_dt_decreto(rs.getTimestamp("fil_dt_decreto"));
		filial.setFil_natureza_beneficio(rs.getString("fil_natureza_beneficio"));
		filial.setFil_ind_cobranca_icms(rs.getString("fil_ind_cobranca_icms"));
		filial.setFil_tipo_ligacao_energia(rs.getString("fil_tipo_ligacao_energia"));
		filial.setFil_grupo_tensao(rs.getString("fil_grupo_tensao"));
		filial.setFil_data_certificado(rs.getTimestamp("fil_data_certificado"));
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return filial;
    }

    public boolean inserirNomeCertificado(String nomeCertificado, String senha, String serie, String fil_cod) {
	String sql = "UPDATE Filiais SET Fil_certificado = ?, Fil_certificado_senha = ?, Fil_certificado_serie = ? WHERE Fil_cod = ?;";
	PreparedStatement ps = null;
	boolean retorno = false;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setString(1, nomeCertificado);
	    ps.setString(2, senha);
	    ps.setString(3, serie);
	    ps.setString(4, fil_cod);
	    ps.execute();
	    retorno = true;
	} catch (SQLException ex) {
	    retorno = false;
	    Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }

}
