/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.Inutilizacao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class InutilizacaoDAO {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public boolean inserir(Inutilizacao inutilizacao) {
        String sql = "INSERT INTO Inutilizacao (Inu_id, Inu_ano, Inu_numeronotainicial, Inu_numeronotafinal, Inu_justificativa, Inu_enviada, Inu_serie, Inu_numeroprotocolo, Inu_datahora, Inu_filial) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        boolean retorno = false;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, inutilizacao.getInu_id());
            ps.setString(2, inutilizacao.getInu_ano());
            ps.setInt(3, inutilizacao.getInu_numeronotainicial());
            ps.setInt(4, inutilizacao.getInu_numeronotafinal());
            ps.setString(5, inutilizacao.getInu_justificativa());
            ps.setString(6, inutilizacao.getInu_enviada());
            ps.setString(7, inutilizacao.getInu_serie());
            ps.setString(8, inutilizacao.getInu_numeroprotocolo());
            ps.setString(9, inutilizacao.getInu_datahora());
            ps.setString(10, inutilizacao.getInu_filial());
            ps.execute();
            retorno = true;
        } catch (SQLException ex) {
            Logger.getLogger(InutilizacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(InutilizacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public boolean alterar(Inutilizacao inutilizacao) {
        String sql = "UPDATE Inutilizacao SET Inu_ano = ?, Inu_numeronotainicial = ?, Inu_numeronotafinal = ?, Inu_justificativa = ?, Inu_enviada = ?, Inu_serie = ?, Inu_numeroprotocolo = ?, Inu_datahora = ?, Inu_filial = ? WHERE Inu_id = ?;";
        boolean retorno = false;
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, inutilizacao.getInu_ano());
            ps.setInt(2, inutilizacao.getInu_numeronotainicial());
            ps.setInt(3, inutilizacao.getInu_numeronotafinal());
            ps.setString(4, inutilizacao.getInu_justificativa());
            ps.setString(5, inutilizacao.getInu_enviada());
            ps.setString(6, inutilizacao.getInu_serie());
            ps.setString(7, inutilizacao.getInu_numeroprotocolo());
            ps.setString(8, inutilizacao.getInu_datahora());
            ps.setString(9, inutilizacao.getInu_filial());
            ps.setInt(10, inutilizacao.getInu_id());
            ps.execute();
            retorno = true;
        } catch (SQLException ex) {
            Logger.getLogger(InutilizacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                System.out.println(ps.toString());
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(InutilizacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public List<Inutilizacao> listar() {
        String sql = "SELECT * FROM Inutilizacao ORDER BY Inu_id ASC";
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Inutilizacao> retorno = new ArrayList<>();
        Inutilizacao inutilizacao = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                inutilizacao = new Inutilizacao();
                inutilizacao.setInu_id(rs.getInt("inu_id"));
                inutilizacao.setInu_ano(rs.getString("inu_ano"));
                inutilizacao.setInu_numeronotainicial(rs.getInt("inu_numeronotainicial"));
                inutilizacao.setInu_numeronotafinal(rs.getInt("inu_numeronotainicial"));
                inutilizacao.setInu_justificativa(rs.getString("inu_justificativa"));
                inutilizacao.setInu_enviada(rs.getString("inu_enviada"));
                inutilizacao.setInu_serie(rs.getString("inu_serie"));
                inutilizacao.setInu_numeroprotocolo(rs.getString("inu_numeroprotocolo"));
                inutilizacao.setInu_datahora(rs.getString("inu_datahora"));
                inutilizacao.setInu_filial(rs.getString("inu_filial"));
                retorno.add(inutilizacao);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InutilizacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(InutilizacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
            return retorno;
        }

    }

    public int ultimoId() {
        String sql = "select max(inu_id) as inu_id from inutilizacao;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        int retorno = 0;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getInt("inu_id") + 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(InutilizacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

    public boolean excluir(Inutilizacao inu) {
        String sql = "delete from inutilizacao where inu_id = ?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, inu.getInu_id());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Erro ao executar delete: " + ps.toString());
            return false;
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(InutilizacaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
