/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqLite;
import acom.gerenciadornfe.modelo.entidades.NFDestinatario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Allison
 */
public class NFDestinatarioDAO {

    private ConexaoSqLite conexao = ConexaoSqLite.getInstance();
    private Connection connection = conexao.getConexao();

    public boolean inserir(NFDestinatario nfd) {
	String sql = "INSERT INTO nota_fiscal (nf_id, emp_id, nf_serie, nf_numero, nf_cnpj, nf_nome, nf_emissao, nf_valor, nf_tipo, nf_situacao, nf_chave, nf_ie, nf_protocolo, nf_data_recibo, nf_manifestacao, nf_download, nf_ambiente, nf_nsu, nf_visivel) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setInt(1, nfd.getNf_id());
	    ps.setInt(2, nfd.getEmp_id());
	    ps.setInt(3, nfd.getNf_serie());
	    ps.setInt(4, nfd.getNf_numero());
	    ps.setString(5, nfd.getNf_cnpj());
	    ps.setString(6, nfd.getNf_nome());
	    ps.setTimestamp(7, nfd.getNf_emissao());
	    ps.setDouble(8, nfd.getNf_valor());
	    ps.setString(9, nfd.getNf_tipo());
	    ps.setString(10, nfd.getNf_situacao());
	    ps.setString(11, nfd.getNf_chave());
	    ps.setString(12, nfd.getNf_ie());
	    ps.setString(13, nfd.getNf_protocolo());
	    ps.setTimestamp(14, nfd.getNf_data_recibo());
	    ps.setString(15, nfd.getNf_manifestacao());
	    ps.setString(16, nfd.getNf_download());
	    ps.setString(17, nfd.getNf_ambiente());
	    ps.setString(18, nfd.getNf_nsu());
	    ps.setString(19, nfd.getNf_visivel());
	    ps.execute();
	    return true;
	} catch (SQLException ex) {
	    Logger.getLogger(NFDestinatarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	    return false;
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(NFDestinatarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public boolean alterar(NFDestinatario nfd) {
	String sql = "UPDATE nota_fiscal SET nf_cnpj = ?, nf_nome = ?, nf_emissao = ?, nf_valor = ?, nf_tipo = ?, nf_situacao = ?, nf_chave = ?, nf_ie = ?, nf_protocolo = ?, nf_data_recibo = ?, nf_manifestacao = ?, nf_download = ?, nf_ambiente = ?, nf_nsu = ?, nf_visivel = ? WHERE nf_id = ? AND  emp_id = ? AND  nf_serie = ? AND  nf_numero = ?;";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setString(1, nfd.getNf_cnpj());
	    ps.setString(2, nfd.getNf_nome());
	    ps.setTimestamp(3, nfd.getNf_emissao());
	    ps.setDouble(4, nfd.getNf_valor());
	    ps.setString(5, nfd.getNf_tipo());
	    ps.setString(6, nfd.getNf_situacao());
	    ps.setString(7, nfd.getNf_chave());
	    ps.setString(8, nfd.getNf_ie());
	    ps.setString(9, nfd.getNf_protocolo());
	    ps.setTimestamp(10, nfd.getNf_data_recibo());
	    ps.setString(11, nfd.getNf_manifestacao());
	    ps.setString(12, nfd.getNf_download());
	    ps.setString(13, nfd.getNf_ambiente());
	    ps.setString(14, nfd.getNf_nsu());
	    ps.setString(15, nfd.getNf_visivel());
	    ps.setInt(16, nfd.getNf_id());
	    ps.setInt(17, nfd.getEmp_id());
	    ps.setInt(18, nfd.getNf_serie());
	    ps.setInt(19, nfd.getNf_numero());
	    ps.execute();
	    return true;
	} catch (SQLException ex) {
	    Logger.getLogger(NFDestinatarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	    return false;
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(NFDestinatarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public boolean excluir(NFDestinatario nfd) {
	String sql = "DELETE FROM nota_fiscal WHERE nf_id = ? AND  emp_id = ? AND  nf_serie = ? AND  nf_numero = ?;";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setInt(1, nfd.getNf_id());
	    ps.setInt(2, nfd.getEmp_id());
	    ps.setInt(3, nfd.getNf_serie());
	    ps.setInt(4, nfd.getNf_numero());
	    ps.execute();
	    return true;
	} catch (SQLException ex) {
	    Logger.getLogger(NFDestinatarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	    return false;
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(NFDestinatarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public List<NFDestinatario> listar() {
	String sql = "";
	List<NFDestinatario> retorno = new ArrayList<>();
	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    ps = connection.prepareStatement(sql);
	    //condicao
	    rs = ps.executeQuery();
	    while (rs.next()) {
		NFDestinatario nfd = new NFDestinatario();
		nfd.setNf_id(rs.getInt("nf_id"));
		nfd.setEmp_id(rs.getInt("emp_id"));
		nfd.setNf_serie(rs.getInt("nf_serie"));
		nfd.setNf_numero(rs.getInt("nf_numero"));
		nfd.setNf_cnpj(rs.getString("nf_cnpj"));
		nfd.setNf_nome(rs.getString("nf_nome"));
		nfd.setNf_emissao(rs.getTimestamp("nf_emissao"));
		nfd.setNf_valor(rs.getDouble("nf_valor"));
		nfd.setNf_tipo(rs.getString("nf_tipo"));
		nfd.setNf_situacao(rs.getString("nf_situacao"));
		nfd.setNf_chave(rs.getString("nf_chave"));
		nfd.setNf_ie(rs.getString("nf_ie"));
		nfd.setNf_protocolo(rs.getString("nf_protocolo"));
		nfd.setNf_data_recibo(rs.getTimestamp("nf_data_recibo"));
		nfd.setNf_manifestacao(rs.getString("nf_manifestacao"));
		nfd.setNf_download(rs.getString("nf_download"));
		nfd.setNf_ambiente(rs.getString("nf_ambiente"));
		nfd.setNf_nsu(rs.getString("nf_nsu"));
		nfd.setNf_visivel(rs.getString("nf_visivel"));
		retorno.add(nfd);
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(NFDestinatarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(NFDestinatarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }

    public int getMaxNsu() {
	String sql = "select MAX(nf_nsu) from nota_fiscal;";
	PreparedStatement ps = null;
	ResultSet rs = null;
	int retorno = 0;
	try {
	    ps = connection.prepareStatement(sql);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		System.out.println("entrou if rs.next()");
		
		Integer v = rs.getInt("nf_nsu");
		if (v > 0) {
		    retorno = v;
		} else {
		    retorno = 0;
		}
	    }else{
		retorno = 0;
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(NFDestinatarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(NFDestinatarioDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }

}
