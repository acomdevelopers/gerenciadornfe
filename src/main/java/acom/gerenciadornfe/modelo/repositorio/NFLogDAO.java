package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqLite;
import acom.gerenciadornfe.modelo.entidades.NFLog;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Allison
 */
public class NFLogDAO {

    private ConexaoSqLite conexao = ConexaoSqLite.getInstance();
    private Connection connection = conexao.getConexao();

    public boolean inserir(NFLog log) {
	String sql = "INSERT INTO nota_fiscal_log (nflog_id, nflog_id_nf, nflog_operacao, nflog_data_hora, nflog_historico) VALUES (?, ?, ?, ?, ?);";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setInt(1, log.getNflog_id());
	    ps.setInt(2, log.getNflog_id_nf());
	    ps.setInt(3, log.getNflog_operacao());
	    ps.setTimestamp(4, log.getNflog_data_hora());
	    ps.setString(5, log.getNflog_historico());
	    ps.execute();
	    return true;
	} catch (SQLException ex) {
	    Logger.getLogger(NFLogDAO.class.getName()).log(Level.SEVERE, null, ex);
	    return false;
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(NFLogDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public boolean alterar(NFLog log) {
	String sql = "UPDATE nota_fiscal_log SET nflog_operacao = ?, nflog_data_hora = ?, nflog_historico = ? WHERE nflog_id = ? AND  nflog_id_nf = ?;";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setInt(1, log.getNflog_operacao());
	    ps.setTimestamp(2, log.getNflog_data_hora());
	    ps.setString(3, log.getNflog_historico());
	    ps.setInt(4, log.getNflog_id());
	    ps.setInt(5, log.getNflog_id_nf());
	    ps.execute();
	    return true;
	} catch (SQLException ex) {
	    Logger.getLogger(NFLogDAO.class.getName()).log(Level.SEVERE, null, ex);
	    return false;
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(NFLogDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public boolean excluir(NFLog log) {
	String sql = "DELETE FROM nota_fiscal_log WHERE nflog_id_nf = ?;";
	PreparedStatement ps = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setInt(1, log.getNflog_id_nf());
	    ps.execute();
	    return true;
	} catch (SQLException ex) {
	    Logger.getLogger(NFLogDAO.class.getName()).log(Level.SEVERE, null, ex);
	    return false;
	} finally {
	    try {
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(NFLogDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
    }

    public List<NFLog> logNota(NFLog log) {
	String sql = "SELECT nflog_id, nflog_id_nf, nflog_operacao, nflog_data_hora, nflog_historico FROM nota_fiscal_log WHERE nflog_id_nf = ?;";
	List<NFLog> retorno = new ArrayList<>();
	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setInt(1, log.getNflog_id_nf());
	    rs = ps.executeQuery();
	    while (rs.next()) {
		NFLog novoLog = new NFLog();
		novoLog.setNflog_id(rs.getInt("nflog_id"));
		novoLog.setNflog_id_nf(rs.getInt("nflog_id_nf"));
		novoLog.setNflog_operacao(rs.getInt("nflog_operacao"));
		novoLog.setNflog_data_hora(rs.getTimestamp("nflog_data_hora"));
		novoLog.setNflog_historico(rs.getString("nflog_historico"));
		retorno.add(novoLog);
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(NFLogDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(NFLogDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }
}
