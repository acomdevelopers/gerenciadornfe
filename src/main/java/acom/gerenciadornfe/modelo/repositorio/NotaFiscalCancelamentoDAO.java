/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.NotaFiscalCancelamento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class NotaFiscalCancelamentoDAO {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public boolean inserir(NotaFiscalCancelamento nfc) {

        String sql = " INSERT INTO Nota_fiscal_cancelamento (Nfec_id ,Nfec_chaveacesso ,Nfec_seqevento ,Nfec_protocolo_nfe ,Nfec_justificativa ,Nfec_lote ,Nfec_cstat ,Nfec_motivo_resposta ,Nfec_descricao_resposta ,Nfec_data_hora ,Nfec_protocolo_evento ,Nfec_cnpj) VALUES (? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,?);";
        PreparedStatement ps = null;
        boolean nfcRetorno = false;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, nfc.getNfec_id());
            ps.setString(2, nfc.getNfec_chaveacesso());
            ps.setInt(3, nfc.getNfec_seqevento());
            ps.setLong(4, nfc.getNfec_protocolo_nfe());
            ps.setString(5, nfc.getNfec_justificativa());
            ps.setLong(6, nfc.getNfec_lote());
            ps.setInt(7, nfc.getNfec_cstat());
            ps.setString(8, nfc.getNfec_motivo_resposta());
            ps.setString(9, nfc.getNfec_descricao_resposta());
            ps.setString(10, nfc.getNfec_data_hora());
            ps.setLong(11, nfc.getNfec_protocolo_evento());
            ps.setString(12, nfc.getNfec_cnpj());
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalCancelamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalCancelamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return nfcRetorno;
    }

    public boolean alterar(NotaFiscalCancelamento nfc) {
        String sql = "UPDATE Nota_fiscal_cancelamento SET Nfec_chaveacesso = ?, Nfec_seqevento = ?, Nfec_protocolo_nfe = ?, Nfec_justificativa = ?, Nfec_lote = ?, Nfec_cstat = ?, Nfec_motivo_resposta = ?, Nfec_descricao_resposta = ?, Nfec_data_hora = ?, Nfec_protocolo_evento = ?, Nfec_cnpj = ? WHERE Nfec_id = ?;";
        PreparedStatement ps = null;
        boolean retornoNfc = false;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, nfc.getNfec_chaveacesso());
            ps.setInt(2, nfc.getNfec_seqevento());
            ps.setLong(3, nfc.getNfec_protocolo_nfe());
            ps.setString(4, nfc.getNfec_justificativa());
            ps.setLong(5, nfc.getNfec_lote());
            ps.setInt(6, nfc.getNfec_cstat());
            ps.setString(7, nfc.getNfec_motivo_resposta());
            ps.setString(8, nfc.getNfec_descricao_resposta());
            ps.setString(9, nfc.getNfec_data_hora());
            ps.setLong(10, nfc.getNfec_protocolo_evento());
            ps.setString(11, nfc.getNfec_cnpj());
            ps.setInt(12, nfc.getNfec_id());
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalCancelamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalCancelamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retornoNfc;
    }

    public int getMaxIdCancelamento() {
        String sql = "SELECT MAX (nfec_id) as maiorId FROM nota_fiscal_cancelamento;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        int retorno = 0;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getInt("maiorId");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalCancelamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalCancelamentoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

}
