/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.NotaFiscalCartaCorrecao;
import com.fincatto.documentofiscal.nfe400.classes.evento.cartacorrecao.NFProtocoloEventoCartaCorrecao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class NotaFiscalCartaCorrecaoDAO {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public boolean inserir(NotaFiscalCartaCorrecao cc) {
        String sql = "INSERT INTO Nota_fiscal_carta_correcao(Nfcc_chavenfe ,Nfcc_item ,Nfcc_data ,Nfcc_hora ,Nfcc_correcao ,Nfcc_enviada ,Nfcc_cnpj, Nfcc_orgao, NFcc_modelo, Nfcc_serie, Nfcc_numero_nota, filial) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        PreparedStatement ps = null;
        boolean retorno = false;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, cc.getNfcc_chavenfe());
            ps.setString(2, cc.getNfcc_item());
            ps.setTimestamp(3, cc.getNfcc_data());
            ps.setTimestamp(4, cc.getNfcc_hora());
            ps.setString(5, cc.getNfcc_correcao());
            ps.setString(6, cc.getNfcc_enviada());
            ps.setString(7, cc.getNfcc_cnpj());
	    ps.setString(8, cc.getNfcc_orgao());
	    ps.setString(9, cc.getNfcc_modelo());
	    ps.setString(10, cc.getNfcc_serie());
	    ps.setString(11, cc.getNfcc_numero_nota());
	    ps.setString(12, cc.getFilia());
            ps.execute();
            retorno = true;
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalCartaCorrecaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ps.toString());
            retorno = false;
        } finally {
            try {
                System.out.println(ps.toString());
                ps.close();
            } catch (SQLException ex) {
                System.out.println(ps.toString());
                Logger.getLogger(NotaFiscalCartaCorrecaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public boolean alterar(NotaFiscalCartaCorrecao cc) {
        String sql = "UPDATE Nota_fiscal_carta_correcao SET Nfcc_correcao = ? WHERE Nfcc_chavenfe = ? and Nfcc_item = ?;";
        PreparedStatement ps = null;
        boolean retorno = false;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, cc.getNfcc_correcao());
            ps.setString(2, cc.getNfcc_chavenfe());
            ps.setString(3, cc.getNfcc_item());
            ps.execute();
            retorno = true;
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalCartaCorrecaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            retorno = false;
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalCartaCorrecaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public boolean alterarDepoisEnvio(String enviada, String chave, String item, NFProtocoloEventoCartaCorrecao nfcc) {
        String sql = "UPDATE Nota_fiscal_carta_correcao SET Nfcc_protocolo = ?, Nfcc_enviada = ?, Nfcc_orgao = ?, Nfcc_retorno_sefaz = ?, Nfcc_numero_evento = ? WHERE Nfcc_chavenfe = ? and Nfcc_item = ?;";
        PreparedStatement ps = null;
        boolean retorno = false;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, nfcc.getEventoRetorno().getInfoEventoRetorno().getNumeroProtocolo()); //protocolo
            ps.setString(2, enviada); //enviada
	    ps.setString(3, nfcc.getEvento().getInfoEvento().getOrgao().getCodigo()); //orgao
	    ps.setString(4, String.valueOf(nfcc.getEventoRetorno().getInfoEventoRetorno().getCodigoStatus()) + " - " + nfcc.getEventoRetorno().getInfoEventoRetorno().getMotivo()); //msg sefaz
	    ps.setString(5, String.valueOf(nfcc.getEventoRetorno().getInfoEventoRetorno().getNumeroSequencialEvento())); //numero evento
	    ps.setString(6, chave); //chave
            ps.setString(7, item); //item
	    ps.execute();
            retorno = true;
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalCartaCorrecaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            retorno = false;
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalCartaCorrecaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
    
    public String getNumeroSequencial(String chaveNfe) {
        String sql = "SELECT MAX(Nota_fiscal_carta_correcao.nfcc_item) AS nfcc_item FROM Nota_Fiscal_carta_correcao WHERE Nfcc_chavenfe = ?;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
	    ps.setString(1, chaveNfe);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("nfcc_item");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalCartaCorrecaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            retorno = null;
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalCartaCorrecaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public List<NotaFiscalCartaCorrecao> listar(String parametro) {
//        String sql = "select Nota_fiscal_carta_correcao.*, Nota_fiscal.nf_nome from Nota_fiscal_carta_correcao inner join nota_fiscal on nota_fiscal.nfechaveacesso = nota_fiscal_carta_correcao.nfcc_chavenfe";
	  String sql = "select Nota_fiscal_carta_correcao.*, Nota_fiscal.nf_nome from Nota_fiscal_carta_correcao inner join nota_fiscal on nota_fiscal.nfechaveacesso = nota_fiscal_carta_correcao.nfcc_chavenfe where (Nota_fiscal_carta_correcao.Nfcc_chavenfe + Nota_fiscal.Nf_nome) like ? ;";
	PreparedStatement ps = null;
        ResultSet rs = null;
        List<NotaFiscalCartaCorrecao> retorno = new ArrayList<>();
        NotaFiscalCartaCorrecao nfcc;
        try {
            ps = connection.prepareStatement(sql);
	    ps.setString(1, parametro);
            rs = ps.executeQuery();
            while (rs.next()) {
                nfcc = new NotaFiscalCartaCorrecao();
                nfcc.setNfcc_chavenfe(rs.getString("nfcc_chavenfe"));
                nfcc.setNfcc_item(rs.getString("nfcc_item"));
                nfcc.setNfcc_data(rs.getTimestamp("nfcc_data"));
                nfcc.setNfcc_hora(rs.getTimestamp("nfcc_hora"));
                nfcc.setNfcc_correcao(rs.getString("nfcc_correcao"));
                nfcc.setNfcc_protocolo(rs.getString("nfcc_protocolo"));
                nfcc.setNfcc_enviada(rs.getString("nfcc_enviada"));
                nfcc.setNfcc_cnpj(rs.getString("nfcc_cnpj"));
                nfcc.setNf_nome(rs.getString("nf_nome"));
		nfcc.setNfcc_orgao(rs.getString("nfcc_orgao"));
		nfcc.setNfcc_retorno_sefaz(rs.getString("nfcc_retorno_sefaz"));
		nfcc.setNfcc_modelo(rs.getString("nfcc_modelo"));
		nfcc.setNfcc_serie(rs.getString("nfcc_serie"));
		nfcc.setNfcc_numero_nota(rs.getString("nfcc_numero_nota"));
		nfcc.setNfcc_numero_evento(rs.getString("nfcc_numero_evento"));
		nfcc.setFilia(rs.getString("filial"));
                retorno.add(nfcc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalCartaCorrecaoDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            System.out.println(ps.toString());
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalCartaCorrecaoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public boolean excluir(NotaFiscalCartaCorrecao carta) {
        String sql = "delete from nota_fiscal_carta_correcao where Nfcc_chavenfe = ? and Nfcc_item = ?";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, carta.getNfcc_chavenfe());
            ps.setString(2, carta.getNfcc_item());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Erro ao executar delete: " + ps.toString());
            return false;
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalCartaCorrecao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
