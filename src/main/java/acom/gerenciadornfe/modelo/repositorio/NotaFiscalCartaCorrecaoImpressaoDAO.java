/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.NotaFiscalCartaCorrecaoImpressao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Allison
 */
public class NotaFiscalCartaCorrecaoImpressaoDAO {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public NotaFiscalCartaCorrecaoImpressao listar(String chave, String item) {
	String sql = "SELECT \n"
		+ "  Nota_fiscal_carta_correcao.*\n"
		+ "  , Nota_fiscal_carta_correcao.Filial\n"
		+ "  , Filiais.Fil_nome\n"
		+ "  , Filiais.Fil_nreduz\n"
		+ "  , Filiais.Fil_endereço\n"
		+ "  , Filiais.Fil_numero\n"
		+ "  , Filiais.Fil_bairro\n"
		+ "  , Filiais.Fil_cep\n"
		+ "  , Filiais.Fil_cidade\n"
		+ "  , Filiais.Fil_estado\n"
		+ "  , Filiais.Fil_cnpj\n"
		+ "  , Filiais.Fil_insc\n"
		+ "  , Filiais.Fil_fone\n"
		+ "  , Pessoal.Pes_nome45\n"
		+ "  , Pessoal.Pes_nome25\n"
		+ "  , Pessoal.Pes_fj\n"
		+ "  , Pessoal.Pes_cep\n"
		+ "  , Pessoal.Pes_end\n"
		+ "  , Pessoal.Pes_nrend\n"
		+ "  , Pessoal.Pes_bai\n"
		+ "  , Pessoal.Pes_cidade\n"
		+ "  , Pessoal.Pes_uf\n"
		+ "  , Pessoal.Pes_cnpj\n"
		+ "  , Pessoal.Pes_ie\n"
		+ "  , Pessoal.Pes_cpf\n"
		+ "  , Pessoal.Pes_fone1\n"
		+ "FROM Nota_fiscal_carta_correcao \n"
		+ "INNER JOIN Pessoal ON Nota_fiscal_carta_correcao.Nfcc_cnpj = Pessoal.Pes_cnpj \n"
		+ "INNER JOIN Filiais ON Nota_fiscal_carta_correcao.Filial = Filiais.Fil_cod\n"
		+ "WHERE \n"
		+ "Nota_fiscal_carta_correcao.Nfcc_chavenfe = ?\n"
		+ " AND Nota_fiscal_carta_correcao.Nfcc_item = ?";
	PreparedStatement ps = null;
	ResultSet rs = null;
	NotaFiscalCartaCorrecaoImpressao nfcc = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setString(1, chave);
	    ps.setString(2, item);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		nfcc = new NotaFiscalCartaCorrecaoImpressao();
		nfcc.setNfcc_chavenfe(rs.getString("nfcc_chavenfe"));
		nfcc.setNfcc_item(rs.getString("nfcc_item"));
		nfcc.setNfcc_data(rs.getTimestamp("nfcc_data"));
		nfcc.setNfcc_hora(rs.getTimestamp("nfcc_hora"));
		nfcc.setNfcc_correcao(rs.getString("nfcc_correcao"));
		nfcc.setNfcc_protocolo(rs.getString("nfcc_protocolo"));
		nfcc.setNfcc_enviada(rs.getString("nfcc_enviada"));
		nfcc.setNfcc_cnpj(rs.getString("nfcc_cnpj"));
		nfcc.setNfcc_retorno_sefaz(rs.getString("nfcc_retorno_sefaz"));
		nfcc.setNfcc_modelo(rs.getString("nfcc_modelo"));
		nfcc.setNfcc_serie(rs.getString("nfcc_serie"));
		nfcc.setNfcc_numero_nota(rs.getString("nfcc_numero_nota"));
		nfcc.setNfcc_numero_evento(rs.getString("nfcc_numero_evento"));
		nfcc.setFilia(rs.getString("filial"));
		nfcc.setFil_nome(rs.getString("fil_nome"));
		nfcc.setFil_nreduz(rs.getString("fil_nreduz"));
		nfcc.setFil_endereco(rs.getString("fil_endereço"));
		nfcc.setFil_numero(rs.getString("fil_numero"));
		nfcc.setFil_bairro(rs.getString("fil_bairro"));
		nfcc.setFil_cep(rs.getString("fil_cep"));
		nfcc.setFil_cidade(rs.getString("fil_cidade"));
		nfcc.setFil_estado(rs.getString("fil_estado"));
		nfcc.setFil_cnpj(rs.getString("fil_cnpj"));
		nfcc.setFil_insc(rs.getString("fil_insc"));
		nfcc.setFil_fone(rs.getString("fil_fone"));
		nfcc.setPes_nome45(rs.getString("pes_nome45"));
		nfcc.setPes_nome25(rs.getString("pes_nome25"));
		nfcc.setPes_fj(rs.getString("pes_fj"));
		nfcc.setPes_cep(rs.getString("pes_cep"));
		nfcc.setPes_end(rs.getString("pes_end"));
		nfcc.setPes_nrend(rs.getString("pes_nrend"));
		nfcc.setPes_bai(rs.getString("pes_bai"));
		nfcc.setPes_cidade(rs.getString("pes_cidade"));
		nfcc.setPes_uf(rs.getString("pes_uf"));
		nfcc.setPes_cnpj(rs.getString("pes_cnpj"));
		nfcc.setPes_ie(rs.getString("pes_ie"));
		nfcc.setPes_cpf(rs.getString("pes_cpf"));
		nfcc.setPes_fone1(rs.getString("pes_fone1"));
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(NotaFiscalCartaCorrecaoImpressaoDAO.class.getName()).log(Level.SEVERE, null, ex);
	}
	return nfcc;
    }

}
