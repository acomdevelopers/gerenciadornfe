package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.NotaFiscal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author allison
 */
public class NotaFiscalDAO {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public boolean alterarNotaGerada(String notaNumero, String chave, String status) {
        String sql = "UPDATE Nota_fiscal SET Nfestatus = ?, NfeChaveAcesso = ? where Nf_numero = ?;";
        PreparedStatement ps = null;
        boolean retorno = false;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, status);
            ps.setString(2, chave);
            ps.setString(3, notaNumero);
            ps.execute();
            retorno = true;
        } catch (SQLException ex) {
            retorno = false;
            Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public boolean alterarNotaEnviada(String status, String protocolo, String protocoloDataHota, String recibo, boolean nfeEnviada, boolean nfeDenegada, String chave) {
        String sql = "UPDATE Nota_fiscal SET Nfestatus = ?, NFeProtocolo =?, NFeProtocoloDataHora = ?, NFeRecibo = ? , NFeEnviada = ? , Nf_denegada = ? WHERE NFeChaveAcesso = ?;";
        PreparedStatement ps = null;
        boolean retorno = false;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, status);
            ps.setString(2, protocolo);
            ps.setString(3, protocoloDataHota);
            ps.setString(4, recibo);
            ps.setBoolean(5, nfeEnviada);
            ps.setBoolean(6, nfeDenegada);
            ps.setString(7, chave);
            ps.execute();
            retorno = true;
        } catch (SQLException ex) {
            retorno = false;
            Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public boolean alterarNotaNaoRecibo(String status, String protocolo, String protocoloDataHota, boolean nfeEnviada, String chave) {
        String sql = "UPDATE Nota_fiscal SET Nfestatus = ?, NFeProtocolo =?, NFeProtocoloDataHora = ? , NFeEnviada = ? WHERE NFeChaveAcesso = ?;";
        PreparedStatement ps = null;
        boolean retorno = false;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, status);
            ps.setString(2, protocolo);
            ps.setString(3, protocoloDataHota);
            ps.setBoolean(4, nfeEnviada);
            ps.setString(5, chave);
            ps.execute();
            retorno = true;
        } catch (SQLException ex) {
            retorno = false;
            Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public boolean alterarNotaCancelada(boolean cancelada, String status, String protocoloCancelamento, String justificativa, String chave) {
        String sql = "UPDATE Nota_fiscal SET NFeCancelada = ?, Nfestatus = ?, NFeCanceladaProtocolo = ?, NFeCanceladaJustificativa = ? WHERE NFeChaveAcesso = ?;";
        PreparedStatement ps = null;
        boolean retorno = false;
        try {
            ps = connection.prepareStatement(sql);
            ps.setBoolean(1, cancelada);
            ps.setString(2, status);
            ps.setString(3, protocoloCancelamento);
            ps.setString(4, justificativa);
            ps.setString(5, chave);
            ps.execute();
            retorno = true;
        } catch (SQLException ex) {
            retorno = false;
            Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public boolean alterarNotaConsultada(String status, String protocolo, String chave) {
        String sql = "UPDATE Nota_fiscal SET Nfestatus = ?, NfeProtocolo = ? WHERE NFeChaveAcesso = ?;";
        PreparedStatement ps = null;
        boolean retorno = false;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, status);
            ps.setString(2, protocolo);
            ps.setString(3, chave);
            ps.execute();
            retorno = true;
        } catch (SQLException ex) {
            retorno = false;
            Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public List<NotaFiscal> listar(String filtro) {
        String sql = "SELECT TOP 500 \n"
                + "Nota_fiscal.*,\n"
                + "Filiais.Fil_cod,\n"
                + "Filiais.Fil_nome,\n"
                + "Filiais.Fil_bairro,\n"
                + "Filiais.Fil_cidade,\n"
                + "Filiais.Fil_compl,\n"
                + "Filiais.Fil_endereço,\n"
                + "Filiais.Fil_estado,\n"
                + "Filiais.Fil_fone,\n"
                + "Filiais.Fil_nreduz,\n"
                + "Filiais.Fil_numero,\n"
                + "Filiais.Fil_cep,\n"
                + "Filiais.Fil_cnpj,\n"
                + "Filiais.Fil_insc,\n"
                + "Filiais.Fil_cnae,\n"
                + "Filiais.Fil_insc_mun,\n"
                + "Filiais.Fil_obs_padrao,\n"
                + "Filiais.Fil_crt,\n"
                + "Filiais.Fil_crt_sn,\n"
                + "Filiais.Fil_forma_tributacao,\n"
                + "Filiais.Fil_pcredsn,\n"
                + "Filiais.Fil_tipo_atividade,\n"
		+ "Filiais.Fil_emailcont,\n"
                + "Pessoal.Pes_bai,\n"
                + "Pessoal.Pes_cidade,\n"
                + "Pessoal.Pes_compl,\n"
                + "Pessoal.Pes_end,\n"
                + "Pessoal.Pes_fone1,\n"
                + "Pessoal.Pes_nome45,\n"
                + "Pessoal.Pes_nrend,\n"
                + "Pessoal.Pes_uf,\n"
                + "Pessoal.Pes_cep,\n"
                + "Pessoal.Pes_cnpj,\n"
                + "Pessoal.Pes_cpf,\n"
                + "Pessoal.Pes_ie,\n"
                + "Pessoal.Pes_fj,\n"
                + "Pessoal.Pes_email,\n"
                + "Pessoal.Pes_indicador_consumidor,\n"
                + "Pessoal.Pes_indicador_ie,\n"
                + "Pessoal.Pes_aliq_subst,\n"
                + "Transp.Pes_fj AS Transp_fj,\n"
                + "Transp.Pes_nome45 AS Transp_nome45,\n"
                + "Transp.Pes_cnpj AS Transp_cnpj,\n"
                + "Transp.Pes_ie AS Transp_ie,\n"
                + "Transp.Pes_cpf AS Transp_cpf,\n"
                + "Transp.Pes_end AS Transp_end,\n"
                + "Transp.Pes_nrend AS Transp_nrend,\n"
                + "Transp.Pes_bai AS Transp_bai,\n"
                + "Transp.Pes_cidade AS Transp_cidade,\n"
                + "Transp.Pes_uf AS Transp_uf,\n"
                + "Transp.Pes_codigo_antt as transp_codigo_antt,\n"
                + "Codigo_fiscal_operacao.Cfo_texto,\n"
                + "Tipo_entrada_saida.Tes_texto\n"
                + "FROM Nota_fiscal\n"
                + "INNER JOIN Pessoal ON Nota_fiscal.Nf_clifor = Pessoal.Pes_cod6\n"
                + "INNER JOIN Filiais ON Nota_fiscal.Nf_filial = Filiais.Fil_cod\n"
                + "INNER JOIN Codigo_fiscal_operacao ON Nota_fiscal.Nf_cfop = Codigo_fiscal_operacao.Cfo_cod\n"
                + "INNER JOIN Tipo_entrada_saida ON Tipo_entrada_saida.Tes_cod = Nota_fiscal.Nf_tes\n"
                + "LEFT OUTER JOIN Pessoal AS Transp ON Nota_fiscal.Nf_transportadora = Transp.Pes_cod6 "
                + "WHERE Nota_fiscal.Nf_formulario = 'Sim' AND Nota_fiscal.Nf_flag = 'S' " + filtro + "ORDER BY nf_numero ASC;";
        List<NotaFiscal> retorno = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        NotaFiscal nf = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                nf = new NotaFiscal();
                nf.setNf_numero(rs.getString("nf_numero"));
                nf.setNf_tipomov(rs.getString("nf_tipomov"));
                nf.setNf_formulario(rs.getString("nf_formulario"));
                nf.setNf_serie(rs.getString("nf_serie"));
                nf.setNf_nrnota(rs.getString("nf_nrnota"));
                nf.setNf_filial(rs.getString("nf_filial"));
                nf.setNf_clifor(rs.getString("nf_clifor"));
                nf.setNf_uf(rs.getString("nf_uf"));
                nf.setNf_dtemissao(rs.getTimestamp("nf_dtemissao"));
                nf.setNf_data(rs.getTimestamp("nf_data"));
                nf.setNf_nome(rs.getString("nf_nome"));
                nf.setNf_base_n(rs.getDouble("nf_base_n"));
                nf.setNf_picms_n(rs.getDouble("nf_picms_n"));
                nf.setNf_vicms_n(rs.getDouble("nf_vicms_n"));
                nf.setNf_isento(rs.getDouble("nf_isento"));
                nf.setNf_outros(rs.getDouble("nf_outros"));
                nf.setNf_piss(rs.getDouble("nf_piss"));
                nf.setNf_viss(rs.getDouble("nf_viss"));
                nf.setNf_base_s(rs.getDouble("nf_base_s"));
                nf.setNf_vicms_s(rs.getDouble("nf_vicms_s"));
                nf.setNf_vipi(rs.getDouble("nf_vipi"));
                nf.setNf_vproduto(rs.getDouble("nf_vproduto"));
                nf.setNf_totnota(rs.getDouble("nf_totnota"));
                nf.setNf_obs(rs.getString("nf_obs"));
                nf.setNf_volume(rs.getInt("nf_volume"));
                nf.setNf_pbruto(rs.getDouble("nf_pbruto"));
                nf.setNf_pliquido(rs.getDouble("nf_pliquido"));
                nf.setNf_serieselo(rs.getString("nf_serieselo"));
                nf.setNf_nrselo(rs.getString("nf_nrselo"));
                nf.setNf_tes(rs.getString("nf_tes"));
                nf.setNf_cfop(rs.getString("nf_cfop"));
                nf.setNf_cancelada(rs.getString("nf_cancelada"));
                nf.setNf_frete(rs.getDouble("nf_frete"));
                nf.setNf_despesas(rs.getDouble("nf_despesas"));
                nf.setNf_desconto(rs.getDouble("nf_desconto"));
                nf.setNf_tipo(rs.getInt("nf_tipo"));
                nf.setNf_transportadora(rs.getString("nf_transportadora"));
                nf.setNf_tipofrete(rs.getInt("nf_tipofrete"));
                nf.setNf_flag(rs.getString("nf_flag"));
                nf.setFilial(rs.getString("filial"));
                nf.setNf_dup1_numero(rs.getString("nf_dup1_numero"));
                nf.setNf_dup1_vencimento(rs.getTimestamp("nf_dup1_vencimento"));
                nf.setNf_dup1_valor(rs.getDouble("nf_dup1_valor"));
                nf.setNf_dup2_numero(rs.getString("nf_dup2_numero"));
                nf.setNf_dup2_vencimento(rs.getTimestamp("nf_dup2_vencimento"));
                nf.setNf_dup2_valor(rs.getDouble("nf_dup2_valor"));
                nf.setNf_dup3_numero(rs.getString("nf_dup3_numero"));
                nf.setNf_dup3_vencimento(rs.getTimestamp("nf_dup3_vencimento"));
                nf.setNf_dup3_valor(rs.getDouble("nf_dup3_valor"));
                nf.setNf_dup4_numero(rs.getString("nf_dup4_numero"));
                nf.setNf_dup4_vencimento(rs.getTimestamp("nf_dup4_vencimento"));
                nf.setNf_dup4_valor(rs.getDouble("nf_dup4_valor"));
                nf.setNf_dup5_numero(rs.getString("nf_dup5_numero"));
                nf.setNf_dup5_vencimento(rs.getTimestamp("nf_dup5_vencimento"));
                nf.setNf_dup5_valor(rs.getDouble("nf_dup5_valor"));
                nf.setNf_dup6_numero(rs.getString("nf_dup6_numero"));
                nf.setNf_dup6_vencimento(rs.getTimestamp("nf_dup6_vencimento"));
                nf.setNf_dup6_valor(rs.getDouble("nf_dup6_valor"));
                nf.setNf_almoxarifado(rs.getString("nf_almoxarifado"));
                nf.setCodlan(rs.getString("codlan"));
                nf.setNf_baseipi(rs.getDouble("nf_baseipi"));
                nf.setNf_modelo(rs.getString("nf_modelo"));
                nf.setNf_seguro(rs.getDouble("nf_seguro"));
                nf.setNf_msg_corpo(rs.getString("nf_msg_corpo"));
                nf.setNf_marca(rs.getString("nf_marca"));
                nf.setNf_cod_energia(rs.getString("nf_cod_energia"));
                nf.setNf_cfop_nota(rs.getString("nf_cfop_nota"));
                nf.setNf_placa(rs.getString("nf_placa"));
                nf.setNf_especie(rs.getString("nf_especie"));
                nf.setNfe_numeracao_volume(rs.getString("nfe_numeracao_volume"));
                nf.setNfeIndicadorFormaPagamento(rs.getString("nfeIndicadorFormaPagamento"));
                nf.setNfeFormatoImpressaoDANFe(rs.getString("nfeFormatoImpressaoDANFe"));
                nf.setNfeFormatoEmissao(rs.getString("nfeFormatoEmissao"));
                nf.setNfeEnviada(rs.getBoolean("nfeEnviada"));
                nf.setNfeCodigoNota(rs.getInt("nfeCodigoNota"));
                nf.setNfeChaveAcesso(rs.getString("nfeChaveAcesso"));
                nf.setNfeChaveAdicional(rs.getString("nfeChaveAdicional"));
                nf.setNfeRecibo(rs.getLong("nfeRecibo"));
                nf.setNfeProtocolo(rs.getLong("nfeProtocolo"));
                nf.setNfeProtocoloDataHora(rs.getString("nfeProtocoloDataHora"));
                nf.setNfeCanceladaProtocolo(rs.getLong("nfeCanceladaProtocolo"));
                nf.setNfeCanceladaJustificativa(rs.getString("nfeCanceladaJustificativa"));
                nf.setNfeCancelada(rs.getBoolean("nfeCancelada"));
                nf.setNfefinalidade(rs.getString("nfefinalidade"));
                nf.setNfeemail(rs.getString("nfeemail"));
                nf.setNfecontingenciadatahora(rs.getString("nfecontingenciadatahora"));
                nf.setNfecontingenciajustificativa(rs.getString("nfecontingenciajustificativa"));
                nf.setNfe_crt(rs.getString("nfe_crt"));
                nf.setMarcado(rs.getBoolean("marcado"));
                nf.setNf_tipo_importacao(rs.getBoolean("nf_tipo_importacao"));
                nf.setNf_imposto_importacao(rs.getDouble("nf_imposto_importacao"));
                nf.setNf_valor_pis(rs.getDouble("nf_valor_pis"));
                nf.setNf_valor_cofins(rs.getDouble("nf_valor_cofins"));
                nf.setNf_outros_impostos_importacao(rs.getDouble("nf_outros_impostos_importacao"));
                nf.setNf_despesas_adjuaneiras(rs.getDouble("nf_despesas_adjuaneiras"));
                nf.setNf_subtotal_importacao(rs.getDouble("nf_subtotal_importacao"));
                nf.setnFeAmbiente(rs.getString("NFeAmbiente"));
                nf.setNf_caixa(rs.getString("nf_caixa"));
                nf.setNf_imprime_dtsaida(rs.getBoolean("nf_imprime_dtsaida"));
                nf.setNf_denegada(rs.getBoolean("nf_denegada"));
                nf.setNfeChaveNFeoriginal(rs.getString("nfeChaveNFeoriginal"));
                nf.setNf_icms_antecipado(rs.getDouble("nf_icms_antecipado"));
                nf.setNf_altman(rs.getBoolean("nf_altman"));
                nf.setNf_dmi_numero(rs.getString("nf_dmi_numero"));
                nf.setNf_dmi_data(rs.getTimestamp("nf_dmi_data"));
                nf.setNf_dmi_cidade(rs.getString("nf_dmi_cidade"));
                nf.setNf_dmi_uf(rs.getString("nf_dmi_uf"));
                nf.setNf_dmi_codigo_exportador(rs.getString("nf_dmi_codigo_exportador"));
                nf.setNf_tributos_valor(rs.getDouble("nf_tributos_valor"));
                nf.setNf_tributos_aliquota(rs.getDouble("nf_tributos_aliquota"));
                nf.setNf_parcelado(rs.getString("nf_parcelado"));
                nf.setNf_qtd_parcelas(rs.getInt("nf_qtd_parcelas"));
                nf.setNf_dias_prazo(rs.getInt("nf_dias_prazo"));
                nf.setNf_protocolo(rs.getLong("nf_protocolo"));
                nf.setSeq_sincronizacao(rs.getLong("seq_sincronizacao"));
                nf.setNf_icm_creditado(rs.getDouble("nf_icm_creditado"));
                nf.setNf_base_icm_creditado(rs.getDouble("nf_base_icm_creditado"));
                nf.setNf_efd_mes_ano(rs.getString("nf_efd_mes_ano"));
                nf.setNf_dmi_viatransporte(rs.getString("nf_dmi_viatransporte"));
                nf.setNf_dmi_valor_afrmm(rs.getDouble("nf_dmi_valor_afrmm"));
                nf.setNf_incentivo_prodepe(rs.getString("nf_incentivo_prodepe"));
                nf.setNf_ecf_numero(rs.getString("nf_ecf_numero"));
                nf.setControle_alteracao(rs.getString("controle_alteracao"));
                nf.setNf_tipo_operacao(rs.getString("nf_tipo_operacao"));
                nf.setNf_opi_vicms_destino(rs.getDouble("nf_opi_vicms_destino"));
                nf.setNf_opi_vicms_fcp(rs.getDouble("nf_opi_vicms_origem"));
                nf.setNf_opi_vicms_fcp(rs.getDouble("nf_opi_vicms_fcp"));
                nf.setNf_vicms_interestadual(rs.getDouble("nf_vicms_interestadual"));
                nf.setNf_importacao_deferido(rs.getString("nf_importacao_deferido"));
                nf.setNf_importacao_deferido_p(rs.getDouble("nf_importacao_deferido_p"));
                nf.setNf_valor_importacao(rs.getDouble("nf_valor_importacao"));
                nf.setNf_modelo_importacao(rs.getString("nf_modelo_importacao"));
                nf.setNfestatus(rs.getString("nfestatus"));
                nf.setNf_vfcp(rs.getDouble("nf_vfcp"));
                nf.setNf_vicmsufdest(rs.getDouble("nf_vicmsufdest"));
                nf.setNf_vicmsufremet(rs.getDouble("nf_vicmsufremet"));
                nf.setNf_fcpst_basecalculo(rs.getDouble("nf_fcpst_basecalculo"));
                nf.setNf_fcpst_valor(rs.getDouble("nf_fcpst_valor"));
                nf.setNf_fcpst_valorretido(rs.getDouble("nf_fcpst_valorretido"));
                nf.setNf_infadicionaisfisco(rs.getString("nf_infadicionaisfisco"));
                nf.setNf_documento(rs.getString("nf_documento"));
		nf.setHora(rs.getTimestamp("hora"));
                nf.setFil_cod(rs.getString("fil_cod"));
                nf.setFil_nome(rs.getString("fil_nome"));
                nf.setFil_bairro(rs.getString("fil_bairro"));
                nf.setFil_cidade(rs.getString("fil_cidade"));
                nf.setFil_compl(rs.getString("fil_compl"));
                nf.setFil_endereço(rs.getString("fil_endereço"));
                nf.setFil_estado(rs.getString("fil_estado"));
                nf.setFil_fone(rs.getString("fil_fone"));
                nf.setFil_nreduz(rs.getString("fil_nreduz"));
                nf.setFil_numero(rs.getString("fil_numero"));
                nf.setFil_cep(rs.getString("fil_cep"));
                nf.setFil_cnpj(rs.getString("fil_cnpj"));
                nf.setFil_insc(rs.getString("fil_insc"));
                nf.setFil_cnae(rs.getString("fil_cnae"));
                nf.setFil_insc_mun(rs.getString("fil_insc_mun"));
                nf.setFil_obs_padrao(rs.getString("fil_obs_padrao"));
                nf.setFil_crt(rs.getString("fil_crt"));
                nf.setFil_crt_sn(rs.getString("fil_crt_sn"));
                nf.setFil_forma_tributacao(rs.getString("fil_forma_tributacao"));
                nf.setFil_pcredsn(rs.getDouble("fil_pcredsn"));
                nf.setFil_tipo_atividade("fil_tipo_atividade");
		nf.setFil_emailcont(rs.getString("fil_emailcont"));
                nf.setPes_bai(rs.getString("pes_bai"));
                nf.setPes_cidade(rs.getString("pes_cidade"));
                nf.setPes_compl(rs.getString("pes_compl"));
                nf.setPes_end(rs.getString("pes_end"));
                nf.setPes_fone1(rs.getString("pes_fone1"));
                nf.setPes_nome45(rs.getString("pes_nome45"));
                nf.setPes_nrend(rs.getString("pes_nrend"));
                nf.setPes_uf(rs.getString("pes_uf"));
                nf.setPes_cep(rs.getString("pes_cep"));
                nf.setPes_cnpj(rs.getString("pes_cnpj"));
                nf.setPes_cpf(rs.getString("pes_cpf"));
                nf.setPes_ie(rs.getString("pes_ie"));
                nf.setPes_fj(rs.getInt("pes_fj"));
                nf.setPes_email(rs.getString("pes_email"));
                nf.setPes_indicador_consumidor(rs.getString("pes_indicador_consumidor"));
                nf.setPes_indicador_ie(rs.getString("pes_indicador_ie"));
                nf.setPes_aliq_subst(rs.getDouble("pes_aliq_subst"));
                nf.setTransp_fj(rs.getInt("transp_fj"));
                nf.setTransp_nome45(rs.getString("transp_nome45"));
                nf.setTransp_cnpj(rs.getString("transp_cnpj"));
                nf.setTransp_ie(rs.getString("transp_ie"));
                nf.setTransp_cpf(rs.getString("transp_cpf"));
                nf.setTransp_end(rs.getString("transp_end"));
                nf.setTransp_nrend(rs.getString("transp_nrend"));
                nf.setTransp_bai(rs.getString("transp_bai"));
                nf.setTransp_cidade(rs.getString("transp_cidade"));
                nf.setTransp_uf(rs.getString("transp_uf"));
                nf.setTransp_codigo_antt(rs.getString("transp_codigo_antt"));
                nf.setCfo_texto(rs.getString("cfo_texto"));
                nf.setTes_texto(rs.getString("tes_texto"));
                retorno.add(nf);
            }
        } catch (SQLException ex) {
            System.out.println(ps.toString());
            Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                System.out.println(ps.toString());
                rs.close();
                ps.close();;
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public NotaFiscal getNotaFiscalChave(String chave) {
        String sql = "SELECT Nota_fiscal.*,\n"
                + "Filiais.Fil_cod,\n"
                + "Filiais.Fil_nome,\n"
                + "Filiais.Fil_bairro,\n"
                + "Filiais.Fil_cidade,\n"
                + "Filiais.Fil_compl,\n"
                + "Filiais.Fil_endereço,\n"
                + "Filiais.Fil_estado,\n"
                + "Filiais.Fil_fone,\n"
                + "Filiais.Fil_nreduz,\n"
                + "Filiais.Fil_numero,\n"
                + "Filiais.Fil_cep,\n"
                + "Filiais.Fil_cnpj,\n"
                + "Filiais.Fil_insc,\n"
                + "Filiais.Fil_cnae,\n"
                + "Filiais.Fil_insc_mun,\n"
                + "Filiais.Fil_obs_padrao,\n"
                + "Filiais.Fil_crt,\n"
                + "Filiais.Fil_crt_sn,\n"
                + "Filiais.Fil_forma_tributacao,\n"
                + "Filiais.Fil_pcredsn,\n"
                + "Filiais.Fil_tipo_atividade,\n"
		+ "Filiais.Fil_emailcont,\n"
                + "Pessoal.Pes_bai,\n"
                + "Pessoal.Pes_cidade,\n"
                + "Pessoal.Pes_compl,\n"
                + "Pessoal.Pes_end,\n"
                + "Pessoal.Pes_fone1,\n"
                + "Pessoal.Pes_nome45,\n"
                + "Pessoal.Pes_nrend,\n"
                + "Pessoal.Pes_uf,\n"
                + "Pessoal.Pes_cep,\n"
                + "Pessoal.Pes_cnpj,\n"
                + "Pessoal.Pes_cpf,\n"
                + "Pessoal.Pes_ie,\n"
                + "Pessoal.Pes_fj,\n"
                + "Pessoal.Pes_email,\n"
                + "Pessoal.Pes_indicador_consumidor,\n"
                + "Pessoal.Pes_indicador_ie,\n"
                + "Pessoal.Pes_aliq_subst,\n"
                + "Transp.Pes_fj AS Transp_fj,\n"
                + "Transp.Pes_nome45 AS Transp_nome45,\n"
                + "Transp.Pes_cnpj AS Transp_cnpj,\n"
                + "Transp.Pes_ie AS Transp_ie,\n"
                + "Transp.Pes_cpf AS Transp_cpf,\n"
                + "Transp.Pes_end AS Transp_end,\n"
                + "Transp.Pes_nrend AS Transp_nrend,\n"
                + "Transp.Pes_bai AS Transp_bai,\n"
                + "Transp.Pes_cidade AS Transp_cidade,\n"
                + "Transp.Pes_uf AS Transp_uf,\n"
                + "Transp.Pes_codigo_antt as transp_codigo_antt,\n"
                + "Codigo_fiscal_operacao.Cfo_texto,\n"
                + "Tipo_entrada_saida.Tes_texto\n"
                + "FROM Nota_fiscal\n"
                + "INNER JOIN Pessoal ON Nota_fiscal.Nf_clifor = Pessoal.Pes_cod6\n"
                + "INNER JOIN Filiais ON Nota_fiscal.Nf_filial = Filiais.Fil_cod\n"
                + "INNER JOIN Codigo_fiscal_operacao ON Nota_fiscal.Nf_cfop = Codigo_fiscal_operacao.Cfo_cod\n"
                + "INNER JOIN Tipo_entrada_saida ON Tipo_entrada_saida.Tes_cod = Nota_fiscal.Nf_tes\n"
                + "LEFT OUTER JOIN Pessoal AS Transp ON Nota_fiscal.Nf_transportadora = Transp.Pes_cod6 "
                + "WHERE NfeChaveAcesso = ?;";
        PreparedStatement ps = null;
        ResultSet rs = null;

        NotaFiscal nf = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, chave);
            rs = ps.executeQuery();
            if (rs.next()) {
                nf = new NotaFiscal();
                nf.setNf_numero(rs.getString("nf_numero"));
                nf.setNf_tipomov(rs.getString("nf_tipomov"));
                nf.setNf_formulario(rs.getString("nf_formulario"));
                nf.setNf_serie(rs.getString("nf_serie"));
                nf.setNf_nrnota(rs.getString("nf_nrnota"));
                nf.setNf_filial(rs.getString("nf_filial"));
                nf.setNf_clifor(rs.getString("nf_clifor"));
                nf.setNf_uf(rs.getString("nf_uf"));
                nf.setNf_dtemissao(rs.getTimestamp("nf_dtemissao"));
                nf.setNf_data(rs.getTimestamp("nf_data"));
                nf.setNf_nome(rs.getString("nf_nome"));
                nf.setNf_base_n(rs.getDouble("nf_base_n"));
                nf.setNf_picms_n(rs.getDouble("nf_picms_n"));
                nf.setNf_vicms_n(rs.getDouble("nf_vicms_n"));
                nf.setNf_isento(rs.getDouble("nf_isento"));
                nf.setNf_outros(rs.getDouble("nf_outros"));
                nf.setNf_piss(rs.getDouble("nf_piss"));
                nf.setNf_viss(rs.getDouble("nf_viss"));
                nf.setNf_base_s(rs.getDouble("nf_base_s"));
                nf.setNf_vicms_s(rs.getDouble("nf_vicms_s"));
                nf.setNf_vipi(rs.getDouble("nf_vipi"));
                nf.setNf_vproduto(rs.getDouble("nf_vproduto"));
                nf.setNf_totnota(rs.getDouble("nf_totnota"));
                nf.setNf_obs(rs.getString("nf_obs"));
                nf.setNf_volume(rs.getInt("nf_volume"));
                nf.setNf_pbruto(rs.getDouble("nf_pbruto"));
                nf.setNf_pliquido(rs.getDouble("nf_pliquido"));
                nf.setNf_serieselo(rs.getString("nf_serieselo"));
                nf.setNf_nrselo(rs.getString("nf_nrselo"));
                nf.setNf_tes(rs.getString("nf_tes"));
                nf.setNf_cfop(rs.getString("nf_cfop"));
                nf.setNf_cancelada(rs.getString("nf_cancelada"));
                nf.setNf_frete(rs.getDouble("nf_frete"));
                nf.setNf_despesas(rs.getDouble("nf_despesas"));
                nf.setNf_desconto(rs.getDouble("nf_desconto"));
                nf.setNf_tipo(rs.getInt("nf_tipo"));
                nf.setNf_transportadora(rs.getString("nf_transportadora"));
                nf.setNf_tipofrete(rs.getInt("nf_tipofrete"));
                nf.setNf_flag(rs.getString("nf_flag"));
                nf.setFilial(rs.getString("filial"));
                nf.setNf_dup1_numero(rs.getString("nf_dup1_numero"));
                nf.setNf_dup1_vencimento(rs.getTimestamp("nf_dup1_vencimento"));
                nf.setNf_dup1_valor(rs.getDouble("nf_dup1_valor"));
                nf.setNf_dup2_numero(rs.getString("nf_dup2_numero"));
                nf.setNf_dup2_vencimento(rs.getTimestamp("nf_dup2_vencimento"));
                nf.setNf_dup2_valor(rs.getDouble("nf_dup2_valor"));
                nf.setNf_dup3_numero(rs.getString("nf_dup3_numero"));
                nf.setNf_dup3_vencimento(rs.getTimestamp("nf_dup3_vencimento"));
                nf.setNf_dup3_valor(rs.getDouble("nf_dup3_valor"));
                nf.setNf_dup4_numero(rs.getString("nf_dup4_numero"));
                nf.setNf_dup4_vencimento(rs.getTimestamp("nf_dup4_vencimento"));
                nf.setNf_dup4_valor(rs.getDouble("nf_dup4_valor"));
                nf.setNf_dup5_numero(rs.getString("nf_dup5_numero"));
                nf.setNf_dup5_vencimento(rs.getTimestamp("nf_dup5_vencimento"));
                nf.setNf_dup5_valor(rs.getDouble("nf_dup5_valor"));
                nf.setNf_dup6_numero(rs.getString("nf_dup6_numero"));
                nf.setNf_dup6_vencimento(rs.getTimestamp("nf_dup6_vencimento"));
                nf.setNf_dup6_valor(rs.getDouble("nf_dup6_valor"));
                nf.setNf_almoxarifado(rs.getString("nf_almoxarifado"));
                nf.setCodlan(rs.getString("codlan"));
                nf.setNf_baseipi(rs.getDouble("nf_baseipi"));
                nf.setNf_modelo(rs.getString("nf_modelo"));
                nf.setNf_seguro(rs.getDouble("nf_seguro"));
                nf.setNf_msg_corpo(rs.getString("nf_msg_corpo"));
                nf.setNf_marca(rs.getString("nf_marca"));
                nf.setNf_cod_energia(rs.getString("nf_cod_energia"));
                nf.setNf_cfop_nota(rs.getString("nf_cfop_nota"));
                nf.setNf_placa(rs.getString("nf_placa"));
                nf.setNf_especie(rs.getString("nf_especie"));
                nf.setNfe_numeracao_volume(rs.getString("nfe_numeracao_volume"));
                nf.setNfeIndicadorFormaPagamento(rs.getString("nfeIndicadorFormaPagamento"));
                nf.setNfeFormatoImpressaoDANFe(rs.getString("nfeFormatoImpressaoDANFe"));
                nf.setNfeFormatoEmissao(rs.getString("nfeFormatoEmissao"));
                nf.setNfeEnviada(rs.getBoolean("nfeEnviada"));
                nf.setNfeCodigoNota(rs.getInt("nfeCodigoNota"));
                nf.setNfeChaveAcesso(rs.getString("nfeChaveAcesso"));
                nf.setNfeChaveAdicional(rs.getString("nfeChaveAdicional"));
                nf.setNfeRecibo(rs.getLong("nfeRecibo"));
                nf.setNfeProtocolo(rs.getLong("nfeProtocolo"));
                nf.setNfeProtocoloDataHora(rs.getString("nfeProtocoloDataHora"));
                nf.setNfeCanceladaProtocolo(rs.getLong("nfeCanceladaProtocolo"));
                nf.setNfeCanceladaJustificativa(rs.getString("nfeCanceladaJustificativa"));
                nf.setNfeCancelada(rs.getBoolean("nfeCancelada"));
                nf.setNfefinalidade(rs.getString("nfefinalidade"));
                nf.setNfeemail(rs.getString("nfeemail"));
                nf.setNfecontingenciadatahora(rs.getString("nfecontingenciadatahora"));
                nf.setNfecontingenciajustificativa(rs.getString("nfecontingenciajustificativa"));
                nf.setNfe_crt(rs.getString("nfe_crt"));
                nf.setMarcado(rs.getBoolean("marcado"));
                nf.setNf_tipo_importacao(rs.getBoolean("nf_tipo_importacao"));
                nf.setNf_imposto_importacao(rs.getDouble("nf_imposto_importacao"));
                nf.setNf_valor_pis(rs.getDouble("nf_valor_pis"));
                nf.setNf_valor_cofins(rs.getDouble("nf_valor_cofins"));
                nf.setNf_outros_impostos_importacao(rs.getDouble("nf_outros_impostos_importacao"));
                nf.setNf_despesas_adjuaneiras(rs.getDouble("nf_despesas_adjuaneiras"));
                nf.setNf_subtotal_importacao(rs.getDouble("nf_subtotal_importacao"));
                nf.setnFeAmbiente(rs.getString("NFeAmbiente"));
                nf.setNf_caixa(rs.getString("nf_caixa"));
                nf.setNf_imprime_dtsaida(rs.getBoolean("nf_imprime_dtsaida"));
                nf.setNf_denegada(rs.getBoolean("nf_denegada"));
                nf.setNfeChaveNFeoriginal(rs.getString("nfeChaveNFeoriginal"));
                nf.setNf_icms_antecipado(rs.getDouble("nf_icms_antecipado"));
                nf.setNf_altman(rs.getBoolean("nf_altman"));
                nf.setNf_dmi_numero(rs.getString("nf_dmi_numero"));
                nf.setNf_dmi_data(rs.getTimestamp("nf_dmi_data"));
                nf.setNf_dmi_cidade(rs.getString("nf_dmi_cidade"));
                nf.setNf_dmi_uf(rs.getString("nf_dmi_uf"));
                nf.setNf_dmi_codigo_exportador(rs.getString("nf_dmi_codigo_exportador"));
                nf.setNf_tributos_valor(rs.getDouble("nf_tributos_valor"));
                nf.setNf_tributos_aliquota(rs.getDouble("nf_tributos_aliquota"));
                nf.setNf_parcelado(rs.getString("nf_parcelado"));
                nf.setNf_qtd_parcelas(rs.getInt("nf_qtd_parcelas"));
                nf.setNf_dias_prazo(rs.getInt("nf_dias_prazo"));
                nf.setNf_protocolo(rs.getLong("nf_protocolo"));
                nf.setSeq_sincronizacao(rs.getLong("seq_sincronizacao"));
                nf.setNf_icm_creditado(rs.getDouble("nf_icm_creditado"));
                nf.setNf_base_icm_creditado(rs.getDouble("nf_base_icm_creditado"));
                nf.setNf_efd_mes_ano(rs.getString("nf_efd_mes_ano"));
                nf.setNf_dmi_viatransporte(rs.getString("nf_dmi_viatransporte"));
                nf.setNf_dmi_valor_afrmm(rs.getDouble("nf_dmi_valor_afrmm"));
                nf.setNf_incentivo_prodepe(rs.getString("nf_incentivo_prodepe"));
                nf.setNf_ecf_numero(rs.getString("nf_ecf_numero"));
                nf.setControle_alteracao(rs.getString("controle_alteracao"));
                nf.setNf_tipo_operacao(rs.getString("nf_tipo_operacao"));
                nf.setNf_opi_vicms_destino(rs.getDouble("nf_opi_vicms_destino"));
                nf.setNf_opi_vicms_fcp(rs.getDouble("nf_opi_vicms_origem"));
                nf.setNf_opi_vicms_fcp(rs.getDouble("nf_opi_vicms_fcp"));
                nf.setNf_vicms_interestadual(rs.getDouble("nf_vicms_interestadual"));
                nf.setNf_importacao_deferido(rs.getString("nf_importacao_deferido"));
                nf.setNf_importacao_deferido_p(rs.getDouble("nf_importacao_deferido_p"));
                nf.setNf_valor_importacao(rs.getDouble("nf_valor_importacao"));
                nf.setNf_modelo_importacao(rs.getString("nf_modelo_importacao"));
                nf.setNfestatus(rs.getString("nfestatus"));
                nf.setNf_vfcp(rs.getDouble("nf_vfcp"));
                nf.setNf_vicmsufdest(rs.getDouble("nf_vicmsufdest"));
                nf.setNf_vicmsufremet(rs.getDouble("nf_vicmsufremet"));
                nf.setNf_fcpst_basecalculo(rs.getDouble("nf_fcpst_basecalculo"));
                nf.setNf_fcpst_valor(rs.getDouble("nf_fcpst_valor"));
                nf.setNf_fcpst_valorretido(rs.getDouble("nf_fcpst_valorretido"));
                nf.setNf_infadicionaisfisco(rs.getString("nf_infadicionaisfisco"));
                nf.setNf_documento(rs.getString("nf_documento"));
		nf.setHora(rs.getTimestamp("hora"));
                nf.setFil_cod(rs.getString("fil_cod"));
                nf.setFil_nome(rs.getString("fil_nome"));
                nf.setFil_bairro(rs.getString("fil_bairro"));
                nf.setFil_cidade(rs.getString("fil_cidade"));
                nf.setFil_compl(rs.getString("fil_compl"));
                nf.setFil_endereço(rs.getString("fil_endereço"));
                nf.setFil_estado(rs.getString("fil_estado"));
                nf.setFil_fone(rs.getString("fil_fone"));
                nf.setFil_nreduz(rs.getString("fil_nreduz"));
                nf.setFil_numero(rs.getString("fil_numero"));
                nf.setFil_cep(rs.getString("fil_cep"));
                nf.setFil_cnpj(rs.getString("fil_cnpj"));
                nf.setFil_insc(rs.getString("fil_insc"));
                nf.setFil_cnae(rs.getString("fil_cnae"));
                nf.setFil_insc_mun(rs.getString("fil_insc_mun"));
                nf.setFil_obs_padrao(rs.getString("fil_obs_padrao"));
                nf.setFil_crt(rs.getString("fil_crt"));
                nf.setFil_crt_sn(rs.getString("fil_crt_sn"));
                nf.setFil_forma_tributacao(rs.getString("fil_forma_tributacao"));
                nf.setFil_pcredsn(rs.getDouble("fil_pcredsn"));
                nf.setFil_tipo_atividade("fil_tipo_atividade");
		nf.setFil_emailcont(rs.getString("fil_emailcont"));
                nf.setPes_bai(rs.getString("pes_bai"));
                nf.setPes_cidade(rs.getString("pes_cidade"));
                nf.setPes_compl(rs.getString("pes_compl"));
                nf.setPes_end(rs.getString("pes_end"));
                nf.setPes_fone1(rs.getString("pes_fone1"));
                nf.setPes_nome45(rs.getString("pes_nome45"));
                nf.setPes_nrend(rs.getString("pes_nrend"));
                nf.setPes_uf(rs.getString("pes_uf"));
                nf.setPes_cep(rs.getString("pes_cep"));
                nf.setPes_cnpj(rs.getString("pes_cnpj"));
                nf.setPes_cpf(rs.getString("pes_cpf"));
                nf.setPes_ie(rs.getString("pes_ie"));
                nf.setPes_fj(rs.getInt("pes_fj"));
                nf.setPes_email(rs.getString("pes_email"));
                nf.setPes_indicador_consumidor(rs.getString("pes_indicador_consumidor"));
                nf.setPes_indicador_ie(rs.getString("pes_indicador_ie"));
                nf.setPes_aliq_subst(rs.getDouble("pes_aliq_subst"));
                nf.setTransp_fj(rs.getInt("transp_fj"));
                nf.setTransp_nome45(rs.getString("transp_nome45"));
                nf.setTransp_cnpj(rs.getString("transp_cnpj"));
                nf.setTransp_ie(rs.getString("transp_ie"));
                nf.setTransp_cpf(rs.getString("transp_cpf"));
                nf.setTransp_end(rs.getString("transp_end"));
                nf.setTransp_nrend(rs.getString("transp_nrend"));
                nf.setTransp_bai(rs.getString("transp_bai"));
                nf.setTransp_cidade(rs.getString("transp_cidade"));
                nf.setTransp_uf(rs.getString("transp_uf"));
                nf.setTransp_codigo_antt(rs.getString("transp_codigo_antt"));
                nf.setCfo_texto(rs.getString("cfo_texto"));
                nf.setTes_texto(rs.getString("tes_texto"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return nf;
    }

    public String getStatus(String chave) {
        String sql = "select Nota_fiscal.nfestatus from Nota_fiscal where Nota_fiscal.NfeChaveAcesso = ?;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, chave);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("nfestatus");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getProtocolo(String chave) {
        String sql = "select Nota_fiscal.Nf_protocolo from Nota_fiscal where Nota_fiscal.NfeChaveAcesso = ?;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, chave);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("Nf_protocolo");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getData(String chave) {
        String sql = "select nf_data from nota_fiscal where nfechaveacesso = ?;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, chave);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("nf_data");
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

}
