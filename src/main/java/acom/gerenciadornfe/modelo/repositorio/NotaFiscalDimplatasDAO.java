/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.NotaFiscalDuplicatas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class NotaFiscalDimplatasDAO {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public List<NotaFiscalDuplicatas> listar(String numeroNota) {
        String sql = "select * from nota_fiscal_duplicatas where nfd_numero = ?";
        PreparedStatement ps = null;
        ResultSet rs = null;
        NotaFiscalDuplicatas nfDuplicatas;
        List<NotaFiscalDuplicatas> retorno = new ArrayList<>();
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, numeroNota);
            rs = ps.executeQuery();
            while (rs.next()) {
                nfDuplicatas = new NotaFiscalDuplicatas();
                nfDuplicatas.setNfd_numero(rs.getString("nfd_numero"));
                nfDuplicatas.setNfd_parcela(rs.getString("nfd_parcela"));
                nfDuplicatas.setNfd_vencimento(rs.getTimestamp("nfd_vencimento"));
                nfDuplicatas.setNfd_documento(rs.getString("nfd_documento"));
                nfDuplicatas.setNfd_valor(rs.getDouble("nfd_valor"));
                retorno.add(nfDuplicatas);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalDimplatasDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalDimplatasDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

}
