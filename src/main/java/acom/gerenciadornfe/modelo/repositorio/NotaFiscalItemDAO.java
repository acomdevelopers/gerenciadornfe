/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.NotaFiscalItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class NotaFiscalItemDAO {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public List<NotaFiscalItem> listar(String numero) {

        String sql = " SELECT\n"
                + " Nota_fiscal_item.*,\n"
                + " Produto.Pro_cst_ipi_ent,\n"
                + " Produto.Pro_um,\n"
                + " Produto.Pro_origem,\n"
                + " Produto.Pro_codean,\n"
                + " Produto.Pro_tipocod,\n"
                + " Produto.Pro_coddun,\n"
                + " Produto.Pro_ncm,\n"
                + " Produto.Pro_cest, \n"
                + " Produto.Pro_cst_ipi,\n"
                + " Produto.Pro_codigo_anp,\n"
                + " Produto.Pro_codigo_codif,\n"
                + " Tipo_entrada_saida.Tes_gera_piscofins\n"
                + " FROM Nota_fiscal_item\n"
                + " INNER JOIN Produto ON Nota_fiscal_item.Nfi_codint = Produto.Pro_cod\n"
                + " INNER JOIN Tipo_entrada_saida on Tipo_entrada_saida.tes_cod = Nota_fiscal_item.nfi_tes\n"
                + " WHERE nfi_numero = ?"
                + " ORDER BY Nota_fiscal_item.Nfi_item;";

        PreparedStatement ps = null;
        ResultSet rs = null;
        List<NotaFiscalItem> retorno = new ArrayList<>();
        NotaFiscalItem nfi = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, numero);
            rs = ps.executeQuery();
            while (rs.next()) {
                nfi = new NotaFiscalItem();
                nfi.setNfi_item(rs.getString("nfi_item"));
                nfi.setNfi_numero(rs.getString("nfi_numero"));
                nfi.setNfi_clifor(rs.getString("nfi_clifor"));
                nfi.setNfi_serie(rs.getString("nfi_serie"));
                nfi.setNfi_nrnota(rs.getString("nfi_nrnota"));
                nfi.setNfi_codpro(rs.getString("nfi_codpro"));
                nfi.setNfi_nome(rs.getString("nfi_nome"));
                nfi.setNfi_qtdfat(rs.getDouble("nfi_qtdfat"));
                nfi.setNfi_prunit(rs.getDouble("nfi_prunit"));
                nfi.setNfi_total(rs.getDouble("nfi_total"));
                nfi.setNfi_picms_n(rs.getDouble("nfi_picms_n"));
                nfi.setNfi_vicms_n(rs.getDouble("nfi_vicms_n"));
                nfi.setNfi_vicms_s(rs.getDouble("nfi_vicms_s"));
                nfi.setNfi_tes(rs.getString("nfi_tes"));
                nfi.setNfi_pipi(rs.getDouble("nfi_pipi"));
                nfi.setNfi_vipi(rs.getDouble("nfi_vipi"));
                nfi.setNfi_cfop(rs.getString("nfi_cfop"));
                nfi.setNfi_sittrib(rs.getString("nfi_sittrib"));
                nfi.setNfi_piss(rs.getDouble("nfi_piss"));
                nfi.setNfi_viss(rs.getDouble("nfi_viss"));
                nfi.setNfi_destino(rs.getString("nfi_destino"));
                nfi.setNfi_dtemissao(rs.getTimestamp("nfi_dtemissao"));
                nfi.setNfi_data(rs.getTimestamp("nfi_data"));
                nfi.setNfi_obs(rs.getString("nfi_obs"));
                nfi.setNfi_pbruto(rs.getDouble("nfi_pbruto"));
                nfi.setNfi_pliquido(rs.getDouble("nfi_pliquido"));
                nfi.setNfi_tipo(rs.getInt("nfi_tipo"));
                nfi.setNfi_codint(rs.getString("nfi_codint"));
                nfi.setFilial(rs.getString("filial"));
                nfi.setIncluido(rs.getString("incluido"));
                nfi.setHorainc(rs.getTimestamp("horainc"));
                nfi.setDtinc(rs.getTimestamp("dtinc"));
                nfi.setAlterado(rs.getString("alterado"));
                nfi.setHoraalt(rs.getTimestamp("horaalt"));
                nfi.setDtalt(rs.getTimestamp("dtalt"));
                nfi.setNfi_filial(rs.getString("nfi_filial"));
                nfi.setNfi_pedido(rs.getString("nfi_pedido"));
                nfi.setNfi_cupom(rs.getString("nfi_cupom"));
                nfi.setNfi_documento(rs.getString("nfi_documento"));
                nfi.setNfi_almoxarifado(rs.getString("nfi_almoxarifado"));
                nfi.setNfi_base_calculo(rs.getDouble("nfi_base_calculo"));
                nfi.setCodlan(rs.getString("codlan"));
                nfi.setNfi_pipifrete(rs.getDouble("nfi_pipifrete"));
                nfi.setNfi_baseipifrete(rs.getDouble("nfi_baseipifrete"));
                nfi.setNfi_vipifrete(rs.getDouble("nfi_vipifrete"));
                nfi.setNfi_baseipi(rs.getDouble("nfi_baseipi"));
                nfi.setNfi_desconto(rs.getDouble("nfi_desconto"));
                nfi.setNfi_valorfrete(rs.getDouble("nfi_valorfrete"));
                nfi.setNfi_valordespesa(rs.getDouble("nfi_valordespesa"));
                nfi.setNfi_isento(rs.getDouble("nfi_isento"));
                nfi.setNfi_outros(rs.getDouble("nfi_outros"));
                nfi.setNfi_valorseguro(rs.getDouble("nfi_valorseguro"));
                nfi.setNfi_base_s(rs.getDouble("nfi_base_s"));
                nfi.setNfi_codigodanfe(rs.getLong("nfi_codigodanfe"));
                nfi.setNfi_valor_pis(rs.getDouble("nfi_valor_pis"));
                nfi.setNfi_valor_cofins(rs.getDouble("nfi_valor_cofins"));
                nfi.setNfi_icms_antecipado(rs.getDouble("nfi_icms_antecipado"));
                nfi.setNfi_cst_pis(rs.getString("nfi_cst_pis"));
                nfi.setNfi_cst_cofins(rs.getString("nfi_cst_cofins"));
                nfi.setNfi_aliquota_pis(rs.getDouble("nfi_aliquota_pis"));
                nfi.setNfi_aliquota_cofins(rs.getDouble("nfi_aliquota_cofins"));
                nfi.setNfi_atualizado_pis_cofins(rs.getBoolean("nfi_atualizado_pis_cofins"));
                nfi.setNfi_natureza_receita(rs.getString("nfi_natureza_receita"));
                nfi.setNfi_tributos_valor(rs.getDouble("nfi_tributos_valor"));
                nfi.setNfi_tributos_aliquota(rs.getDouble("nfi_tributos_aliquota"));
                nfi.setSeq_sincronizacao(rs.getLong("seq_sincronizacao"));
                nfi.setControle_alteracao(rs.getString("controle_alteracao"));
                nfi.setNfi_ecf_numero(rs.getString("nfi_ecf_numero"));
                nfi.setNfi_ecf_coo(rs.getString("nfi_ecf_coo"));
                nfi.setNfi_chaveacesso_ref(rs.getString("nfi_chaveacesso_ref"));
                nfi.setNfi_icm_creditado(rs.getDouble("nfi_icm_creditado"));
                nfi.setNfi_base_icm_creditado(rs.getDouble("nfi_base_icm_creditado"));
                nfi.setNfi_imposto_importacao(rs.getDouble("nfi_imposto_importacao"));
                nfi.setNfi_opi_vicms_destino(rs.getDouble("nfi_opi_vicms_destino"));
                nfi.setNfi_opi_vicms_origem(rs.getDouble("nfi_opi_vicms_origem"));
                nfi.setNfi_opi_picms_destino(rs.getDouble("nfi_opi_picms_destino"));
                nfi.setNfi_opi_picms_fcp(rs.getDouble("nfi_opi_picms_fcp"));
                nfi.setNfi_opi_vicms_fcp(rs.getDouble("nfi_opi_vicms_fcp"));
                nfi.setNfi_opi_vicms_interestadual(rs.getDouble("nfi_opi_vicms_interestadual"));
                nfi.setNfi_altman(rs.getString("nfi_altman"));
                nfi.setNfi_um(rs.getString("nfi_um"));
                nfi.setNfi_fatorqtd(rs.getDouble("nfi_fatorqtd"));
                nfi.setNfi_qtd_entsai(rs.getDouble("nfi_qtd_entsai"));
                nfi.setNfi_vbcufdest(rs.getDouble("nfi_vbcufdest"));
                nfi.setNfi_pfcpufdest(rs.getDouble("nfi_pfcpufdest"));
                nfi.setNfi_picmsufdest(rs.getDouble("nfi_picmsufdest"));
                nfi.setNfi_picmsinterestadual(rs.getDouble("nfi_picmsinterestadual"));
                nfi.setNfi_picmsinterpart(rs.getDouble("nfi_picmsinterpart"));
                nfi.setNfi_vfcpufdest(rs.getDouble("nfi_vfcpufdest"));
                nfi.setNfi_vicmsufdest(rs.getDouble("nfi_vicmsufdest"));
                nfi.setNfi_vicmsufremet(rs.getDouble("nfi_vicmsufremet"));
                nfi.setNfi_vfcp(rs.getDouble("nfi_vfcp"));
                nfi.setNfi_pfcp(rs.getDouble("nfi_pfcp"));
                nfi.setNfi_cst_ipi(rs.getString("nfi_cst_ipi"));
                nfi.setNfi_ipi_cenq(rs.getString("nfi_ipi_cenq"));
		nfi.setNfi_codean(rs.getString("nfi_codean"));
		nfi.setNfi_codeantrib(rs.getString("nfi_codeantrib"));
                nfi.setPro_cst_ipi_ent(rs.getString("pro_cst_ipi_ent"));
                nfi.setPro_um(rs.getString("pro_um"));
                nfi.setPro_origem(rs.getString("pro_origem"));
                nfi.setPro_codean(rs.getString("pro_codean"));
                nfi.setPro_tipocod(rs.getInt("pro_tipocod"));
                nfi.setPro_coddun(rs.getString("pro_coddun"));
                nfi.setPro_ncm(rs.getString("pro_ncm"));
                nfi.setPro_cest(rs.getString("pro_cest"));
                nfi.setPro_cst_ipi(rs.getString("pro_cst_ipi"));
                nfi.setPro_codigo_anp(rs.getString("pro_codigo_anp"));
                nfi.setPro_codigo_codif(rs.getString("pro_codigo_codif"));
                nfi.setTes_gera_piscofins(rs.getString("tes_gera_piscofins"));
                retorno.add(nfi);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
//                System.out.println(ps.toString());
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public List<String> listaNotasReferenciadas(String nfi_numero) {
        String sql = "select Nfi_chaveacesso_ref from nota_fiscal_item where nfi_numero = ? AND Nfi_chaveacesso_ref <> '' group by Nfi_chaveacesso_ref;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<String> retorno = new ArrayList<>();
        String chave = "";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, nfi_numero);
            rs = ps.executeQuery();
            while (rs.next()) {
                chave = rs.getString("Nfi_chaveacesso_ref");
                retorno.add(chave);
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalItemDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalItemDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    } 
    
    public List<NotaFiscalItem> listaCuponsReferenciados(String nfi_numero) {
	String sql = "SELECT Nfi_ecf_numero, Nfi_ecf_coo FROM Nota_fiscal_item WHERE Nfi_numero = ? AND Nfi_ecf_coo <> '' GROUP BY Nfi_ecf_numero, Nfi_ecf_coo;";
	PreparedStatement ps = null;
	ResultSet rs = null;
	List<NotaFiscalItem> retorno = new ArrayList<>();
	NotaFiscalItem nfi = null;
	try {
	    ps = connection.prepareStatement(sql);
	    ps.setString(1, nfi_numero);
	    rs = ps.executeQuery();
	    while (rs.next()) {
		nfi = new NotaFiscalItem();
		nfi.setNfi_ecf_numero(rs.getString("Nfi_ecf_numero"));
		nfi.setNfi_ecf_coo(rs.getString("Nfi_ecf_coo"));
		retorno.add(nfi);
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(NotaFiscalItemDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(NotaFiscalItemDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }
    
}
