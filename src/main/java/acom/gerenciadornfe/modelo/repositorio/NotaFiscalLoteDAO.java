
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.NotaFiscalLote;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class NotaFiscalLoteDAO {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public boolean inserir(NotaFiscalLote lote) {
        String sql = "INSERT INTO Nota_fiscal_lote (Nfe_lote ,Nfe_data ,Nfe_protocolo ,Nfe_protocolodatahora ,Nfe_recibo ,Nfe_qtdnotas ,Nfe_fechado ,NFeFormatoEmissao ,Nfe_filial ,Nfe_tipo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement ps = null;
        boolean retorno = false;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, lote.getNfe_lote());
            ps.setTimestamp(2, lote.getNfe_data());
            ps.setString(3, lote.getNfe_protocolo());
            ps.setString(4, lote.getNfe_protocolodatahora());
            ps.setString(5, lote.getNfe_recibo());
            ps.setInt(6, lote.getNfe_qtdnotas());
            ps.setString(7, lote.getNfe_fechado());
            ps.setString(8, lote.getnFeFormatoEmissao());
            ps.setString(9, lote.getNfe_filial());
            ps.setString(10, lote.getNfe_tipo());
            retorno = true;
        } catch (SQLException ex) {
            retorno = false;
            Logger.getLogger(NotaFiscalLoteDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(NotaFiscalLoteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getNumeroLote() {
        String sql = "SELECT MAX (Nfe_lote) AS numero_lote FROM Nota_fiscal_lote;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("numero_lote");
                if (retorno == null || retorno.equals(null) || retorno.equals("")) {
                    retorno = "0";
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(NotaFiscalLoteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

}
