/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqLite;
import acom.gerenciadornfe.modelo.entidades.Parametros;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class ParametrosDAO {

    private ConexaoSqLite conexao = ConexaoSqLite.getInstance();
    private Connection connection = conexao.getConexao();

    public boolean alterar(Parametros parametro) {
        String sql = "UPDATE parametros SET par_valor = ? WHERE par_nome = ?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, parametro.getPar_valor().toString().trim());
            ps.setString(2, parametro.getPar_nome().trim());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public List<Parametros> listarParametros() {
        String sql = "SELECT * FROM parametros where par_nome <> 'par_nome_certificado' and par_nome <> 'par_senha_certificado' and par_nome <> 'par_tipo_certificado';";
        List<Parametros> lista = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Parametros p = new Parametros();
                p.setPar_nome(rs.getString("par_nome").trim());
                p.setPar_descricao(rs.getString("par_descricao").trim());
                p.setPar_valor(rs.getString("par_valor").trim());
                lista.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lista;
    }

    public List<Parametros> listarParametrosCertificado() {
        String sql = "SELECT * FROM parametros where par_nome = 'par_nome_certificado' or par_nome = 'par_senha_certificado' or par_nome = 'par_tipo_certificado';";
        List<Parametros> lista = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Parametros p = new Parametros();
                p.setPar_nome(rs.getString("par_nome").trim());
                p.setPar_descricao(rs.getString("par_descricao").trim());
                p.setPar_valor(rs.getString("par_valor").trim());
                lista.add(p);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return lista;
    }
    
    public String getHost() {
        String sql = "SELECT par_valor FROM parametros WHERE par_nome = 'par_host';";
        String retorno = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("par_valor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getInstancia() {
        String sql = "SELECT par_valor FROM parametros WHERE par_nome = 'par_instancia';";
        String retorno = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("par_valor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getBanco() {
        String sql = "SELECT par_valor FROM parametros WHERE par_nome = 'par_banco';";
        String retorno = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("par_valor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getPorta() {
        String sql = "SELECT par_valor FROM parametros WHERE par_nome = 'par_porta';";
        String retorno = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("par_valor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getDiretorioProjeto() {
        String sql = "SELECT par_valor FROM parametros WHERE par_nome = 'par_diretorio_projeto';";
        String retorno = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("par_valor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getNomeCertificado() {
        String sql = "SELECT par_valor FROM parametros WHERE par_nome = 'par_nome_certificado';";
        String retorno = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("par_valor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getSenhaCertificado() {
        String sql = "SELECT par_valor FROM parametros WHERE par_nome = 'par_senha_certificado';";
        String retorno = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("par_valor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

     public String getTipoCertificado() {
	String sql = "SELECT par_valor FROM parametros WHERE par_nome = 'par_tipo_certificado';";
	String retorno = "";
	PreparedStatement ps = null;
	ResultSet rs = null;
	try {
	    ps = connection.prepareStatement(sql);
	    rs = ps.executeQuery();
	    if (rs.next()) {
		retorno = rs.getString("par_valor");
	    }
	} catch (SQLException ex) {
	    Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
	} finally {
	    try {
		rs.close();
		ps.close();
	    } catch (SQLException ex) {
		Logger.getLogger(ParametrosDAO.class.getName()).log(Level.SEVERE, null, ex);
	    }
	}
	return retorno;
    }
    
      public String getContingencia() {
        String sql = "SELECT par_valor FROM parametros WHERE par_nome = 'par_contigencia';";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("par_valor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
     
      public String getParAliasCertificado() {
        String sql = "SELECT par_valor FROM parametros WHERE par_nome = 'par_alias_certificado';";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("par_valor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
      
      public String getParFilial() {
        String sql = "SELECT par_valor FROM parametros WHERE par_nome = 'par_filial';";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("par_valor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
      
}
