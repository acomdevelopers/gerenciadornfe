/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.ParametrosFiscal;
import acom.gerenciadornfe.util.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class ParametrosFiscalDAO {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public ParametrosFiscal listar() {
        String sql = "SELECT * FROM Parametros_fiscal;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        ParametrosFiscal pf = null;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                pf = new ParametrosFiscal();
                pf.setCfg_aliquota_cofins(rs.getDouble("cfg_aliquota_cofins"));
                pf.setCfg_aliquota_pis(rs.getDouble("cfg_aliquota_pis"));
                pf.setCfg_beneficiario_prodepe(rs.getInt("cfg_beneficiario_prodepe"));
                pf.setCfg_considera_cod_xml_ent(rs.getString("cfg_considera_cod_xml_ent"));
                pf.setCfg_considera_natureza_geranf(rs.getBoolean("cfg_considera_natureza_geranf"));
                pf.setCfg_dir_xml_destinatario(rs.getString("cfg_dir_xml_destinatario"));
                pf.setCfg_diretoriopdf(rs.getString("cfg_diretoriopdf"));
                pf.setCfg_diretoriotxt(rs.getString("cfg_diretoriotxt"));
                pf.setCfg_importa_produto_xml_ent(rs.getBoolean("cfg_importa_produto_xml_ent"));
                pf.setCfg_importa_produto_xml_saida(rs.getBoolean("cfg_importa_produto_xml_saida"));
                pf.setCfg_lei_transparencia(rs.getBoolean("cfg_lei_transparencia"));
                pf.setCfg_limite_venda_pessoa_fisica(rs.getDouble("cfg_limite_venda_pessoa_fisica"));
                pf.setCfg_logomarca_danfe(rs.getString("cfg_logomarca_danfe"));
                pf.setCfg_modelo_nfe(rs.getString("cfg_modelo_nfe"));
                pf.setCfg_modeloemissaodanfe(rs.getString("cfg_modeloemissaodanfe"));
                pf.setCfg_mostraretornonfe(rs.getBoolean("cfg_mostraretornonfe"));
                pf.setCfg_percentual_prodepe(rs.getDouble("cfg_percentual_prodepe"));
                pf.setCfg_tipo_volumes_nf(rs.getString("cfg_tipo_volumes_nf"));
                pf.setCfg_versao_leiaute(rs.getString("cfg_versao_leiaute"));
                pf.setCfg_versaoschema(rs.getString("cfg_versaoschema"));
                pf.setCfg_webservice(rs.getString("cfg_webservice"));
                pf.setNfeAmbiente(rs.getString("nfeAmbiente"));
                pf.setNfeCertificadoDigital(rs.getString("nfeCertificadoDigital"));
                pf.setNfeDiretorioXML(rs.getString("nfeDiretorioXML"));
                pf.setNfeFormatoEmissao(rs.getString("nfeFormatoEmissao"));
                pf.setNfeFormatoImpressaoDANFe(rs.getString("nfeFormatoImpressaoDANFe"));
                pf.setNfeLicencaDLL(rs.getString("nfeLicencaDLL"));
                pf.setUsaNFe(rs.getBoolean("usaNFe"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return pf;
    }

    public String getFormatoEmissao() {
        String sql = "SELECT NFeFormatoEmissao FROM Parametros_fiscal;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = Util.getNumeros(rs.getString("NFeFormatoEmissao"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getTipoAmbiente() {
        String sql = "SELECT NFeAmbiente FROM Parametros_fiscal;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = Util.getNumeros(rs.getString("NFeAmbiente"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
    
    public String getDiretorioLogomarcaDanfe() {
        String sql = "select Cfg_logomarca_danfe from Parametros_fiscal;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("Cfg_logomarca_danfe");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getDiretorioXml() {
        String sql = "select nfeDiretorioXml from Parametros_fiscal;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("nfeDiretorioXml");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
    
    public String getDiretorioPdf() {
        String sql = "select Cfg_diretoriopdf from Parametros_fiscal;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("Cfg_diretoriopdf");
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosFiscalDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
    
}
