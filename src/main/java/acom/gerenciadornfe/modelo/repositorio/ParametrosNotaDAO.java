/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class ParametrosNotaDAO {
    
    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();
    
    public String getCodigoMunicipio(String municipio){
        
        String sql = "SELECT Cidades.Cid_uf_cod + Cidades.Cid_codigo as codigoMun FROM Cidades WHERE cid_nome = ?;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, municipio);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("codigoMun").trim();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosNotaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosNotaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }

    public String getCodigoMunicipioDestinatario(String municipio, String codUf){
        
        String sql = "SELECT Cidades.Cid_uf_cod + Cidades.Cid_codigo as codigoMun FROM Cidades WHERE cid_nome = ? and cid_uf_cod = ?;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, municipio);
            ps.setString(2, codUf);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("codigoMun").trim();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosNotaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosNotaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
    
    public String getCodigoEstado(String estado){
        String sql = "SELECT TOP 1 cid_uf_cod FROM cidades WHERE cid_uf_sigla = ?;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, estado);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("cid_uf_cod").trim();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ParametrosNotaDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(ParametrosNotaDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
    
}
