/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.ProdutoCombustivel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class UtilDAO {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public String getCodigoUF(String codigo) {
        String sql = "select cid_uf_cod from cidades inner join Pessoal on Pessoal.Pes_cidade = Cidades.Cid_nome and pessoal.Pes_uf = Cidades.Cid_uf_sigla where pessoal.Pes_cod6 = ?;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, codigo);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("cid_uf_cod");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                System.out.println(ps.toString());
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(UtilDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
    
    /*select top 1 cid_uf_cod from cidades where cid_uf_sigla = 'PE';*/
    public String getCodEstadoEmitente(String codigo) {
        String sql = "select top 1 cid_uf_cod from cidades where cid_uf_sigla = ?;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String retorno = "";
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, codigo);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("cid_uf_cod");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FilialDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                System.out.println(ps.toString());
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(UtilDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
    
    public ProdutoCombustivel getProdutoCombustivel(String codint){
        String sql = "select * from produto_combustivel where pcom_codint = ?;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        ProdutoCombustivel retorno = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, codint);
            rs = ps.executeQuery();
            if(rs.next()){
                retorno = new ProdutoCombustivel();
                retorno.setPcom_codint(rs.getString("pcom_codint"));
                retorno.setPcom_descricao_anp(rs.getString("pcom_descricao_anp"));
                retorno.setPcom_codigo_anp(rs.getString("pcom_codigo_anp"));
                retorno.setPcom_codif(rs.getString("pcom_codif"));
                retorno.setPcom_pglp(rs.getDouble("pcom_pglp"));
                retorno.setPcom_pgn(rs.getDouble("pcom_pgn"));
                retorno.setPcom_pgni(rs.getDouble("pcom_pgni"));
                retorno.setPcom_vpartida(rs.getDouble("pcom_vpartida"));
                retorno.setPcom_unidadetributavel(rs.getString("pcom_unidadetributavel"));
                retorno.setPcom_qtdtributavel(rs.getDouble("Pcom_qtdtributavel"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UtilDAO.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                rs.close();
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(UtilDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return retorno;
    }
    
}
