package acom.gerenciadornfe.modelo.repositorio;

import acom.gerenciadornfe.modelo.conexao.ConexaoSqlServer;
import acom.gerenciadornfe.modelo.entidades.Veiculo;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author allison
 */
public class VeiculoDAO implements Serializable {

    private ConexaoSqlServer conexao = ConexaoSqlServer.getInstancia();
    private Connection connection = conexao.getConexao();

    public boolean inserir(Veiculo veiculo) {
        String sql = "INSERT INTO veiculo (vei_codigo, vei_placa, vei_renavam, vei_tara, vei_capkg, vei_capm3, vei_tprod, vei_tpcar, vei_uf) VALUES (?,?,?,?,?,?,?,?,?);";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, veiculo.getVei_codigo());//Vei_codigo
            ps.setString(2, veiculo.getVei_placa());//Vei_placa
            ps.setString(3, veiculo.getVei_renavam());//Vei_renavam
            ps.setInt(4, veiculo.getVei_tara());//Vei_tara
            ps.setLong(5, veiculo.getVei_capkg());//Vei_capkg
            ps.setLong(6, veiculo.getVei_capm3());//Vei_capm3
            ps.setString(7, veiculo.getVei_tprod());//Vei_tprod
            ps.setString(8, veiculo.getVei_tpcar());//Vei_tpcar
            ps.setString(9, veiculo.getVei_uf());//Vei_uf
            ps.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Erro ao executar insert: " + ps.toString());
            return false;
        }finally{
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean alterar(Veiculo veiculo) {
        String sql = "update veiculo set vei_placa = ?, vei_renavam = ?, vei_tara = ?, vei_capkg = ?,  vei_capm3 = ?, vei_tprod = ?, vei_tpcar = ?, vei_uf = ? where vei_codigo = ?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, veiculo.getVei_placa());//Vei_placa
            ps.setString(2, veiculo.getVei_renavam());//Vei_renavam
            ps.setInt(3, veiculo.getVei_tara());//Vei_tara
            ps.setLong(4, veiculo.getVei_capkg());//Vei_capkg
            ps.setLong(5, veiculo.getVei_capm3());//Vei_capm3
            ps.setString(6, veiculo.getVei_tprod());//Vei_tprod
            ps.setString(7, veiculo.getVei_tpcar());//Vei_tpcar
            ps.setString(8, veiculo.getVei_uf());//Vei_uf
            ps.setInt(9, veiculo.getVei_codigo());//Vei_codigo
            ps.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Erro ao executar update: " + ps.toString());
            return false;
        }finally{
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public boolean excluir(Veiculo veiculo){
        String sql = "delete from veiculo where vei_codigo = ?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, veiculo.getVei_codigo());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Erro ao executar delete: " + ps.toString());
            return false;
        } finally {
            try {
                ps.close();
            } catch (SQLException ex) {
                Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public List listar() {
        String sql = "select * from veiculo order by vei_codigo asc;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Veiculo> listaRetorno = new ArrayList<>();
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Veiculo veiculo = new Veiculo();
                veiculo.setVei_codigo(rs.getInt("vei_codigo"));
                veiculo.setVei_placa(rs.getString("vei_placa"));
                veiculo.setVei_renavam(rs.getString("vei_renavam"));
                veiculo.setVei_tara(rs.getInt("vei_tara"));
                veiculo.setVei_capkg(rs.getLong("vei_capkg"));
                veiculo.setVei_capm3(rs.getLong("vei_capm3"));
                veiculo.setVei_tprod(rs.getString("vei_tprod"));
                veiculo.setVei_tpcar(rs.getString("vei_tpcar"));
                veiculo.setVei_uf(rs.getString("vei_uf"));
                listaRetorno.add(veiculo);
            }
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println(ps.toString());
        }
        return listaRetorno;
    }

    public int ultimoId() {
        String sql = "select max(vei_codigo) as vei_codigo from veiculo;";
        PreparedStatement ps = null;
        ResultSet rs = null;
        int retorno = 0;
        try {
            ps = connection.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getInt("vei_codigo") + 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(VeiculoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retorno;
    }

}
