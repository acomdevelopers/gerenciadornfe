/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package acom.gerenciadornfe.util;

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author allison
 */
public class Util {

    public static void restartApplication(Runnable runBeforeRestart) throws IOException {
	try {
	    // java binary
	    String java = System.getProperty("java.home") + "/bin/java";
	    // vm arguments
	    List<String> vmArguments = ManagementFactory.getRuntimeMXBean().getInputArguments();
	    StringBuffer vmArgsOneLine = new StringBuffer();
	    for (String arg : vmArguments) {
		// if it's the agent argument : we ignore it otherwise the
		// address of the old application and the new one will be in conflict
		if (!arg.contains("-agentlib")) {
		    vmArgsOneLine.append(arg);
		    vmArgsOneLine.append(" ");
		}
	    }
	    // init the command to execute, add the vm args
	    final StringBuffer cmd = new StringBuffer("\"" + java + "\" " + vmArgsOneLine);
	    // program main and program arguments (be careful a sun property. might not be supported by all JVM) 
	    String[] mainCommand = System.getProperty("sun.java.command").split(" ");
	    // program main is a jar
	    if (mainCommand[0].endsWith(".jar")) {
		// if it's a jar, add -jar mainJar
		cmd.append("-jar " + new File(mainCommand[0]).getPath());
	    } else {
		// else it's a .class, add the classpath and mainClass
		cmd.append("-cp \"" + System.getProperty("java.class.path") + "\" " + mainCommand[0]);
	    }
	    // finally add program arguments
	    for (int i = 1; i < mainCommand.length; i++) {
		cmd.append(" ");
		cmd.append(mainCommand[i]);
	    }
	    // execute the command in a shutdown hook, to be sure that all the
	    // resources have been disposed before restarting the application
	    Runtime.getRuntime().addShutdownHook(new Thread() {
		@Override
		public void run() {
		    try {
			Runtime.getRuntime().exec(cmd.toString());
		    } catch (IOException e) {
			e.printStackTrace();
		    }
		}
	    });
	    // execute some custom code before restarting
	    if (runBeforeRestart != null) {
		runBeforeRestart.run();
	    }
	    // exit
	    System.exit(0);
	} catch (Exception e) {
	    // something went wrong
	    throw new IOException("Erro ao tentar reiniciar a aplicação.", e);
	}
    }

    public static String gerarMD5(String original) throws NoSuchAlgorithmException {
	StringBuffer sb = new StringBuffer();
	try {
	    MessageDigest md = MessageDigest.getInstance("MD5");
	    md.update(original.getBytes());
	    byte[] digest = md.digest();
	    for (byte b : digest) {
		sb.append(String.format("%02x", b & 0xff));
	    }
	    System.out.println("PARAMETROS.....: " + original);
	    System.out.println("MD5............: " + sb.toString());
	} catch (NoSuchAlgorithmException e) {
	    e.printStackTrace();
	}
	return sb.toString();
    }

    public static String getNumeros(String valor) {
	return valor.replaceAll("[^0-9]", "");
    }

    public static String formatarNumeros(String formato, String valor) {
	NumberFormat formatter = new DecimalFormat(formato);
	String sequenciaTEF = formatter.format(Integer.parseInt(valor));
	return sequenciaTEF;
    }

    public static Timestamp convertStringToTimestamp(String str_date) {
	try {
	    DateFormat formatter;
	    formatter = new SimpleDateFormat("dd/MM/yyyy");
	    Date date = (Date) formatter.parse(str_date);
	    java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

	    return timeStampDate;
	} catch (ParseException e) {
	    System.out.println("Exception :" + e);
	    return null;
	}
    }

    public static Timestamp convertStringHoraToTimestamp(String str_date) {
	try {
	    DateFormat formatter;
	    formatter = new SimpleDateFormat("HH:mm:ss");
	    Date date = (Date) formatter.parse(str_date);
	    java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

	    return timeStampDate;
	} catch (ParseException e) {
	    System.out.println("Exception :" + e);
	    return null;
	}
    }

    public static String removeAcentos(String str) {
	str = Normalizer.normalize(str, Normalizer.Form.NFD);
	str = str.replaceAll("[^\\p{ASCII}]", "");
	str = str.replace("&", "e");
	str = str.replace("º", " ");
	str = str.replace("*", " ");
	str = str.replace("°", " ");
	str = str.replace("ª", " ");
	return str;
    }

    public static String camposDescricoes(String str) {
	str = Normalizer.normalize(str, Normalizer.Form.NFD);
	str = str.replaceAll("[A-Za-z0-9ç,.-]", "");
	return str;
    }

    public static byte[] getBytesLogo(String filename) throws IOException {
	File imgPath = new File(filename);
	BufferedImage bufferedImage = ImageIO.read(imgPath);
	ByteOutputStream bos = null;
	try {
	    bos = new ByteOutputStream();
	    ImageIO.write(bufferedImage, "png", bos);
	} finally {
	    try {
		bos.close();
	    } catch (Exception e) {
	    }
	}
	return bos == null ? null : bos.getBytes();
    }

    public static String formatarData(String dataTimestamp) {
	try {
	    String dataAFormatar = dataTimestamp;
	    SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
	    Date date = formato.parse(dataAFormatar);
	    formato.applyPattern("dd/MM/yyyy");
	    String dataFormatada = formato.format(date);
	    return dataFormatada;
	} catch (ParseException ex) {
	    Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
	    return dataTimestamp;
	}
    }

    public static String formatarHora(String horaTimestamp) {
	String horaAFormatar = horaTimestamp;
	String hora = horaAFormatar.substring(11, 19);
	return hora;

    }

    public static void copiarArquivo(File source, File destino) throws IOException {
	if (destino.exists()) {
	    destino.delete();
	}
	FileChannel sourceChannel = null;
	FileChannel destinationChannel = null;
	try {
	    sourceChannel = new FileInputStream(source).getChannel();
	    destinationChannel = new FileOutputStream(destino).getChannel();
	    sourceChannel.transferTo(0, sourceChannel.size(),
		    destinationChannel);
	} finally {
	    if (sourceChannel != null && sourceChannel.isOpen()) {
		sourceChannel.close();
	    }
	    if (destinationChannel != null && destinationChannel.isOpen()) {
		destinationChannel.close();
	    }
	}
    }

}
